// tslint:disable: file-name-casing because this an Angular auto-generate file and this file name is the norm
export interface IEnvironment {
    production: boolean;
}
