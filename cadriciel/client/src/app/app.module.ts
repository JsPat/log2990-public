import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule} from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule} from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule} from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSliderModule} from '@angular/material/slider';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';

import { MatSnackBarModule } from '@angular/material';
import { ColorPaletteModule } from './color-palette/color-palette.module';
import { AppComponent } from './components/app/app.component';
import { AttributsComponent } from './components/app/attributes/attributs.component';
import { CalligraphyComponent } from './components/app/attributes/calligraphy/calligraphy.component';
import { ColorizeComponent } from './components/app/attributes/colorize/colorize.component';
import { EllipseComponent } from './components/app/attributes/ellipse/ellipse.component';
import { EraserComponent } from './components/app/attributes/eraser/eraser.component';
import { LineComponent } from './components/app/attributes/line/line.component';
import { PaintBrushComponent } from './components/app/attributes/paint-brush/paint-brush.component';
import { PaintBucketComponent } from './components/app/attributes/paint-bucket/paint-bucket.component';
import { PenComponent } from './components/app/attributes/pen/pen.component';
import { PipetteComponent } from './components/app/attributes/pipette/pipette.component';
import { PolygoneComponent } from './components/app/attributes/polygone/polygone.component';
import { RectangleComponent } from './components/app/attributes/rectangle/rectangle.component';
import { SelectorComponent } from './components/app/attributes/selector/selector.component';
import { SprayPaintComponent } from './components/app/attributes/spray-paint/spray-paint.component';
import { StampComponent } from './components/app/attributes/stamp/stamp.component';
import { TextComponent } from './components/app/attributes/text/text.component';
import { ConfirmPanelComponent } from './components/app/confirm-panel/confirm-panel.component';
import { DrawSheetComponent } from './components/app/draw-sheet/draw-sheet.component';
import { DrawViewComponent } from './components/app/draw-view/draw-view.component';
import { GridComponent } from './components/app/draw-view/grid/grid.component';
import { ExportComponent } from './components/app/export/export.component';
import { GalleryComponent } from './components/app/gallery/gallery.component';
import { HomePageComponent} from './components/app/home-page/home-page.component';
import { NewDrawingComponent } from './components/app/new-drawing/new-drawing.component';
import { SaveComponent } from './components/app/save/save.component';
import { GuideComponent } from './components/guide/guide.component';
import { LoadingHttpInterceptor } from './services/database-drawings/loading-http-interceptor/loading-http-interceptor';
import { LoadingSpinnerService } from './services/database-drawings/loading-spinner/loading-spinner.service';
import { DrawSheetService } from './services/draw-sheet/draw-sheet.service';

export const routes: Routes = [
  { path: '', component : HomePageComponent, runGuardsAndResolvers: 'always' },
  { path: 'help', component : GuideComponent, runGuardsAndResolvers: 'always' },
  { path: 'drawing', component: DrawViewComponent, runGuardsAndResolvers: 'always' },
  { path: 'gallery', component: GalleryComponent, runGuardsAndResolvers: 'always' },
  { path: 'save', component: SaveComponent, runGuardsAndResolvers: 'always' },

];

@NgModule({
    declarations: [
        AppComponent,
        DrawViewComponent,
        DrawSheetComponent,
        AttributsComponent,
        GalleryComponent,
        GridComponent,
        PenComponent,
        PaintBrushComponent,
        CalligraphyComponent,
        SprayPaintComponent,
        RectangleComponent,
        EllipseComponent,
        ExportComponent,
        PolygoneComponent,
        LineComponent,
        PaintBucketComponent,
        EraserComponent,
        StampComponent,
        TextComponent,
        AppComponent,
        GuideComponent,
        HomePageComponent,
        NewDrawingComponent,
        SaveComponent,
        ExportComponent,
        SelectorComponent,
        PipetteComponent,
        ColorizeComponent,
        ConfirmPanelComponent,
    ],
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MatCardModule,
        MatDialogModule,
        MatDividerModule,
        MatSliderModule,
        MatButtonToggleModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatToolbarModule,
        MatRadioModule,
        MatSidenavModule,
        MatSlideToggleModule,
        MatListModule,
        MatButtonModule,
        MatIconModule,
        MatExpansionModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot(routes,
        {onSameUrlNavigation:
          'reload'}),
        ColorPaletteModule,
        DragDropModule,
        MatChipsModule,
        MatProgressSpinnerModule,
        MatSnackBarModule
      ],
    providers: [
      DrawSheetService,
      LoadingSpinnerService,
      {
        provide: HTTP_INTERCEPTORS,
        useClass: LoadingHttpInterceptor,
        multi: true
      }
    ],
    bootstrap: [AppComponent],
    entryComponents: [
        NewDrawingComponent,
        GalleryComponent,
        SaveComponent,
        ExportComponent,
        ConfirmPanelComponent
    ],
    exports: [
      RouterModule,
      MatChipsModule,
      MatProgressSpinnerModule,
    ]
})
export class AppModule {}
