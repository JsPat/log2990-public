export interface GuideSectionContent {
    title: string;
    paragraphs: string[];
}
