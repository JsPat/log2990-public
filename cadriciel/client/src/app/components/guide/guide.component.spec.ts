/* tslint:disable:no-unused-variable */
// tslint:disable: no-string-literal

import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import { NO_ERRORS_SCHEMA, QueryList } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuideComponent } from './guide.component';

describe('GuideComponent', () => {
  let component: GuideComponent;
  let fixture: ComponentFixture<GuideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuideComponent ],
      providers: [Location, { provide: LocationStrategy, useClass: PathLocationStrategy }],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(async () => {
    fixture = TestBed.createComponent(GuideComponent);
    component = fixture.componentInstance;
    await fixture.whenStable();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('Searching for an invalid guideContent (negative index) resets the index to 0', () => {
    component['loadNewGuideContent'](-1);
    expect(component['contentIndex']).toBe(0);
  });
  it('Searching for an invalid guideContent (greater than lastIndex) resets the index to 0', () => {
    component['loadNewGuideContent'](component['lastIndex'] + 1);
    expect(component['contentIndex']).toBe(0);
  });
  it('Valid index sets the currentIndex to index', () => {
    component['loadNewGuideContent'](2);
    expect(component['contentIndex']).toBe(2);
  });
  it('Should get the good content\'s title with the good index (0 for first content)', () => {
    component['loadNewGuideContent'](0);
    expect(component['currentlyDisplayedContent'].title).toEqual('Bienvenue dans le guide de l\'utilisateur de PolyDessin');
  });
  it('Pressing Previous or Next causes the sidebar content to expand, regardless of the previous expansion state', () => {
    const matExpPanelSpy = jasmine.createSpyObj('MatExpansionPanel', ['open', 'close']);

    component.viewCategories.reset([matExpPanelSpy]);

    component['switchExpandSubjects']();
    component.viewCategories.forEach((subject) => {
      expect(subject.open).toHaveBeenCalled();
    });
  });
  it('should open subject with switchExpandSubjects()', () => {
    const child = jasmine.createSpyObj('MatExpansionPanel', ['open']);
    component.viewCategories = new QueryList();
    component.viewCategories.reset([child]);
    component.viewCategories.forEach((subject) => {
      component['switchExpandSubjects']();
      expect(child.open).toHaveBeenCalled();
    });
  });
  // incrementContentIndex() tests
  it('this.contentIndex should be incremented if incrementContentIndex() is called and this.contentIndex < this.lastIndex', () => {
    component['contentIndex'] = 1;
    // tslint:disable-next-line: no-magic-numbers
    component['lastIndex'] = 3;
    component['incrementContentIndex']();
    expect(component['contentIndex']).toEqual(2);
    });
  it('this.contentIndex should be incremented if incrementContentIndex() is called and this.contentIndex == 0', () => {
    component['contentIndex'] = 0;
    // tslint:disable-next-line: no-magic-numbers
    component['lastIndex'] = 3;
    component['incrementContentIndex']();
    expect(component['contentIndex']).toEqual(1);
    });
  it('this.contentIndex should not be incremented if incrementContentIndex() is called and this.contentIndex == this.lastIndex', () => {
    component['contentIndex'] = 1;
    component['lastIndex'] = 1;
    component['incrementContentIndex']();
    expect(component['contentIndex']).toEqual(1);
    });
  it('this.contentIndex should not be incremented if incrementContentIndex() is called and this.contentIndex > this.lastIndex', () => {
    component['contentIndex'] = 2;
    component['lastIndex'] = 1;
    component['incrementContentIndex']();
    expect(component['contentIndex']).toEqual(2);
    });
  it('this.contentIndex should not be incremented if incrementContentIndex() is called and this.contentIndex < 0', () => {
    component['contentIndex'] = -1;
    component['lastIndex'] = 2;
    component['incrementContentIndex']();
    expect(component['contentIndex']).toEqual(-1);
    });
  // decrementContentIndex() tests
  it('this.contentIndex should be decremented if decrementContentIndex() is called and this.contentIndex > 0', () => {
    component['contentIndex'] = 1;
    component['decrementContentIndex']();
    expect(component['contentIndex']).toEqual(0);
    });
  it('this.contentIndex should be decremented if decrementContentIndex() is called and this.contentIndex == this.lastIndex', () => {
    component['contentIndex'] = 1;
    component['lastIndex'] = 1;
    component['decrementContentIndex']();
    expect(component['contentIndex']).toEqual(0);
    });
  it('this.contentIndex should not be decremented if decrementContentIndex() is called and this.contentIndex = 0', () => {
    component['contentIndex'] = 0;
    component['decrementContentIndex']();
    expect(component['contentIndex']).toEqual(0);
    });
  it('this.contentIndex should not be decremented if decrementContentIndex() is called and this.contentIndex < 0', () => {
    component['contentIndex'] = -1;
    component['decrementContentIndex']();
    expect(component['contentIndex']).toEqual(-1);
    });
  it('this.contentIndex should not be decremented if decrementContentIndex() is called and this.contentIndex > this.lastIndex', () => {
    component['contentIndex'] = 2;
    component['lastIndex'] = 1;
    component['decrementContentIndex']();
    expect(component['contentIndex']).toEqual(2);
    });

  it('Pushing the rigth arrow should call incrementContentIndex()', () => {
      // tslint:disable-next-line: no-any
      spyOn<any>(component, 'incrementContentIndex');
      const mockKeyboard = new KeyboardEvent('keydown.ArrowRight');
      component['incrementContentIndexShortCut'](mockKeyboard);
      expect(component['incrementContentIndex']).toHaveBeenCalledTimes(1);
    });

  it('Pushing the left arrow should call decrementContentIndex()', () => {
      // tslint:disable-next-line: no-any
      spyOn<any>(component, 'decrementContentIndex');
      const mockKeyboard = new KeyboardEvent('keydown.ArrowLeft');
      component['decrementContentIndexShortCut'](mockKeyboard);
      expect(component['decrementContentIndex']).toHaveBeenCalledTimes(1);
    });

});
