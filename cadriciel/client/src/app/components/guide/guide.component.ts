import { Component, HostListener, QueryList, ViewChildren } from '@angular/core';
import { MatExpansionPanel } from '@angular/material';
import * as JSON from './guide-content.json';
import { GuideSectionContent } from './guide-section-content';

enum SECTION_INDEXES {
  WELCOME,
  NEW_DRAWING,
  GALLERY,
  CONTINUE,
  SAVE,
  EXPORT,
  COLOR,
  PEN,
  PAINT_BRUSH,
  CALLIGRAPHY,
  SPRAY_PAINT,
  RECTANGLE,
  ELLIPSE,
  POLYGON,
  LINE,
  TEXT,
  COLORIZE,
  PAINT_BUCKET,
  ERASER,
  STAMP,
  PIPETTE,
  SELECTION_RECTANGLE,
  INVERSE_SELECTION_RECTANGLE,
  MOVE,
  SCALE,
  ROTATION,
  SELECTION_MANIP,
  GRID,
  MAGNET,
  UNDO_REDO,
  SHORTCUTS,
  SELECTION_TOOL
}

const config = JSON.default;

@Component({
  selector: 'app-guide',
  templateUrl: './guide.component.html',
  styleUrls: ['./guide.component.scss']
})
export class GuideComponent {
  // tslint:disable-next-line: no-any --- disable here because there is not really a type that match the enum
  protected readonly SECTION_INDEXES: any = SECTION_INDEXES;

  protected contentIndex: number = 0;
  protected lastIndex: number = JSON.default.guideContent.length - 1;
  protected currentlyDisplayedContent: GuideSectionContent = config.guideContent[this.contentIndex];

  @ViewChildren(MatExpansionPanel) viewCategories: QueryList<MatExpansionPanel>;
  // to view all categories to expand or collapse them
  // to allow expand/collapse with buttons and manually:
  protected switchExpandSubjects(): void {
    this.viewCategories.forEach((subject) => {
        subject.open();
    });
  }

  protected incrementContentIndex(): void {
    if (this.contentIndex < this.lastIndex && this.contentIndex >= 0) {
      this.currentlyDisplayedContent = config.guideContent[++this.contentIndex];
    }
  }

  protected decrementContentIndex(): void {
    if (this.contentIndex > 0 && this.contentIndex <= this.lastIndex) {
      this.currentlyDisplayedContent = config.guideContent[--this.contentIndex];
    }
  }

  // Using the index passed in parameter, verifies that the index is valid and loads the new content
  // at the specified index accordingly
  protected loadNewGuideContent(index: number): void {
    this.contentIndex = (index >= 0 && index <= this.lastIndex) ? index : 0;
    this.currentlyDisplayedContent = config.guideContent[this.contentIndex];
  }

  @HostListener(config.keyDown.arrowRight, [config.keyDown.event])
  protected incrementContentIndexShortCut(event: KeyboardEvent): void {
    event.preventDefault();
    this.incrementContentIndex();
    this.switchExpandSubjects();
  }

  @HostListener(config.keyDown.arrowLeft, [config.keyDown.event])
  protected decrementContentIndexShortCut(event: KeyboardEvent): void {
    event.preventDefault();
    this.decrementContentIndex();
    this.switchExpandSubjects();
  }
}
