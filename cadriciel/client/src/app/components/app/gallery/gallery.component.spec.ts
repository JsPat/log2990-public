// tslint:disable: no-string-literal
// tslint:disable: no-any

import { HttpClient } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogRef, MatProgressSpinnerModule, MatSnackBar } from '@angular/material';
import { Observable, of } from 'rxjs';
import { Subject } from 'rxjs/internal/Subject';
import { DatabaseDrawingsService } from 'src/app/services/database-drawings/database-drawings.service';
import { LoadingSpinnerService } from 'src/app/services/database-drawings/loading-spinner/loading-spinner.service';
import { HandleRequestErrorService } from 'src/app/services/handle-request-error/handle-request-error.service';
import { Drawing } from '../../../../../../common/drawing';
import { GalleryComponent } from './gallery.component';

class MatDialogMock {
  // tslint:disable-next-line: no-any because the return type is MatDialogRef and it needs unknowed templates
  open(): any {
    return {
      afterClosed: () => of({action: true})
    };
  }
}
// tslint:disable-next-line: max-classes-per-file
class HTMLInputElementMock {
  value: string;
}
// tslint:disable-next-line: max-classes-per-file
class DatabaseDrawingsServiceMock extends DatabaseDrawingsService {
  getDrawings(): Observable<Drawing[]> {
    return new Subject<Drawing[]>().asObservable();
  }
}

describe('GalleryComponent', () => {
  let component: GalleryComponent;
  let databaseDrawingsServiceMock: DatabaseDrawingsServiceMock;
  // tslint:disable-next-line: prefer-const because can't initialize http if it's const
  let http: HttpClient;
  // tslint:disable-next-line: prefer-const because can't initialize HTMLInputElementMock if it's const
  let hTMLInputElementMock: HTMLInputElementMock;
  let matSnackBarMock: any;
  let fixture: ComponentFixture<GalleryComponent>;
  let dialogRefSpy: any;

  const mockContext = {
    drawImage: () => { // simulating drawImage function
    },
  } as unknown as CanvasRenderingContext2D;
  const mockCanvas = {
    toDataURL: () => { // simulating drawImage function
    }
  } as unknown as HTMLCanvasElement;

  beforeEach(async(() => {
    matSnackBarMock = jasmine.createSpyObj('MatSnackBar', ['']);
    databaseDrawingsServiceMock = new DatabaseDrawingsServiceMock(http, new HandleRequestErrorService(matSnackBarMock));

    dialogRefSpy = jasmine.createSpyObj('MatDialogRef', ['close']);

    TestBed.configureTestingModule({
      declarations: [ GalleryComponent ],
      providers: [
        MatProgressSpinnerModule,
        LoadingSpinnerService,
        { provide: MatSnackBar, useValue: matSnackBarMock },
        { provide: HTMLInputElement, useValue: hTMLInputElementMock},
        { provide: MatDialog, useClass: MatDialogMock},
        { provide: MatDialogRef, useValue: dialogRefSpy},
        { provide: DatabaseDrawingsService, useValue: databaseDrawingsServiceMock }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(async () => {
    fixture = TestBed.createComponent(GalleryComponent);
    component = fixture.componentInstance;

    await fixture.whenStable();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // checkIfNoDrawingsToShow
  it('checkIfNoDrawingsToShow() should set noDrawingsToShow = true if no drawings', () => {
    databaseDrawingsServiceMock['allDrawings$'] = of(new Array<Drawing>());
    // tslint:disable-next-line: no-string-literal
    component['checkIfNoDrawingsToShow']();
    expect(component['noDrawingsToShow']).toBeTruthy();
  });

  it('checkIfNoDrawingsToShow() should set noDrawingsToShow = false if there is a drawing', () => {
    const drawing = new Drawing('0', '<svg>');
    const drawings = new Array<Drawing>(drawing);
    databaseDrawingsServiceMock['allDrawings$'] = of(drawings);
    // tslint:disable-next-line: no-string-literal
    component['checkIfNoDrawingsToShow']();
    expect(component['noDrawingsToShow']).toBeFalsy();
  });

  // getDrawingsByTag
  it('getDrawingsByTag() should update drawingsToShow to new drawings to show', async(() => {
    const drawing = new Drawing('0', '<svg>');
    const drawingsExpected = new Array<Drawing>(drawing);
    spyOn(databaseDrawingsServiceMock, 'getDrawings').and.returnValue(of(drawingsExpected));
    component['drawingsToShow'] = new Array<Drawing>();
    // tslint:disable-next-line: no-string-literal
    component['getDrawingsByTag']();
    fixture.detectChanges();
    expect(component['drawingsToShow']).toEqual(drawingsExpected);
  }));

  it('getDrawingsByTag() should update drawingsToShow to not contain what it contained before', async(() => {
    const drawing = new Drawing('0', '<svg>');
    const drawingsExpected = new Array<Drawing>(drawing);
    spyOn(databaseDrawingsServiceMock, 'getDrawings').and.returnValue(of(drawingsExpected));
    const drawingToNotBeShown = new Drawing('noGoodDrawing', '');
    component['drawingsToShow'] = new Array<Drawing>(drawingToNotBeShown);
    // tslint:disable-next-line: no-string-literal
    component['getDrawingsByTag']();
    fixture.detectChanges();
    expect(component['drawingsToShow']).not.toContain(drawingToNotBeShown);
  }));

  it('getDrawingsByTag() should call checkIfNoDrawingsToShow()', () => {
    // tslint:disable-next-line: no-any
    spyOn<any>(component, 'getDrawingsByTag').and.callThrough();
    // tslint:disable-next-line: no-any
    spyOn<any>(component, 'checkIfNoDrawingsToShow');
    // tslint:disable-next-line: no-string-literal
    component['getDrawingsByTag']();
    // tslint:disable-next-line: no-string-literal
    expect(component['checkIfNoDrawingsToShow']).toHaveBeenCalled();
  });

  // deleteDrawing
  it('deleteDrawing() should call getDrawingsByTag if drawing is defined', async(() => {
    const drawingExpected = new Drawing('0', '<svg>');
    spyOn(databaseDrawingsServiceMock, 'deleteDrawing').and.returnValue(of(drawingExpected));
    // tslint:disable-next-line: no-any
    spyOn<any>(component, 'deleteDrawing').and.callThrough();
    // tslint:disable-next-line: no-any
    spyOn<any>(component, 'getDrawingsByTag');
    // tslint:disable-next-line: no-string-literal
    component['deleteDrawing'](drawingExpected);
    fixture.detectChanges();
    // tslint:disable-next-line: no-string-literal
    expect(component['getDrawingsByTag']).toHaveBeenCalled();
  }));

  it('deleteDrawing() should call allDrawings.splice if drawing is defined', async(() => {
    const drawingExpected = new Drawing('0', '<svg>');
    component['allDrawings'].push(drawingExpected, new Drawing('1', '<svg>'));
    spyOn(databaseDrawingsServiceMock, 'deleteDrawing').and.returnValue(of(drawingExpected));
    // tslint:disable-next-line: no-any
    spyOn<any>(component, 'deleteDrawing').and.callThrough();
    // tslint:disable-next-line: no-any
    spyOn<any>(component['allDrawings'], 'splice');
    // tslint:disable-next-line: no-string-literal
    component['deleteDrawing'](drawingExpected);
    fixture.detectChanges();
    // tslint:disable-next-line: no-string-literal
    expect(component['allDrawings'].splice).toHaveBeenCalledWith(component['allDrawings'].indexOf(drawingExpected), 1);
  }));

  it('deleteDrawing() should call drawingsToShow.splice if drawing is defined', async(() => {
    const drawingExpected = new Drawing('0', '<svg>');
    component['drawingsToShow'].push(drawingExpected, new Drawing('1', '<svg>'));
    spyOn(databaseDrawingsServiceMock, 'deleteDrawing').and.returnValue(of(drawingExpected));
    // tslint:disable-next-line: no-any
    spyOn<any>(component, 'deleteDrawing').and.callThrough();
    // tslint:disable-next-line: no-any
    spyOn<any>(component['drawingsToShow'], 'splice');
    // tslint:disable-next-line: no-string-literal
    component['deleteDrawing'](drawingExpected);
    fixture.detectChanges();
    // tslint:disable-next-line: no-string-literal
    expect(component['drawingsToShow'].splice).toHaveBeenCalledWith(component['drawingsToShow'].indexOf(drawingExpected), 1);
  }));

  it('deleteDrawing() should set showLoadingSpinner to false if drawing is defined', async(() => {
    const drawingExpected = new Drawing('0', '<svg>');
    spyOn(databaseDrawingsServiceMock, 'deleteDrawing').and.returnValue(of(drawingExpected));
    component['showLoadingSpinner'] = true;
    // tslint:disable-next-line: no-string-literal
    component['deleteDrawing'](drawingExpected);
    fixture.detectChanges();
    // tslint:disable-next-line: no-string-literal
    expect(component['showLoadingSpinner']).toBeFalsy();
  }));

  // loadDrawing
  it('loadDrawing() should update currentDrawing if drawing is defined', async(() => {
    const drawingExpected = new Drawing('0', '<svg>');
    spyOn(databaseDrawingsServiceMock, 'getCurrentDrawing').and.returnValue(of(drawingExpected));
    const drawingToNotBeLoaded = new Drawing('noGoodDrawing', '');
    component['currentDrawing'] = drawingToNotBeLoaded;
    // tslint:disable-next-line: no-string-literal
    component['loadDrawing'](drawingExpected);
    fixture.detectChanges();
    expect(component['currentDrawing']).toEqual(drawingExpected);
  }));

  it('loadDrawing() should set showLoadingSpinner to false if drawing is defined', async(() => {
    const drawingExpected = new Drawing('0', '<svg>');
    component['showLoadingSpinner'] = true;
    spyOn(databaseDrawingsServiceMock, 'getCurrentDrawing').and.returnValue(of(drawingExpected));
    // tslint:disable-next-line: no-string-literal
    component['loadDrawing'](drawingExpected);
    fixture.detectChanges();
    expect(component['showLoadingSpinner']).toBeFalsy();
  }));

  // addTagFilter
  it('addTagFilter() should call databaseDrawingsService.addTag', () => {
    const eventInput = new HTMLInputElementMock();
    eventInput.value = 'eventInputvalue';
    const eventValue = 'eventValue';
    // tslint:disable-next-line: no-any
    const event: any = {input: eventInput, value: eventValue};
    spyOn(component, 'addTagFilter').and.callThrough();
    // tslint:disable-next-line: no-string-literal
    spyOn(component['databaseDrawingsService'], 'addTag');
    component.addTagFilter(event);
    // tslint:disable-next-line: no-string-literal
    expect(component['databaseDrawingsService'].addTag).toHaveBeenCalled();
  });

  it('addTagFilter() should call getDrawingsByTag', () => {
    const eventInput = new HTMLInputElementMock();
    eventInput.value = 'eventInputvalue';
    const eventValue = 'eventValue';
    // tslint:disable-next-line: no-any
    const event: any = {input: eventInput, value: eventValue};
    spyOn(component, 'addTagFilter').and.callThrough();
    // tslint:disable-next-line: no-any
    spyOn<any>(component, 'getDrawingsByTag');
    component.addTagFilter(event);
    // tslint:disable-next-line: no-string-literal
    expect(component['getDrawingsByTag']).toHaveBeenCalled();
  });

  // removeTagFilter
  it('removeTagFilter() should call databaseDrawingsService.removeTag', () => {
    const tag = 'a';
    component['allTagFilters'] = new Array<string>('a', 'b', 'c');
    spyOn(component, 'removeTagFilter').and.callThrough();
    // tslint:disable-next-line: no-string-literal
    spyOn(component['databaseDrawingsService'], 'removeTag');
    component.removeTagFilter(tag);
    // tslint:disable-next-line: no-string-literal
    expect(component['databaseDrawingsService'].removeTag).toHaveBeenCalled();
  });

  it('removeTagFilter() should call getDrawingsByTag', () => {
    const tag = 'a';
    spyOn(component, 'removeTagFilter').and.callThrough();
    // tslint:disable-next-line: no-any
    spyOn<any>(component, 'getDrawingsByTag');
    component.removeTagFilter(tag);
    // tslint:disable-next-line: no-string-literal
    expect(component['getDrawingsByTag']).toHaveBeenCalled();
  });

  // openConfirmPanel
  it('openConfirmPanel(load, drawing) should open a dialog openConfirmPanel and call loadDrawing', () => {
    // tslint:disable-next-line: no-any because unknow type
    const subject = new Subject<any>();
    const mathDialogSpy = jasmine.createSpyObj('MatDialogRef', ['afterClosed']);
    // tslint:disable-next-line: no-any because unknow type
    const dialogSpy = spyOn<any>(component, 'loadDrawing');
    mathDialogSpy.afterClosed.and.returnValue(subject.asObservable());
    spyOn(component.dialog, 'open').and.returnValue(mathDialogSpy);

    const action = 'charger';
    const drawing = new Drawing('testId', '');

    component.openConfirmPanel(action, drawing);
    subject.next(action);
    expect(dialogSpy).toHaveBeenCalled();
    dialogRefSpy.close();
  });

  it('openConfirmPanel(delete, drawing) should open a dialog openConfirmPanel and call deleteDrawing', () => {
    // tslint:disable-next-line: no-any because unknow type
    const subject = new Subject<any>();
    const mathDialogSpy = jasmine.createSpyObj('MatDialogRef', ['afterClosed']);
    // tslint:disable-next-line: no-any because unknow type
    const dialogSpy = spyOn<any>(component, 'deleteDrawing');
    mathDialogSpy.afterClosed.and.returnValue(subject.asObservable());
    spyOn(component.dialog, 'open').and.returnValue(mathDialogSpy);

    const action = 'supprimer';
    const drawing = new Drawing('testId', '');

    component.openConfirmPanel(action, drawing);
    subject.next(action);
    expect(dialogSpy).toHaveBeenCalled();
    dialogRefSpy.close();
  });

  it('imageOnLoad should call drawImage', () => {
    const drawing = new Drawing('testId', '');
    component['context'] = mockContext;
    component['canvas'] = mockCanvas;
    spyOn<any>(component['context'], 'drawImage');
    spyOn<any>(component['canvas'], 'toDataURL');
    component['imageOnLoad'](drawing);

    expect(component['context'].drawImage).toHaveBeenCalled();
    expect(component['canvas'].toDataURL).toHaveBeenCalled();
  });

  it('imageOnLoad should not call drawImage when context is null', () => {
    component['context'] = mockContext;
    const drawImageSpy = spyOn<any>(component['context'], 'drawImage');
    const drawing = new Drawing('testId', '');
    component['context'] = null;
    component['imageOnLoad'](drawing);

    expect(drawImageSpy).toHaveBeenCalledTimes(0);
  });

});
