import { ENTER, SPACE } from '@angular/cdk/keycodes';
import { Component, Renderer2 } from '@angular/core';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { WHITE } from 'src/app/color-palette/constant';
import { DatabaseDrawingsService } from 'src/app/services/database-drawings/database-drawings.service';
import { LoadingSpinnerService } from 'src/app/services/database-drawings/loading-spinner/loading-spinner.service';
import { DrawSheetService } from 'src/app/services/draw-sheet/draw-sheet.service';
import { Drawing } from '../../../../../../common/drawing';
import { ConfirmPanelComponent } from '../confirm-panel/confirm-panel.component';
import * as JSON from '../export/export.json';

const config = JSON.default;

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent {

  readonly SEPARATOR_KEYS_CODES: number[] = [ENTER, SPACE];
  // constant values in french, because they are displayed
  readonly DELETE_ACTION: string = 'supprimer';
  readonly LOAD_ACTION: string = 'charger';
  readonly PREVIEW_MAX_SIZE: number = 1000;

  protected drawingsToShow: Drawing[];
  protected noDrawingsToShow: boolean;
  protected showLoadingSpinner: boolean;
  protected allTagFilters: string[];

  private context: CanvasRenderingContext2D | null;
  private image: HTMLImageElement;
  private canvas: HTMLCanvasElement;

  private allDrawings: Drawing[];
  private currentDrawing: Drawing;

  constructor(private databaseDrawingsService: DatabaseDrawingsService,
              private renderer: Renderer2,
              public loadingSpinnerService: LoadingSpinnerService,
              public dialog: MatDialog,
              public spinner: MatProgressSpinnerModule,
              private data: DrawSheetService,
              private dialogRef: MatDialogRef<GalleryComponent>) {

    this.data.resizePage();
    this.data.color.sendBaseColor(WHITE);

    this.allDrawings = new Array<Drawing>();
    this.drawingsToShow = new Array<Drawing>();
    this.allTagFilters = new Array<string>();
    this.noDrawingsToShow = false;
    this.loadingSpinnerService.isVisible.subscribe((visible) => {
      this.showLoadingSpinner = visible;
    });
    this.getDrawingsByTag();
  }

  addTagFilter(event: MatChipInputEvent): void {
    this.databaseDrawingsService.addTag(event, this.allTagFilters);
    this.getDrawingsByTag();
  }

  removeTagFilter(tag: string): void {
    this.databaseDrawingsService.removeTag(this.allTagFilters, tag);
    this.getDrawingsByTag();
  }

  openConfirmPanel(action: string, drawing: Drawing): void {
    const dialogRef = this.dialog.open(ConfirmPanelComponent, {
      data: { action, name: drawing.name }
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === this.DELETE_ACTION) {
        this.deleteDrawing(drawing);
      }
      if (result === this.LOAD_ACTION) {
        this.loadDrawing(drawing);
        this.dialogRef.close();
      }
    });
  }

  private checkIfNoDrawingsToShow(): void {
    this.databaseDrawingsService.allDrawings$.subscribe((allDrawings$) => {
      allDrawings$.length === 0 ? this.noDrawingsToShow = true : this.noDrawingsToShow = false;
    });
  }

  private getDrawingsByTag(): void {
    this.databaseDrawingsService.getDrawings(this.allTagFilters).subscribe(async (drawingsToShow) => {
      const drawingsToShowNoDuplicate = new Map<string, Drawing>(drawingsToShow.map((drawing) => [drawing.id, drawing]));
      this.drawingsToShow = Array.from(drawingsToShowNoDuplicate.values());
      await this.computePreviews(this.drawingsToShow);
    });
    this.checkIfNoDrawingsToShow();
  }

  private deleteDrawing(drawing: Drawing): void {
    this.databaseDrawingsService.deleteDrawing(drawing).subscribe(() => {
      this.getDrawingsByTag();
    });
    this.allDrawings.splice(this.allDrawings.indexOf(drawing), 1);
    this.drawingsToShow.splice(this.drawingsToShow.indexOf(drawing), 1);
    this.showLoadingSpinner = false;
  }

  private loadDrawing(drawing: Drawing): void {
    this.showLoadingSpinner = false;
    this.databaseDrawingsService.getCurrentDrawing(drawing).subscribe((drawingToLoad) => {
      this.currentDrawing = drawingToLoad;
      this.data.changeDataDrawSheet(this.currentDrawing.svgContent);
    });
  }

  private async computePreviews(drawings: Drawing[]): Promise<void> {
    for (const drawing of drawings) {
      this.canvas = this.renderer.createElement(config.canvas);
      this.context = this.canvas.getContext(config.twoDimensions);

      this.canvas.width = this.PREVIEW_MAX_SIZE;
      this.canvas.height = this.PREVIEW_MAX_SIZE;

      const blop = new Blob([drawing.svgContent], {type: config.type});
      const url = window.URL.createObjectURL(blop);
      this.image = new Image();
      this.image.src = url;

      await this.loadPreview(drawing);
    }
  }

  private async loadPreview(drawing: Drawing): Promise<void> {
    return new Promise((resolve) => {
      this.image.onload = () => resolve(this.imageOnLoad(drawing));
    });
  }

  private imageOnLoad(drawing: Drawing): void {
    if (this.context) {
      this.context.drawImage(this.image, 0, 0);
      drawing.pngContent = this.canvas.toDataURL(config.image + config.png);
    }
  }
}
