import { Component , HostListener, OnInit} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AutoSaveService } from 'src/app/services/auto-save/auto-save.service';
import { DrawSheetService } from 'src/app/services/draw-sheet/draw-sheet.service';
import { GuideComponent } from '../../guide/guide.component';
import { GalleryComponent } from '../gallery/gallery.component';
import { NewDrawingComponent } from '../new-drawing/new-drawing.component';
import * as JSON from './home-page.json';

const config = JSON.default;

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})

export class HomePageComponent  implements OnInit {

  protected show: boolean;
  private shortcutsEnabled: boolean = true;

  constructor(public dialog: MatDialog, private data: DrawSheetService, private save: AutoSaveService) {}

  ngOnInit(): void {
    if ( this.save.storedItem !== undefined ) {
      this.show = this.save.storedItem.length !== 0;
    } else {
      this.show = false;
    }
  }

  openNewDraw(): void {
    const dialogRef = this.dialog.open(NewDrawingComponent);
    this.shortcutsEnabled = false;
    dialogRef.afterClosed().subscribe((result) => {
      this.shortcutsEnabled = true;
    });
  }

  openGallery(): void {
    const dialogRef = this.dialog.open(GalleryComponent, {width: config.width, maxWidth: config.maxWidthGallery});
    this.shortcutsEnabled = false;
    dialogRef.afterClosed().subscribe((result) => {
      this.shortcutsEnabled = true;
    });
  }

  openGuide(): void {
    const dialogRef4 = this.dialog.open(GuideComponent, { maxWidth: config.maxWidthGuide});
    dialogRef4.afterClosed().subscribe();
  }

  continueDrawing(): void {
    this.data.resizePage();
    this.data.changeDataDrawSheet(this.save.storedItem);
  }

  @HostListener(config.keyDownControlO, [config.event]) openNewDrawShortCut(event: KeyboardEvent): void {
    event.preventDefault();
    if (this.shortcutsEnabled) {
      this.openNewDraw();
    }
  }

  @HostListener(config.keyDownControlG, [config.event]) openGalleryShortCut(event: KeyboardEvent): void {
    event.preventDefault();
    if (this.shortcutsEnabled) {
      this.openGallery();
    }
  }

}
