// tslint:disable: no-any
// tslint:disable: no-string-literal
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material';
import { of } from 'rxjs/internal/observable/of';

import { EMPTY } from 'rxjs/internal/observable/empty';
import { Subject } from 'rxjs/internal/Subject';
import { HomePageComponent } from './home-page.component';

export class MatDialogMock {
  // tslint:disable-next-line: typedef
  open() {
    return {
      afterClosed: () => of({action: true})
    };
  }
}

describe('HomePageComponent', () => {
  let component: HomePageComponent;
  let fixture: ComponentFixture<HomePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePageComponent ],
      providers: [{
        provide: MatDialog,
        useClass: MatDialogMock
      }]
    })
    .compileComponents();
  }));

  beforeEach(async () => {
    fixture = TestBed.createComponent(HomePageComponent);
    component = fixture.componentInstance;

    component.ngOnInit();
    await fixture.whenStable();
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit should set show to false if we dont a have a current drawing', () => {
    spyOnProperty(component['save'], 'storedItem').and.returnValue('');
    component.ngOnInit();
    expect(component['show']).toBeFalsy();
  });

  it('ngOnInit should set show to true if we a have a current drawing', () => {
    spyOnProperty(component['save'], 'storedItem').and.returnValue(' ');
    component.ngOnInit();
    expect(component['show']).toBeTruthy();
  });

  it('ngOnInit should set show to false if the current drawing is undefined', () => {
    spyOnProperty(component['save'], 'storedItem').and.returnValue(undefined);
    component.ngOnInit();
    expect(component['show']).toBeFalsy();
  });

  it('openGallery() should open a dialog GalleryComponent', () => {
    const dialogSpy = spyOn(component.dialog, 'open').and.returnValue({afterClosed: () => EMPTY} as any);
    component.openGallery();
    expect(dialogSpy).toHaveBeenCalled();
  });

  it('openNewDraw() should open a dialog NewDrawingComponent', () => {
    component['shortcutsEnabled'] = false;
    const subject = new Subject<any>();
    const mathDialogSpy = jasmine.createSpyObj('MatDialogRef', ['afterClosed']);
    mathDialogSpy.afterClosed.and.returnValue(subject.asObservable());
    spyOn(component.dialog, 'open').and.returnValue(mathDialogSpy);
    component.openNewDraw();
    subject.next(null);
    expect(component['shortcutsEnabled']).toBeTruthy();
  });

  it('openGuide() should open a dialog GuideComponent', () => {
    const dialogSpy = spyOn(component.dialog, 'open').and.returnValue({afterClosed: () => EMPTY} as any);
    component.openGuide();
    expect(dialogSpy).toHaveBeenCalled();
  });

  it('Pushing the control and o button down simultaniously should call openNewDrawShortcut if shortcuts are enabled', () => {
    spyOn(component, 'openNewDraw');
    const mockKeyboard = new KeyboardEvent('keydown.control.o');
    component['shortcutsEnabled'] = true;
    component.openNewDrawShortCut(mockKeyboard);
    expect(component.openNewDraw).toHaveBeenCalledTimes(1);
  });

  it('Pushing the control and o button down simultaniously should call nothing when shortcut is disabled', () => {
    spyOn(component, 'openNewDraw');
    const mockKeyboard = new KeyboardEvent('keydown.control.o');
    component['shortcutsEnabled'] = false;
    component.openNewDrawShortCut(mockKeyboard);
    expect(component.openNewDraw).toHaveBeenCalledTimes(0);
  });

  it('Pushing the control and g button down simultaniously should call nothing when shortcut is disabled', () => {
    spyOn(component, 'openGallery');
    const mockKeyboard = new KeyboardEvent('keydown.control.g');
    component['shortcutsEnabled'] = false;
    component.openGalleryShortCut(mockKeyboard);
    expect(component.openGallery).toHaveBeenCalledTimes(0);
  });

  it('Pushing the control and g button down simultaniously should call openGalleryShortCut if shortcuts are enabled', () => {
    spyOn(component, 'openGallery');
    const mockKeyboard = new KeyboardEvent('keydown.control.g');
    component['shortcutsEnabled'] = true;
    component.openGalleryShortCut(mockKeyboard);
    expect(component.openGallery).toHaveBeenCalledTimes(1);
  });

  it('openGallery() should open a dialog GalleryComponent', () => {
    component['shortcutsEnabled'] = false;
    const subject = new Subject<any>();
    const mathDialogSpy = jasmine.createSpyObj('MatDialogRef', ['afterClosed']);
    mathDialogSpy.afterClosed.and.returnValue(subject.asObservable());
    spyOn(component.dialog, 'open').and.returnValue(mathDialogSpy);
    component.openGallery();
    subject.next(null);
    expect(component['shortcutsEnabled']).toBeTruthy();
  });

  it('continueDrawing() should call resizePage and changeDataDrawSheet of data', () => {
    spyOn(component['data'], 'resizePage');
    spyOn(component['data'], 'changeDataDrawSheet');
    component.continueDrawing();
    expect(component['data'].resizePage).toHaveBeenCalledTimes(1);
    expect(component['data'].changeDataDrawSheet).toHaveBeenCalledTimes(1);
  });
});
