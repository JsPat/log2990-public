import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import 'hammerjs';
import { AutoSaveService } from 'src/app/services/auto-save/auto-save.service';
import { DrawSheetService } from 'src/app/services/draw-sheet/draw-sheet.service';

export let browserRefresh = false;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

    constructor(private router: Router, private data: DrawSheetService, private save: AutoSaveService) {
    }

    ngOnInit(): void {
        this.router.events.subscribe((event) => {
            if (event instanceof NavigationStart) {
                this.data.resizePage();
                this.data.changeDataDrawSheet(this.save.storedItem);
            }
        });
    }

}
