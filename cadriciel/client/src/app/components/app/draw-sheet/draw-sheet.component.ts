import { Component, ElementRef, HostListener, OnInit, Renderer2 } from '@angular/core';
import { Color } from 'src/app/color-palette/color/color';
import { WHITE } from 'src/app/color-palette/constant';
import { ToolHolderService } from 'src/app/services/attributes/tools-holder/tools-holder.service';
import { AutoSaveService } from 'src/app/services/auto-save/auto-save.service';
import { CommandInvokerService } from 'src/app/services/command-invoker/command-invoker.service';
import { PossibleTools } from 'src/app/services/constants';
import { DrawSheetService } from 'src/app/services/draw-sheet/draw-sheet.service';
import { ShapesHolderService } from 'src/app/services/shapes-holder/shapes-holder.service';
import { SVGChildService } from 'src/app/services/svg-child/svg-child.service';
import { SVGFilters } from '../attributes/svg-filters/svg-filters';
import { DrawSheetData } from '../new-drawing/draw-sheet-model';
import * as JSON from './draw-sheet.json';

const config = JSON.default;

@Component({
  selector: 'app-drawSheet',
  templateUrl: './draw-sheet.component.html',
  styleUrls: ['./draw-sheet.component.css'],
})

export class DrawSheetComponent implements OnInit {

  private clickDrag: boolean = false;
  private shiftPressed: boolean = false;

  // drawing sheet
  private sheetColor: Color = WHITE;
  private drawSheetData: DrawSheetData = new DrawSheetData(0, 0, this.sheetColor);

  // shapes
  svgFilters: SVGFilters;
  private sheet: SVGElement;
  private draw: SVGElement;
  private topLevel: SVGElement;

  constructor(private data: DrawSheetService,
              private svgService: SVGChildService,
              private renderer: Renderer2,
              private el: ElementRef,
              private shapesHolder: ShapesHolderService,
              private toolHolder: ToolHolderService,
              private commandInvoker: CommandInvokerService,
              private autoSave: AutoSaveService) {

    this.drawSheetData = this.data.dataDrawSheet;
    this.sheetColor = this.drawSheetData.color;
  }

  ngOnInit(): void {
    this.data.currentDataDrawSheet.subscribe(
      (drawSheetData) => {
        this.drawSheetData = drawSheetData;
        this.updateDraw();
        });
    this.data.toggleObs.subscribe( (newTools: PossibleTools) => {
      this.toolHolder.toolSwap(newTools);
    });

    this.svgService.appendObs.subscribe((newShape) => {
      this.renderer.appendChild(this.draw, newShape);
    });

    this.svgService.removeObs.subscribe((shape) => {
      this.renderer.removeChild(this.draw, shape);
      this.renderer.removeChild(this.topLevel, shape);
    });

    this.svgService.appendOnTopObs.subscribe((shape) => {
      if (shape instanceof SVGGElement && this.topLevel.firstChild !== null) {
        this.renderer.insertBefore(this.topLevel, shape, this.topLevel.firstChild);
      } else {
        this.renderer.appendChild(this.topLevel, shape);
      }
    });

    this.svgService.replaceObs.subscribe(([newSVG, oldSVG]) => {
      this.renderer.insertBefore(this.draw, newSVG, oldSVG);
      this.renderer.removeChild(this.draw, oldSVG);
    });

    this.createNewEmptySheet();
  }

  updateDraw(): void {
    this.renderer.removeChild(this.el, this.sheet);
    this.createNewEmptySheet();
  }

  @HostListener(config.mousedown, [config.event]) mouseDown(event: MouseEvent): void {
    this.clickDrag = true;
    this.toolHolder.activeTool.mouseDown(event);
  }

  @HostListener(config.mouseup, [config.event]) mouseUp(event: MouseEvent): void {
    this.clickDrag = false;
    this.toolHolder.activeTool.mouseUp();
    this.autoSave.save();
  }

  @HostListener(config.mousemove, [config.event]) mouseDrag(event: MouseEvent): void {
    (this.clickDrag) ? this.toolHolder.activeTool.mouseDrag(event) : this.toolHolder.activeTool.mouseMove(event);
  }

  @HostListener(config.click, [config.event]) click(event: MouseEvent): void {
    this.toolHolder.activeTool.click(event);
    this.autoSave.save();
  }

  @HostListener(config.dblclick, [config.event]) dblClick(event: MouseEvent): void {
    this.toolHolder.activeTool.dblClick(event);
    // this.autoSave.save();
  }

  // if leave the svg window, stop the tool
  @HostListener(config.mouseleave, [config.event]) mouseLeave(event: MouseEvent): void {
    this.clickDrag = false;
    this.toolHolder.activeTool.mouseLeave(event);
    this.autoSave.save();
  }

  @HostListener(config.keydownShift, [config.event]) shiftDown(): void {
    if (!this.shiftPressed) {
      this.shiftPressed = true;
      this.toolHolder.activeTool.shiftDown();
    }
  }

  @HostListener(config.keyupShift, [config.event]) shiftUp(): void {
    if (this.shiftPressed) {
      this.shiftPressed = false;
      this.toolHolder.activeTool.shiftUp();
    }
  }

  @HostListener(config.keydownEscape, [config.event]) escapeDown(): void {
    this.toolHolder.activeTool.escapeDown();
  }

  @HostListener(config.keydownBackspace, [config.event]) backSpaceDown(): void {
    this.toolHolder.activeTool.backSpaceDown();
  }

  // Arrow is released
  @HostListener(config.keyupArrowDown, [config.event]) downArrowUp(event: KeyboardEvent): void {
    this.toolHolder.activeTool.arrowKeyUp(event);
  }
  @HostListener(config.keyupArrowUp, [config.event]) upArrowUp(event: KeyboardEvent): void {
    this.toolHolder.activeTool.arrowKeyUp(event);
  }
  @HostListener(config.keyupArrowLeft, [config.event]) leftArrowUp(event: KeyboardEvent): void {
    this.toolHolder.activeTool.arrowKeyUp(event);
  }
  @HostListener(config.keyupArrowRight, [config.event]) rightArrowUp(event: KeyboardEvent): void {
    this.toolHolder.activeTool.arrowKeyUp(event);
  }
  // Arrow is pressed
  @HostListener(config.keydownArrowDown, [config.event]) downArrowDown(event: KeyboardEvent): void {
    this.preventArrowKeyScrolling(event);
  }
  @HostListener(config.keydownArrowUp, [config.event]) upArrowDown(event: KeyboardEvent): void {
    this.preventArrowKeyScrolling(event);
  }
  @HostListener(config.keydownArrowLeft, [config.event]) leftArrowDown(event: KeyboardEvent): void {
    this.preventArrowKeyScrolling(event);
  }
  @HostListener(config.keydownArrowRight, [config.event]) rightArrowDown(event: KeyboardEvent): void {
    this.preventArrowKeyScrolling(event);
  }
  @HostListener(config.contextMenu, [config.event]) rightClick(event: MouseEvent): void {
    event.preventDefault();
    this.toolHolder.activeTool.rightClick(event);
  }

  @HostListener(config.keydownControlZ, [config.event]) undo(event: KeyboardEvent): void {
    event.preventDefault();
    this.toolHolder.activeTool.toolSwap();
    this.commandInvoker.undo();
    this.autoSave.save();
  }

  @HostListener(config.keydownControlShiftZ, [config.event]) redo(event: KeyboardEvent): void {
    event.preventDefault();
    this.toolHolder.activeTool.toolSwap();
    this.commandInvoker.redo();
    this.autoSave.save();
  }

  @HostListener(config.wheel, [config.event]) wheel(event: WheelEvent): void {
    // Find a way to prevent scrolling when handling a selection
    // Potential solution: overflow CSS on drawview.css for drawsheet class
    // Set to hidden when having a selection and scroll when no selection
    this.toolHolder.activeTool.wheel(event);
  }

  private preventArrowKeyScrolling(event: KeyboardEvent): void {
    event.preventDefault();
    this.toolHolder.activeTool.arrowKeyDown(event);
  }

  private createNewEmptySheet(): void {
    this.commandInvoker.reset();
    this.shapesHolder.clear();
    this.updateSheetData();
    this.createSheet();
    this.createDraw();
    this.createTopLevel();
  }

  private updateSheetData(): void {
    this.sheetColor = this.drawSheetData.color;
  }

  private createSheet(): void {
    this.sheet = this.renderer.createElement(config.svg.svg, config.svg.svg);
    this.renderer.setAttribute(this.sheet, config.svg.attribute.width, `${this.drawSheetData.width}`);
    this.renderer.setAttribute(this.sheet, config.svg.attribute.height, `${this.drawSheetData.height}`);
    this.renderer.appendChild(this.el.nativeElement, this.sheet);
  }

  private createDraw(): void {
    this.draw = this.renderer.createElement(config.svg.svg, config.svg.svg);
    this.renderer.setAttribute(this.draw, config.svg.attribute.width, `${this.drawSheetData.width}`);
    this.renderer.setAttribute(this.draw, config.svg.attribute.height, `${this.drawSheetData.height}`);
    this.renderer.setAttribute(this.draw, config.svg.attribute.id, config.svg.value.drawing);

    if ( this.drawSheetData.svg !== undefined ) {
      this.draw.innerHTML = this.getExistingSVG();
    } else {
      // definitions (filters and markers)
      this.svgFilters = new SVGFilters(this.renderer);
      this.renderer.appendChild(this.draw, this.svgFilters.filters);

      // background
      const background = this.renderer.createElement(config.svg.background, config.svg.svg);
      this.renderer.setAttribute(background, config.svg.attribute.x, config.svg.value.zero);
      this.renderer.setAttribute(background, config.svg.attribute.y, config.svg.value.zero);
      this.renderer.setAttribute(background, config.svg.attribute.width, `${this.drawSheetData.width}`);
      this.renderer.setAttribute(background, config.svg.attribute.height, `${this.drawSheetData.height}`);
      this.renderer.setAttribute(background, config.svg.attribute.fill, `#${this.sheetColor.rgb_hexa}`);
      this.renderer.setAttribute(background, config.svg.attribute.dataIsSelectable, config.svg.value.false);
      this.renderer.appendChild(this.draw, background);
    }
    this.renderer.appendChild(this.sheet, this.draw);
    this.data.svg = this.draw as SVGSVGElement;
    this.populateShapesHolder();
    this.autoSave.save();
  }

  private populateShapesHolder(): void {
    let currentNode: SVGGraphicsElement = this.draw.firstChild as SVGGraphicsElement;
    currentNode = this.renderer.nextSibling(currentNode);
    while (currentNode !== null) {
      const tempNode = currentNode;
      currentNode = this.renderer.nextSibling(currentNode);
      if (tempNode instanceof SVGDefsElement || tempNode.dataset.shouldSave === config.svg.value.false) {
        this.renderer.removeChild(this.draw, tempNode);
      } else if (tempNode.dataset.isSelectable !== config.svg.value.false) {
        this.shapesHolder.addShape(tempNode as SVGGraphicsElement);
      }
    }
  }

  private createTopLevel(): void {
    this.topLevel = this.renderer.createElement(config.svg.svg, config.svg.svg);
    this.renderer.setAttribute(this.topLevel, config.svg.attribute.width, `${this.drawSheetData.width}`);
    this.renderer.setAttribute(this.topLevel, config.svg.attribute.height, `${this.drawSheetData.height}`);
    this.renderer.appendChild(this.sheet, this.topLevel);
  }

  private getExistingSVG(): string {
    const index = this.drawSheetData.svg.indexOf(config.svg.lastCharSVG) + 1;
    return this.drawSheetData.svg.slice(index, this.drawSheetData.svg.lastIndexOf(config.svg.svgFirstClosingTag));
  }
}
