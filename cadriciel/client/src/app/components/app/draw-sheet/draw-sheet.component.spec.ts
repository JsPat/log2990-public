/* tslint:disable:no-unused-variable */
// tslint:disable no-string-literal
// tslint:disable: no-any
import { NO_ERRORS_SCHEMA, Renderer2 } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Subject } from 'rxjs';
import { ToolsService } from 'src/app/services/attributes/toolsService/tools-service';
import { DrawSheetService } from 'src/app/services/draw-sheet/draw-sheet.service';
import { Tool } from '../attributes/tool';

import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { AutoSaveService } from 'src/app/services/auto-save/auto-save.service';
import { PossibleTools } from 'src/app/services/constants';
import { SVGChildService } from 'src/app/services/svg-child/svg-child.service';
import { DrawSheetData } from '../new-drawing/draw-sheet-model';
import { DrawSheetComponent } from './draw-sheet.component';

class ToolMock extends Tool {
}

// tslint:disable-next-line: max-classes-per-file
class DrawSheetServiceMock extends DrawSheetService {
  svgToString(): string {
    return '<svg></svg>';
  }
}

describe('DrawSheetComponent', () => {

  let component: DrawSheetComponent;
  let fixture: ComponentFixture<DrawSheetComponent>;

  let drawSheetDataSpy: DrawSheetService;
  let toolsServiceSpy: ToolsService;
  let colorSpy: ColorService;
  let rendererSpy: any;
  let svgServiceSpy: SVGChildService;
  let autoSaveSpy: AutoSaveService;
  let drawSpy: any;
  let topLevelSpy: any;
  let elSpy: any;

  beforeEach(async(() => {
    rendererSpy = jasmine.createSpyObj('Renderer2', ['removeChild', 'appendChild', 'insertBefore', 'nextSibling']);
    toolsServiceSpy = new ToolsService();
    colorSpy = new ColorService();
    drawSheetDataSpy = new DrawSheetServiceMock(toolsServiceSpy, colorSpy);
    svgServiceSpy = new SVGChildService(jasmine.createSpyObj('ShapesHolderService', ['addShape', 'removeShape']));
    autoSaveSpy = jasmine.createSpyObj('AutoSave', ['save']);
    drawSpy = jasmine.createSpyObj('SVGElement', ['firstChild', 'innerHTML']);
    topLevelSpy = jasmine.createSpyObj('SVGElement', ['firstChild']);
    elSpy = jasmine.createSpyObj('ElementRef', ['nativeElement']);

    TestBed.configureTestingModule({
      declarations: [ DrawSheetComponent ],
      providers: [
        { provide: Renderer2, usevalue: rendererSpy},
        { provide: ToolsService, useValue: toolsServiceSpy },
        { provide: DrawSheetService, useValue: drawSheetDataSpy },
        { provide: AutoSaveService, useValue: autoSaveSpy },
        { provide: SVGChildService, useValue: svgServiceSpy}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawSheetComponent);
    component = fixture.componentInstance;
    component['toolHolder']['currentTool'] = new ToolMock();
    fixture.detectChanges();
    rendererSpy = jasmine.createSpyObj('Renderer2',
                 ['createElement', 'setAttribute', 'appendChild', 'removeChild', 'insertBefore', 'nextSibling']);
    component['renderer'] = rendererSpy;
    component['draw'] = drawSpy;
    component['topLevel'] = topLevelSpy;
    component['el'] = elSpy;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Pushing the mouse button down should call mouseDown of the currentTool and clickDrag should be enabled', () => {
      spyOn(component['toolHolder'].activeTool, 'mouseDown');
      const mockClick = new MouseEvent('mousedown', {
        screenX: 0,
        screenY: 0,
        clientX: 0,
        clientY: 0,
        ctrlKey: false,
        shiftKey: false,
        altKey: false,
        metaKey: false,
        button: 0,
        buttons: 1,
        relatedTarget: null
      });
      component['clickDrag'] = false;
      component.mouseDown(mockClick);
      expect(component['toolHolder'].activeTool.mouseDown).toBeTruthy();
      expect(component['toolHolder'].activeTool.mouseDown).toHaveBeenCalledTimes(1);
      expect(component['clickDrag']).toBeTruthy();
  });

  it('Releasing the mouse button should call mouseUp of the currentTool and clickDrag should be disabled', () => {
    spyOn(component['toolHolder'].activeTool, 'mouseUp');
    component['clickDrag'] = true;
    const up = new MouseEvent('mouseup');
    component.mouseUp(up);
    expect(component['toolHolder'].activeTool.mouseUp).toHaveBeenCalledTimes(1);
    expect(component['autoSave'].save).toHaveBeenCalled();
    expect(component['clickDrag']).toBeFalsy();
  });

  it('Moving the mouse with mouse button down should call mouseDrag of the currentTool', () => {
    spyOn(component['toolHolder'].activeTool, 'mouseDrag');
    spyOn(component['toolHolder'].activeTool, 'mouseMove');
    const mockDrag = new MouseEvent('mousemove', {
      screenX: 0,
      screenY: 0,
      clientX: 0,
      clientY: 0,
      ctrlKey: false,
      shiftKey: false,
      altKey: false,
      metaKey: false,
      button: 0,
      buttons: 1,
      relatedTarget: null
    });
    component['clickDrag'] = true;
    component.mouseDrag(mockDrag);
    expect(component['toolHolder'].activeTool.mouseDrag).toHaveBeenCalledTimes(1);
    expect(component['toolHolder'].activeTool.mouseMove).toHaveBeenCalledTimes(0);
  });

  it('Moving the mouse with mouse button up should call mouseMove of the currentTool', () => {
    spyOn(component['toolHolder'].activeTool, 'mouseDrag');
    spyOn(component['toolHolder'].activeTool, 'mouseMove');
    const mockDrag = new MouseEvent('mousemove', {
      screenX: 0,
      screenY: 0,
      clientX: 0,
      clientY: 0,
      ctrlKey: false,
      shiftKey: false,
      altKey: false,
      metaKey: false,
      button: 0,
      buttons: 1,
      relatedTarget: null
    });
    component['clickDrag'] = false;
    component.mouseDrag(mockDrag);
    expect(component['toolHolder'].activeTool.mouseDrag).toHaveBeenCalledTimes(0);
    expect(component['toolHolder'].activeTool.mouseMove).toHaveBeenCalledTimes(1);
  });

  it('Pressing and releasing mouse button should call click of the currentTool', () => {
    spyOn<any>(component['toolHolder'].activeTool, 'click');
    const mockClick = new MouseEvent('click');
    component.click(mockClick);
    expect(component['toolHolder'].activeTool.click).toHaveBeenCalledTimes(1);
    expect(component['autoSave'].save).toHaveBeenCalled();
  });

  it('Pressing and releasing mouse button two times in a row should call dblClick of the currentTool', () => {
    spyOn(component['toolHolder'].activeTool, 'dblClick');
    const mockDblClick = new MouseEvent('dblclick');
    component.dblClick(mockDblClick);
    expect(component['toolHolder'].activeTool.dblClick).toHaveBeenCalledTimes(1);
  });

  it('Leaving the component with the mouse should call mouseLeave of the currentTool', () => {
    spyOn(component['toolHolder'].activeTool, 'mouseLeave');
    const mockLeave = new MouseEvent('mouseleave');
    component.mouseLeave(mockLeave);
    expect(component['toolHolder'].activeTool.mouseLeave).toHaveBeenCalledTimes(1);
  });

  it('Pressing shift button down should call shiftDown of the currentTool', () => {
    spyOn(component['toolHolder'].activeTool, 'shiftDown');
    component['shiftPressed'] = false;
    component.shiftDown();
    expect(component['toolHolder'].activeTool.shiftDown).toHaveBeenCalledTimes(1);
    expect(component['shiftPressed']).toBeTruthy();
  });

  it('Pressing shift button down should not call shiftDown if already pressed', () => {
    spyOn(component['toolHolder'].activeTool, 'shiftDown');
    component['shiftPressed'] = true;
    component.shiftDown();
    expect(component['toolHolder'].activeTool.shiftDown).toHaveBeenCalledTimes(0);
    expect(component['shiftPressed']).toBeTruthy();
  });

  it('Releasing shift button should call shiftUp of the currentTool', () => {
    spyOn(component['toolHolder'].activeTool, 'shiftUp');
    component['shiftPressed'] = true;
    component.shiftUp();
    expect(component['toolHolder'].activeTool.shiftUp).toHaveBeenCalledTimes(1);
    expect(component['shiftPressed']).toBeFalsy();
  });

  it('Releasing shift button should not call shiftUp if it is already released', () => {
    spyOn(component['toolHolder'].activeTool, 'shiftUp');
    component['shiftPressed'] = false;
    component.shiftUp();
    expect(component['toolHolder'].activeTool.shiftUp).toHaveBeenCalledTimes(0);
    expect(component['shiftPressed']).toBeFalsy();
  });

  it('Pressing escape button down should call escapeDown of the currentTool', () => {
    spyOn(component['toolHolder'].activeTool, 'escapeDown');
    component.escapeDown();
    expect(component['toolHolder'].activeTool.escapeDown).toHaveBeenCalledTimes(1);
  });

  it('Pressing backspace button down should call backSpaceDown of the currentTool', () => {
    spyOn(component['toolHolder'].activeTool, 'backSpaceDown');
    component.backSpaceDown();
    expect(component['toolHolder'].activeTool.backSpaceDown).toHaveBeenCalledTimes(1);
  });
  it('Pressing downArrowUp button down should call arrowKeyUp of the activeTool', () => {
    const mockEvent = new KeyboardEvent('window:keyup.arrowright');
    spyOn<any>(component['toolHolder'].activeTool, 'arrowKeyUp');
    component['downArrowUp'](mockEvent);
    expect(component['toolHolder'].activeTool.arrowKeyUp).toHaveBeenCalledTimes(1);
  });

  it('Pressing upArrowUp button down should call arrowKeyUp of the activeTool', () => {
    const mockEvent = new KeyboardEvent('window:keyup.arrowright');
    spyOn<any>(component['toolHolder'].activeTool, 'arrowKeyUp');
    component['upArrowUp'](mockEvent);
    expect(component['toolHolder'].activeTool.arrowKeyUp).toHaveBeenCalledTimes(1);
  });

  it('Pressing leftArrowUp button down should call arrowKeyUp of the activeTool', () => {
    const mockEvent = new KeyboardEvent('window:keyup.arrowright');
    spyOn<any>(component['toolHolder'].activeTool, 'arrowKeyUp');
    component['leftArrowUp'](mockEvent);
    expect(component['toolHolder'].activeTool.arrowKeyUp).toHaveBeenCalledTimes(1);
  });

  it('Pressing rightArrowUp button down should call arrowKeyUp of the activeTool', () => {
    const mockEvent = new KeyboardEvent('window:keyup.arrowright');
    spyOn<any>(component['toolHolder'].activeTool, 'arrowKeyUp');
    component['rightArrowUp'](mockEvent);
    expect(component['toolHolder'].activeTool.arrowKeyUp).toHaveBeenCalledTimes(1);
  });

  it('Pressing upArrowDown button down should call preventArrowKeyScrolling', () => {
    const mockEvent = new KeyboardEvent('window:keyup.arrowright');
    spyOn<any>(component, 'preventArrowKeyScrolling');
    component['upArrowDown'](mockEvent);
    expect(component['preventArrowKeyScrolling']).toHaveBeenCalledTimes(1);
  });

  it('Pressing downArrowDown button down should call preventArrowKeyScrolling', () => {
    const mockEvent = new KeyboardEvent('window:keyup.arrowright');
    spyOn<any>(component, 'preventArrowKeyScrolling');
    component['downArrowDown'](mockEvent);
    expect(component['preventArrowKeyScrolling']).toHaveBeenCalledTimes(1);
  });

  it('Pressing leftArrowDown button down should call preventArrowKeyScrolling', () => {
    const mockEvent = new KeyboardEvent('window:keyup.arrowright');
    spyOn<any>(component, 'preventArrowKeyScrolling');
    component['leftArrowDown'](mockEvent);
    expect(component['preventArrowKeyScrolling']).toHaveBeenCalledTimes(1);
  });

  it('Pressing rightArrowDown button down should call preventArrowKeyScrolling', () => {
    const mockEvent = new KeyboardEvent('window:keyup.arrowright');
    spyOn<any>(component, 'preventArrowKeyScrolling');
    component['rightArrowDown'](mockEvent);
    expect(component['preventArrowKeyScrolling']).toHaveBeenCalledTimes(1);
  });

  it('Pressing rightClick button down should call rightClick of the activeTool', () => {
    const mockEvent = new MouseEvent('click');
    spyOn(component['toolHolder'].activeTool, 'rightClick');
    component.rightClick(mockEvent);
    expect(component['toolHolder'].activeTool.rightClick).toHaveBeenCalledTimes(1);
  });

  it('Pressing z button down should call only undo of the commandInvoker', () => {
    spyOn(component['toolHolder'].activeTool, 'toolSwap');
    spyOn(component['commandInvoker'], 'undo');

    const event = jasmine.createSpyObj('KeyboardEvent', ['preventDefault']);

    component.undo(event);
    expect(component['toolHolder'].activeTool.toolSwap).toHaveBeenCalledTimes(1);
    expect(component['commandInvoker'].undo).toHaveBeenCalledTimes(1);
    expect(event.preventDefault).toHaveBeenCalled();
  });

  it('Pressing z + shift button down should call redo of the commandInvoker', () => {
    spyOn(component['toolHolder'].activeTool, 'toolSwap');
    spyOn(component['commandInvoker'], 'redo');

    const event = jasmine.createSpyObj('KeyboardEvent', ['preventDefault']);

    component.redo(event);
    expect(component['toolHolder'].activeTool.toolSwap).toHaveBeenCalledTimes(1);
    expect(component['commandInvoker'].redo).toHaveBeenCalledTimes(1);
    expect(event.preventDefault).toHaveBeenCalled();
  });

  it('mouse wheel should call wheel of the activeTool', () => {
    spyOn(component['toolHolder'].activeTool, 'wheel');
    const event = jasmine.createSpyObj('WheelEvent', ['']);
    component.wheel(event);
    expect(component['toolHolder'].activeTool.wheel).toHaveBeenCalledTimes(1);
  });

  it('updateSheetData() should save drawSheetData into the good attributes', () => {
    const sheetHeight = 5;
    const sheetWidth = 5;
    const sheetColor = jasmine.createSpyObj('Color', ['']);

    component['drawSheetData'] = new DrawSheetData(sheetHeight, sheetWidth, sheetColor);
    component['updateSheetData']();

    expect(component['sheetColor']).toEqual(sheetColor);
  });

  it('createNewEmptySheet() should call all methods needed to create a new Sheet', () => {
    spyOn<any>(component, 'updateSheetData');
    spyOn<any>(component, 'createSheet');
    spyOn<any>(component, 'createDraw');
    spyOn<any>(component, 'createTopLevel');

    component['createNewEmptySheet']();

    expect(component['updateSheetData']).toHaveBeenCalled();
    expect(component['createSheet']).toHaveBeenCalled();
    expect(component['createDraw']).toHaveBeenCalled();
    expect(component['createTopLevel']).toHaveBeenCalled();
  });

  it('createSheet should create a new sheet', () => {
    component['createSheet']();

    expect(rendererSpy.createElement).toHaveBeenCalledWith('svg', 'svg');
    expect(rendererSpy.appendChild).toHaveBeenCalledWith(component['el'].nativeElement, component['sheet']);
  });

  it('createDraw  should create a new svg draw', () => {
    spyOn<any>(component, 'populateShapesHolder').and.callFake(() => null);
    component['createDraw']();

    expect(rendererSpy.createElement).toHaveBeenCalledWith('svg', 'svg');
    expect(rendererSpy.appendChild).toHaveBeenCalledWith(component['sheet'], component['draw']);
  });

  it('createDraw  should create an existing svg draw', () => {
    spyOn<any>(component, 'populateShapesHolder').and.callFake(() => null);
    const svgSpy = jasmine.createSpyObj('SVGElement', ['']);
    Object.defineProperty(svgSpy, 'innerHtml', {value: undefined});
    (rendererSpy.createElement as any).and.returnValue(svgSpy);
    component['drawSheetData'].svg = '<svg>allo</svg>';

    component['createDraw']();

    expect(rendererSpy.createElement).toHaveBeenCalledWith('svg', 'svg');
    expect(rendererSpy.appendChild).toHaveBeenCalledWith(component['sheet'], component['draw']);
  });

  it('createTopLevel should create a new svg canavs that will hold top level', () => {
    component['createTopLevel']();

    expect(rendererSpy.createElement).toHaveBeenCalledWith('svg', 'svg');
    expect(rendererSpy.appendChild).toHaveBeenCalledWith(component['sheet'], component['topLevel']);
  });

  it('updateDraw() should have removeChild and call createNewEmptySheet', () => {
    spyOn<any>(component, 'createNewEmptySheet');
    component.updateDraw();
    expect(rendererSpy.removeChild).toHaveBeenCalled();
    expect(component['createNewEmptySheet']).toHaveBeenCalled();
  });

  it('createDraw() should subscribe data.toggleObs', () => {
    const toggleSubject = new Subject<PossibleTools>();
    spyOnProperty(toolsServiceSpy, 'toggleObs', 'get').and.returnValue(toggleSubject.asObservable());
    const toolSwap = spyOn<any>(component['toolHolder'], 'toolSwap');
    spyOn<any>(component, 'populateShapesHolder').and.callFake(() => null);

    component.ngOnInit();

    toggleSubject.next(PossibleTools.ERASER);

    expect(toolSwap).toHaveBeenCalledWith(PossibleTools.ERASER);
  });
  it('ngOnInit() should subscribe drawSheetData', () => {
    spyOn<any>(component, 'populateShapesHolder').and.callFake(() => null);
    const subject = new Subject<DrawSheetData>();

    spyOn(component, 'updateDraw');
    spyOnProperty(drawSheetDataSpy, 'currentDataDrawSheet', 'get').and.returnValue(subject.asObservable());

    const color = jasmine.createSpyObj('Color', ['']);
    const data = new DrawSheetData(0, 0, color);

    component.ngOnInit();

    subject.next(data);

    expect(component['drawSheetData']).toEqual(data);
    expect(component.updateDraw).toHaveBeenCalled();
  });

  it('ngOnInit() should subscribe to the append obs of the svgService', () => {
    spyOn<any>(component, 'populateShapesHolder').and.callFake(() => null);
    const subject = new Subject<SVGElement>();
    spyOnProperty(svgServiceSpy, 'appendObs', 'get').and.returnValue(subject.asObservable());

    component.ngOnInit();

    subject.next();

    expect(rendererSpy.appendChild).toHaveBeenCalled();
  });

  it('ngOnInit() should subscribe to the removeobs of the svgService', () => {
    spyOn<any>(component, 'populateShapesHolder').and.callFake(() => null);
    const subject = new Subject<SVGElement>();
    spyOnProperty(svgServiceSpy, 'removeObs', 'get').and.returnValue(subject.asObservable());

    component.ngOnInit();

    subject.next();

    expect(rendererSpy.removeChild).toHaveBeenCalled();
  });

  it('ngOnInit() should subscribe to the appendOnTopObs of the svgService', () => {
    spyOn<any>(component, 'populateShapesHolder').and.callFake(() => null);
    const subject = new Subject<SVGElement>();
    spyOnProperty(svgServiceSpy, 'appendOnTopObs', 'get').and.returnValue(subject.asObservable());

    component.ngOnInit();

    subject.next();

    expect(rendererSpy.appendChild).toHaveBeenCalled();
  });

  it('ngOnInit() should subscribe to the appendOnTopObs of the svgService and call insert before', () => {
    spyOn<any>(component, 'populateShapesHolder').and.callFake(() => null);
    const subject = new Subject<SVGElement>();

    const top = document.createElementNS('http://www.w3.org/2000/svg', 'svg') as SVGElement;
    const child = document.createElementNS('http://www.w3.org/2000/svg', 'g') as SVGGElement;
    component['topLevel'] = top;
    spyOnProperty(svgServiceSpy, 'appendOnTopObs', 'get').and.returnValue(subject.asObservable());
    const spy = spyOnProperty<any>(component['topLevel'], 'firstChild').and.returnValue(child);
    const spy2 = spyOn<any>(component, 'createNewEmptySheet');
    component.ngOnInit();

    subject.next(child);
    expect(spy).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
    expect(rendererSpy.insertBefore).toHaveBeenCalled();

  });

  it('ngOnInit() should subscribe to the replaceObs of the svgService', () => {
    spyOn<any>(component, 'populateShapesHolder').and.callFake(() => null);
    const subject = new Subject<[SVGElement, SVGElement]>();
    spyOnProperty(svgServiceSpy, 'replaceObs', 'get').and.returnValue(subject.asObservable());
    const svgElementSpy = jasmine.createSpyObj('SVGElement', ['']);

    component.ngOnInit();

    subject.next([svgElementSpy, svgElementSpy]);

    expect(rendererSpy.removeChild).toHaveBeenCalled();
    expect(rendererSpy.insertBefore).toHaveBeenCalled();
  });

  it('getExistingSVG() should call indexOf and slice', () => {
    component['drawSheetData'].svg = '<svg>allo</svg>';
    const test = component['getExistingSVG']();

    expect(test).toBe('allo');
  });

  it('preventArrowKeyScrolling() should call arrowKeyDown', () => {
    spyOn(component['toolHolder'].activeTool, 'arrowKeyDown');
    const mockEvent = new KeyboardEvent('window:keydown.shift');
    component['preventArrowKeyScrolling'](mockEvent);

    expect(component['toolHolder'].activeTool.arrowKeyDown).toHaveBeenCalled();
  });

  it('populateShapesHolder() should call firstChild, nextSibling and removeshape', () => {
    const child = jasmine.createSpyObj('SVGGraphicsElement', ['nextSibling']);
    const nextSibling = null;
    const dataset = {
      shouldSave: 'false',
  };
    Object.defineProperty(child, 'dataset', {value: dataset});
    // spyOn<any>(component['draw'], 'firstChild').and.returnValues(child);
    rendererSpy.nextSibling.and.returnValues(child, child, nextSibling);
    component['populateShapesHolder']();

    expect(rendererSpy.removeChild).toHaveBeenCalled();
  });

  it('populateShapesHolder() should call firstChild, nextSibling and addShape', () => {
    spyOn(component['shapesHolder'], 'addShape');
    const child = jasmine.createSpyObj('SVGGraphicsElement', ['nextSibling']);
    const nextSibling = null;
    const dataset = {
      isSelectable: 'true',
  };
    Object.defineProperty(child, 'dataset', {value: dataset});
    // spyOn<any>(component['draw'], 'firstChild').and.returnValues(child);
    rendererSpy.nextSibling.and.returnValues(child, child, nextSibling);
    component['populateShapesHolder']();

    expect(component['shapesHolder'].addShape).toHaveBeenCalled();
  });

// tslint:disable-next-line: max-file-line-count
});
