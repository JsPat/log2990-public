/* tslint:disable:no-unused-variable */
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ConfirmPanelComponent } from './confirm-panel.component';

describe('ConfirmPanelComponent', () => {
  let component: ConfirmPanelComponent;
  let fixture: ComponentFixture<ConfirmPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmPanelComponent ],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: [] },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set (true) drawNotNull to dataSheet.buttonContinue if LOAD_ACTION', () => {
    // tslint:disable-next-line: no-string-literal
    component.data.action = component['LOAD_ACTION'];
    // tslint:disable-next-line: no-string-literal
    component['dataSheet']['enableButton'] = true;
    component.drawNotNull = false;

    fixture = TestBed.createComponent(ConfirmPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(component.drawNotNull).toBeTruthy();
  });

  it('should set (false) drawNotNull to dataSheet.buttonContinue if LOAD_ACTION', () => {
    // tslint:disable-next-line: no-string-literal
    component.data.action = component['LOAD_ACTION'];
    // tslint:disable-next-line: no-string-literal
    component['dataSheet']['enableButton'] = false;
    component.drawNotNull = true;

    fixture = TestBed.createComponent(ConfirmPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(component.drawNotNull).toBeFalsy();
  });
});
