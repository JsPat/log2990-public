import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DrawSheetService } from 'src/app/services/draw-sheet/draw-sheet.service';
import { GalleryComponent } from '../gallery/gallery.component';
import { DialogConfirmationData } from './dialog-confirmation-data';

@Component({
  selector: 'app-confirm-panel',
  templateUrl: './confirm-panel.component.html',
  styleUrls: ['./confirm-panel.component.css']
})
export class ConfirmPanelComponent {
  // constant values in french, because they are displayed
  private readonly LOAD_ACTION: string = 'charger';
  drawNotNull: boolean = false;
  deleteEnabled: boolean;

  constructor(public dialogRef: MatDialogRef<GalleryComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogConfirmationData, private  dataSheet: DrawSheetService) {
  }

  ngOnInit(): void {
    if (this.data.action === this.LOAD_ACTION) {
      this.deleteEnabled = true;
      this.drawNotNull =  this.dataSheet.buttonContinue;

    } else {
      this.deleteEnabled = false;
    }
  }

}
