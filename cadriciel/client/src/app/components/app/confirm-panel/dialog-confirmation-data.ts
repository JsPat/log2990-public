export interface DialogConfirmationData {
    action: string;
    name: string;
  }
