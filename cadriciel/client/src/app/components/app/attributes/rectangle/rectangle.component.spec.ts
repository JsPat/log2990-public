/* tslint:disable:no-unused-variable */
/* tslint:disable:no-string-literal*/
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RectangleComponent } from './rectangle.component';

describe('RectangleComponent', () => {
  let component: RectangleComponent;
  let fixture: ComponentFixture<RectangleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RectangleComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RectangleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // button
  it('fillButtonDisabled should be false if borderEnabled is true when onBorderButtonChange is called', () => {
    component['service'].borderEnabled = true;
    component.onBorderButtonChange();
    expect(component['fillButtonDisabled']).toBeFalsy();
  });

  it('fillButtonDisabled should be true if borderEnabled is false when onBorderButtonChange is called', () => {
    component['service'].borderEnabled = false;
    component.onBorderButtonChange();
    expect(component['fillButtonDisabled']).toBeTruthy();
  });

  it('borderButtonDisabled should be false if fillEnabled is true when onFillButtonChange is called', () => {
    component['service'].fillEnabled = true;
    component.onFillButtonChange();
    expect(component['borderButtonDisabled']).toBeFalsy();
  });

  it('borderButtonDisabled should be true if fillEnabled is false when onFillButtonChange is called', () => {
    component['service'].fillEnabled = false;
    component.onFillButtonChange();
    expect(component['borderButtonDisabled']).toBeTruthy();
  });
});
