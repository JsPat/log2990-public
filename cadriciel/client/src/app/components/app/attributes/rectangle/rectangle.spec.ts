// tslint:disable: no-string-literal
// tslint:disable: no-any
// tslint:disable: no-magic-numbers

import { TestBed } from '@angular/core/testing';

import { Renderer2 } from '@angular/core';
import { Color } from 'src/app/color-palette/color/color';
import { Coordinate } from '../coordinate';
import { Rectangle } from './rectangle';

describe('rectangle class', () => {
    let rectangle: Rectangle;
    let color: Color;
    let strokeColor: Color;
    let position: Coordinate;
    let rendererSpy: Renderer2;

    class MockColor extends Color {
        constructor() {
            super(0, 0, 0);
        }
    }

    beforeEach(async () => {
        TestBed.configureTestingModule({
            providers: [
                Renderer2,
                Coordinate,
                {provide: Color, useClass: MockColor}
            ]
        }).compileComponents();
    });
    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute', 'appendChild']);
        color = new MockColor();
        strokeColor = new MockColor();
        position = new Coordinate();
        position.x = 1;
        position.y = 1;
        rectangle = new Rectangle(position, color, 5, strokeColor, false, false, rendererSpy);
        rectangle['width'] = 10;
        rectangle['height'] = 10;
    });

    it('should create', () => {
        expect(rectangle).toBeTruthy();
    });

    it('updateShape should call drawShape', () => {
        spyOn<any>(rectangle, 'drawShape');
        rectangle.updateShape({x: 2, y: 3}, false);
        expect(rectangle['drawShape']).toHaveBeenCalled();
    });

    it('if shift is pressed, the width should be the same of height and it should be the min of both', () => {
        // tslint:disable-next-line: no-magic-numbers
        spyOn<any>(rectangle, 'findHeight').and.returnValue(20);
        // tslint:disable-next-line: no-magic-numbers
        spyOn<any>(rectangle, 'findWidth').and.returnValue(10);

        rectangle.updateShape({x: 2, y: 3}, true);

        // tslint:disable-next-line: no-magic-numbers
        expect(rectangle['width']).toEqual(10);
        // tslint:disable-next-line: no-magic-numbers
        expect(rectangle['height']).toEqual(10);
    });

    it('if shift is not pressed, the width should be the value given by the findWidth', () => {
        // tslint:disable-next-line: no-magic-numbers
        spyOn<any>(rectangle, 'findWidth').and.returnValue(15);

        rectangle.updateShape({x: 2, y: 3}, false);

        // tslint:disable-next-line: no-magic-numbers
        expect(rectangle['width']).toEqual(15);
    });

    it('if shift is not pressed, the height should be the value given by the findHeight', () => {
        // tslint:disable-next-line: no-magic-numbers
        spyOn<any>(rectangle, 'findHeight').and.returnValue(17);

        rectangle.updateShape({x: 2, y: 3}, false);

        // tslint:disable-next-line: no-magic-numbers
        expect(rectangle['height']).toEqual(17);
    });

    it('drawShape should setAttribute to update the x position to current x', () => {
        rectangle['position'] = {x: 10, y: 6};
        rectangle['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(rectangle['svg'], 'x', '10');
    });

    it('drawShape should setAttribute to update the y position to current x', () => {
        rectangle['position'] = {x: 7, y: 8};
        rectangle['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(rectangle['svg'], 'y', '8');
    });

    it('drawShape should setAttribute to update the height position to current height', () => {
        // tslint:disable-next-line: no-magic-numbers
        rectangle['height'] = 12;
        rectangle['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(rectangle['svg'], 'height', '12');
    });

    it('drawShape should setAttribute to update the width position to current width', () => {
        // tslint:disable-next-line: no-magic-numbers
        rectangle['width'] = 8;
        rectangle['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(rectangle['svg'], 'width', '8');
    });

    it('drawShape should setAttribute to update the opacity to the color opacity', () => {
        // tslint:disable-next-line: no-magic-numbers
        rectangle['color'] = new Color(0, 0, 0, 12);
        rectangle['drawShape']();
        // tslint:disable-next-line: no-magic-numbers
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(rectangle['svg'], 'fill-opacity', `${12 / 255}`);
    });

    it('drawShape should setAttribute to update the fill to the color if fill is activated', () => {
        rectangle['color'] = new Color(171, 205, 239);
        rectangle['fill'] = true;
        rectangle['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(rectangle['svg'], 'fill', '#ABCDEF');
    });

    it('drawShape should setAttribute to update the fill to none if fill is not activated', () => {
        rectangle['fill'] = false;
        rectangle['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(rectangle['svg'], 'fill', 'none');
    });

    it('drawShape should setAttribute to update the stroke to the stroke color if border is enbale', () => {
        rectangle['border'] = true;
        rectangle['strokeColor'] = new Color(254, 220, 186);
        rectangle['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(rectangle['svg'], 'stroke', '#FEDCBA');
    });

    it('drawShape should setAttribute to update the stroke-opacity to the opacity if border is enable', () => {
        rectangle['border'] = true;
        // tslint:disable-next-line: no-magic-numbers
        rectangle['strokeColor'] = new Color(0, 0, 0, 24);
        rectangle['drawShape']();
        // tslint:disable-next-line: no-magic-numbers
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(rectangle['svg'], 'stroke-opacity', `${24 / 255}`);
    });

    it('drawShape should setAttribute to update the stroke-width to the given width if border is enable', () => {
        rectangle['border'] = true;
        // tslint:disable-next-line: no-magic-numbers
        rectangle['strokeWidth'] = 8;
        rectangle['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(rectangle['svg'], 'stroke-width', '8');
    });

    it('drawShape should setAttribute to update the isSelectable to true', () => {
        rectangle['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(rectangle['svg'], 'data-is-selectable', 'true');
    });

    it('drawShape should call setAttribute 10 time if border is not enable', () => {
        rectangle['border'] = false;
        rectangle['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(10);
    });

    it('drawShape should call setAttribute 14 time if border enable', () => {
        rectangle['border'] = true;
        rectangle['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(14);
    });

    it('getStartingPosition should return the starting x if the given x is bigger', () => {
        rectangle['start'] = {x: 10, y: 15};
        const newStart = rectangle['getStartingPosition']({x: 20, y: 25});
        expect(newStart.x).toEqual(10);
    });

    it('getStartingPosition should return the starting y if the given y is bigger', () => {
        rectangle['start'] = {x: 10, y: 15};
        const newStart = rectangle['getStartingPosition']({x: 20, y: 25});
        expect(newStart.y).toEqual(15);
    });

    it('getStartingPosition should return the given x if the given x is lower then the starting', () => {
        rectangle['start'] = {x: 20, y: 15};
        const newStart = rectangle['getStartingPosition']({x: 10, y: 5});

        // tslint:disable-next-line: no-magic-numbers
        expect(newStart.x).toEqual(10);
    });

    it('getStartingPosition should return the given y if the given y is lower then the starting', () => {
        rectangle['start'] = {x: 20, y: 15};
        const newStart = rectangle['getStartingPosition']({x: 10, y: 5});

        // tslint:disable-next-line: no-magic-numbers
        expect(newStart.y).toEqual(5);
    });

    it('findWidth return the difference between the starting x position and the given one', () => {
        rectangle['start'] = {x: 0, y: 0};
        const width = rectangle['findWidth']({x: 20, y: 30});

        // tslint:disable-next-line: no-magic-numbers
        expect(width).toEqual(20);
    });

    it('findHeight return the difference between the starting y position and the given one', () => {
        rectangle['start'] = {x: 0, y: 0};
        const height = rectangle['findHeight']({x: 20, y: 30});

        // tslint:disable-next-line: no-magic-numbers
        expect(height).toEqual(30);
    });

    it('isNull should return true if the rectangle width is null and height is not null', () => {
        rectangle['height'] = 2;
        rectangle['width'] = 0;
        expect(rectangle['isNull']()).toBeTruthy();
    });

    it('isNull should return true if the rectangle width is not null and height is null', () => {
        rectangle['height'] = 0;
        rectangle['width'] = 3;
        expect(rectangle['isNull']()).toBeTruthy();
    });

    it('isNull should return false if the rectangle width is not null and height is not null', () => {
        rectangle['height'] = 2;
        rectangle['width'] = 3;
        expect(rectangle['isNull']()).toBeFalsy();
    });

});
