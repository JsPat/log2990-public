import { Renderer2 } from '@angular/core';
import { Color } from 'src/app/color-palette/color/color';
import { Coordinate } from '../coordinate';
import { SVGHolder } from '../svg-holder';
import * as JSON from './rectangle.json';

const config = JSON.default;

export class Rectangle extends SVGHolder {
    protected position: Coordinate;
    protected height: number;
    protected width: number;
    protected color: Color;
    protected strokeWidth: number;
    protected strokeColor: Color;

    protected border: boolean;
    protected fill: boolean;
    protected start: Coordinate;
    protected renderer: Renderer2;

    constructor(position: Coordinate, color: Color, strokeWidth: number, strokeColor: Color,
                border: boolean, fill: boolean, renderer: Renderer2) {
        super();

        this.position = position;
        this.start = position;
        this.height = 0;
        this.width = 0;
        this.color = color;
        this.strokeWidth = strokeWidth;
        this.strokeColor = strokeColor;

        this.border = border;
        this.fill = fill;

        this.renderer = renderer;

        this.svg = this.renderer.createElement(config.element.type, config.element.from);
    }

    updateShape(mousePosition: Coordinate, shiftPressed: boolean): void {
        const height = this.findHeight(mousePosition);
        const width = this.findWidth(mousePosition);

        if (shiftPressed) {
            this.width = this.height = Math.min(height, width);
        } else {
            this.height = height;
            this.width = width;
        }
        this.position = this.getStartingPosition(mousePosition);

        this.drawShape();
    }

    isNull(): boolean {
        return this.width === 0 || this.height === 0;
    }

    private drawShape(): void {
        this.renderer.setAttribute(this.svg, config.position.x, `${this.position.x}`);
        this.renderer.setAttribute(this.svg, config.position.y, `${this.position.y}`);
        this.renderer.setAttribute(this.svg, config.dimension.height, `${this.height}`);
        this.renderer.setAttribute(this.svg, config.dimension.width, `${this.width}`);
        this.renderer.setAttribute(this.svg, config.fill.opacity, `${this.color.a}`);
        this.renderer.setAttribute(this.svg, config.selectable.identifier, config.selectable.true);
        this.renderer.setAttribute(this.svg, config.eraserPreview, config.border.color);

        if (this.border) {
            this.renderer.setAttribute(this.svg, config.border.color, `#${this.strokeColor.rgb_hexa}`);
            this.renderer.setAttribute(this.svg, config.border.opacity, `${this.strokeColor.a}`);
            this.renderer.setAttribute(this.svg, config.border.width, `${this.strokeWidth}`);
            this.renderer.setAttribute(this.svg, config.border.position, config.border.inside);
            this.renderer.setAttribute(this.svg, config.secondaryColor, config.border.color);
        } else {
            this.renderer.setAttribute(this.svg, config.secondaryColor, config.none);
        }

        if (this.fill) {
            this.renderer.setAttribute(this.svg, config.mainColor, config.fill.color);
            this.renderer.setAttribute(this.svg, config.fill.color, `#${this.color.rgb_hexa}`);
        } else {
            this.renderer.setAttribute(this.svg, config.fill.color, config.none);
            this.renderer.setAttribute(this.svg, config.mainColor, config.none);
        }
    }

    private getStartingPosition(mousePosition: Coordinate): Coordinate {
        const x = (mousePosition.x < this.start.x) ? this.start.x - this.width  : this.start.x;
        const y = (mousePosition.y < this.start.y) ? this.start.y - this.height : this.start.y;
        return {x, y};
    }

    private findWidth(mousePosition: Coordinate): number {
        return Math.abs(this.start.x - mousePosition.x);
    }

    private findHeight(mousePosition: Coordinate): number {
        return Math.abs(this.start.y - mousePosition.y);
    }
}
