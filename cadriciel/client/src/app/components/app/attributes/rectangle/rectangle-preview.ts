import { Renderer2 } from '@angular/core';
import { Color } from 'src/app/color-palette/color/color';
import { BLACK, TRANSPARENT } from 'src/app/color-palette/constant';
import { Coordinate } from '../coordinate';
import { Rectangle } from './rectangle';
import * as JSON from './rectangle.json';

const config = JSON.default;
const PREVIEW_COLOR: Color = BLACK;
const PREVIEW_WIDTH: number = 1;

export class RectanglePreview extends Rectangle {

    constructor(position: Coordinate, renderer: Renderer2) {
        super(position, TRANSPARENT, PREVIEW_WIDTH, PREVIEW_COLOR, true, false, renderer);
        this.renderer.setAttribute(this.svg, config.shouldSave, config.valueFalse);
    }

    makeInvisible(): void {
        this.renderer.setAttribute(this.svg, config.border.color, config.none);
    }
}
