import { Component } from '@angular/core';
import { RectangleService } from 'src/app/services/attributes/rectangle/rectangle.service';

@Component({
  selector: 'app-rectangle',
  templateUrl: './rectangle.component.html',
  styleUrls: ['./rectangle.component.css']
})

export class RectangleComponent {
  protected borderButtonDisabled: boolean;
  protected fillButtonDisabled: boolean;
  protected service: RectangleService;

  constructor(service: RectangleService) {
    this.service = service;
    this.onBorderButtonChange();
    this.onFillButtonChange();

  }

  onBorderButtonChange(): void {
    this.fillButtonDisabled = this.service.fillEnabled && !this.service.borderEnabled;
  }

  onFillButtonChange(): void {
    this.borderButtonDisabled = this.service.borderEnabled && !this.service.fillEnabled;
  }
}
