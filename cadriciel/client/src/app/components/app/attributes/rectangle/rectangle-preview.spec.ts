// tslint:disable: no-string-literal
// tslint:disable: no-magic-numbers
// tslint:disable: no-any

import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { Coordinate } from '../coordinate';
import { RectanglePreview } from './rectangle-preview';

describe('rectangle class', () => {
    let rectangle: RectanglePreview;
    let position: Coordinate;
    let rendererSpy: Renderer2;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            providers: [
                Renderer2,
                Coordinate
            ]
        }).compileComponents();
    });
    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute', 'appendChild']);
        position = new Coordinate();
        position.x = 1;
        position.y = 1;
        rectangle = new RectanglePreview(position, rendererSpy);
        rectangle['width'] = 10;
        rectangle['height'] = 10;
    });

    it('should create', () => {
        expect(rectangle).toBeTruthy();
    });

    it('makeInivisble should set the attribute stroke to none', () => {
        rectangle.makeInvisible();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(rectangle['svg'], 'stroke', 'none');
    });
});
