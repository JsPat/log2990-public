// tslint:disable: no-any
// tslint:disable: no-magic-numbers
// tslint:disable: no-string-literal
// tslint:disable: no-invalid-this
// tslint:disable: typedef

import { LIGHT_BLUE, WHITE } from 'src/app/color-palette/constant';
import { SelectorSharedDataService } from 'src/app/services/attributes/selector/selector-shared-data.service';
import { Direction } from 'src/app/services/constants';
import { SelectorRectangle } from './selector-rectangle';

describe('SelectorRectangle Class', () => {

    let dataServiceSpy: SelectorSharedDataService;
    let rendererSpy: any;
    let drawViewServiceSpy: any;
    let svgServiceSpy: any;

    let selectorRectangle: SelectorRectangle;

    const HEIGHT: number = 40;
    const WIDTH: number = 50;

    beforeEach(() => {

        dataServiceSpy = jasmine.createSpyObj('SelectorSharedDataService', ['']);
        rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute']);
        drawViewServiceSpy = {
            navBarOpened: true,
            get navBarState() {
                return this.navBarOpened;
            }
        };
        svgServiceSpy = jasmine.createSpyObj('SVGChildService', ['appendNewSVG', 'removeSVG', 'appendSVGOnTop']);

        selectorRectangle = new SelectorRectangle(dataServiceSpy, rendererSpy, drawViewServiceSpy, svgServiceSpy);

        selectorRectangle['height'] = HEIGHT;
        selectorRectangle['width'] = WIDTH;

    });

    it('shouldCreate',  () => {
        expect(selectorRectangle).toBeTruthy();
    });

    it('constructor should create the rectangle and the 4 point', () => {
        expect(rendererSpy.createElement).toHaveBeenCalledTimes(5);
    });

    it('drawShapeshould set the right attribute of the rectangle', () => {
        const drawControlPoint = spyOn<any>(selectorRectangle, 'drawControlPoints');
        selectorRectangle['drawShape']();

        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(selectorRectangle['svg'], 'id', 'selectionRectangle');
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(selectorRectangle['svg'], 'x', `${selectorRectangle.position.x}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(selectorRectangle['svg'], 'y', `${selectorRectangle.position.y}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(selectorRectangle['svg'], 'height', `${selectorRectangle['height']}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(selectorRectangle['svg'], 'width', `${selectorRectangle['width']}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(selectorRectangle['svg'], 'data-is-selectable', 'false');
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(selectorRectangle['svg'], 'fill', 'none');
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(selectorRectangle['svg'], 'stroke', `#${LIGHT_BLUE.rgb_hexa}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(selectorRectangle['svg'], 'stroke-width', '1');

        expect(drawControlPoint).toHaveBeenCalled();
    });

    it('drawControlPoint should set the common attribute to each control point', () => {
        selectorRectangle['drawControlPoints']();

        for (let i = 0; i < Direction.N_DIRECTIONS; ++i) {
            // Common attributes
            expect(rendererSpy.setAttribute).toHaveBeenCalledWith(
                selectorRectangle['svgControlPoints'][i], 'height', `${selectorRectangle['CONTROL_POINT_DIAMETER']}`
            );
            expect(rendererSpy.setAttribute).toHaveBeenCalledWith(
                selectorRectangle['svgControlPoints'][i], 'width', `${selectorRectangle['CONTROL_POINT_DIAMETER']}`
            );
            expect(rendererSpy.setAttribute).toHaveBeenCalledWith(
                selectorRectangle['svgControlPoints'][i], 'data-is-selectable', 'false'
            );
            expect(rendererSpy.setAttribute).toHaveBeenCalledWith(
                selectorRectangle['svgControlPoints'][i], 'fill', `#${WHITE.rgb_hexa}`
            );
            expect(rendererSpy.setAttribute).toHaveBeenCalledWith(
                selectorRectangle['svgControlPoints'][i], 'stroke', `#${LIGHT_BLUE.rgb_hexa}`
            );
            expect(rendererSpy.setAttribute).toHaveBeenCalledWith(
                selectorRectangle['svgControlPoints'][i], 'stroke-width', '1'
            );
        }
    });

    it('drawControlPoint should set the UP point at the right position', () => {
        selectorRectangle['drawControlPoints']();

        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(
            selectorRectangle['svgControlPoints'][Direction.UP],
            'x',
            `${selectorRectangle.position.x + WIDTH / 2 - selectorRectangle['CONTROL_POINT_DIAMETER'] / 2}`
        );

        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(
            selectorRectangle['svgControlPoints'][Direction.UP],
            'y',
            `${selectorRectangle.position.y - selectorRectangle['CONTROL_POINT_DIAMETER'] / 2}`
        );
    });

    it('drawControlPoint should set the DOWN point at the right position', () => {
        selectorRectangle['drawControlPoints']();

        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(
            selectorRectangle['svgControlPoints'][Direction.DOWN],
            'x',
            `${selectorRectangle.position.x + WIDTH / 2 - selectorRectangle['CONTROL_POINT_DIAMETER'] / 2}`
        );

        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(
            selectorRectangle['svgControlPoints'][Direction.DOWN],
            'y',
            `${selectorRectangle.position.y + HEIGHT - selectorRectangle['CONTROL_POINT_DIAMETER'] / 2}`
        );
    });

    it('drawControlPoint should set the LEFT point at the right position', () => {
        selectorRectangle['drawControlPoints']();

        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(
            selectorRectangle['svgControlPoints'][Direction.LEFT],
            'x',
            `${selectorRectangle.position.x - selectorRectangle['CONTROL_POINT_DIAMETER'] / 2}`
        );

        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(
            selectorRectangle['svgControlPoints'][Direction.LEFT],
            'y',
            `${selectorRectangle.position.y + HEIGHT / 2 - selectorRectangle['CONTROL_POINT_DIAMETER'] / 2}`
        );
    });

    it('drawControlPoint should set the RIGHT point at the right position', () => {
        selectorRectangle['drawControlPoints']();

        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(
            selectorRectangle['svgControlPoints'][Direction.RIGHT],
            'x',
            `${selectorRectangle.position.x + WIDTH - selectorRectangle['CONTROL_POINT_DIAMETER'] / 2}`
        );

        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(
            selectorRectangle['svgControlPoints'][Direction.RIGHT],
            'y',
            `${selectorRectangle.position.y + HEIGHT / 2 - selectorRectangle['CONTROL_POINT_DIAMETER'] / 2}`
        );
    });

    it('isWhitinArea, should return false if isDrawn is false', () => {
        selectorRectangle.isDrawn = false;
        const event = jasmine.createSpyObj('MouseEvent', ['']);

        expect(selectorRectangle.isWithinArea(event)).toBeFalsy();
    });

    it('isWhitinArea, should return false if isDrawn is true and event.offsetX < selectorRectangle.position.x', () => {
        selectorRectangle.isDrawn = true;
        selectorRectangle.position = {x: 110, y: 190};

        const event = jasmine.createSpyObj('MouseEvent', ['']);

        Object.defineProperty(event, 'offsetX', {value: 100});
        Object.defineProperty(event, 'offsetY', {value: 200});

        expect(selectorRectangle.isWithinArea(event)).toBeFalsy();
    });

    it('isWhitinArea, should return false if isDrawn is true and event.offsetX > selectorRectangle.position.x + WIDTH', () => {
        selectorRectangle.isDrawn = true;
        selectorRectangle.position = {x: 90, y: 190};

        const event = jasmine.createSpyObj('MouseEvent', ['']);

        Object.defineProperty(event, 'offsetX', {value: 100 + WIDTH});
        Object.defineProperty(event, 'offsetY', {value: 200});

        expect(selectorRectangle.isWithinArea(event)).toBeFalsy();
    });

    it('isWhitinArea, should return false if isDrawn is true and event.offsetY < selectorRectangle.position.y', () => {
        selectorRectangle.isDrawn = true;
        selectorRectangle.position = {x: 90, y: 220};

        const event = jasmine.createSpyObj('MouseEvent', ['']);

        Object.defineProperty(event, 'offsetX', {value: 100});
        Object.defineProperty(event, 'offsetY', {value: 200});

        expect(selectorRectangle.isWithinArea(event)).toBeFalsy();
    });

    it('isWhitinArea, should return false if isDrawn is true and event.offsetY > selectorRectangle.position.y + HEIGHT', () => {
        selectorRectangle.isDrawn = true;
        selectorRectangle.position = {x: 90, y: 190};

        const event = jasmine.createSpyObj('MouseEvent', ['']);

        Object.defineProperty(event, 'offsetX', {value: 100});
        Object.defineProperty(event, 'offsetY', {value: 200 + HEIGHT});

        expect(selectorRectangle.isWithinArea(event)).toBeFalsy();
    });

    it('isWhitinArea, should return true if isDrawn is true and event position is whitin the given area', () => {
        selectorRectangle.isDrawn = true;
        selectorRectangle.position = {x: 90, y: 190};

        const event = jasmine.createSpyObj('MouseEvent', ['']);

        Object.defineProperty(event, 'offsetX', {value: 100});
        Object.defineProperty(event, 'offsetY', {value: 200});

        expect(selectorRectangle.isWithinArea(event)).toBeTruthy();
    });

    it('isAControlPoint should return false if we pass null to it', () => {
        expect(selectorRectangle.isAControlPoint(null)).toBeFalsy();
    });

    it('isAControlPoint should return false if we pass an eventTarget that is not instance of SVGRectElement', () => {
        const el = jasmine.createSpyObj('EventTarget', ['']);
        expect(selectorRectangle.isAControlPoint(el)).toBeFalsy();
    });

    it('isAControlPoint should return false if we pass an eventTarget that is an SVGRectElement but not in the svgControlPoints', () => {
        const el = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
        expect(selectorRectangle.isAControlPoint(el)).toBeFalsy();
    });

    it('isAControlPoint should return true if we pass an eventTarget that is an SVGRectElement contained in the svgControlPoints', () => {
        const el = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
        selectorRectangle['svgControlPoints'][0] = el;
        expect(selectorRectangle.isAControlPoint(el)).toBeTruthy();
    });

    it('redraw should call removeFromDrawing', () => {
        spyOn(selectorRectangle, 'addShapesToSelection');
        const removeFromDrawing = spyOn(selectorRectangle, 'removeFromDrawing');
        selectorRectangle.redraw();
        expect(removeFromDrawing).toHaveBeenCalled();
    });

    it('redraw should call addShapesToSelection with the currently selected shaps if no parameter is specidfied', () => {
        const addShapes = spyOn(selectorRectangle, 'addShapesToSelection');
        spyOn(selectorRectangle, 'removeFromDrawing');

        dataServiceSpy.selectedShapes = [
            jasmine.createSpyObj('SVGGraphicElement', ['']),
            jasmine.createSpyObj('SVGGraphicElement', [''])
        ];
        selectorRectangle.redraw();
        expect(addShapes).toHaveBeenCalledWith(dataServiceSpy.selectedShapes);
    });

    it('redraw should call addShapesToSelection with the given shapes', () => {
        const addShapes = spyOn(selectorRectangle, 'addShapesToSelection');
        spyOn(selectorRectangle, 'removeFromDrawing');

        const shapes = [
            jasmine.createSpyObj('SVGGraphicElement', ['']),
            jasmine.createSpyObj('SVGGraphicElement', [''])
        ];
        selectorRectangle.redraw(shapes);
        expect(addShapes).toHaveBeenCalledWith(shapes);
    });

    it('redraw should call appendNewSVG 1 + (number of point) times if isDrawn is true', () => {
        spyOn(selectorRectangle, 'addShapesToSelection');
        spyOn(selectorRectangle, 'removeFromDrawing');

        selectorRectangle.isDrawn = true;
        selectorRectangle.redraw();
        expect(svgServiceSpy.appendSVGOnTop).toHaveBeenCalledTimes(1 + Direction.N_DIRECTIONS);
    });

    it('removeFromDrawing should call removeSVG 1 + (number of point) times and set isDrawn to false', () => {
        selectorRectangle.isDrawn = true;
        selectorRectangle.removeFromDrawing();
        expect(svgServiceSpy.removeSVG).toHaveBeenCalledTimes(1 + Direction.N_DIRECTIONS);
        expect(selectorRectangle.isDrawn).toBeFalsy();
    });
    it('addShapeToSelection should change the rectangle to fit all supplied graphic elements', () => {
        const graphicsArray = new Array<SVGGraphicsElement>();
        const rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect') as SVGRectElement;
        rect.setAttribute('x', '5');
        rect.setAttribute('y', '5');
        rect.setAttribute('width', '2');
        rect.setAttribute('height', '3');
        graphicsArray.push(rect);
        const previousSelectionWidth = selectorRectangle['width'];
        selectorRectangle.addShapesToSelection(graphicsArray);
        expect(selectorRectangle['width']).not.toEqual(previousSelectionWidth);
    });
    it('addShapeToSelection should not display and keeps isDrawn to false if there are no elements in the array', () => {
        const graphicsArray = new Array<SVGGraphicsElement>();
        selectorRectangle.isDrawn = false;
        selectorRectangle.addShapesToSelection(graphicsArray);
        expect(selectorRectangle.isDrawn).toBe(false);
    });
    it('addShapeToSelection takes into account if the sidenav is open', () => {
        const graphicsArray = new Array<SVGGraphicsElement>();
        const rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect') as SVGRectElement;
        rect.setAttribute('x', '5');
        rect.setAttribute('y', '5');
        rect.setAttribute('width', '2');
        rect.setAttribute('height', '3');
        graphicsArray.push(rect);
        selectorRectangle.isDrawn = false;
        drawViewServiceSpy.navBarOpened = false;
        selectorRectangle.addShapesToSelection(graphicsArray);
        drawViewServiceSpy.navBarOpened = true;
        selectorRectangle.addShapesToSelection(graphicsArray);
        expect(selectorRectangle.isDrawn).toBe(true);
    });

    // getCenterOfRectangle
    it('getCenterOfRectangle should return  {x: this.position.x + (this.width / 2), y: this.position.y + (this.height / 2)}', () => {
        selectorRectangle.position.x = 5;
        selectorRectangle.position.y = 5;
        selectorRectangle['width'] = 2;
        selectorRectangle['height'] = 2;
        const position = selectorRectangle.getCenterOfRectangle();
        expect(position.x).toEqual(6);
        expect(position.y).toEqual(6);
    });
});
