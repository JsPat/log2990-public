import { Component } from '@angular/core';
import { SelectorService } from 'src/app/services/attributes/selector/selector.service';

@Component({
  selector: 'app-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.scss']
})
export class SelectorComponent {

  protected service: SelectorService;

  constructor(service: SelectorService) {
    this.service = service;
  }

  protected delete(): void {
    this.service.deleteSelection();
  }

  protected copy(): void {
    this.service.copySelection();
  }

  protected duplicate(): void {
    this.service.duplicateSelection();
  }

  protected cut(): void {
    this.service.cutSelection();
  }

  protected paste(): void {
    this.service.pasteClipboard();
  }

  protected selectAll(): void {
    this.service.selectAll();
  }
}
