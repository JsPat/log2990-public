/* tslint:disable:no-string-literal */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectorService } from '../../../../services/attributes/selector/selector.service';
import { SelectorComponent } from './selector.component';

describe('SelectorComponent', () => {
  let component: SelectorComponent;
  let serviceSpy: SelectorService;
  let fixture: ComponentFixture<SelectorComponent>;

  beforeEach(async(() => {
    serviceSpy = jasmine.createSpyObj('SelectorService',
                                      ['deleteSelection',
                                      'copySelection',
                                      'duplicateSelection',
                                      'cutSelection',
                                      'pasteClipboard',
                                      'selectAll']);
    TestBed.configureTestingModule({
      providers: [
        {provide: SelectorService, useValue: serviceSpy},
      ],
      declarations: [ SelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have the right service for his attribute', () => {
    expect(component['service']).toEqual(serviceSpy);
  });
  it('delete should call the service delete function', () => {
    component['delete']();
    expect(serviceSpy.deleteSelection).toHaveBeenCalled();
  });
  it('selectAll should call the service selectAll function', () => {
    component['selectAll']();
    expect(serviceSpy.selectAll).toHaveBeenCalled();
  });
  it('duplicate should call the service duplicate function', () => {
    component['duplicate']();
    expect(serviceSpy.duplicateSelection).toHaveBeenCalled();
  });
  it('cut should call the service cut function', () => {
    component['cut']();
    expect(serviceSpy.cutSelection).toHaveBeenCalled();
  });
  it('paste should call the service paste function', () => {
    component['paste']();
    expect(serviceSpy.pasteClipboard).toHaveBeenCalled();
  });
  it('copy should call the service copy function', () => {
    component['copy']();
    expect(serviceSpy.copySelection).toHaveBeenCalled();
  });
});
