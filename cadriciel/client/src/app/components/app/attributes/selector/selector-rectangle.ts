import { Renderer2 } from '@angular/core';
import { LIGHT_BLUE, WHITE } from 'src/app/color-palette/constant';
import { SelectorSharedDataService } from 'src/app/services/attributes/selector/selector-shared-data.service';
import { Direction } from 'src/app/services/constants';
import { DrawViewService } from 'src/app/services/draw-view/draw-view.service';
import { SVGChildService } from 'src/app/services/svg-child/svg-child.service';
import { Coordinate } from '../coordinate';
import { SVGHolder } from '../svg-holder';
import * as JSON from './selector-rectangle.json';

const config = JSON.default;

export class SelectorRectangle extends SVGHolder {

    private readonly CONTROL_POINT_DIAMETER: number = 5;

    position: Coordinate = {x: Infinity, y: Infinity};
    isDrawn: boolean = false;

    private height: number;
    private width: number;
    private svgControlPoints: SVGGraphicsElement[] = new Array<SVGRectElement>();

    constructor(private sharedData: SelectorSharedDataService,
                private renderer: Renderer2,
                private drawView: DrawViewService,
                private svgService: SVGChildService) {
        super();
        this.svg = this.renderer.createElement(config.attribute.rect, config.attribute.svg);
        for (let i = 0; i < Direction.N_DIRECTIONS; i++) {
            this.svgControlPoints[i] = this.renderer.createElement(config.attribute.rect, config.attribute.svg);
        }
    }

    addShapesToSelection(el: SVGGraphicsElement[]): void {
        if (el.length !== 0) {
            let maxX = -Infinity;
            let maxY = -Infinity;
            this.position = {x: Infinity, y: Infinity};

            el.forEach( (elem) => {
                const rect = elem.getBoundingClientRect();
                const left = rect.left + window.scrollX;
                const top = rect.top + window.scrollY;
                const right = rect.right + window.scrollX;
                const bottom = rect.bottom + window.scrollY;
                const navBarOffset = (this.drawView.navBarState) ? this.drawView.TOOLS_MENU_SIZE : 0;
                maxX = Math.max(maxX, right - this.drawView.SIDE_NAV - navBarOffset);
                maxY = Math.max(maxY, bottom);

                this.position.x = Math.min(this.position.x, left - this.drawView.SIDE_NAV - navBarOffset);
                this.position.y = Math.min(this.position.y, top);
            });

            // If the computed width/height of the rectangle would be less than 5px, we force default to 5px
            this.width = Math.max( this.CONTROL_POINT_DIAMETER, maxX - this.position.x);
            this.height = Math.max( this.CONTROL_POINT_DIAMETER, maxY - this.position.y);
            this.isDrawn = true;
            this.drawShape();
        }
    }

    isWithinArea(event: MouseEvent): boolean {
        if (this.isDrawn) {
            const point = {x: event.offsetX, y: event.offsetY};

            return (point.x > this.position.x &&
                    point.x < this.position.x + this.width &&
                    point.y > this.position.y &&
                    point.y < this.position.y + this.height);
        } else {
            return false;
        }
    }

    isAControlPoint(target: EventTarget | null): boolean {
        if (target instanceof SVGRectElement) {
            return this.svgControlPoints.includes(target);
        } else {
            return false;
        }
    }

    redraw(shapes?: SVGGraphicsElement[]): void {
        this.removeFromDrawing();
        this.addShapesToSelection((shapes !== undefined) ? shapes : this.sharedData.selectedShapes);
        if (this.isDrawn) {
            this.svgService.appendSVGOnTop(this.svg);
            this.svgControlPoints.forEach((point) => {
                this.svgService.appendSVGOnTop(point);
            });
        }
    }

    removeFromDrawing(): void {
        this.svgService.removeSVG(this.svg);
        this.svgControlPoints.forEach((elem) => {
            this.svgService.removeSVG(elem);
        });
        this.isDrawn = false;
    }

    getCenterOfRectangle(): Coordinate {
        return {x: this.position.x + (this.width / 2), y: this.position.y + (this.height / 2)};
    }

    private drawShape(): void {
        this.renderer.setAttribute(this.svg, config.attribute.id, config.value.selectionRectangle);
        this.renderer.setAttribute(this.svg, config.attribute.x, `${this.position.x}`);
        this.renderer.setAttribute(this.svg, config.attribute.y, `${this.position.y}`);
        this.renderer.setAttribute(this.svg, config.attribute.height, `${this.height}`);
        this.renderer.setAttribute(this.svg, config.attribute.width, `${this.width}`);
        this.renderer.setAttribute(this.svg, config.attribute.dataIsSelectable, config.value.false);
        this.renderer.setAttribute(this.svg, config.attribute.fill, config.value.none);
        this.renderer.setAttribute(this.svg, config.attribute.stroke, `#${LIGHT_BLUE.rgb_hexa}`);
        this.renderer.setAttribute(this.svg, config.attribute.strokeWidth, config.value.one);
        this.drawControlPoints();
    }

    private drawControlPoints(): void {
        for (let i = 0; i < Direction.N_DIRECTIONS; i++) {
            // Common attributes
            this.renderer.setAttribute(this.svgControlPoints[i], config.attribute.height, `${this.CONTROL_POINT_DIAMETER}`);
            this.renderer.setAttribute(this.svgControlPoints[i], config.attribute.width, `${this.CONTROL_POINT_DIAMETER}`);
            this.renderer.setAttribute(this.svgControlPoints[i], config.attribute.dataIsSelectable, config.value.false);
            this.renderer.setAttribute(this.svgControlPoints[i], config.attribute.fill, `#${WHITE.rgb_hexa}`);
            this.renderer.setAttribute(this.svgControlPoints[i], config.attribute.stroke, `#${LIGHT_BLUE.rgb_hexa}`);
            this.renderer.setAttribute(this.svgControlPoints[i], config.attribute.strokeWidth, config.value.one);
            // Attributes depending on direction
            let offSetX = 0;
            let offSetY = 0;
            switch (i) {
                case Direction.UP:      offSetX = this.width / 2;
                                        break;
                case Direction.RIGHT:   offSetX = this.width;
                                        offSetY = this.height / 2;
                                        break;
                case Direction.DOWN:    offSetX = this.width / 2;
                                        offSetY = this.height;
                                        break;
                case Direction.LEFT:    offSetY = this.height / 2;
                                        break;
            }
            this.renderer.setAttribute(this.svgControlPoints[i], config.attribute.x,
                `${this.position.x + offSetX - this.CONTROL_POINT_DIAMETER / 2}`);
            this.renderer.setAttribute(this.svgControlPoints[i], config.attribute.y,
                `${this.position.y + offSetY - this.CONTROL_POINT_DIAMETER / 2}`);
        }
    }
}
