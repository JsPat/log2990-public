import { Renderer2 } from '@angular/core';
import { SelectorSharedDataService } from 'src/app/services/attributes/selector/selector-shared-data.service';
import { DrawViewService } from 'src/app/services/draw-view/draw-view.service';
import { ShapesHolderService } from 'src/app/services/shapes-holder/shapes-holder.service';
import { SVGChildService } from 'src/app/services/svg-child/svg-child.service';
import { Coordinate } from '../coordinate';
import { SVGHolder } from '../svg-holder';
import * as JSON from './selector-area.json';

const config = JSON.default;

export class SelectionAreaRectangle extends SVGHolder {
    private topLeftCorner: Coordinate;
    private bottomRightCorner: Coordinate;
    intersectingElements: SVGGraphicsElement[] = new Array<SVGGraphicsElement>();
    isDrawn: boolean = false;

    constructor(private sharedData: SelectorSharedDataService,
                private renderer: Renderer2,
                private shapeHolder: ShapesHolderService,
                private drawViewService: DrawViewService,
                private svgService: SVGChildService) {
        super();
        this.svg = this.renderer.createElement(config.attribute.rect, config.attribute.svg);
    }

    drawShape(corner1: Coordinate, corner2: Coordinate): void {
        this.isDrawn = true;
        this.setCornerCoordinates(corner1, corner2);
        this.renderer.setAttribute(this.svg, config.attribute.x, `${this.topLeftCorner.x}`);
        this.renderer.setAttribute(this.svg, config.attribute.y, `${this.topLeftCorner.y}`);
        this.renderer.setAttribute(this.svg, config.attribute.height, `${this.bottomRightCorner.y - this.topLeftCorner.y}`);
        this.renderer.setAttribute(this.svg, config.attribute.width, `${this.bottomRightCorner.x - this.topLeftCorner.x}`);
        this.renderer.setAttribute(this.svg, config.attribute.fill, config.value.none);
        this.renderer.setAttribute(this.svg, config.attribute.stroke, config.value.color);
        this.renderer.setAttribute(this.svg, config.attribute.strokeWidth, config.value.strokeWidthValue);
        this.renderer.setAttribute(this.svg, config.attribute.strokeDasharray, config.value.strokeDasharrayValue);
        this.renderer.setAttribute(this.svg, config.attribute.shouldSave, config.value.false);
        this.findIntersectingElements();
    }

    removeArea(): void {
        this.svgService.removeSVG(this.svg);
        this.isDrawn = false;
    }

    redrawSelectionArea(): void {
        this.removeArea();
        this.svgService.appendNewSVG(this.svg);
        this.isDrawn = true;
    }

    invertedAreaSelection(): SVGGraphicsElement[] {
        const selection = this.sharedData.selectedShapes.slice();
        this.intersectingElements.forEach((elem) => {
            (selection.includes(elem)) ? selection.splice(selection.indexOf(elem), 1) :
                                        selection.push(elem);
        });
        return selection;
    }

    private findIntersectingElements(): void {
        const shapeIterator = this.shapeHolder.iterator;
        let elem = shapeIterator.next();
        this.intersectingElements = new Array<SVGGraphicsElement>();
        while (!elem.done) {
            if (this.isWithinSelection(elem.value.getBoundingClientRect() as DOMRect)) {
                this.intersectingElements.push(elem.value);
            }
            elem = shapeIterator.next();
        }
    }

    private setCornerCoordinates(corner1: Coordinate, corner2: Coordinate): void {
        this.topLeftCorner = {x: Math.min(corner1.x, corner2.x), y: Math.min(corner1.y, corner2.y)};
        this.bottomRightCorner = {x: Math.max(corner1.x, corner2.x), y: Math.max(corner1.y, corner2.y)};
    }

    private isWithinSelection(boundingBox: DOMRect): boolean {
        const topLeftBoundingBox = this.drawViewService.alignCoordinateToDrawView({x : boundingBox.left, y : boundingBox.top});
        const bottomRightBoundingBox = this.drawViewService.alignCoordinateToDrawView({x : boundingBox.right, y : boundingBox.bottom});
        return ((bottomRightBoundingBox.x >= this.topLeftCorner.x && bottomRightBoundingBox.y >= this.topLeftCorner.y) &&
                (topLeftBoundingBox.x <= this.bottomRightCorner.x && topLeftBoundingBox.y <= this.bottomRightCorner.y));
    }
}
