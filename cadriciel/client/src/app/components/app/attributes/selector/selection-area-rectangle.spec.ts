/* tslint:disable:no-unused-variable */
/* tslint:disable:no-any */
/* tslint:disable:no-string-literal */
/* tslint:disable:no-magic-numbers */

import { Renderer2 } from '@angular/core';
import { async, TestBed } from '@angular/core/testing';
import { SelectorSharedDataService } from 'src/app/services/attributes/selector/selector-shared-data.service';
import { DrawViewService } from 'src/app/services/draw-view/draw-view.service';
import { ShapesHolderService } from 'src/app/services/shapes-holder/shapes-holder.service';
import { Coordinate } from '../coordinate';
import { SelectionAreaRectangle } from './selection-area-rectangle';

describe('Class: SelectionAreaRectangle', () => {
    let rectangle: SelectionAreaRectangle;
    let sharedData = new SelectorSharedDataService();
    let rendererSpy: any;
    let shapeHolder = new ShapesHolderService();
    let drawView = new DrawViewService();
    let svgChildSpy: any;
    let element: SVGRectElement;
    beforeEach(async(() => {
        svgChildSpy = jasmine.createSpyObj('SVGChildService', ['removeSVG', 'appendNewSVG']);
        TestBed.configureTestingModule({
            providers: [Renderer2,
                        Coordinate,
                        SelectionAreaRectangle]
        });
    }));
    beforeEach( () => {
        sharedData = new SelectorSharedDataService();
        shapeHolder = new ShapesHolderService();
        drawView = new DrawViewService();
        element = document.createElementNS('http://www.w3.org/2000/svg', 'rect') as SVGRectElement;
        rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute']);
        rectangle = new SelectionAreaRectangle(sharedData, rendererSpy, shapeHolder, drawView, svgChildSpy);
    });
    it('constructor should generate the SVGRectElement used to display the area rectangle', () => {
        expect(rendererSpy.createElement).toHaveBeenCalled();
    });
    it('removeArea should set isDrawn to false', () => {
        rectangle.removeArea();
        expect(rectangle.isDrawn).toBe(false);
    });
    it('redrawSelectionArea should call removeArea, append using svgChildService and set isDrawn to true', () => {
        spyOn(rectangle, 'removeArea');
        rectangle.redrawSelectionArea();
        expect(rectangle.removeArea).toHaveBeenCalled();
        expect(svgChildSpy.appendNewSVG).toHaveBeenCalled();
        expect(rectangle.isDrawn).toBe(true);
    });
    it('drawShape should set all relevant attributes for the shape, using the provided points', () => {
        const mockPoint1 = {x: 0, y: 0};
        const mockPoint2 = {x: 5, y: 7};
        rectangle.drawShape(mockPoint1, mockPoint2);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(9);
    });
    it('invertedAreaSelection returns the elements that should be selected during an inverseSelect', () => {
        sharedData.selectedShapes.push(element);
        rectangle.intersectingElements.push(element);
        const list = rectangle.invertedAreaSelection();
        expect(list.length).toEqual(0);
    });
    it('invertedAreaSelection should add an element to the array if it is not selected', () => {
        rectangle.intersectingElements.push(element);
        const list = rectangle.invertedAreaSelection();
        expect(list.length).toEqual(1);
    });
    it('findIntersectingElements adds all items that intersect with the area into the intersectingElements array', () => {
        const secondElement = document.createElementNS('http://www.w3.org/2000/svg', 'rect') as SVGRectElement;
        const spy1 = spyOn<any>(rectangle, 'isWithinSelection').and.returnValue(true);
        shapeHolder.addShape(element);
        shapeHolder.addShape(secondElement);
        rectangle['findIntersectingElements']();
        expect(spy1).toHaveBeenCalled();
        expect(rectangle.intersectingElements.length).toEqual(2);
    });
    it('findIntersectingElements does not add an element if it does not intersect with the area', () => {
        const secondElement = document.createElementNS('http://www.w3.org/2000/svg', 'rect') as SVGRectElement;
        const spy1 = spyOn<any>(rectangle, 'isWithinSelection').and.returnValue(false);
        shapeHolder.addShape(element);
        shapeHolder.addShape(secondElement);
        rectangle['findIntersectingElements']();
        expect(spy1).toHaveBeenCalled();
        expect(rectangle.intersectingElements.length).toEqual(0);
    });

    // isWithinSelection
    it('isWithinSelection should call alignCoordinateToDrawView 2 times', () => {
        spyOn(rectangle['drawViewService'], 'alignCoordinateToDrawView').and.returnValue({x: 5, y: 5});
        const domRect = new DOMRect(5, 5, 5, 5);
        rectangle['bottomRightCorner'] = {x: 5, y: 5};
        rectangle['topLeftCorner'] = {x: 5, y: 5};
        rectangle['isWithinSelection'](domRect);
        expect(rectangle['drawViewService'].alignCoordinateToDrawView).toHaveBeenCalledTimes(2);
    });

    // tslint:disable-next-line: max-line-length
    it('isWithinSelection should return true, if bottomRightBoundingBox is bigger then topLeftCorner and topLeftBoundingBox is smaller then the bottom right', () => {
        spyOn(rectangle['drawViewService'], 'alignCoordinateToDrawView').and.returnValue({x: 5, y: 5});
        const domRect = new DOMRect(5, 5, 5, 5);
        rectangle['bottomRightCorner'] = {x: 5, y: 5};
        rectangle['topLeftCorner'] = {x: 5, y: 5};
        const rect = rectangle['isWithinSelection'](domRect);
        expect(rect).toBeTruthy();
    });
});
