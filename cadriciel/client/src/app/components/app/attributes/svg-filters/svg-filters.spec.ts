import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { SVGFilters } from './svg-filters';

describe('svgFilters class', () => {
    let svgFilters: SVGFilters;
    let rendererSpy: Renderer2;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            providers: [ Renderer2 ]
        }).compileComponents();
    });
    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute', 'appendChild']);
        svgFilters = new SVGFilters(rendererSpy);
    });

    it('get filters() should return filtersDefinitions', () => {
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters.filters).toEqual(svgFilters['filtersDefinitions']);
    });

    it('createFilters() should call createElement() of renderer', () => {
        // tslint:disable-next-line: no-string-literal
        svgFilters['createFilters']();
        expect(rendererSpy.createElement).toHaveBeenCalled();
    });
    it('createFilters() should call all filters creations', () => {
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'createFilters').and.callThrough();
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'createFilterSapeli');
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'createFilterAmboine');
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'createFilterThuja');
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'createFilterEbony');
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'createFilterOak');
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'createFilterLuminanceToAlpha');
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'createFilterHueRotate');
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'createFilterSaturate');
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'createFilterFiftyShadesOfGrey');
        // tslint:disable-next-line: no-any because unknow type;
        spyOn<any>(svgFilters, 'createFilterSepia');
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'createFilterReverse');
        // tslint:disable-next-line: no-string-literal
        svgFilters['createFilters']();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['createFilterSapeli']).toHaveBeenCalled();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['createFilterAmboine']).toHaveBeenCalled();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['createFilterThuja']).toHaveBeenCalled();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['createFilterEbony']).toHaveBeenCalled();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['createFilterOak']).toHaveBeenCalled();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['createFilterLuminanceToAlpha']).toHaveBeenCalled();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['createFilterHueRotate']).toHaveBeenCalled();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['createFilterSaturate']).toHaveBeenCalled();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['createFilterFiftyShadesOfGrey']).toHaveBeenCalled();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['createFilterSepia']).toHaveBeenCalled();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['createFilterReverse']).toHaveBeenCalled();
    });
    it('createFilter() should call render.createElement() if the id is valid', () => {
        // tslint:disable-next-line: no-string-literal
        svgFilters['createFilter']('@sapeli');
        expect(rendererSpy.createElement).toHaveBeenCalled();
    });
    it('createFilter() should return undefined if the id is not valid', () => {
        // tslint:disable-next-line: no-string-literal
        const returnValue = svgFilters['createFilter']('not a valid id');
        expect(returnValue).toBeUndefined();
    });
    it ('createFilterSapeli() should call effectGaussianBlur()', () => {
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'createFilterSapeli').and.callThrough();
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'effectGaussianBlur');
        // tslint:disable-next-line: no-string-literal
        svgFilters['createFilterSapeli']();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['effectGaussianBlur']).toHaveBeenCalled();
    });
    it ('createFilterAmboine() should call effectTurbulence() and effectDisplacementMap()', () => {
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'createFilterAmboine').and.callThrough();
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'effectTurbulence');
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'effectDisplacementMap');
        // tslint:disable-next-line: no-string-literal
        svgFilters['createFilterAmboine']();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['effectTurbulence']).toHaveBeenCalled();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['effectDisplacementMap']).toHaveBeenCalled();
    });
    it ('createFilterThuja() should call effectTurbulence() and effectDisplacementMap()', () => {
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'createFilterThuja').and.callThrough();
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'effectTurbulence');
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'effectDisplacementMap');
        // tslint:disable-next-line: no-string-literal
        svgFilters['createFilterThuja']();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['effectTurbulence']).toHaveBeenCalled();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['effectDisplacementMap']).toHaveBeenCalled();
    });
    it ('createFilterEbony() should call effectMorphology(), effectOffset(), effectComposite() and effectGaussianBlur()', () => {
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'createFilterEbony').and.callThrough();
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'effectMorphology');
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'effectOffset');
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'effectComposite');
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'effectGaussianBlur');
        // tslint:disable-next-line: no-string-literal
        svgFilters['createFilterEbony']();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['effectMorphology']).toHaveBeenCalled();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['effectOffset']).toHaveBeenCalled();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['effectComposite']).toHaveBeenCalled();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['effectGaussianBlur']).toHaveBeenCalled();
    });
    it ('createFilterOak() should call effectTurbulence(), effectDisplacementMap() and effectGaussianBlur()', () => {
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'createFilterOak').and.callThrough();
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'effectTurbulence');
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'effectDisplacementMap');
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'effectGaussianBlur');
        // tslint:disable-next-line: no-string-literal
        svgFilters['createFilterOak']();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['effectTurbulence']).toHaveBeenCalled();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['effectDisplacementMap']).toHaveBeenCalled();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['effectGaussianBlur']).toHaveBeenCalled();
    });

    it ('createFilterLuminanceToAlpha() should call effectColorMatrix()', () => {
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'createFilterLuminanceToAlpha').and.callThrough();
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'effectColorMatrix');
        // tslint:disable-next-line: no-string-literal
        svgFilters['createFilterLuminanceToAlpha']();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['effectColorMatrix']).toHaveBeenCalled();
    });
    it ('createFilterHueRotate() should call effectColorMatrix()', () => {
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'createFilterHueRotate').and.callThrough();
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'effectColorMatrix');
        // tslint:disable-next-line: no-string-literal
        svgFilters['createFilterHueRotate']();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['effectColorMatrix']).toHaveBeenCalled();
    });
    it ('createFilterSaturate() should call effectColorMatrix()', () => {
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'createFilterSaturate').and.callThrough();
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'effectColorMatrix');
        // tslint:disable-next-line: no-string-literal
        svgFilters['createFilterSaturate']();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['effectColorMatrix']).toHaveBeenCalled();
    });
    it ('createFilterFiftyShadesOfGrey() should call effectColorMatrix()', () => {
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'createFilterFiftyShadesOfGrey').and.callThrough();
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'effectColorMatrix');
        // tslint:disable-next-line: no-string-literal
        svgFilters['createFilterFiftyShadesOfGrey']();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['effectColorMatrix']).toHaveBeenCalled();
    });
    it ('createFilterSepia() should call effectColorMatrix()', () => {
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'createFilterSepia').and.callThrough();
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'effectColorMatrix');
        // tslint:disable-next-line: no-string-literal
        svgFilters['createFilterSepia']();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['effectColorMatrix']).toHaveBeenCalled();
    });
    it ('createFilterReverse() should call effectColorMatrix()', () => {
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'createFilterReverse').and.callThrough();
        // tslint:disable-next-line: no-any because unknow type
        spyOn<any>(svgFilters, 'effectColorMatrix');
        // tslint:disable-next-line: no-string-literal
        svgFilters['createFilterReverse']();
        // tslint:disable-next-line: no-string-literal
        expect(svgFilters['effectColorMatrix']).toHaveBeenCalled();
    });
});
