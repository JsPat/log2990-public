import { Renderer2 } from '@angular/core';
import * as JSON from './filters-effects-values.json';

enum Filters {
    SAPELI,
    AMBOINE,
    THUJA,
    EBONY,
    OAK,
    LUMINANCE_TO_ALPHA,
    HUE_ROTATE,
    SATURATE,
    FIFTY_SHADES_OF_GREY,
    SEPIA,
    REVERSE,
    NORMAL,
    NB_FILTERS
}

const config = JSON.default;

export class SVGFilters {

    private readonly filterIDs: string[] = new Array<string>(Filters.NB_FILTERS);
    private filtersDefinitions: SVGDefsElement;

    constructor(private renderer: Renderer2) {
        this.filterIDs[Filters.SAPELI] = config.sapeli.name;
        this.filterIDs[Filters.AMBOINE] = config.amboine.name;
        this.filterIDs[Filters.THUJA] = config.thuja.name;
        this.filterIDs[Filters.EBONY] = config.ebony.name;
        this.filterIDs[Filters.OAK] = config.oak.name;
        this.filterIDs[Filters.LUMINANCE_TO_ALPHA] = config.luminanceToAlpha.name;
        this.filterIDs[Filters.HUE_ROTATE] = config.hueRotate.name;
        this.filterIDs[Filters.SATURATE] = config.saturate.name;
        this.filterIDs[Filters.FIFTY_SHADES_OF_GREY] = config.fiftyShadesOfGrey.name;
        this.filterIDs[Filters.SEPIA] = config.sepia.name;
        this.filterIDs[Filters.REVERSE] = config.reverse.name;
        this.filterIDs[Filters.NORMAL] = config.normal.name;
        this.createFilters();
    }

    get filters(): SVGDefsElement {
        return this.filtersDefinitions;
    }

    private createFilters(): void {
        this.filtersDefinitions =  this.renderer.createElement(config.defs, config.svg);
        this.createFilterSapeli();
        this.createFilterAmboine();
        this.createFilterThuja();
        this.createFilterEbony();
        this.createFilterOak();

        this.createFilterLuminanceToAlpha();
        this.createFilterHueRotate();
        this.createFilterSaturate();
        this.createFilterFiftyShadesOfGrey();
        this.createFilterSepia();
        this.createFilterReverse();
        this.createFilterNoChanges();
    }

    private createFilter(id: string): SVGFilters {
        const svgFilter = this.renderer.createElement(config.filter, config.svg);
        this.renderer.setAttribute(svgFilter, config.attributes.id, `${id}`);
        this.renderer.setAttribute(svgFilter, config.attributes.filterUnits, config.attributes.userSpaceOnUse);
        this.renderer.appendChild(this.filtersDefinitions, svgFilter);
        return svgFilter;
    }

    private effectGaussianBlur(svgFilter: SVGFilters, stdDeviation: SVGAnimatedNumber): void {
        const feGaussianBlur = this.renderer.createElement(config.feGaussianBlur.name, config.svg);
        this.renderer.setAttribute(feGaussianBlur, config.feGaussianBlur.stdDeviation, `${stdDeviation}`);
        this.renderer.appendChild(svgFilter, feGaussianBlur);
    }

    private effectTurbulence(svgFilter: SVGFilters, baseFrequency: SVGAnimatedNumber,
                             numOctaves: SVGAnimatedNumber, seed?: SVGAnimatedNumber, type?: SVGAnimatedEnumeration): void {
        const feTurbulence = this.renderer.createElement(config.feTurbulence.name, config.svg);
        this.renderer.setAttribute(feTurbulence, config.feTurbulence.baseFrequency, `${baseFrequency}`);
        this.renderer.setAttribute(feTurbulence, config.feTurbulence.numOctaves, `${numOctaves}`);
        if (seed !== undefined) {
            this.renderer.setAttribute(feTurbulence, config.feTurbulence.seed, `${seed}`);
        }
        if (type !== undefined) {
            this.renderer.setAttribute(feTurbulence, config.feTurbulence.type, `${type}`);
        }
        this.renderer.setAttribute(feTurbulence, config.feTurbulence.result, config.feTurbulence.turbulence);
        this.renderer.appendChild(svgFilter, feTurbulence);
    }

    private effectDisplacementMap(  svgFilter: SVGFilters,
                                    inEffect1: SVGAnimatedString,
                                    inEffect2: SVGAnimatedString,
                                    scale: SVGAnimatedNumber,
                                    xChannelSelector?: SVGAnimatedEnumeration,
                                    yChannelSelector?: SVGAnimatedEnumeration): void {
        const feDisplacementMap = this.renderer.createElement(config.feDisplacementMap.name, config.svg);
        this.renderer.setAttribute(feDisplacementMap, config.feDisplacementMap.in, `${inEffect1}`);
        this.renderer.setAttribute(feDisplacementMap, config.feDisplacementMap.in2, `${inEffect2}`);
        this.renderer.setAttribute(feDisplacementMap, config.feDisplacementMap.scale, `${scale}`);
        this.renderer.appendChild(svgFilter, feDisplacementMap);
        if (xChannelSelector !== undefined) {
            this.renderer.setAttribute(feDisplacementMap, config.feDisplacementMap.xChannelSelector, `${xChannelSelector}`);
        }
        if (yChannelSelector !== undefined) {
            this.renderer.setAttribute(feDisplacementMap, config.feDisplacementMap.yChannelSelector, `${yChannelSelector}`);
        }
    }

    private effectMorphology(svgFilter: SVGFilters, operator: SVGAnimatedEnumeration,
                             radius: SVGAnimatedNumber, inEffect1: SVGAnimatedString): void {
        const feMorphology = this.renderer.createElement(config.feMorphology.name, config.svg);
        this.renderer.setAttribute(feMorphology, config.feMorphology.operator, `${operator}`);
        this.renderer.setAttribute(feMorphology, config.feMorphology.radius, `${radius}`);
        this.renderer.setAttribute(feMorphology, config.feMorphology.in, `${inEffect1}`);
        this.renderer.setAttribute(feMorphology, config.feMorphology.result, config.feMorphology.morphology);
        this.renderer.appendChild(svgFilter, feMorphology);

    }

    private effectOffset(svgFilter: SVGFilters, dx: SVGAnimatedNumber, dy: SVGAnimatedNumber): void {
        const feOffset = this.renderer.createElement(config.feOffset.name, config.svg);
        this.renderer.setAttribute(feOffset, config.feOffset.dx, `${dx}`);
        this.renderer.setAttribute(feOffset, config.feOffset.dy, `${dy}`);
        this.renderer.setAttribute(feOffset, config.feOffset.result, config.feOffset.offset);
        this.renderer.appendChild(svgFilter, feOffset);
    }

    private effectComposite(svgFilter: SVGFilters, operator: SVGAnimatedEnumeration,
                            inEffect1: SVGAnimatedString, inEffect2: SVGAnimatedString): void {
        const feComposite = this.renderer.createElement(config.feComposite.name, config.svg);
        this.renderer.setAttribute(feComposite, config.feComposite.operator, `${operator}`);
        this.renderer.setAttribute(feComposite, config.feComposite.in, `${inEffect1}`);
        this.renderer.setAttribute(feComposite, config.feComposite.in2, `${inEffect2}`);
        this.renderer.setAttribute(feComposite, config.feComposite.result, config.feComposite.composite);
        this.renderer.appendChild(svgFilter, feComposite);
    }

    private effectColorMatrix(svgFilter: SVGFilters, inEffect1: SVGAnimatedString, type: SVGAnimatedEnumeration,
                              values?: SVGAnimatedNumberList): void {
        const feColorMatrix = this.renderer.createElement(config.feColorMatrix.name, config.svg);
        this.renderer.setAttribute(feColorMatrix, config.feColorMatrix.in, `${inEffect1}`);
        this.renderer.setAttribute(feColorMatrix, config.feColorMatrix.type, `${type}`);
        if (values !== undefined) {
            this.renderer.setAttribute(feColorMatrix, config.feColorMatrix.values, `${values}`);
        }
        this.renderer.appendChild(svgFilter, feColorMatrix);
    }

    private createFilterSapeli(): void {
        const svgFilter = this.createFilter(this.filterIDs[Filters.SAPELI]);
        this.effectGaussianBlur(
            svgFilter,
            config.sapeli.effectGaussianBlur.stdDeviationX
        );
    }

    private createFilterAmboine(): void {
        const svgFilter = this.createFilter(this.filterIDs[Filters.AMBOINE]);
        const effectTurbulenceAmboineValues = config.amboine.effectTurbulence;
        const effectDisplacementMapAmboineValues = config.amboine.effectDisplacementMap;
        this.effectTurbulence(
            svgFilter,
            effectTurbulenceAmboineValues.baseFrequencyX,
            effectTurbulenceAmboineValues.numOctaves,
            effectTurbulenceAmboineValues.seed,
            effectTurbulenceAmboineValues.type
        );
        this.effectDisplacementMap(
            svgFilter,
            effectDisplacementMapAmboineValues.in1,
            effectDisplacementMapAmboineValues.in2,
            effectDisplacementMapAmboineValues.scale
        );
    }

    private createFilterThuja(): void {
        const svgFilter = this.createFilter(this.filterIDs[Filters.THUJA]);
        const effectTurbulenceThujaValues = config.thuja.effectTurbulence;
        const effectDisplacementMapThujaValues = config.thuja.effectDisplacementMap;
        this.effectTurbulence(
            svgFilter,
            effectTurbulenceThujaValues.baseFrequencyX,
            effectTurbulenceThujaValues.numOctaves,
            effectTurbulenceThujaValues.seed,
            effectTurbulenceThujaValues.type
        );
        this.effectDisplacementMap(
            svgFilter,
            effectDisplacementMapThujaValues.in1,
            effectDisplacementMapThujaValues.in2,
            effectDisplacementMapThujaValues.scale
        );
    }

    private createFilterEbony(): void {
        const svgFilter = this.createFilter(this.filterIDs[Filters.EBONY]);
        const effectMorphologyEbeneValues = config.ebony.effectMorphology;
        const effectOffsetEbeneValues = config.ebony.effectOffset;
        const effectCompositeEbeneValues = config.ebony.effectComposite;
        this.effectMorphology(
            svgFilter,
            effectMorphologyEbeneValues.operator,
            effectMorphologyEbeneValues.radiusX,
            effectMorphologyEbeneValues.in1
        );
        this.effectOffset(
            svgFilter,
            effectOffsetEbeneValues.dy,
            effectOffsetEbeneValues.dy
        );
        this.effectComposite(
            svgFilter,
            effectCompositeEbeneValues.operator,
            effectCompositeEbeneValues.in1,
            effectCompositeEbeneValues.in2
        );
        this.effectGaussianBlur(svgFilter, config.ebony.effectGaussianBlur.stdDeviationX);
    }

    private createFilterOak(): void {
        const svgFilter = this.createFilter(this.filterIDs[Filters.OAK]);
        const effectTurbulenceOakValues = config.oak.effectTurbulence;
        const effectDisplacementMapOakValues = config.oak.effectDisplacementMap;
        this.effectTurbulence(
            svgFilter,
            effectTurbulenceOakValues.baseFrequencyX,
            effectTurbulenceOakValues.numOctaves,
            effectTurbulenceOakValues.seed,
            effectTurbulenceOakValues.type
        );
        this.effectDisplacementMap(
            svgFilter,
            effectDisplacementMapOakValues.in1,
            effectDisplacementMapOakValues.in2,
            effectDisplacementMapOakValues.scale,
            effectDisplacementMapOakValues.xChannelSelector,
            effectDisplacementMapOakValues.yChannelSelector
        );
        this.effectGaussianBlur(svgFilter, config.oak.effectGaussianBlur.stdDeviationX);
    }

    private createFilterLuminanceToAlpha(): void {
        const svgFilter = this.createFilter(this.filterIDs[Filters.LUMINANCE_TO_ALPHA]);
        const effectColorMatrixValues = config.luminanceToAlpha.colorMatrix;
        this.effectColorMatrix(
            svgFilter,
            effectColorMatrixValues.in1,
            effectColorMatrixValues.type
        );
    }

    private createFilterHueRotate(): void {
        const svgFilter = this.createFilter(this.filterIDs[Filters.HUE_ROTATE]);
        const effectColorMatrixValues = config.hueRotate.colorMatrix;
        this.effectColorMatrix(
            svgFilter,
            effectColorMatrixValues.in1,
            effectColorMatrixValues.type,
            effectColorMatrixValues.values
        );
    }

    private createFilterSaturate(): void {
        const svgFilter = this.createFilter(this.filterIDs[Filters.SATURATE]);
        const effectColorMatrixValues = config.saturate.colorMatrix;
        this.effectColorMatrix(
            svgFilter,
            effectColorMatrixValues.in1,
            effectColorMatrixValues.type,
            effectColorMatrixValues.values
        );
    }

    private createFilterFiftyShadesOfGrey(): void {
        const svgFilter = this.createFilter(this.filterIDs[Filters.FIFTY_SHADES_OF_GREY]);
        const effectColorMatrixValues = config.fiftyShadesOfGrey.colorMatrix;
        this.effectColorMatrix(
            svgFilter,
            effectColorMatrixValues.in1,
            effectColorMatrixValues.type,
            effectColorMatrixValues.values
        );
    }

    private createFilterSepia(): void {
        const svgFilter = this.createFilter(this.filterIDs[Filters.SEPIA]);
        const effectColorMatrixValues = config.sepia.colorMatrix;
        this.effectColorMatrix(
            svgFilter,
            effectColorMatrixValues.in1,
            effectColorMatrixValues.type,
            effectColorMatrixValues.values
        );
    }

    private createFilterReverse(): void {
        const svgFilter = this.createFilter(this.filterIDs[Filters.REVERSE]);
        const effectColorMatrixValues = config.reverse.colorMatrix;
        this.effectColorMatrix(
            svgFilter,
            effectColorMatrixValues.in1,
            effectColorMatrixValues.type,
            effectColorMatrixValues.values
        );
    }

    private createFilterNoChanges(): void {
        const svgFilter = this.createFilter(this.filterIDs[Filters.NORMAL]);
        const effectColorMatrixValues = config.normal.colorMatrix;
        this.effectColorMatrix(
            svgFilter,
            effectColorMatrixValues.in1,
            effectColorMatrixValues.type,
            effectColorMatrixValues.values
        );
    }

}
