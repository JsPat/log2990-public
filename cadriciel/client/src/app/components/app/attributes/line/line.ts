import { Renderer2 } from '@angular/core';
import { Color } from 'src/app/color-palette/color/color';
import { Coordinate } from 'src/app/components/app/attributes/coordinate';
import { SVGHolder } from '../svg-holder';
import * as JSON from './line.json';

const config = JSON.default;

export class Line extends SVGHolder {
    private readonly DEGREES_IN_CIRCLE: number = 360;
    private readonly DEGREES_IN_QUADRANT: number = 90;
    private readonly RAD_TO_DEGREE_CONVERSION_FACTOR: number = (this.DEGREES_IN_QUADRANT * 2) / Math.PI;
    private readonly MAX_DISTANCE_TO_CONNECT: number = 5;

    dots: Coordinate[] = new Array();
    cursorLocation: Coordinate;

    private line: SVGPolylineElement;
    private circles: SVGCircleElement[];

    constructor(public color: Color,
                firstDot: Coordinate,
                public strokeSize: number,
                private renderer: Renderer2,
                private dottedEdge: boolean,
                public dottedEdgeDiameter: number = 0) {
        // To generate the original dot, we push the first position twice
        super();
        this.dots.push(firstDot);
        this.dots.push(firstDot);
        this.cursorLocation = firstDot;
        this.svg = this.renderer.createElement(config.element.group, config.element.from);
        this.line = this.renderer.createElement(config.element.line, config.element.from);
        this.circles = new Array<SVGCircleElement>();
        this.drawShape();
    }

    drawShape(): void {
        this.generateLine();
        this.setGAttributes();
        if (this.dottedEdge) {
            this.dots.forEach( (dot) => {
                this.circles.push(this.generateCircle(dot));
            });
        }
    }

    // Updates the preview line to end at the specified coordinate or to the mouse location if no coordinate is passed
    updatePreviewLine(dot?: Coordinate): void {
        this.dots.pop();
        if (dot) {
            this.dots.push(dot);
        } else {
            this.dots.push(this.cursorLocation);
        }
        if (this.dottedEdge) {
            const circle = this.circles[this.circles.length - 1];
            this.renderer.setAttribute(circle, config.circle.position.x, `${this.dots[this.dots.length - 1].x}`);
            this.renderer.setAttribute(circle, config.circle.position.y, `${this.dots[this.dots.length - 1].y}`);
        }
        this.renderer.setAttribute(this.line, config.points, this.parseDotLocations());
    }

    getLastDrawnDot(): Coordinate {
        return this.dots[this.dots.length - 2];
    }

    getCursorDot(): Coordinate {
        return this.dots[this.dots.length - 1];
    }

    // Called when backspace is pressed during the drawing process
    removePreviousDot(): void {
        // Only removes a dot if there is more than 2 dots in the line
        if (this.dots.length > 2) {
            if (this.dottedEdge) {
                const circleToRemove = this.circles[this.circles.length - 2];
                this.renderer.removeChild(this.svg, circleToRemove);
                this.circles.splice(this.circles.length - 2, 1);
            }
            this.dots.splice(this.dots.length - 2, 1);
            this.renderer.setAttribute(this.line, config.points, this.parseDotLocations());
        }
    }

    addNewDotToLine(dot: Coordinate): void {
        if (this.dottedEdge) {
            this.circles.push(this.generateCircle(dot));
        }
        this.dots.push(dot);
        this.renderer.setAttribute(this.line, config.points, this.parseDotLocations());
    }

    completeLine(): void {
        this.updatePreviewLine(this.dots[0]);
    }

    private roundAngle45(xVariation: number, yVariation: number): number {
        // Value between +- 0 to 180 degrees
        const angleInDeg = Math.atan2(yVariation, xVariation) * this.RAD_TO_DEGREE_CONVERSION_FACTOR;
        // Rounds the number to a multiple of 45
        const roundedAngle = Math.round(angleInDeg / (this.DEGREES_IN_QUADRANT / 2) ) * (this.DEGREES_IN_QUADRANT / 2);
        return ((roundedAngle < 0) ? roundedAngle + this.DEGREES_IN_CIRCLE : roundedAngle) % this.DEGREES_IN_CIRCLE;
    }

    connectsToStartPoint(endCoords: Coordinate): boolean {
        const initCoords = this.dots[0];
        return (Math.abs(initCoords.x - endCoords.x) <= this.MAX_DISTANCE_TO_CONNECT &&
                Math.abs(initCoords.y - endCoords.y) <= this.MAX_DISTANCE_TO_CONNECT);
    }

    computeShiftedEndPoint(cursorCoords: Coordinate): Coordinate {
        const initCoords = this.getLastDrawnDot();
        let xVariation = cursorCoords.x - initCoords.x;
        let yVariation = cursorCoords.y - initCoords.y;
        const angle = this.roundAngle45(xVariation, yVariation);
        if ( angle % (this.DEGREES_IN_QUADRANT * 2) === 0) {
            // Angle is horizontal (0 or 180)
            yVariation = 0;
        } else if (angle % this.DEGREES_IN_QUADRANT === 0) {
            // Angle is vertical (90 or 270)
            xVariation = 0;
        } else {
            // Angle is diagonal (45, 135, 225, 315)
            yVariation = Math.abs(xVariation);
            if (angle >= this.DEGREES_IN_QUADRANT * 2) {
                // Angle in third or fourth quadrant
                // So we invert the variation
                yVariation *= -1;
            }
        }
        return {x: initCoords.x + xVariation, y: initCoords.y + yVariation};
    }

    private generateLine(): void {
        this.renderer.setAttribute(this.line, config.points, this.parseDotLocations());
        this.renderer.setAttribute(this.line, config.line.fill, config.none);
        this.renderer.setAttribute(this.line, config.line.width, `${this.strokeSize}`);
        this.renderer.setAttribute(this.line, config.line.join, config.round);
        this.renderer.setAttribute(this.line, config.line.cap, config.round);
        this.renderer.setAttribute(this.line, config.selectable.identifier, config.selectable.true);
        this.renderer.setAttribute(this.line, config.takeParent.identifier, config.takeParent.true);

        this.renderer.appendChild(this.svg, this.line);
    }

    private parseDotLocations(): string {
        let coordinates = '';
        this.dots.forEach((dot) => {
            coordinates = coordinates.concat(`${dot.x},${dot.y} `);
        });
        return coordinates;
    }
    private generateCircle(dot: Coordinate): SVGCircleElement {
        const circle = this.renderer.createElement(config.element.circle, config.element.from) as SVGCircleElement;
        this.renderer.setAttribute(circle, config.circle.border, config.none);
        this.renderer.setAttribute(circle, config.circle.radius, (this.dottedEdgeDiameter / 2).toString());
        this.renderer.setAttribute(circle, config.circle.position.x, `${dot.x}`);
        this.renderer.setAttribute(circle, config.circle.position.y, `${dot.y}`);
        this.renderer.setAttribute(circle, config.selectable.identifier, config.selectable.true);
        this.renderer.setAttribute(circle, config.takeParent.identifier, config.takeParent.true);
        this.renderer.appendChild(this.svg, circle);
        return circle;
    }
    private setGAttributes(): void {
        this.renderer.setAttribute(this.svg, config.line.color, `#${this.color.rgb_hexa}`);
        this.renderer.setAttribute(this.svg, config.line.opacity, `${this.color.a}`);
        this.renderer.setAttribute(this.svg, config.circle.color, `#${this.color.rgb_hexa}`);
        this.renderer.setAttribute(this.svg, config.circle.opacity, `${this.color.a}`);
        this.renderer.setAttribute(this.svg, config.selectable.identifier, config.selectable.true);
        this.renderer.setAttribute(this.svg, config.takeParent.identifier, config.takeParent.false);

        this.renderer.setAttribute(this.svg, config.mainColor, config.fillAndStroke);
        this.renderer.setAttribute(this.svg, config.secondaryColor, config.none);
        this.renderer.setAttribute(this.svg, config.eraserPreview, config.fillAndStroke);
    }
}
