// tslint:disable: no-string-literal
// tslint:disable: no-magic-numbers
// tslint:disable: no-any
import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { Color } from 'src/app/color-palette/color/color';
import { Coordinate } from '../coordinate';
import { Line } from './line';
describe('line class', () => {
    let line: Line;
    let color: Color;
    const strokeSize = 5;
    const firstDot = {x: 1, y: 1};
    let rendererSpy: any;
    class MockColor extends Color {
        constructor() {
            super(0, 0, 0);
        }
    }
    beforeEach(async () => {
        TestBed.configureTestingModule({
            providers: [
                Renderer2,
                Coordinate,
                {provide: Color, useClass: MockColor},
            ]
        }).compileComponents();
    });
    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute', 'appendChild', 'removeChild']);
        color = new MockColor();
        line = new Line(color, firstDot, strokeSize, rendererSpy, false);
    });
    it('constructor should call renderer.createElement() 2 times if there are no joints to render', () => {
        expect(rendererSpy.createElement).toHaveBeenCalledTimes(2);
    });
    it('constructor should call renderer.setAttribute() 15 times if there are no joints to render', () => {
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(16);
    });
    it('constructor should set all of line attributes', () => {
        expect(line.strokeSize).toEqual(strokeSize);
        expect(line.color).toEqual(color);
        expect(line.dottedEdgeDiameter).toEqual(0);
        expect(line.dots).toEqual([firstDot, firstDot]);
    });
    it('constructor should set dottedEdgeDiameter to its proper value if it is defined', () => {
        line = new Line(color, firstDot, strokeSize, rendererSpy, true, 5);
        expect(line.dottedEdgeDiameter).toEqual(5);
    });
    it('updatePreviewLine changes the last dot in the dots array', () => {
        const mockDot = {x: 5, y: 3};
        line.updatePreviewLine(mockDot);
        expect(line.dots).toEqual([firstDot, mockDot]);
    });
    it('updatePreviewLine changes the last joint of the line if dottedEdge is true', () => {
        line = new Line(color, firstDot, strokeSize, rendererSpy, true, 5);
        const mockDot = {x: 5, y: 3};
        line.updatePreviewLine(mockDot);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(15 * 2 + 7 * 2 + 3);
    });
    it('updatePreviewLine without a parameter sets the last dot to the cursor location', () => {
        const mockDot = {x: 5, y: 3};
        line.cursorLocation = mockDot;
        line.updatePreviewLine();
        expect(line.dots).toEqual([firstDot, mockDot]);
    });
    it('addNewDotToLine adds a new dot to the array', () => {
        const mockDot = {x: 5, y: 3};
        line.addNewDotToLine(mockDot);
        expect(line.dots[line.dots.length - 1]).toEqual(mockDot);
    });
    it('addNewDotToLine adds a new circle to the circle array if dottedEdge is true', () => {
        line = new Line(color, firstDot, strokeSize, rendererSpy, true, 5);
        const mockDot = {x: 5, y: 3};
        const circleArrayLength = line['circles'].length;
        line.addNewDotToLine(mockDot);
        expect(line['circles'].length).toEqual(circleArrayLength + 1);
    });
    it('removePreviousDot does not remove a dot unless there are more than 2 dots in the array', () => {
        const mockDot = {x: 5, y: 3};
        line.removePreviousDot();
        expect(line.dots).toEqual([firstDot, firstDot]);
        line.addNewDotToLine(mockDot);
        expect(line.dots).toEqual([firstDot, firstDot, mockDot]);
        line.removePreviousDot();
        expect(line.dots).toEqual([firstDot, mockDot]);
    });
    it('removePreviousDot does not remove a circle unless there are more than 2 dots in the array', () => {
        line = new Line(color, firstDot, strokeSize, rendererSpy, true, 5);
        const mockDot = {x: 5, y: 3};
        const originalCircleArrayLength = line['circles'].length;
        line.removePreviousDot();
        expect(line['circles'].length).toEqual(originalCircleArrayLength);
        line.addNewDotToLine(mockDot);
        expect(line['circles'].length).toEqual(originalCircleArrayLength + 1);
        line.removePreviousDot();
        expect(line['circles'].length).toEqual(originalCircleArrayLength);
    });
    it('getLastDrawnDot returns the coordinates of the second to last dot in the array', () => {
        const mockDot1 = {x: 5, y: 3};
        const mockDot2 = {x: 3, y: 2};
        const mockDot3 = {x: 9, y: 0};
        line.addNewDotToLine(mockDot1);
        expect(line.getLastDrawnDot()).toEqual(firstDot);
        line.addNewDotToLine(mockDot2);
        expect(line.getLastDrawnDot()).toEqual(mockDot1);
        line.addNewDotToLine(mockDot3);
        expect(line.getLastDrawnDot()).toEqual(mockDot2);
    });
    it('getCursorDot returns the coordinates of the last dot in the array', () => {
        const mockDot1 = {x: 5, y: 3};
        const mockDot2 = {x: 3, y: 2};
        const mockDot3 = {x: 9, y: 0};
        expect(line.getCursorDot()).toEqual(firstDot);
        line.addNewDotToLine(mockDot1);
        expect(line.getCursorDot()).toEqual(mockDot1);
        line.addNewDotToLine(mockDot2);
        expect(line.getCursorDot()).toEqual(mockDot2);
        line.addNewDotToLine(mockDot3);
        expect(line.getCursorDot()).toEqual(mockDot3);
    });
    it('completeLine sets the last dot in the array to the same value as the first', () => {
        const midDot = {x: 5, y: 3};
        const endDot = {x: 6, y: 3};
        line.addNewDotToLine(midDot);
        line.addNewDotToLine(endDot);
        line.completeLine();
        expect(line.dots[line.dots.length - 1]).toEqual(firstDot);
    });
    it('computedShiftedEndPoint returns a point shifted 270 degrees when it should be', () => {
        line.dots = [{x: 5, y: 5}, {x: 5, y: 5}];
        const mockDot = {x: 6, y: 2};
        expect(line.computeShiftedEndPoint(mockDot)).toEqual({x: 5, y: 2});
    });
    it('computedShiftedEndPoint returns a point shifted 315 degrees when it should be', () => {
        line.dots = [{x: 5, y: 5}, {x: 5, y: 5}];
        const mockDot = {x: 7, y: 2};
        expect(line.computeShiftedEndPoint(mockDot)).toEqual({x: 7, y: 3});
    });
    it('computedShiftedEndPoint returns a point shifted 45 degrees when it should be', () => {
        line.dots = [{x: 5, y: 5}, {x: 5, y: 5}];
        const mockDot = {x: 6, y: 6};
        expect(line.computeShiftedEndPoint(mockDot)).toEqual({x: 6, y: 6});
    });
    it('computedShiftedEndPoint returns a point shifted 0 degrees when it should be', () => {
        line.dots = [{x: 5, y: 5}, {x: 5, y: 5}];
        const mockDot = {x: 8, y: 4};
        expect(line.computeShiftedEndPoint(mockDot)).toEqual({x: 8, y: 5});
    });
    it('computedShiftedEndPoint returns a point shifted 180 degrees when it should be', () => {
        line.dots = [{x: 5, y: 5}, {x: 5, y: 5}];
        const mockDot = {x: 6, y: 8};
        expect(line.computeShiftedEndPoint(mockDot)).toEqual({x: 5, y: 8});
    });
    it('connectsToStartPoint() should return true if we are close enough to connect it back to start', () => {
        line.dots[0] = {x: 2, y: 2};
        const newCoords = {x: 2 + line['MAX_DISTANCE_TO_CONNECT'], y: 2 + line['MAX_DISTANCE_TO_CONNECT']};
        expect(line.connectsToStartPoint(newCoords)).toBeTruthy();
    });
    it('connectsToStartPoint() should return false if we are not close enough to connect it back to start', () => {
        line.dots[0] = {x: 2, y: 2};
        const newCoords = {x: 2 + line['MAX_DISTANCE_TO_CONNECT'] + 1, y: 2 + line['MAX_DISTANCE_TO_CONNECT'] + 1};
        expect(line.connectsToStartPoint(newCoords)).toBeFalsy();
    });
});
