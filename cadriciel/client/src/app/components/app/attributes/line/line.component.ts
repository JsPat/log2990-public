import { Component } from '@angular/core';
import { LineService } from 'src/app/services/attributes/line/line.service';

@Component({
    selector: 'app-line',
    templateUrl: './line.component.html',
    styleUrls: ['./line.component.css']
})
export class LineComponent {

    protected service: LineService;

    constructor(service: LineService) {
        this.service = service;
    }

}
