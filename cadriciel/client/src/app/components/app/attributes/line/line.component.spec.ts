// /* tslint:disable:no-unused-variable */
// /* tslint:disable: no-string-literal */

// import { NO_ERRORS_SCHEMA } from '@angular/core';
// import { async, ComponentFixture, TestBed } from '@angular/core/testing';

// import { LineService } from 'src/app/services/attributes/line/line.service';
// import { LineComponent } from './line.component';

// describe('LineComponent', () => {
//   let component: LineComponent;
//   let fixture: ComponentFixture<LineComponent>;
//   let serviceSpy: LineService;

//   beforeEach(async(() => {
//     serviceSpy = jasmine.createSpyObj('LineService', ['']);

//     TestBed.configureTestingModule({
//       providers: [
//         {provide: LineService, useValue: serviceSpy},
//       ],
//       declarations: [ LineComponent ],
//       schemas: [NO_ERRORS_SCHEMA]
//     })
//     .compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(LineComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });

//   it('should have the right service for his attibute', () => {
//       expect(component['service']).toEqual(serviceSpy);
//   });

// });
