export abstract class SVGHolder {
    protected svg: SVGGraphicsElement;

    get svgDrawElement(): SVGGraphicsElement {
        return this.svg;
    }
}
