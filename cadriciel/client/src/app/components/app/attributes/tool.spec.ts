// tslint:disable: no-string-literal

import { Tool } from './tool';

class ToolStub extends Tool {
    constructor() {
        super();
    }
}

describe('Tools', () => {
    let tool: ToolStub;

    beforeEach(() => {
        tool = new ToolStub();
    });

    it('LEFT_CLICK should equal 0', () => {
        expect(tool['LEFT_CLICK']).toEqual(0);
    });

    it('RIGHT_CLICK should equal 2', () => {
        expect(tool['RIGHT_CLICK']).toEqual(2);
    });

    it('mouseMove should return undefined', () => {
        const mockMove = new MouseEvent('mousemove');
        const returnValue = tool.mouseMove(mockMove);
        expect(returnValue).toBe(undefined);
    });

    it('mouseDown should return undefined', () => {
        const mockClick = new MouseEvent('mousedown');
        const returnValue = tool.mouseDown(mockClick);
        expect(returnValue).toBe(undefined);
    });

    it('mouseUp should return undefined', () => {
        const returnValue = tool.mouseUp();
        expect(returnValue).toBe(undefined);
    });

    it('mouseLeave should return undefined', () => {
        const mockLeave = new MouseEvent('mouseleave');
        const returnValue = tool.mouseLeave(mockLeave);
        expect(returnValue).toBe(undefined);
    });

    it('click should return undefined', () => {
        const mockClick = new MouseEvent('click');
        const returnValue = tool.click(mockClick);
        expect(returnValue).toBe(undefined);
    });

    it('dblClick should return undefined', () => {
        const mockDblClick = new MouseEvent('dblclick');
        const returnValue = tool.dblClick(mockDblClick);
        expect(returnValue).toBe(undefined);
    });

    it('mouseDrag should return undefined', () => {
        const mockDrag = new MouseEvent('mousemove');
        const returnValue = tool.mouseDrag(mockDrag);
        expect(returnValue).toBe(undefined);
    });

    it('shiftDown should return undefined', () => {
        const returnValue = tool.shiftDown();
        expect(returnValue).toBe(undefined);
    });

    it('shiftUp should return undefined', () => {
        const returnValue = tool.shiftUp();
        expect(returnValue).toBe(undefined);
    });

    it('escapeDown should return undefined', () => {
        const returnValue = tool.escapeDown();
        expect(returnValue).toBe(undefined);
    });

    it('backSpaceDown should return undefined', () => {
        const returnValue = tool.backSpaceDown();
        expect(returnValue).toBe(undefined);
    });

    it('deleteDown should return undefined', () => {
        const returnValue = tool.deleteDown();
        expect(returnValue).toBe(undefined);
    });

    it('arrowKeyDown should return undefined', () => {
        const event = jasmine.createSpyObj('KeyboardEvent', ['']);
        const returnValue = tool.arrowKeyDown(event);
        expect(returnValue).toBe(undefined);
    });

    it('arrowKeyUp should return undefined', () => {
        const event = jasmine.createSpyObj('KeyboardEvent', ['']);
        const returnValue = tool.arrowKeyUp(event);
        expect(returnValue).toBe(undefined);
    });

    it('toolSwap should return undefined', () => {
        const returnValue = tool.toolSwap();
        expect(returnValue).toBe(undefined);
    });

    it('rightClick should return undefined', () => {
        const event = jasmine.createSpyObj('KeyboardEvent', ['']);
        const returnValue = tool.rightClick(event);
        expect(returnValue).toBe(undefined);
    });

    it('selectAll should return undefined', () => {
        const returnValue = tool.selectAll();
        expect(returnValue).toBe(undefined);
    });

    it('wheel should return undefined', () => {
        const event = jasmine.createSpyObj('WheelEvent', ['']);
        const returnValue = tool.wheel(event);
        expect(returnValue).toBe(undefined);
    });

});
