// tslint:disable: no-string-literal
// tslint:disable: no-any
// tslint:disable: no-magic-numbers

import { TestBed } from '@angular/core/testing';

import { Renderer2 } from '@angular/core';
import { Color } from 'src/app/color-palette/color/color';
import { Coordinate } from '../coordinate';
import { Eraser } from './eraser';

describe('eraser class', () => {
    let eraser: Eraser;
    let position: Coordinate;
    let rendererSpy: Renderer2;

    class MockColor extends Color {
        constructor() {
            super(0, 0, 0);
        }
    }

    beforeEach(async () => {
        TestBed.configureTestingModule({
            providers: [
                Renderer2,
                Coordinate,
                {provide: Color, useClass: MockColor}
            ]
        }).compileComponents();
    });
    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute', 'appendChild']);
        position = new Coordinate();
        position.x = 1;
        position.y = 1;
        eraser = new Eraser(position, 5, rendererSpy);
        eraser['width'] = 10;
    });

    it('should create', () => {
        expect(eraser).toBeTruthy();
    });

    it('updateShape should call drawShape', () => {
        spyOn<any>(eraser, 'drawShape');
        eraser.updateShape({x: 2, y: 3}, 5);
        expect(eraser['drawShape']).toHaveBeenCalled();
    });

    it('drawShape should setAttribute to update the x position to current x', () => {
        eraser['position'] = {x: 10, y: 6};
        eraser['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(eraser['svg'], 'x', '10');
    });

    it('drawShape should setAttribute to update the y position to current x', () => {
        eraser['position'] = {x: 7, y: 8};
        eraser['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(eraser['svg'], 'y', '8');
    });

    it('drawShape should setAttribute to update the height position to current height', () => {
        // tslint:disable-next-line: no-magic-numbers
        eraser['width'] = 12;
        eraser['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(eraser['svg'], 'height', '12');
    });

    it('drawShape should setAttribute to update the width position to current width', () => {
        // tslint:disable-next-line: no-magic-numbers
        eraser['width'] = 8;
        eraser['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(eraser['svg'], 'width', '8');
    });

    it('drawShape should setAttribute to update the isSelectable to true', () => {
        eraser['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(eraser['svg'], 'data-is-selectable', 'false');
    });

    it('getStartingPosition should return the starting x', () => {
        eraser['width'] = 6;
        const newStart = eraser['getStartingPosition']({x: 20, y: 25});
        expect(newStart.x).toEqual(17);
    });

    it('getStartingPosition should return the starting y', () => {
        eraser['width'] = 5;
        const newStart = eraser['getStartingPosition']({x: 20, y: 25});
        expect(newStart.y).toEqual(22.5);
    });

    it('makeInvisible should setAttribute to update the borderColor to none', () => {
        eraser['makeInvisible']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(eraser['svg'], 'stroke', 'none');
    });
});
