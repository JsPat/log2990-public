import { Component } from '@angular/core';
import { EraserService } from 'src/app/services/attributes/eraser/eraser.service';

@Component({
  selector: 'app-eraser',
  templateUrl: './eraser.component.html',
  styleUrls: ['./eraser.component.css']
})
export class EraserComponent {

  protected service: EraserService;

  constructor(service: EraserService) {
    this.service = service;
  }

}
