
import { Renderer2 } from '@angular/core';
import { BLACK } from 'src/app/color-palette/constant';
import { Coordinate } from '../coordinate';
import * as JSON from '../rectangle/rectangle.json';
import { SVGHolder } from '../svg-holder';

const config = JSON.default;

export class Eraser extends SVGHolder {

    position: Coordinate;
    private width: number;
    private renderer: Renderer2;

    constructor(position: Coordinate, width: number, renderer: Renderer2) {
        super();

        this.position = position;
        this.width = width;

        this.renderer = renderer;

        this.svg = this.renderer.createElement(config.element.type, config.element.from);
        this.drawShape();
    }

    updateShape(mousePosition: Coordinate, width: number): void {
        this.width = width;
        this.position = this.getStartingPosition(mousePosition);
        this.drawShape();
    }

    makeInvisible(): void {
        this.renderer.setAttribute(this.svg, config.border.color, config.none);
    }

    private drawShape(): void {
        this.renderer.setAttribute(this.svg, config.position.x, `${this.position.x}`);
        this.renderer.setAttribute(this.svg, config.position.y, `${this.position.y}`);
        this.renderer.setAttribute(this.svg, config.dimension.width, `${this.width}`);
        this.renderer.setAttribute(this.svg, config.dimension.height, `${this.width}`);
        this.renderer.setAttribute(this.svg, config.fill.color, config.none);
        this.renderer.setAttribute(this.svg, config.selectable.identifier, config.selectable.false);
        this.renderer.setAttribute(this.svg, config.border.color, `#${BLACK.rgb_hexa}`);
        this.renderer.setAttribute(this.svg, config.border.width, config.valueOne);
        this.renderer.setAttribute(this.svg, config.pointerEvent, config.none);
        this.renderer.setAttribute(this.svg, config.shouldSave, config.valueFalse);
    }

    private getStartingPosition(mousePosition: Coordinate): Coordinate {
        const x = mousePosition.x - this.width / 2;
        const y = mousePosition.y - this.width / 2;
        return {x, y};
    }
}
