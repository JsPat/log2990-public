import { Renderer2 } from '@angular/core';
import { Color } from 'src/app/color-palette/color/color';
import { Coordinate } from '../coordinate';
import { Pen } from '../pen/pen';
import * as JSON from './paint-brush.json';

const config = JSON.default;

export class PaintBrush extends Pen {
    private filter: string;

    constructor(dot: Coordinate, color: Color, strokeWidth: number, filter: string, renderer: Renderer2) {
        super(dot, color, strokeWidth, renderer);
        this.filter = filter;
    }

    protected drawShape(): void {
        super.drawShape();
        this.renderer.setAttribute(this.svg, config.line.filter, `url(#${this.filter})`);
    }

}
