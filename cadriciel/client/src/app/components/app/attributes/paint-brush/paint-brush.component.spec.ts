/* tslint:disable:no-unused-variable */
/* tslint:disable:no-string-literal*/
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaintBrushService } from 'src/app/services/attributes/paint-brush/paint-brush.service';
import { PaintBrushComponent } from './paint-brush.component';

describe('PaintBrushComponent', () => {
  let component: PaintBrushComponent;
  let fixture: ComponentFixture<PaintBrushComponent>;
  let serviceSpy: PaintBrushService;

  beforeEach(async(() => {

    serviceSpy = jasmine.createSpyObj('PaintBrushService', ['']);

    TestBed.configureTestingModule({
      declarations: [ PaintBrushComponent ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [ {provide: PaintBrushService, useValue: serviceSpy} ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaintBrushComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have the right service for his attibute', () => {
    expect(component['service']).toEqual(serviceSpy);
  });

});
