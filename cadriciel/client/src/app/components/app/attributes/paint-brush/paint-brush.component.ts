// tslint:disable: no-string-literal
import { Component } from '@angular/core';
import { PaintBrushService } from 'src/app/services/attributes/paint-brush/paint-brush.service';
import { PenComponent } from '../pen/pen.component';

@Component({
  selector: 'app-paintBrush',
  templateUrl: './paint-brush.component.html',
  styleUrls: ['./paint-brush.component.css']
})

export class PaintBrushComponent extends PenComponent {

  constructor(service: PaintBrushService) {
    super(service);
  }
}
