// tslint:disable: no-string-literal
// tslint:disable: no-magic-numbers

import { TestBed } from '@angular/core/testing';

import { Renderer2 } from '@angular/core';
import { Color } from 'src/app/color-palette/color/color';
import { Coordinate } from '../coordinate';
import { PaintBrush } from './paint-brush';

describe('paintBrush class', () => {
    let paintBrush: PaintBrush;
    let color: Color;
    let position: Coordinate;
    const filter = '@sapeli';
    // tslint:disable-next-line: no-any
    let rendererSpy: any;

    class MockColor extends Color {
        constructor() {
            super(0, 0, 0);
        }
    }

    beforeEach(async () => {
        TestBed.configureTestingModule({
            providers: [
                Renderer2,
                Coordinate,
                {provide: Color, useClass: MockColor}
            ]
        }).compileComponents();
    });
    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute', 'appendChild']);
        color = new MockColor();
        position = {x: 0, y: 0};
        paintBrush = new PaintBrush(position, color, 5, filter, rendererSpy);
    });

    it('drawShape should setAttribute to update the fill to none', () => {
        paintBrush['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(paintBrush['svg'], 'fill', 'none');
    });

    it('drawShape should setAttribute to update the stroke to the color', () => {
        paintBrush['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(paintBrush['svg'], 'stroke', `#${paintBrush['color'].rgb_hexa}`);
    });

    it('drawShape should setAttribute to update the stroke-width to the current strokewidth', () => {
        paintBrush['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(paintBrush['svg'], 'stroke-width', `${paintBrush['strokeWidth']}`);
    });

    it('drawShape should setAttribute to update the stroke-opacity to opacity of the color', () => {
        paintBrush['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(paintBrush['svg'], 'stroke-opacity', `${paintBrush['color'].a_string}`);
    });

    it('drawShape should setAttribute to update the stroke-linejoin to the round', () => {
        paintBrush['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(paintBrush['svg'], 'stroke-linejoin', 'round');
    });

    it('drawShape should setAttribute to update the filter to the current filter', () => {
        paintBrush['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(paintBrush['svg'], 'filter', `url(#${paintBrush['filter']})`);
    });

    it('drawShape should setAttribute to update the points to the dotsToWrtie', () => {
        paintBrush['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(paintBrush['svg'], 'points', `${paintBrush['dotsToWrite']}`);
    });

    it('drawShape should setAttribute to to be selectable', () => {
        paintBrush['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(paintBrush['svg'], 'data-is-selectable', 'true');
    });

});
