import { Component } from '@angular/core';
import { PaintBucketService } from 'src/app/services/attributes/paint-bucket/paint-bucket.service';

@Component({
  selector: 'app-paintBucket',
  templateUrl: './paint-bucket.component.html',
  styleUrls: ['./paint-bucket.component.css']
})
export class PaintBucketComponent {

  protected service: PaintBucketService;

  constructor(service: PaintBucketService) {
    this.service = service;
  }

}
