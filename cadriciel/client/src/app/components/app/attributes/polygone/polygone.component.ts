// inspired from: https://codepen.io/winkerVSbecks/pen/wrZQQm

import { Component } from '@angular/core';
import { PolygoneService } from 'src/app/services/attributes/polygone/polygone.service';

@Component({
  selector: 'app-polygone',
  templateUrl: './polygone.component.html',
  styleUrls: ['./polygone.component.css']
})
export class PolygoneComponent {

  protected service: PolygoneService;
  protected fillButtonDisabled: boolean;
  protected borderButtonDisabled: boolean;

  constructor(service: PolygoneService) {
    this.service = service;
    this.onBorderButtonChange();
    this.onFillButtonChange();
  }

  onBorderButtonChange(): void {
    this.fillButtonDisabled = this.service.fillEnabled && !this.service.borderEnabled;
  }

  onFillButtonChange(): void {
    this.borderButtonDisabled = this.service.borderEnabled && !this.service.fillEnabled;
  }
}
