// tslint:disable: no-string-literal
// tslint:disable: no-magic-numbers
// tslint:disable: no-any
import { TestBed } from '@angular/core/testing';

import { Renderer2 } from '@angular/core';
import { Color } from 'src/app/color-palette/color/color';
import { Coordinate } from '../coordinate';
import { Polygone } from './polygone';

describe('polygone class', () => {
    let polygone: Polygone;
    let color: Color;
    let strokeColor: Color;
    let position: Coordinate;
    let rendererSpy: any;

    class MockColor extends Color {
        constructor() {
            super(0, 0, 0);
        }
    }

    beforeEach(async () => {
        TestBed.configureTestingModule({
            providers: [
                Renderer2,
                Coordinate,
                {provide: Color, useClass: MockColor}
            ]
        });
    });
    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute', 'appendChild']);
        color = new MockColor();
        strokeColor = new MockColor();
        position = new Coordinate();
        position.x = 1;
        position.y = 1;
        polygone = new Polygone(color, strokeColor, 5, {x: 0, y: 0}, true, true, 5, rendererSpy);
    });

    // constructor
    it('constructor should call renderer.createElement()', () => {
        expect(rendererSpy.createElement).toHaveBeenCalled();
    });

    it('constructor should call renderer.createElement()', () => {
        expect(rendererSpy.createElement).toHaveBeenCalled();
    });
    it('constructor should set the attributes', () => {
        const newColor = new MockColor();
        const newStrokeColor = new MockColor();
        polygone = new Polygone(color, strokeColor, 5, {x: 0, y: 0}, true, true, 5, rendererSpy);
        expect(polygone['color']).toEqual(newColor);
        expect(polygone['strokeWidth']).toEqual(5);
        expect(polygone['strokeColor']).toEqual(newStrokeColor);
    });

    //
    it('drawShape should call findDotsToWrite', () => {
        spyOn<any>(polygone, 'findDotsToWrite');
        polygone.drawShape([], true, false);
        expect(polygone['findDotsToWrite']).toHaveBeenCalled();
    });
    it('drawShape should call renderer.setAttribute() 15 times if border=true, fill=true', () => {
        polygone.drawShape([], true, false);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(15);
    });
    it('drawShape should call renderer.setAttribute() 11 times if border=true, fill=false', () => {
        polygone.drawShape([], false, false);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(11);
    });
    it('drawShape should call renderer.setAttribute() 14 times if border=false, fill=true', () => {
        polygone.drawShape([], true, true);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(15);
    });
    it('drawShape should call renderer.setAttribute() 11 times if border=false, fill=false', () => {
        polygone.drawShape([], false, true);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(11);
    });

    // findDotsToWrite
    it('dots to write with 0 point', () => {
        const dots = polygone['findDotsToWrite']([]);
        expect(dots).toBe();
    });
    it('dots to write with 1 point', () => {
        let coords: Coordinate[];
        coords = [];
        const coord = new Coordinate();
        coord.x = 10;
        coord.y = 11;
        coords.push(coord);
        polygone['findDotsToWrite'](coords);
        expect(polygone['dotsToWrite']).toBe('10,11 ');
    });
    it('dots to write with 2 point', () => {
        let coords: Coordinate[];
        coords = [];
        const coord = new Coordinate();
        coord.x = 10;
        coord.y = 11;
        coords.push(coord);
        coords.push(coord);
        polygone['findDotsToWrite'](coords);
        expect(polygone['dotsToWrite']).toBe('10,11 10,11 ');
    });
    // updateShape()
    it('updateShape should assign values to attributes if polygone is defined', () => {
    spyOn<any>(polygone, 'updateShape').and.callThrough();
    polygone['strokeWidth'] = 100;
    polygone = new Polygone(color, strokeColor, 5, {x: 0, y: 0}, true, true, 5, rendererSpy);
    polygone['updateShape']({x: 0, y: 0});
    expect(polygone['strokeWidth']).toEqual(polygone['strokeWidth']);
  });
  // findCenter()
    it('when mousePosition < start center should be start.x - Radius', () => {
      polygone['start'] = {x: 10, y: 10};
      polygone['mousePosition'] = {x: 5, y: 5};
      const centre = polygone['findCenter']();
      expect(centre.x).toBe(polygone['start'].x - polygone['findRadius']());
      expect(centre.y).toBe(polygone['start'].y - polygone['findRadius']());
  });
    it('when mousePosition > start center should be start.x + Radius', () => {
    polygone['start'] = {x: 5, y: 5};
    polygone['mousePosition'] = {x: 10, y: 10};
    const centre = polygone['findCenter']();
    expect(centre.x).toBe(polygone['start'].x + polygone['findRadius']());
    expect(centre.y).toBe(polygone['start'].y + polygone['findRadius']());
  });
  // findWidth()
    it('findWidth() should return the absolute value of start.x - position.x when start.x > position.x', () => {
    const position1 = {x: 10, y: 10};
    const position2 = {x: 5, y: 5};
    polygone['start'] = position1;
    polygone['mousePosition'] = position2;
    const width = polygone['findWidth']();
    expect(width).toEqual(Math.abs(position1.x - position2.x) / 2);
  });
    it('findWidth() should return the absolute value of start.x - position.x when start.x < position.x', () => {
    const position1 = {x: 5, y: 5};
    const position2 = {x: 10, y: 10};
    polygone['start'] = position1;
    polygone['mousePosition'] = position2;
    const width = polygone['findWidth']();
    expect(width).toEqual(Math.abs(position1.x - position2.x) / 2);
  });
    it('findWidth() should return the absolute value of start.x - position.x when start.x = position.x', () => {
    const position1 = {x: 5, y: 5};
    const position2 = {x: 5, y: 5};
    polygone['start'] = position1;
    polygone['mousePosition'] = position2;
    const width = polygone['findWidth']();
    expect(width).toEqual(Math.abs(position1.x - position2.x) / 2);
  });
    it('findWidth() should return the absolute value of start.x - position.x when start.x is negative', () => {
    const position1 = {x: -1, y: 5};
    const position2 = {x: 2, y: 5};
    polygone['start'] = position1;
    polygone['mousePosition'] = position2;
    const width = polygone['findWidth']();
    expect(width).toEqual(Math.abs(position1.x - position2.x) / 2);
  });
    it('findWidth() should return the absolute value of start.x - position.x when position.x is negative', () => {
    const position1 = {x: 1, y: 5};
    const position2 = {x: -2, y: 5};
    polygone['start'] = position1;
    polygone['mousePosition'] = position2;
    const width = polygone['findWidth']();
    expect(width).toEqual(Math.abs(position1.x - position2.x) / 2);
  });
    it('findWidth() should return the absolute value of start.x - position.x when start.x and position.x are negative', () => {
    const position1 = {x: -1, y: 5};
    const position2 = {x: -2, y: 5};
    polygone['start'] = position1;
    polygone['mousePosition'] = position2;
    const width = polygone['findWidth']();
    expect(width).toEqual(Math.abs(position1.x - position2.x) / 2);
  });
  // findHeight()
    it('findHeight() should return the absolute value of start.y - position.y when start.y > position.y', () => {
    const position1 = {x: 5, y: 10};
    const position2 = {x: 5, y: 5};
    polygone['start'] = position1;
    polygone['mousePosition'] = position2;
    const height = polygone['findHeight']();
    expect(height).toEqual(Math.abs(position1.y - position2.y) / 2);
  });
    it('findHeight() should return the absolute value of start.y - position.y when start.y < position.y', () => {
    const position1 = {x: 5, y: 5};
    const position2 = {x: 5, y: 10};
    polygone['start'] = position1;
    polygone['mousePosition'] = position2;
    const height = polygone['findHeight']();
    expect(height).toEqual(Math.abs(position1.y - position2.y) / 2);
  });
    it('findHeight() should return the absolute value of start.y - position.y when start.y = position.y', () => {
    const position1 = {x: 5, y: 5};
    const position2 = {x: 5, y: 5};
    polygone['start'] = position1;
    polygone['mousePosition'] = position2;
    const height = polygone['findHeight']();
    expect(height).toEqual(Math.abs(position1.y - position2.y) / 2);
  });
    it('findHeight() should return the absolute value of start.y - position.y when start.y is negative', () => {
    const position1 = {x: 5, y: -1};
    const position2 = {x: 5, y: 2};
    polygone['start'] = position1;
    polygone['mousePosition'] = position2;
    const height = polygone['findHeight']();
    expect(height).toEqual(Math.abs(position1.y - position2.y) / 2);
  });
    it('findHeight() should return the absolute value of start.y - position.y when position.y is negative', () => {
    const position1 = {x: 5, y: 1};
    const position2 = {x: 5, y: -2};
    polygone['start'] = position1;
    polygone['mousePosition'] = position2;
    const height = polygone['findHeight']();
    expect(height).toEqual(Math.abs(position1.y - position2.y) / 2);
  });
    it('findHeight() should return the absolute value of start.y - position.y when start.y and position.y are negative', () => {
    const position1 = {x: 5, y: -1};
    const position2 = {x: 5, y: -2};
    polygone['start'] = position1;
    polygone['mousePosition'] = position2;
    const height = polygone['findHeight']();
    expect(height).toEqual(Math.abs(position1.y - position2.y) / 2);
  });
    // findRadius
    it('should return the right radius for different width and height', () => {
    polygone['start'] = {x: 10, y: 10};
    polygone['mousePosition'] = {x: 4, y: 0};
    const radius = polygone['findRadius']();
    expect(radius).toBe(3);
  });
  // addPoints
    it('the points container should hold the expected points with 3 sides', () => {
    polygone['start'] = {x: 0, y: 0};
    polygone['mousePosition'] = {x: 2, y: 2};
    polygone['sidesNumber'] = 3;
    polygone['offset'] = 2 * Math.PI / polygone['sidesNumber'];
    const points = new Array<Coordinate>();
    let point = new Coordinate();
    point = {x: 1, y: 0};
    points.push(point);
    point = {x: 0.133974597, y: 1.5};
    points.push(point);
    point = {x: 1.866025403, y: 1.5};
    points.push(point);
    polygone['points'] = new Array<Coordinate>();
    polygone['addPoints']();
    expect(polygone['points'][0].x).toBeCloseTo(points[0].x);
    expect(polygone['points'][0].y).toBeCloseTo(points[0].y);
    expect(polygone['points'][1].x).toBeCloseTo(points[1].x);
    expect(polygone['points'][1].y).toBeCloseTo(points[1].y);
    expect(polygone['points'][2].x).toBeCloseTo(points[2].x);
    expect(polygone['points'][2].y).toBeCloseTo(points[2].y);
  });

    it('isNull should return true if the x value of start and mousePosition is equal', () => {
    polygone['mousePosition'] = {x: 1, y: 6};
    polygone['start'] = {x: 1, y: 4};
    expect(polygone.isNull()).toBeTruthy();
  });

    it('isNull should return true if the y value of start and mousePosition is equal', () => {
    polygone['mousePosition'] = {x: 9, y: 4};
    polygone['start'] = {x: 1, y: 4};
    expect(polygone.isNull()).toBeTruthy();
  });

    it('isNull should return false if the x and y value of start and mousePosition is not equal', () => {
    polygone['mousePosition'] = {x: 1, y: 6};
    polygone['start'] = {x: 2, y: 5};
    expect(polygone.isNull()).toBeFalsy();
  });

});
