import { Renderer2 } from '@angular/core';
import { Color } from 'src/app/color-palette/color/color';
import {Coordinate} from '../coordinate';
import { SVGHolder } from '../svg-holder';
import * as JSON from './polygone.json';

const config = JSON.default;

export class Polygone extends SVGHolder {
    private dotsToWrite: string;
    private color: Color;
    private strokeWidth: number;
    private offset: number;
    private strokeColor: Color;
    private points: Coordinate[];
    private start: Coordinate;
    private mousePosition: Coordinate;
    private sidesNumber: number;

    constructor(color: Color, strokeColor: Color,
                strokeWidth: number, start: Coordinate, private border: boolean,
                private fill: boolean, sidesNumber: number, protected renderer: Renderer2) {
        super();
        this.start = start;
        this.sidesNumber = sidesNumber;
        this.offset = 2 * Math.PI / this.sidesNumber;
        this.color = color;
        this.strokeWidth = strokeWidth;
        this.svg = this.renderer.createElement(config.element.type, config.element.from);
        this.strokeColor = strokeColor;
    }

    drawShape(dot: Coordinate[], border: boolean, fill: boolean): void {
        this.renderer.setAttribute(this.svg, config.fill.color, config.none);
        this.renderer.setAttribute(this.svg, config.border.color, `#${this.color.rgb_hexa}`);
        this.renderer.setAttribute(this.svg, config.border.width, `${this.strokeWidth}`);
        this.renderer.setAttribute(this.svg, config.border.opacity, `${this.color.a_string}`);
        this.findDotsToWrite(dot);
        this.renderer.setAttribute(this.svg, config.points, `${this.dotsToWrite}`);
        this.renderer.setAttribute(this.svg, config.selectable.identifier, config.selectable.true);
        this.renderer.setAttribute(this.svg, config.line.cap, config.round);
        this.renderer.setAttribute(this.svg, config.eraserPreview, config.border.color);

        if (border) {
            this.renderer.setAttribute(this.svg, config.border.color, `#${this.strokeColor.rgb_hexa}`);
            this.renderer.setAttribute(this.svg, config.border.opacity, `${this.strokeColor.a}`);
            this.renderer.setAttribute(this.svg, config.border.width, `${this.strokeWidth}`);
            this.renderer.setAttribute(this.svg, config.border.position, config.border.inside);
            this.renderer.setAttribute(this.svg, config.secondaryColor, config.border.color);
        } else {
            this.renderer.setAttribute(this.svg, config.secondaryColor, config.none);
        }

        if (fill) {
            this.renderer.setAttribute(this.svg, config.mainColor, config.fill.color);
            this.renderer.setAttribute(this.svg, config.fill.color, `#${this.color.rgb_hexa}`);
        } else {
            this.renderer.setAttribute(this.svg, config.mainColor, config.none);
            this.renderer.setAttribute(this.svg, config.fill.color, config.none);
        }
    }

    updateShape(mousePosition: Coordinate): void {
        this.mousePosition = mousePosition;
        this.addPoints();
        this.drawShape(this.points, this.border, this.fill);
    }

    isNull(): boolean {
        return this.mousePosition.x === this.start.x || this.mousePosition.y === this.start.y;
    }

    private findDotsToWrite(dot: Coordinate[]): void {
        this.dotsToWrite = '';
        for (const elements of dot) {
            this.dotsToWrite += `${elements.x},${elements.y} `;
        }
    }

    private findCenter(): Coordinate {
      const x = (this.mousePosition.x < this.start.x) ? this.start.x - this.findRadius() : this.start.x + this.findRadius();
      const y = (this.mousePosition.y < this.start.y) ? this.start.y - this.findRadius() : this.start.y + this.findRadius();
      return {x, y};
    }

    private findWidth(): number {
      return Math.abs(this.start.x - this.mousePosition.x) / 2;
    }

    private findHeight(): number {
      return Math.abs(this.start.y - this.mousePosition.y) / 2;
    }

    private findRadius(): number {
      const min = Math.min(this.findHeight(), this.findWidth());
      const rayon = Math.sqrt(Math.pow(min, 2));
      return rayon;
    }

    private addPoints(): void {
      this.points = new Array<Coordinate>();
      const radius = this.findRadius();
      for (let i = 0; i < this.sidesNumber; i++) {
        const tempPoint = this.findCenter();
        tempPoint.x -= radius * Math.sin( this.offset * i);
        tempPoint.y -= radius * Math.cos( this.offset * i);
        this.points.push(tempPoint);
      }
    }
}
