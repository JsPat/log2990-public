import { Component } from '@angular/core';
import { ColorizeService } from 'src/app/services/attributes/colorize/colorize.service';

@Component({
  selector: 'app-colorize',
  templateUrl: './colorize.component.html',
  styleUrls: ['./colorize.component.scss']
})
export class ColorizeComponent {

  protected service: ColorizeService;

  constructor(service: ColorizeService) {
    this.service = service;
  }

}
