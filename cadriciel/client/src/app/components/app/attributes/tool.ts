export const BASIC_WIDTH = 5;

export abstract class Tool {
    protected readonly LEFT_CLICK: number = 0;
    protected readonly RIGHT_CLICK: number = 2;

    mouseMove(event: MouseEvent): void {
        // empty function
    }

    mouseDown(event: MouseEvent): void {
        // empty function
    }

    mouseUp(): void {
        // empty function
    }

    mouseLeave(event: MouseEvent): void {
        // empty function
    }

    click(event: MouseEvent): void {
        // empty function
    }

    dblClick(event: MouseEvent): void {
        // empty function
    }

    mouseDrag(event: MouseEvent): void {
        // empty function
    }

    shiftDown(): void {
        // Empty fonction
    }

    shiftUp(): void {
        // Empty fonction
    }

    escapeDown(): void {
        // Empty fonction
    }

    backSpaceDown(): void {
        // Empty fonction
    }

    deleteDown(): void {
        // empty function
    }
    arrowKeyDown(event: KeyboardEvent): void {
        // empty function
    }
    arrowKeyUp(event: KeyboardEvent): void {
        // empty function
    }

    toolSwap(): void {
        // Empty fonction
    }

    rightClick(event: MouseEvent): void {
        // Empty fonction
    }
    selectAll(): void {
        // Empty function
    }
    wheel(event: WheelEvent): void {
        // Empty function
    }
}
