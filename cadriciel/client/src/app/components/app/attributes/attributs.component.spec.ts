/* tslint:disable:no-unused-variable */
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ToolsService } from 'src/app/services/attributes/toolsService/tools-service';
import { PossibleTools } from '../../../services/constants';

import SpyObj = jasmine.SpyObj;
import { AttributsComponent } from './attributs.component';

describe('AttributsComponent', () => {

  let toolsServiceSpy: SpyObj<ToolsService>;

  let component: AttributsComponent;
  let fixture: ComponentFixture<AttributsComponent>;

  beforeEach(() => {
    toolsServiceSpy = jasmine.createSpyObj('ToolsService', ['toggle']);
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttributsComponent ],
      providers: [{provide: ToolsService, useValue: toolsServiceSpy}],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttributsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.currentTool = PossibleTools.SELECT;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('toggle function should shift the active tool with PEN', () => {
    component.tool[PossibleTools.SELECT] = true;
    component.tool[PossibleTools.PEN] = false;
    component.toggle(PossibleTools.PEN);
    expect(component.currentTool).toBe(PossibleTools.PEN);
    expect(component.tool[PossibleTools.SELECT]).toBe(false);
    expect(component.tool[PossibleTools.PEN]).toBe(true);
    expect(toolsServiceSpy.toggle).toHaveBeenCalledTimes(1);
  });
  it('toggle function should shift the active tool with PAINTBRUSH', () => {
    component.tool[PossibleTools.SELECT] = true;
    component.tool[PossibleTools.PAINTBRUSH] = false;
    component.toggle(PossibleTools.PAINTBRUSH);
    expect(component.currentTool).toBe(PossibleTools.PAINTBRUSH);
    expect(component.tool[PossibleTools.SELECT]).toBe(false);
    expect(component.tool[PossibleTools.PAINTBRUSH]).toBe(true);
    expect(toolsServiceSpy.toggle).toHaveBeenCalledTimes(1);
  });
  it('toggle function should shift the active tool with PIPETTE', () => {
    component.tool[PossibleTools.SELECT] = true;
    component.tool[PossibleTools.PIPETTE] = false;
    component.toggle(PossibleTools.PIPETTE);
    expect(component.currentTool).toBe(PossibleTools.PIPETTE);
    expect(component.tool[PossibleTools.SELECT]).toBe(false);
    expect(component.tool[PossibleTools.PIPETTE]).toBe(true);
    expect(toolsServiceSpy.toggle).toHaveBeenCalledTimes(1);
  });
  it('toggle function should do nothing if active tool is toggled', () => {
    component.tool[PossibleTools.SELECT] = true;
    component.toggle(PossibleTools.SELECT);
    expect(component.currentTool).toBe(PossibleTools.SELECT);
    expect(component.tool[PossibleTools.SELECT]).toBe(true);
    expect(toolsServiceSpy.toggle).toHaveBeenCalledTimes(0);
  });
  it('shortcutPen and toggle() should be called on c key event', () => {
    spyOn(component, 'toggle');
    component.shortcutPen();
    expect(component.toggle).toHaveBeenCalledTimes(1);
  });
  it('shortcutPaintBrush and toggle() should be called on w key event', () => {
    spyOn(component, 'toggle');
    component.shortcutPaintBrush();
    expect(component.toggle).toHaveBeenCalledTimes(1);
  });
  it('shortcutPaintBrush and toggle() should be called on d key event', () => {
    spyOn(component, 'toggle');
    component.shortcutPaintBucket();
    expect(component.toggle).toHaveBeenCalledTimes(1);
  });
  it('shortcutSprayPaint and toggle() should be called on a key event', () => {
    spyOn(component, 'toggle');
    component.shortcutSprayPaint();
    expect(component.toggle).toHaveBeenCalledTimes(1);
  });
  it('shortcutStamp and toggle() should be called on a key event', () => {
    spyOn(component, 'toggle');
    component.shortcutStamp();
    expect(component.toggle).toHaveBeenCalledTimes(1);
  });
  it('shortcutRectangle and toggle() should be called on 1 key event', () => {
    spyOn(component, 'toggle');
    component.shortcutRectangle();
    expect(component.toggle).toHaveBeenCalledTimes(1);
  });
  it('shortcutEllipse and toggle() should be called on 2 key event', () => {
    spyOn(component, 'toggle');
    component.shortcutEllipse();
    expect(component.toggle).toHaveBeenCalledTimes(1);
  });
  it('shortcutPolygone and toggle() should be called on 3 key event', () => {
    spyOn(component, 'toggle');
    component.shortcutPolygone();
    expect(component.toggle).toHaveBeenCalledTimes(1);
  });
  it('shortcutLine and toggle() should be called on l key event', () => {
    spyOn(component, 'toggle');
    component.shortcutLine();
    expect(component.toggle).toHaveBeenCalledTimes(1);
  });
  it('shortcutColorizer and toggle() should be called on r key event', () => {
    spyOn(component, 'toggle');
    component.shortcutColorizer();
    expect(component.toggle).toHaveBeenCalledTimes(1);
  });
  it('shortcutEraser and toggle() should be called on e key event', () => {
    spyOn(component, 'toggle');
    component.shortcutEraser();
    expect(component.toggle).toHaveBeenCalledTimes(1);
  });
  it('shortcutColorPicker and toggle() should be called on i key event', () => {
    spyOn(component, 'toggle');
    component.shortcutColorPicker();
    expect(component.toggle).toHaveBeenCalledTimes(1);
  });
  it('shortcutSelect and toggle() should be called on s key event', () => {
    spyOn(component, 'toggle');
    component.shortcutSelect();
    expect(component.toggle).toHaveBeenCalledTimes(1);
  });
});
