import { Component } from '@angular/core';
import { PipetteService } from 'src/app/services/attributes/pipette/pipette.service';

@Component({
  selector: 'app-pipette',
  templateUrl: './pipette.component.html',
  styleUrls: ['./pipette.component.scss']
})
export class PipetteComponent {

  protected service: PipetteService;

  constructor( service: PipetteService ) {
    this.service = service;
  }

}
