import { Renderer2 } from '@angular/core';
import { Color } from 'src/app/color-palette/color/color';
import { Coordinate } from '../coordinate';
import { SVGHolder } from '../svg-holder';
import * as JSON from './stamp.json';

const config = JSON.default;

export class Stamp extends SVGHolder {

    protected path: string;
    protected coords: Coordinate;
    protected shape: string;
    protected factor: number;
    protected angle: number;
    protected color: Color;
    protected strokeColor: Color;
    protected renderer: Renderer2;

    constructor(renderer: Renderer2, color: Color, strokeColor: Color, coords: Coordinate, factor: number, angle: number, shape: string) {
        super();
        this.color = color;
        this.strokeColor = strokeColor;
        this.coords = coords;
        this.factor = (factor / config.scaleFactor);
        this.angle = angle;
        this.shape = shape;
        this.renderer = renderer;

        this.svg = this.renderer.createElement(config.element.type, config.element.from);
        this.drawShape();

    }

    drawShape(): void {
        this.setPath();
        this.renderer.setAttribute(this.svg, config.border.color, `#${this.strokeColor.rgb_hexa}`);
        this.renderer.setAttribute(this.svg, config.border.opacity, `${this.strokeColor.a_string}`);
        this.renderer.setAttribute(this.svg, config.fill.color, `#${this.color.rgb_hexa}`);
        this.renderer.setAttribute(this.svg, config.fill.opacity, `${this.color.a_string}`);

        this.renderer.setAttribute(this.svg, config.mainColor, config.fill.color);
        this.renderer.setAttribute(this.svg, config.secondaryColor, config.border.color);

        this.renderer.setAttribute(this.svg, config.element.path, `${this.path}`);

        // These "magic strings" stay like that because they are necessary for the attributes and are not values
        this.renderer.setAttribute(this.svg, config.element.transform,
            `translate(${((1 - this.factor) * this.coords.x)},${((1 - this.factor) * this.coords.y)})
            scale(${this.factor})
            rotate(${this.angle},${this.coords.x},${this.coords.y})`);
        this.renderer.setAttribute(this.svg, config.selectable.identifier, config.selectable.true);
        this.renderer.setAttribute(this.svg, config.eraserPreview, config.border.color);
    }

    protected setPath(): void {
        switch (this.shape) {
            case config.shape.heart.name:
                this.path = `m${this.coords.x + config.shape.heart.x} ${this.coords.y + config.shape.heart.y} ` + config.shape.heart.svg;
                break;
            case config.shape.star.name:
                this.path = `m${this.coords.x + config.shape.star.x} ${this.coords.y + config.shape.star.y} ` + config.shape.star.svg;
                break;
            case config.shape.arrow.name:
                this.path = `m${this.coords.x + config.shape.arrow.x} ${this.coords.y + config.shape.arrow.y} ` + config.shape.arrow.svg;
                break;
            case config.shape.thumbsUp.name:
                this.path =
                    `m${this.coords.x + config.shape.thumbsUp.x} ${this.coords.y + config.shape.thumbsUp.y} ` + config.shape.thumbsUp.svg;
                break;
            case config.shape.smile.name:
                this.path = `m${this.coords.x + config.shape.smile.x} ${this.coords.y + config.shape.smile.y} ` + config.shape.smile.svg;
                break;
            default:
                this.path = `m${this.coords.x + config.shape.heart.x} ${this.coords.y + config.shape.heart.y} ` + config.shape.heart.svg;

        }
    }
}
