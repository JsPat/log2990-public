import { Component } from '@angular/core';
import { StampService } from 'src/app/services/attributes/stamp/stamp.service';

@Component({
  selector: 'app-stamp',
  templateUrl: './stamp.component.html',
  styleUrls: ['./stamp.component.css']
})
export class StampComponent {

  protected service: StampService;

  constructor( service: StampService ) {
    this.service = service;
  }

}
