// tslint:disable: no-string-literal
// tslint:disable: no-magic-numbers
// tslint:disable: no-any
import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { Color } from 'src/app/color-palette/color/color';
import { Coordinate } from '../coordinate';
import { Stamp } from './stamp';

import * as JSON from './stamp.json';

const config = JSON.default;

describe('stamp class', () => {
    let stamp: Stamp;
    let color: Color;
    const coords = {x: 1, y: 1};
    let rendererSpy: any;
    const angle = 0;
    const factor = 50;
    const shape = 'heart';
    class MockColor extends Color {
        constructor() {
            super(0, 0, 0);
        }
    }
    beforeEach(async () => {
        TestBed.configureTestingModule({
            providers: [
                Renderer2,
                Coordinate,
                {provide: Color, useClass: MockColor},
            ]
        }).compileComponents();
    });
    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute', 'appendChild']);
        color = new MockColor();
        stamp = new Stamp(rendererSpy, color, color, coords, factor, angle, shape);
    });
    it('constructor should call renderer.createElement() 1 time', () => {
        expect(rendererSpy.createElement).toHaveBeenCalledTimes(1);
    });
    it('setPath should set the correct path for heart shape', () => {
        const pathExpected =    `m${stamp['coords'].x + config.shape.heart.x} ${stamp['coords'].y + config.shape.heart.y} `
                                + config.shape.heart.svg;
        stamp['shape'] = 'heart';
        stamp['setPath']();
        expect(stamp['path']).toBe(pathExpected);
    });

    it('setPath should set the correct path for star shape', () => {
        const pathExpected =    `m${stamp['coords'].x + config.shape.star.x} ${stamp['coords'].y + config.shape.star.y} `
                                + config.shape.star.svg;
        stamp['shape'] = 'star';
        stamp['setPath']();
        expect(stamp['path']).toBe(pathExpected);
    });

    it('setPath should set the correct path for arrow shape', () => {
        const pathExpected =    `m${stamp['coords'].x + config.shape.arrow.x} ${stamp['coords'].y + config.shape.arrow.y} `
                                + config.shape.arrow.svg;
        stamp['shape'] = 'arrow';
        stamp['setPath']();
        expect(stamp['path']).toBe(pathExpected);
    });

    it('setPath should set the correct path for thumbsUp shape', () => {
        const pathExpected =    `m${stamp['coords'].x + config.shape.thumbsUp.x} ${stamp['coords'].y + config.shape.thumbsUp.y} `
                                + config.shape.thumbsUp.svg;
        stamp['shape'] = 'thumbsUp';
        stamp['setPath']();
        expect(stamp['path']).toBe(pathExpected);
    });

    it('setPath should set the correct path for smile shape', () => {
        const pathExpected =    `m${stamp['coords'].x + config.shape.smile.x} ${stamp['coords'].y + config.shape.smile.y} `
                                + config.shape.smile.svg;
        stamp['shape'] = 'smile';
        stamp['setPath']();
        expect(stamp['path']).toBe(pathExpected);
    });

    it('setPath should set the correct path for heart shape if shape is not known', () => {
        const pathExpected =    `m${stamp['coords'].x + config.shape.heart.x} ${stamp['coords'].y + config.shape.heart.y} `
                                + config.shape.heart.svg;
        stamp['shape'] = 'banana';
        stamp['setPath']();
        expect(stamp['path']).toBe(pathExpected);
    });

    it('drawShape should call setPath and setAttribute 20 times (10 constructor and 10 function)', () => {
        spyOn<any>(stamp, 'setPath');
        stamp.drawShape();
        expect(stamp['setPath']).toHaveBeenCalled();
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(20);
    });

});
