import { Component } from '@angular/core';
import { CalligraphyService } from 'src/app/services/attributes/calligraphy/calligraphy.service';

@Component({
  selector: 'app-calligraphy',
  templateUrl: './calligraphy.component.html',
  styleUrls: ['./calligraphy.component.css']
})
export class CalligraphyComponent {

  protected service: CalligraphyService;

  constructor( service: CalligraphyService ) {
    this.service = service;
  }
}
