import { Component } from '@angular/core';
import { TextService } from 'src/app/services/attributes/text/text.service';

@Component({
  selector: 'app-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.css']
})
export class TextComponent {

  protected service: TextService;

  constructor( service: TextService ) {
    this.service = service;
  }

}
