// tslint:disable: no-any
import { SVGHolder } from '../svg-holder';
import { CreateShapeCommand } from './create-shape-command';

describe('CreateShapeCommand', () => {
    let command: CreateShapeCommand;
    let svgService: any;
    let shape: SVGHolder;

    beforeEach(() => {

        shape = jasmine.createSpyObj('SVGHolder', ['']);
        svgService = jasmine.createSpyObj('SVGChildService', ['addNewSVG', 'removeSVG']);

        command = new CreateShapeCommand(shape, svgService);
    });

    it('the do should call addNewSVG of the service and passing the shape', () => {
        command.do();

        expect(svgService.addNewSVG).toHaveBeenCalledTimes(1);
        expect(svgService.addNewSVG).toHaveBeenCalledWith(shape);
    });

    it('the undo should calle removeSVG of the service and passing the shape', () => {
        command.undo();

        expect(svgService.removeSVG).toHaveBeenCalledTimes(1);
        expect(svgService.removeSVG).toHaveBeenCalledWith(shape);
    });

});
