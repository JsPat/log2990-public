import { Command } from 'src/app/services/command-invoker/command';

export class MultiCommand implements Command {
    constructor(private commands: Command[]) {

    }

    addCommandToDo(cmd: Command): void {
        cmd.do();
        this.commands.push(cmd);
    }

    do(): void {
        for (const command of this.commands) {
            command.do();
        }
    }

    undo(): void {
        for (const command of this.commands.reverse()) {
            command.undo();
        }
    }
}
