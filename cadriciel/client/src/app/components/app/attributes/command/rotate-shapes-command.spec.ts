import { RotateShapesCommand } from './rotate-shapes-command';

describe('RotateShapeCommand', () => {
    let command: RotateShapesCommand;
    // tslint:disable: no-any
    // tslint:disable: no-magic-numbers
    // tslint:disable: no-string-literal

    beforeEach(() => {
        const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg') as SVGSVGElement;
        const matrix = svg.createSVGTransform();
        matrix.setRotate(15, 1, 1);
        const mockElement = document.createElementNS('http://www.w3.org/2000/svg', 'rect') as SVGRectElement;
        const elementArray = new Array<SVGGraphicsElement>();
        elementArray.push(mockElement);
        command = new RotateShapesCommand(elementArray, matrix);
        command.do();
    });
    it('should create' , () => {
        expect(command).toBeDefined();
    });
    it('using undo removes the last matrix from all Elements contained in the array', () => {
        command.undo();
        expect(command['shapeHolders'][0].transform.baseVal.numberOfItems).toEqual(0);
    });
    it('using do adds the translation matrix to all Elements contained in the array', () => {
        command.do();
        expect(command['shapeHolders'][0].transform.baseVal.numberOfItems).toEqual(2);
    });
});
