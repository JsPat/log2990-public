import { Command } from 'src/app/services/command-invoker/command';

export class TranslateShapesCommand implements Command {

    constructor(private shapeHolders: SVGGraphicsElement[], private translationMatrix: SVGTransform) {}

    do(): void {
        this.shapeHolders.forEach( (shape) => {
            shape.transform.baseVal.insertItemBefore(this.translationMatrix, 0);
        });
    }

    undo(): void {
        this.shapeHolders.forEach( (shape) => {
            shape.transform.baseVal.removeItem(0);
        });
    }

    replaceCurrentTranslationMatrix(matrix: SVGTransform): void {
        this.undo();
        this.translationMatrix = matrix;
        this.do();
    }

    combineTranslations(matrix: SVGTransform): void {
        this.undo();
        this.translationMatrix.setTranslate(this.translationMatrix.matrix.e + matrix.matrix.e,
                                            this.translationMatrix.matrix.f + matrix.matrix.f);
        this.do();
    }
}
