import { SelectorManipulationService } from 'src/app/services/attributes/selector/selector-manipulation.service';
import { Command } from 'src/app/services/command-invoker/command';
import { SVGChildService } from 'src/app/services/svg-child/svg-child.service';

export class PasteSelectionCommand implements Command {
    constructor(private shapes: SVGGraphicsElement[],
                private svgService: SVGChildService,
                translation: SVGTransform,
                private manipulation: SelectorManipulationService) {
        manipulation.manipulateNumberOfPastes(false);
        shapes.forEach((elem) => {
            elem.transform.baseVal.insertItemBefore(translation, 0);
        });
    }

    do(): void {
        this.manipulation.manipulateNumberOfPastes(true);
        this.shapes.forEach( (elem) => {
            this.svgService.addNewSVG(elem);
        });
    }

    undo(): void {
        this.manipulation.manipulateNumberOfPastes(false);
        this.shapes.forEach( (elem) => {
            this.svgService.removeSVG(elem);
        });
    }
}
