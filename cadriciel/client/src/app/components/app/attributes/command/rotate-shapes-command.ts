import { Command } from 'src/app/services/command-invoker/command';

export class RotateShapesCommand implements Command {

    constructor(private shapeHolders: SVGGraphicsElement[], private rotationMatrix: SVGTransform) {}

    do(): void {
        this.shapeHolders.forEach( (shape) => {
            shape.transform.baseVal.insertItemBefore(this.rotationMatrix, 0);
        });
    }

    undo(): void {
        this.shapeHolders.forEach( (shape) => {
            shape.transform.baseVal.removeItem(0);
        });
    }
}
