// tslint:disable no-any
// tslint:disable: no-string-literal
import { ShapesHolderService } from 'src/app/services/shapes-holder/shapes-holder.service';
import { SVGChildService } from 'src/app/services/svg-child/svg-child.service';
import { PasteSelectionCommand } from './paste-selection-command';

describe('pasteSelectionCommand', () => {
    let command: PasteSelectionCommand;
    let svgService: any;
    let selectionManipulationSpy: any;

    beforeEach(() => {
        svgService = new SVGChildService(new ShapesHolderService());
        selectionManipulationSpy = jasmine.createSpyObj('SelectorManipulationService', ['manipulateNumberOfPastes']);
        const shapeArray = new Array<SVGGraphicsElement>();
        const shape = document.createElementNS('http://www.w3.org/2000/svg', 'rect') as SVGRectElement;
        shapeArray.push(shape);
        const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg') as SVGSVGElement;
        const matrix = svg.createSVGTransform();
        matrix.setTranslate(1, 1);

        command = new PasteSelectionCommand(shapeArray, svgService, matrix, selectionManipulationSpy);
    });

    it('undo should set back the numberOfPastes by 1', () => {
        command.undo();
        expect(selectionManipulationSpy.manipulateNumberOfPastes).toHaveBeenCalledWith(false);
    });
    it('do should set forward the numberOfPastes by 1', () => {
        command.do();
        expect(selectionManipulationSpy.manipulateNumberOfPastes).toHaveBeenCalledWith(true);
    });

});
