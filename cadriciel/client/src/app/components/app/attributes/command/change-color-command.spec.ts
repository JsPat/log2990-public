// tslint:disable: no-any
// tslint:disable: no-string-literal
import { Color } from 'src/app/color-palette/color/color';
import { BLACK } from 'src/app/color-palette/constant';
import { ChangeColorCommand } from './change-color-command';

describe('ChangeColorCommand', () => {
    let command: ChangeColorCommand;
    let el: any;
    let colorTag: string;
    let transparencyTag: string;

    const newColor: Color = BLACK;
    const oldTransparency = 'oldTransparency';
    const oldRGB = 'oldRGB';
    const newRGB = `#${newColor.rgb_hexa}`;
    const newTransparency = `${newColor.a}`;

    beforeEach(() => {
        el = jasmine.createSpyObj('SVGGraphicsElement', ['setAttribute', 'getAttribute']);
        el.getAttribute.and.returnValues(oldRGB, oldTransparency);
        colorTag = 'colorTag';
        transparencyTag = 'transparencyTag';
        command = new ChangeColorCommand(el, colorTag, transparencyTag, newColor);
    });

    it('should create', () => {
        expect(command).toBeTruthy();
    });

    it('constructor should store none there is no oldcolor and no oldTransparency',  () => {
        el.getAttribute.and.returnValues(null, null);
        command = new ChangeColorCommand(el, colorTag, transparencyTag, newColor);
        expect(command['oldRGB']).toEqual('none');
        expect(command['oldTransparency']).toEqual('none');
    });

    it('constructor should store the old color of the element',  () => {
        expect(command['oldRGB']).toEqual(oldRGB);
        expect(command['oldTransparency']).toEqual(oldTransparency);
    });

    it('constructor should store the new color', () => {
        expect(command['newRGB']).toEqual(newRGB);
        expect(command['newTransparency']).toEqual(newTransparency);
    });

    it('do should set the element attribute with the new color', () => {
        command.do();

        expect(el.setAttribute).toHaveBeenCalledWith(colorTag, newRGB);
        expect(el.setAttribute).toHaveBeenCalledWith(transparencyTag, newTransparency);
    });

    it('undo should set the element attribute with the old color', () => {
        command.undo();

        expect(el.setAttribute).toHaveBeenCalledWith(colorTag, oldRGB);
        expect(el.setAttribute).toHaveBeenCalledWith(transparencyTag, oldTransparency);
    });

});
