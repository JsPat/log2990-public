import { Color } from 'src/app/color-palette/color/color';
import { Command } from 'src/app/services/command-invoker/command';

export class ChangeColorCommand implements Command {
    private readonly NO_COLOR: string = 'none';
    private oldRGB: string;
    private oldTransparency: string;
    private newRGB: string;
    private newTransparency: string;

    constructor(private el: SVGGraphicsElement, private colorTag: string, private transparencyTag: string, newColor: Color ) {
        const oldColor = this.el.getAttribute(this.colorTag);
        this.oldRGB = oldColor === null ? this.NO_COLOR : oldColor;

        const oldTransparency = this.el.getAttribute(this.transparencyTag);
        this.oldTransparency = oldTransparency === null ? this.NO_COLOR : oldTransparency;

        this.newRGB = `#${newColor.rgb_hexa}`;
        this.newTransparency = `${newColor.a}`;
    }

    do(): void {
        this.el.setAttribute(this.colorTag, this.newRGB);
        this.el.setAttribute(this.transparencyTag, this.newTransparency);
    }

    undo(): void {
        this.el.setAttribute(this.colorTag, this.oldRGB);
        this.el.setAttribute(this.transparencyTag, this.oldTransparency);
    }

}
