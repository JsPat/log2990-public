import { Renderer2 } from '@angular/core';
import { Command } from 'src/app/services/command-invoker/command';
import { SVGChildService } from 'src/app/services/svg-child/svg-child.service';

const ELEMENT_DEFS: string = 'defs';
const ELEMENT_SVG: string = 'svg';

export class RemoveShapeCommand implements Command {
    private placeHolder: SVGDefsElement;

    constructor(private svgService: SVGChildService,
                private renderer: Renderer2,
                private shape: SVGGraphicsElement) {
        this.placeHolder = this.renderer.createElement(ELEMENT_DEFS, ELEMENT_SVG);
    }

    do(): void {
        this.svgService.replaceSvg(this.placeHolder, this.shape);
    }

    undo(): void {
        this.svgService.replaceSvg(this.shape, this.placeHolder);
    }

}
