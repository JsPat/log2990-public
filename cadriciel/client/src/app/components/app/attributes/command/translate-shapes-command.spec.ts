import { TranslateShapesCommand } from './translate-shapes-command';

describe('TranslateShapeCommand', () => {
    let command: TranslateShapesCommand;
    // tslint:disable: no-any
    // tslint:disable: no-magic-numbers
    // tslint:disable: no-string-literal

    beforeEach(() => {
        const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg') as SVGSVGElement;
        const matrix = svg.createSVGTransform();
        matrix.setTranslate(1, 1);
        const mockElement = document.createElementNS('http://www.w3.org/2000/svg', 'rect') as SVGRectElement;
        const elementArray = new Array<SVGGraphicsElement>();
        elementArray.push(mockElement);
        command = new TranslateShapesCommand(elementArray, matrix);
        command.do();
    });
    it('should create' , () => {
        expect(command).toBeDefined();
    });
    it('combining translation matrices calls undo, combines the matrices and calls do', () => {
        spyOn(command, 'do');
        spyOn(command, 'undo');
        const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg') as SVGSVGElement;
        const matrix = svg.createSVGTransform();
        matrix.setTranslate(2, 3);
        command.combineTranslations(matrix);
        const expectedMatrix = svg.createSVGTransform();
        expectedMatrix.setTranslate(3, 4);
        expect(command.undo).toHaveBeenCalledTimes(1);
        expect(command['translationMatrix']).toEqual(expectedMatrix);
        expect(command.do).toHaveBeenCalledTimes(1);
    });
    it('replacing the existing translation matrix calls undo, changes the matrix stored in the command and calls do', () => {
        spyOn(command, 'do');
        spyOn(command, 'undo');
        const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg') as SVGSVGElement;
        const matrix = svg.createSVGTransform();
        matrix.setTranslate(2, 3);
        command.replaceCurrentTranslationMatrix(matrix);
        expect(command.undo).toHaveBeenCalledTimes(1);
        expect(command['translationMatrix']).toEqual(matrix);
        expect(command.do).toHaveBeenCalledTimes(1);
    });
    it('using undo removes the last matrix from all Elements contained in the array', () => {
        command.undo();
        expect(command['shapeHolders'][0].transform.baseVal.numberOfItems).toEqual(0);
    });
    it('using do adds the translation matrix to all Elements contained in the array', () => {
        command.do();
        expect(command['shapeHolders'][0].transform.baseVal.numberOfItems).toEqual(2);
    });
});
