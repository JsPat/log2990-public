import { Command } from 'src/app/services/command-invoker/command';
import { SVGChildService } from 'src/app/services/svg-child/svg-child.service';
import { SVGHolder } from '../svg-holder';

export class CreateShapeCommand implements Command {
    constructor(private shape: SVGHolder, private svgService: SVGChildService) { }

    do(): void {
        this.svgService.addNewSVG(this.shape);
    }

    undo(): void {
        this.svgService.removeSVG(this.shape);
    }
}
