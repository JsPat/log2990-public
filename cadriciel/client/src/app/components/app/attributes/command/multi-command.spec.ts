import { Command } from 'src/app/services/command-invoker/command';
import { MultiCommand } from './multi-command';

const nCommands = 5;

describe('MultiCommand', () => {
    let command: MultiCommand;
    let allCommands: Command[];

    beforeEach(() => {
        allCommands = new Array<Command>();
        for (let i = 0; i < nCommands; ++i) {
            allCommands.push(jasmine.createSpyObj('Command', ['do', 'undo']));
        }

        command = new MultiCommand(allCommands);
    });

    it('the do should call do on all commands', () => {
        command.do();

        for (const commandCalled of allCommands) {
            expect(commandCalled.do).toHaveBeenCalledTimes(1);
        }
    });

    it('the undo should call undo on all commands in reverse order', () => {
        command.undo();

        for (const commandCalled of allCommands.reverse()) {
            expect(commandCalled.undo).toHaveBeenCalledTimes(1);
        }
    });

    it('addCommandToDo, should call do of the new command then push it', () => {
        const cmd = jasmine.createSpyObj('Command', ['do']);
        command.addCommandToDo(cmd);

        expect(cmd.do).toHaveBeenCalled();
        // tslint:disable-next-line: no-string-literal
        expect(command['commands'].includes(cmd)).toBeTruthy();
    });

});
