// tslint:disable no-any
// tslint:disable: no-string-literal
import { Renderer2 } from '@angular/core';
import { ShapesHolderService } from 'src/app/services/shapes-holder/shapes-holder.service';
import { SVGChildService } from 'src/app/services/svg-child/svg-child.service';
import { RemoveShapeCommand } from './remove-shape-command';

describe('removeShapeCommand', () => {
    let command: RemoveShapeCommand;
    let svgService: any;
    let shape: SVGGraphicsElement;
    let rendererSpy: Renderer2;

    beforeEach(() => {
        svgService = new SVGChildService(new ShapesHolderService());
        rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement']);
        shape = jasmine.createSpyObj('SVGGraphicsElement', ['']);

        command = new RemoveShapeCommand(svgService, rendererSpy, shape);
    });

    // undo
    it('undo should call replaceSvg of svgService and passing the shape', () => {
        spyOn(command['svgService'], 'replaceSvg');
        command.undo();

        expect(svgService.replaceSvg).toHaveBeenCalledTimes(1);
    });

    it('undo should call replaceSvg of svgService and passing the shape', () => {
        spyOn(command['svgService'], 'replaceSvg');
        command.do();

        expect(svgService.replaceSvg).toHaveBeenCalledTimes(1);
    });

});
