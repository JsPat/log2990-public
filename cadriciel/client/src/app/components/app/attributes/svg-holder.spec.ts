// tslint:disable: no-any

import { SVGHolder } from './svg-holder';

class SVGElementStub extends SVGHolder {
    constructor(svg: any) {
        super();
        this.svg = svg;
    }
}

describe('SVGElement', () => {
    let svgElement: SVGElementStub;
    let svg: any;
    beforeEach(() => {
        svg = true;
        svgElement = new SVGElementStub(svg);
    });

    it('should return the SVGElement', () => {
        expect(svgElement.svgDrawElement).toEqual(svg);
    });
});
