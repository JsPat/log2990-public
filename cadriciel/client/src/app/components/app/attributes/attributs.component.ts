import { Component, HostListener, NgModule } from '@angular/core';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { ToolsService } from 'src/app/services/attributes/toolsService/tools-service';
import { PossibleTools } from '../../../services/constants';
import * as JSON from './attributs.json';

const config = JSON.default;

@Component({
  selector: 'app-attributs',
  templateUrl: './attributs.component.html',
  styleUrls: ['./attributs.component.css']
})

@NgModule({
  exports: [
    MatDividerModule , MatSliderModule, MatButtonToggleModule, MatFormFieldModule, MatSelectModule
  ]
})

export class AttributsComponent {

  tool: boolean[] = [];
  currentTool: PossibleTools = PossibleTools.SELECT;

  // tslint:disable-next-line: typedef because it is the type of the enum PossibleTools
  protected readonly PossibleTools = PossibleTools; // to access the enum in the html

  constructor(private toolsService: ToolsService) {
    this.tool.fill(false, 0, PossibleTools.N_TOOLS - 1);
    this.currentTool = this.toolsService.currentTool;
    this.tool[this.currentTool] = true;
  }

  toggle(elem: PossibleTools): void {
    if (!this.tool[elem]) {
      this.tool[elem] = true;
      this.tool[this.currentTool] = false;
      this.currentTool = elem;
      this.toolsService.toggle(elem);
    }
  }

  @HostListener(config.keyDownC, [config.event]) shortcutPen(): void {
    this.toggle(PossibleTools.PEN);
  }

  @HostListener(config.keyDownW, [config.event]) shortcutPaintBrush(): void {
    this.toggle(PossibleTools.PAINTBRUSH);
  }

  @HostListener(config.keyDownB, [config.event]) shortcutPaintBucket(): void {
    this.toggle(PossibleTools.PAINTBUCKET);
  }

  @HostListener(config.keyDownD, [config.event]) shortcutStamp(): void {
    this.toggle(PossibleTools.STAMP);
  }

  @HostListener(config.keyDownA, [config.event]) shortcutSprayPaint(): void {
    this.toggle(PossibleTools.SPRAYPAINT);
  }

  @HostListener(config.keyDown1, [config.event]) shortcutRectangle(): void {
    this.toggle( PossibleTools.RECTANGLE);
  }

  @HostListener(config.keyDown2, [config.event]) shortcutEllipse(): void {
    this.toggle(PossibleTools.ELLIPSE);
  }

  @HostListener(config.keyDown3, [config.event]) shortcutPolygone(): void {
    this.toggle(PossibleTools.POLYGONE);
  }

  @HostListener(config.keyDownL, [config.event]) shortcutLine(): void {
    this.toggle(PossibleTools.LINE);
  }

  @HostListener(config.keyDownR, [config.event]) shortcutColorizer(): void {
    this.toggle(PossibleTools.COLORIZE);
  }

  @HostListener(config.keyDownE, [config.event]) shortcutEraser(): void {
    this.toggle(PossibleTools.ERASER);
  }

  @HostListener(config.keyDownI, [config.event]) shortcutColorPicker(): void {
    this.toggle(PossibleTools.PIPETTE);
  }

  @HostListener(config.keyDownS, [config.event]) shortcutSelect(): void {
    this.toggle(PossibleTools.SELECT);
  }
}
