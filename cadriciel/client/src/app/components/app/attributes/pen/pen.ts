import { Renderer2 } from '@angular/core';
import { Color } from 'src/app/color-palette/color/color';
import {Coordinate} from '../coordinate';
import { SVGHolder } from '../svg-holder';
import * as JSON from './pen.json';

const config = JSON.default;

export class Pen extends SVGHolder {
    protected dotsToWrite: string;
    protected color: Color;
    protected strokeWidth: number;
    protected renderer: Renderer2;

    constructor(dot: Coordinate, color: Color, strokeWidth: number, renderer: Renderer2) {
        super();
        this.dotsToWrite = `${dot.x},${dot.y} `;
        this.color = color;
        this.strokeWidth = strokeWidth;

        this.renderer = renderer;

        this.svg = this.renderer.createElement(config.element.type, config.element.from);
    }

    changeMainSize(size: number): void {
        this.renderer.setAttribute(this.svg, config.line.width, `${size}`);
    }

    protected drawShape(): void {
        this.renderer.setAttribute(this.svg, config.line.fill, config.none);
        this.renderer.setAttribute(this.svg, config.line.color, `#${this.color.rgb_hexa}`);
        this.renderer.setAttribute(this.svg, config.line.width, `${this.strokeWidth}`);
        this.renderer.setAttribute(this.svg, config.line.opacity, `${this.color.a_string}`);
        this.renderer.setAttribute(this.svg, config.line.cap, config.round);
        this.renderer.setAttribute(this.svg, config.line.join, config.round);
        this.renderer.setAttribute(this.svg, config.points, `${this.dotsToWrite}`);
        this.renderer.setAttribute(this.svg, config.selectable.identifier, config.selectable.true);
        this.renderer.setAttribute(this.svg, config.mainColor, config.line.color);
        this.renderer.setAttribute(this.svg, config.secondaryColor, config.none);
        this.renderer.setAttribute(this.svg, config.eraserPreview, config.line.color);
    }

    pushDot(dot: Coordinate): void {
        this.dotsToWrite = this.dotsToWrite.concat(`${dot.x},${dot.y} `);
        this.drawShape();
    }
}
