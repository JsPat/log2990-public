import { Component } from '@angular/core';
import { PenService } from 'src/app/services/attributes/pen/pen.service';

@Component({
  selector: 'app-pen',
  templateUrl: './pen.component.html',
  styleUrls: ['./pen.component.css']
})

export class PenComponent {
  protected service: PenService;

  constructor(service: PenService) {
    this.service = service;
  }
}
