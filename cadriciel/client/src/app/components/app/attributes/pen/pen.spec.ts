// tslint:disable: no-string-literal
// tslint:disable: no-any
// tslint:disable: no-magic-numbers

import { TestBed } from '@angular/core/testing';

import { Renderer2 } from '@angular/core';
import { Color } from 'src/app/color-palette/color/color';
import { Coordinate } from '../coordinate';
import { Pen } from './pen';

describe('pen class', () => {
    let pen: Pen;
    let color: Color;
    let position: Coordinate;
    let rendererSpy: any;

    class MockColor extends Color {
        constructor() {
            super(0, 0, 0);
        }
    }

    beforeEach(async () => {
        TestBed.configureTestingModule({
            providers: [
                Renderer2,
                Coordinate,
                {provide: Color, useClass: MockColor}
            ]
        }).compileComponents();
    });

    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute', 'appendChild']);
        color = new MockColor();
        position = new Coordinate();
        position.x = 1;
        position.y = 1;
        pen = new Pen(position, color, 5, rendererSpy);
    });

    it('constructor should call renderer.createElement()', () => {
        expect(rendererSpy.createElement).toHaveBeenCalled();
    });

    it('changeMainSize should call renderer.setAttribute()', () => {
        pen.changeMainSize(1);
        expect(rendererSpy.setAttribute).toHaveBeenCalled();
    });

    it('drawShape should setAttribute to update the fill to none', () => {
        pen['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(pen['svg'], 'fill', 'none');
    });

    it('drawShape should setAttribute to update the stroke to the color', () => {
        pen['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(pen['svg'], 'stroke', `#${pen['color'].rgb_hexa}`);
    });

    it('drawShape should setAttribute to update the stroke-width to the current strokewidth', () => {
        pen['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(pen['svg'], 'stroke-width', `${pen['strokeWidth']}`);
    });

    it('drawShape should setAttribute to update the stroke-opacity to opacity of the color', () => {
        pen['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(pen['svg'], 'stroke-opacity', `${pen['color'].a_string}`);
    });

    it('drawShape should setAttribute to update the stroke-linejoin to the round', () => {
        pen['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(pen['svg'], 'stroke-linejoin', 'round');
    });

    it('drawShape should setAttribute to update the stroke-linecap to the round', () => {
        pen['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(pen['svg'], 'stroke-linecap', 'round');
    });

    it('drawShape should setAttribute to update the points to the dotsToWrtie', () => {
        pen['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(pen['svg'], 'points', `${pen['dotsToWrite']}`);
    });

    it('drawShape should setAttribute to to be selectable', () => {
        pen['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(pen['svg'], 'data-is-selectable', 'true');
    });

    it('drawShape should setAttribute to set the eraser preview color', () => {
        pen['drawShape']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(pen['svg'], 'data-eraser-preview', 'stroke');
    });

    it('push dots should add the new dots given', () => {
        pen.pushDot({x: 2, y: 3});
        expect(pen['dotsToWrite'].endsWith(`${2},${3} `)).toBeTruthy();
    });
});
