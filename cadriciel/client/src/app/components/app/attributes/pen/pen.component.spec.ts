/* tslint:disable:no-unused-variable */
/* tslint:disable:no-string-literal*/
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PenService } from 'src/app/services/attributes/pen/pen.service';
import { PenComponent } from './pen.component';

describe('PenComponent', () => {
  let component: PenComponent;
  let fixture: ComponentFixture<PenComponent>;
  let serviceSpy: PenService;

  beforeEach(async(() => {

    serviceSpy = jasmine.createSpyObj('PenService', ['']);

    TestBed.configureTestingModule({
      declarations: [ PenComponent ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [ {provide: PenService, useValue: serviceSpy} ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have the right service for his attibute', () => {
    expect(component['service']).toEqual(serviceSpy);
  });

});
