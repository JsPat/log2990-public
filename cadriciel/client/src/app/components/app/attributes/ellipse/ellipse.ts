import { Renderer2 } from '@angular/core';
import { Color } from 'src/app/color-palette/color/color';
import { Coordinate } from '../coordinate';
import { SVGHolder } from '../svg-holder';
import * as JSON from './ellipse.json';

const config = JSON.default;

export class Ellipse extends SVGHolder {
    readonly PREVIEW_COLOR: Color;

    private position: Coordinate;
    private height: number;
    private width: number;
    private color: Color;
    private strokeWidth: number;
    private strokeColor: Color;

    private border: boolean;
    private fill: boolean;

    private start: Coordinate;

    constructor(start: Coordinate, rx: number, ry: number, color: Color,
                strokeWidth: number, strokeColor: Color, border: boolean,
                fill: boolean, private renderer: Renderer2) {
        super(); // svgElement
        this.start = start;
        this.width = rx;
        this.height = ry;
        this.color = color;
        this.strokeWidth = strokeWidth;
        this.strokeColor = strokeColor;
        this.svg = this.renderer.createElement(config.element.type, config.element.from);
        this.PREVIEW_COLOR = new Color(0, 0, 0);
        this.border = border;
        this.fill = fill;
    }

    drawShape(border: boolean, fill: boolean): void {
      this.renderer.setAttribute(this.svg, config.position.x, `${this.position.x}`);
      this.renderer.setAttribute(this.svg, config.position.y, `${this.position.y}`);
      this.renderer.setAttribute(this.svg, config.dimension.width, `${this.width}`);
      this.renderer.setAttribute(this.svg, config.dimension.height, `${this.height}`);
      this.renderer.setAttribute(this.svg, config.fill.opacity, `${this.color.a}`);
      this.renderer.setAttribute(this.svg, config.selectable.identifier, config.selectable.true);
      this.renderer.setAttribute(this.svg, config.eraserPreview, config.border.color);

      if (border) {
          this.renderer.setAttribute(this.svg, config.border.color, `#${this.strokeColor.rgb_hexa}`);
          this.renderer.setAttribute(this.svg, config.border.opacity, `${this.strokeColor.a}`);
          this.renderer.setAttribute(this.svg, config.border.width, `${this.strokeWidth}`);
          this.renderer.setAttribute(this.svg, config.border.position, config.border.inside);
          this.renderer.setAttribute(this.svg, config.secondaryColor, config.border.color);
      } else {
          this.renderer.setAttribute(this.svg, config.secondaryColor, config.none);
      }
      if (fill) {
        this.renderer.setAttribute(this.svg, config.mainColor, config.fill.color);
        this.renderer.setAttribute(this.svg, config.fill.color, `#${this.color.rgb_hexa}`);
      } else {
          this.renderer.setAttribute(this.svg, config.fill.color, config.none);
          this.renderer.setAttribute(this.svg, config.mainColor, config.none);
      }
    }

    updateShape(mousePosition: Coordinate, shiftPressed: boolean): void {
        this.position = mousePosition;
        this.height = this.findHeight();
        this.width = this.findWidth();
        this.strokeWidth = this.strokeWidth;
        if (shiftPressed) {
          this.width = this.height = Math.min(this.height, this.width);
        }
        this.position = this.changeStartingPosition(this.start, this.position, shiftPressed);
        this.drawShape(this.border, this.fill);
    }

    isNull(): boolean {
      return this.height === 0 || this.width === 0;
    }

    private changeStartingPosition(startPosition: Coordinate, mousePosition: Coordinate, shiftPressed: boolean): Coordinate {
      let x = 0;
      let y = 0;
      if (shiftPressed) {
        x = startPosition.x + Math.min(this.height, this.width) * ((mousePosition.x < startPosition.x) ? -1 : 1);
        y = startPosition.y + Math.min(this.height, this.width) * ((mousePosition.y < startPosition.y) ? -1 : 1);
      } else {
        x = startPosition.x + this.width * ((mousePosition.x < startPosition.x) ? -1 : 1);
        y = startPosition.y + this.height * ((mousePosition.y < startPosition.y) ? -1 : 1);
      }
      return {x, y};
    }

    private findWidth(): number {
      return Math.abs(this.start.x - this.position.x) / 2;
    }

    private findHeight(): number {
      return Math.abs(this.start.y - this.position.y) / 2;
    }
}
