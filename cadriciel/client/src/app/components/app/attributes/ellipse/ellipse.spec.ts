// tslint:disable: no-string-literal
// tslint:disable: no-magic-numbers
// tslint:disable: no-any
import { TestBed } from '@angular/core/testing';

import { Renderer2 } from '@angular/core';
import { Color } from 'src/app/color-palette/color/color';
import { Coordinate } from '../coordinate';
import { Ellipse } from './ellipse';

describe('ellipse class', () => {
  let ellipse: Ellipse;
  let color: Color;
  let strokeColor: Color;
  let position: Coordinate;
  let rendererSpy: any;

  class MockColor extends Color {
      constructor() {
          super(0, 0, 0);
      }
  }

  beforeEach(async () => {
      TestBed.configureTestingModule({
          providers: [
              Renderer2,
              Coordinate,
              {provide: Color, useClass: MockColor}
          ]
      }).compileComponents();
  });
  beforeEach(() => {
      rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute', 'addNewSVG']);
      color = new MockColor();
      strokeColor = new MockColor();
      position = new Coordinate();
      position.x = 1;
      position.y = 1;
      ellipse = new Ellipse(position, 10, 10, color, 5, strokeColor, true, true, rendererSpy);
  });

  // constructor
  it('constructor should call renderer.createElement()', () => {
      expect(rendererSpy.createElement).toHaveBeenCalled();
  });

  it('constructor should call renderer.createElement()', () => {
      expect(rendererSpy.createElement).toHaveBeenCalled();
  });
  it('constructor should set the attributes', () => {
      const newColor = new MockColor();
      const newStrokeColor = new MockColor();
      const newPosition = new Coordinate();
      newPosition.x = 100;
      newPosition.y = 100;
      const newCx = 100;
      const newCy = 100;
      const newStroke = 100;
      ellipse = new Ellipse(newPosition, newCx, newCy, newColor, newStroke, newStrokeColor, true, true, rendererSpy);
      expect(ellipse['start']).toEqual(newPosition);
      expect(ellipse['width']).toEqual(newCx);
      expect(ellipse['height']).toEqual(newCy);
      expect(ellipse['color']).toEqual(newColor);
      expect(ellipse['strokeWidth']).toEqual(newStroke);
      expect(ellipse['strokeColor']).toEqual(newStrokeColor);
  });

  // drawShape
  it('drawShape should call renderer.setAttribute() 14 times if border=true, fill=true', () => {
    ellipse['position'] = {x: 10, y: 6};
    ellipse.drawShape(true, true);
    expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(14);
  });
  it('drawShape should call renderer.setAttribute() 14 times if border=true, fill=false', () => {
    ellipse['position'] = {x: 10, y: 6};
    ellipse.drawShape(true, false);
    expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(14);
  });
  it('drawShape should call renderer.setAttribute() 10 times if border=false, fill=true', () => {
    ellipse['position'] = {x: 10, y: 6};
    ellipse.drawShape(false, true);
    expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(10);
  });
  it('drawShape should call renderer.setAttribute() 10 times if border=false, fill=false', () => {
    ellipse['position'] = {x: 10, y: 6};
    ellipse.drawShape(false, false);
    expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(10);
  });
  // updateShape()
  it('updateShape should assign values to attributes if ellipse is defined', () => {
    spyOn(ellipse, 'updateShape').and.callThrough();
    ellipse['strokeWidth'] = 100;
    ellipse = new Ellipse({x: 0, y: 0}, 0, 0, color, 0, strokeColor, true, true, rendererSpy);
    ellipse['updateShape']({x: 0, y: 0}, false);
    expect(ellipse['strokeWidth']).toEqual(ellipse['strokeWidth']);
  });
  it('updateShape should assign same min value to ellipse.width and ellipse.height if shiftPressed (height<width)', () => {
    const minValue = 17;
    const maxValue = 53;
    ellipse = new Ellipse({x: 0, y: 0}, minValue, maxValue, color, 0, strokeColor, true, true, rendererSpy);
    ellipse['updateShape']({x: 34, y: 106}, true);
    expect(ellipse['width']).toEqual(minValue);
    expect(ellipse['height']).toEqual(minValue);
  });
  it('updateShape should assign same min value to ellipse.width and ellipse.height if shiftPressed (height>width)', () => {
    const minValue = 12;
    const maxValue = 23;
    ellipse = new Ellipse({x: 0, y: 0}, maxValue, minValue, color, 0, strokeColor, true, true, rendererSpy);
    ellipse['updateShape']({x: 46, y: 24}, true);
    expect(ellipse['width']).toEqual(minValue);
    expect(ellipse['height']).toEqual(minValue);
  });
  it('updateShape should assign same min value to ellipse.width and ellipse.height if shiftPressed (negative height)', () => {
    const minValue = -12;
    const maxValue = 23;
    ellipse = new Ellipse({x: 0, y: 0}, minValue, maxValue, color, 0, strokeColor, true, true, rendererSpy);
    ellipse['updateShape']({x: -24, y: 46}, true);
    expect(ellipse['width']).toEqual(12);
    expect(ellipse['height']).toEqual(12);
  });
  it('updateShape should assign same min value to ellipse.width and ellipse.height if shiftPressed (negative width)', () => {
    const minValue = -23;
    const maxValue = 12;
    ellipse = new Ellipse({x: 0, y: 0}, maxValue, minValue, color, 0, strokeColor, true, true, rendererSpy);
    ellipse['updateShape']({x: -46, y: 24}, true);
    expect(ellipse['width']).toEqual(maxValue);
    expect(ellipse['height']).toEqual(maxValue);
  });
  it('updateShape should assign same min value to ellipse.width and ellipse.height if shiftPressed (already equal)', () => {
    const minValue = 12;
    const maxValue = 12;
    ellipse = new Ellipse({x: 0, y: 0}, maxValue, minValue, color, 0, strokeColor, true, true, rendererSpy);
    ellipse['updateShape']({x: 24, y: 24}, true);
    expect(ellipse['width']).toEqual(minValue);
    expect(ellipse['height']).toEqual(minValue);
  });
  // changeStartingPosition()
  it('changeStartingPosition should return startPosition + width and height if position > startPosition', () => {
    const testStartPos = {x: 2, y: 2};
    const testCurrentPos = {x: 3, y: 3};
    ellipse['height'] = 1;
    ellipse['width'] = 1;
    const newPos = ellipse['changeStartingPosition'](testStartPos, testCurrentPos, false);
    expect(newPos).toEqual({x: 3, y: 3});
  });
  it('changeStartingPosition should return startPosition - {ellipse.width, ellipse.height} if position < startPosition', () => {
    const testStartPos = {x: 4, y: 4};
    const testCurrentPos = {x: 3, y: 3};
    ellipse['height'] = 1;
    ellipse['width'] = 1;
    const newPos = ellipse['changeStartingPosition'](testStartPos, testCurrentPos, false);
    expect(newPos).toEqual({x: testStartPos.x - ellipse['width'], y: testStartPos.y - ellipse['height']});
  });
  it('changeStartingPosition should return startPosition if position == startPosition', () => {
    const testStartPos = {x: 3, y: 3};
    const testCurrentPos = {x: 3, y: 3};
    ellipse['height'] = 0;
    ellipse['width'] = 0;
    const newPos = ellipse['changeStartingPosition'](testStartPos, testCurrentPos, false);
    expect(newPos).toEqual(testStartPos);
  });
  it('changeStartingPosition should return startPosition + {ellipse.height} if position.x < startPosition.x', () => {
    const testStartPos = {x: 4, y: 3};
    const testCurrentPos = {x: 3, y: 3};
    ellipse['height'] = 1;
    ellipse['width'] = 1;
    const newPos = ellipse['changeStartingPosition'](testStartPos, testCurrentPos, false);
    expect(newPos).toEqual({x: testStartPos.x - ellipse['width'], y: testStartPos.y +  ellipse['height']});
  });
  it('changeStartingPosition should return startPosition - {ellipse.height} if position.y < startPosition.y', () => {
    const testStartPos = {x: 3, y: 4};
    const testCurrentPos = {x: 3, y: 3};
    ellipse['height'] = 1;
    ellipse['width'] = 1;
    const newPos = ellipse['changeStartingPosition'](testStartPos, testCurrentPos, false);
    expect(newPos).toEqual({x: testStartPos.x + ellipse['width'], y: testStartPos.y - ellipse['height']});
  });
  it('changeStartingPosition should return startPosition if position > startPosition (negative values)', () => {
    const testStartPos = {x: -4, y: -4};
    const testCurrentPos = {x: -3, y: -3};
    ellipse['height'] = 1;
    ellipse['width'] = 1;
    const newPos = ellipse['changeStartingPosition'](testStartPos, testCurrentPos, false);
    expect(newPos).toEqual({x: -3, y: -3});
  });
  it('should return startPosition - {ellipse.width, ellipse.height} if position < startPosition (negative values)', () => {
    const testStartPos = {x: -2, y: -2};
    const testCurrentPos = {x: -3, y: -3};
    ellipse['height'] = 1;
    ellipse['width'] = 1;
    const newPos = ellipse['changeStartingPosition'](testStartPos, testCurrentPos, false);
    expect(newPos).toEqual({x: testStartPos.x - ellipse['width'], y: testStartPos.y - ellipse['height']});
  });
  it('ChangeStaringPosition should return the new x and y if shift is pressed while position < startPosition', () => {
    const tempStart = {x: 10, y: 10};
    const mouse = {x: 5, y: 5};
    ellipse['height'] = 5;
    ellipse['width'] = 5;
    const result = ellipse['changeStartingPosition'](tempStart, mouse, true);
    expect(result.x).toEqual(tempStart.x - 5);
    expect(result.y).toEqual(tempStart.y - 5);
  });
  it('ChangeStaringPosition should return the new x and y if shift is pressed while position > startPosition', () => {
    const  mouse = {x: 10, y: 12};
    const start = {x: 5, y: 6};
    ellipse['height'] = 5;
    ellipse['width'] = 5;
    const newPos = ellipse['changeStartingPosition'](start, mouse, false);
    expect(newPos.x).toEqual(start.x + ellipse['width']);
  });
  // findWidth()
  it('findWidth() should return the absolute value of start.x - position.x when start.x > position.x', () => {
    const position1 = {x: 10, y: 10};
    const position2 = {x: 5, y: 5};
    ellipse['start'] = position1;
    ellipse['position'] = position2;
    const width = ellipse['findWidth']();
    expect(width).toEqual(Math.abs(position1.x - position2.x) / 2);
  });
  it('findWidth() should return the absolute value of start.x - position.x when start.x < position.x', () => {
    const position1 = {x: 5, y: 5};
    const position2 = {x: 10, y: 10};
    ellipse['start'] = position1;
    ellipse['position'] = position2;
    const width = ellipse['findWidth']();
    expect(width).toEqual(Math.abs(position1.x - position2.x) / 2);
  });
  it('findWidth() should return the absolute value of start.x - position.x when start.x = position.x', () => {
    const position1 = {x: 5, y: 5};
    const position2 = {x: 5, y: 5};
    ellipse['start'] = position1;
    ellipse['position'] = position2;
    const width = ellipse['findWidth']();
    expect(width).toEqual(Math.abs(position1.x - position2.x) / 2);
  });
  it('findWidth() should return the absolute value of start.x - position.x when start.x is negative', () => {
    const position1 = {x: -1, y: 5};
    const position2 = {x: 2, y: 5};
    ellipse['start'] = position1;
    ellipse['position'] = position2;
    const width = ellipse['findWidth']();
    expect(width).toEqual(Math.abs(position1.x - position2.x) / 2);
  });
  it('findWidth() should return the absolute value of start.x - position.x when position.x is negative', () => {
    const position1 = {x: 1, y: 5};
    const position2 = {x: -2, y: 5};
    ellipse['start'] = position1;
    ellipse['position'] = position2;
    const width = ellipse['findWidth']();
    expect(width).toEqual(Math.abs(position1.x - position2.x) / 2);
  });
  it('findWidth() should return the absolute value of start.x - position.x when start.x and position.x are negative', () => {
    const position1 = {x: -1, y: 5};
    const position2 = {x: -2, y: 5};
    ellipse['start'] = position1;
    ellipse['position'] = position2;
    const width = ellipse['findWidth']();
    expect(width).toEqual(Math.abs(position1.x - position2.x) / 2);
  });
  // findHeight()
  it('findHeight() should return the absolute value of start.y - position.y when start.y > position.y', () => {
    const position1 = {x: 5, y: 10};
    const position2 = {x: 5, y: 5};
    ellipse['start'] = position1;
    ellipse['position'] = position2;
    const height = ellipse['findHeight']();
    expect(height).toEqual(Math.abs(position1.y - position2.y) / 2);
  });
  it('findHeight() should return the absolute value of start.y - position.y when start.y < position.y', () => {
    const position1 = {x: 5, y: 5};
    const position2 = {x: 5, y: 10};
    ellipse['start'] = position1;
    ellipse['position'] = position2;
    const height = ellipse['findHeight']();
    expect(height).toEqual(Math.abs(position1.y - position2.y) / 2);
  });
  it('findHeight() should return the absolute value of start.y - position.y when start.y = position.y', () => {
    const position1 = {x: 5, y: 5};
    const position2 = {x: 5, y: 5};
    ellipse['start'] = position1;
    ellipse['position'] = position2;
    const height = ellipse['findHeight']();
    expect(height).toEqual(Math.abs(position1.y - position2.y) / 2);
  });
  it('findHeight() should return the absolute value of start.y - position.y when start.y is negative', () => {
    const position1 = {x: 5, y: -1};
    const position2 = {x: 5, y: 2};
    ellipse['start'] = position1;
    ellipse['position'] = position2;
    const height = ellipse['findHeight']();
    expect(height).toEqual(Math.abs(position1.y - position2.y) / 2);
  });
  it('findHeight() should return the absolute value of start.y - position.y when position.y is negative', () => {
    const position1 = {x: 5, y: 1};
    const position2 = {x: 5, y: -2};
    ellipse['start'] = position1;
    ellipse['position'] = position2;
    const height = ellipse['findHeight']();
    expect(height).toEqual(Math.abs(position1.y - position2.y) / 2);
  });
  it('findHeight() should return the absolute value of start.y - position.y when start.y and position.y are negative', () => {
    const position1 = {x: 5, y: -1};
    const position2 = {x: 5, y: -2};
    ellipse['start'] = position1;
    ellipse['position'] = position2;
    const height = ellipse['findHeight']();
    expect(height).toEqual(Math.abs(position1.y - position2.y) / 2);
  });

  it('isNull should return true if the ellipse width is null and height is not null', () => {
    ellipse['height'] = 2;
    ellipse['width'] = 0;
    expect(ellipse['isNull']()).toBeTruthy();
  });

  it('isNull should return true if the ellipse width is not null and height is null', () => {
      ellipse['height'] = 0;
      ellipse['width'] = 3;
      expect(ellipse['isNull']()).toBeTruthy();
  });

  it('isNull should return false if the ellipse width is not null and height is not null', () => {
      ellipse['height'] = 2;
      ellipse['width'] = 3;
      expect(ellipse['isNull']()).toBeFalsy();
  });

});
