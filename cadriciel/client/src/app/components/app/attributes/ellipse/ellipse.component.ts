import { Component } from '@angular/core';
import { EllipseService } from 'src/app/services/attributes/ellipse/ellipse.service';

@Component({
  selector: 'app-ellipse',
  templateUrl: './ellipse.component.html',
  styleUrls: ['./ellipse.component.css']
})
export class EllipseComponent {

  protected borderButtonDisabled: boolean;
  protected fillButtonDisabled: boolean;

  protected service: EllipseService;

  constructor(service: EllipseService) {
   this.service = service;
   this.onBorderButtonChange();
   this.onFillButtonChange();
  }

  onBorderButtonChange(): void {
    this.fillButtonDisabled = this.service.fillEnabled && !this.service.borderEnabled;
  }

  onFillButtonChange(): void {
    this.borderButtonDisabled = this.service.borderEnabled && !this.service.fillEnabled;
  }

}
