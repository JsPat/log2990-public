/* tslint:disable:no-unused-variable */
/* tslint:disable:no-string-literal*/
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EllipseService } from 'src/app/services/attributes/ellipse/ellipse.service';
import { EllipseComponent } from './ellipse.component';

describe('EllipseComponent', () => {
  let component: EllipseComponent;
  let fixture: ComponentFixture<EllipseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [ EllipseService ],
      declarations: [ EllipseComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EllipseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('fillButtonDisabled should be false if borderEnabled is true when onBorderButtonChange is called', () => {
    component['service'].borderEnabled = true;
    component.onBorderButtonChange();
    expect(component['fillButtonDisabled']).toBeFalsy();
  });
  it('fillButtonDisabled should be true if borderEnabled is false when onBorderButtonChange is called', () => {
    component['service'].borderEnabled = false;
    component.onBorderButtonChange();
    expect(component['fillButtonDisabled']).toBeTruthy();
  });
  it('borderButtonDisabled should be false if fillEnabled is true when onFillButtonChange is called', () => {
    component['service'].fillEnabled = true;
    component.onFillButtonChange();
    expect(component['borderButtonDisabled']).toBeFalsy();
  });
  it('borderButtonDisabled should be true if fillEnabled is false when onFillButtonChange is called', () => {
    component['service'].fillEnabled = false;
    component.onFillButtonChange();
    expect(component['borderButtonDisabled']).toBeTruthy();
  });
});
