const TRUE = 'true';

export class SVGSelector {
    selectAtPosition(event: MouseEvent): SVGGraphicsElement | null {
        const el = event.target as SVGGraphicsElement ;
        if (el === null || !this.isSelectable(el)) {
            return null;
        }
        return this.getParentIfneeded(el);
    }

    getParentIfneeded(el: SVGGraphicsElement | null): SVGGraphicsElement | null {
        if (el === null) { return null; }

        const takeParent = el.dataset.takeParent;

        if (takeParent === TRUE) {
            return this.getParentIfneeded(el.parentElement as SVGGraphicsElement | null);
        } else {
            return el;
        }
    }

    isSelectable(el: SVGGraphicsElement): boolean {
        const isSelectable = el.dataset.isSelectable;
        return isSelectable === TRUE;
    }
}
