import { SVGSelector } from './svg-selector';

describe('SVGSelector', () => {

    let selector: SVGSelector;

    beforeEach(() => {
        selector = new SVGSelector();
    });

    it('should create', () => {
        expect(selector).toBeTruthy();
    });

    it('selectAtPosition should return null if event as no target', () => {
        const event = new MouseEvent('mousedown');
        Object.defineProperty(event, 'target', {value: null});

        expect(selector.selectAtPosition(event)).toBeNull();
    });

    it('selectAtPosition should return null if the target element has not field isSelectable', () => {
        const event = new MouseEvent('mousedown');
        const target = jasmine.createSpyObj('SVGElement', ['']);
        const dataset = {};

        Object.defineProperty(target, 'dataset', {value: dataset});
        Object.defineProperty(event, 'target', {value: target});

        expect(selector.selectAtPosition(event)).toBeNull();
    });

    it('selectAtPosition should return null if the target element is not selectable', () => {
        const event = new MouseEvent('mousedown');
        const target = jasmine.createSpyObj('SVGElement', ['']);
        const dataset = {
            isSelectable: 'false',
        };

        Object.defineProperty(target, 'dataset', {value: dataset});
        Object.defineProperty(event, 'target', {value: target});

        expect(selector.selectAtPosition(event)).toBeNull();
    });

    it('selectAtPosition should return the event target if the target element is selectable and has no field takeParent', () => {
        const event = new MouseEvent('mousedown');
        const target = jasmine.createSpyObj('SVGElement', ['']);
        const dataset = {
            isSelectable: 'true',
        };

        Object.defineProperty(target, 'dataset', {value: dataset});
        Object.defineProperty(event, 'target', {value: target});

        expect(selector.selectAtPosition(event)).toEqual(target);
    });

    it('selectAtPosition should return the event target if the target element is selectable and takeParent is set to false', () => {
        const event = new MouseEvent('mousedown');
        const target = jasmine.createSpyObj('SVGElement', ['']);
        const dataset = {
            isSelectable: 'true',
            takeParent: 'false',
        };

        Object.defineProperty(target, 'dataset', {value: dataset});
        Object.defineProperty(event, 'target', {value: target});

        expect(selector.selectAtPosition(event)).toEqual(target);
    });

    it('selectAtPosition should return the event target parent if the target element is selectable and takeParent is set to true', () => {
        const event = new MouseEvent('mousedown');
        const target = jasmine.createSpyObj('SVGElement', ['']);
        const parent = jasmine.createSpyObj('SVGElement', ['']);
        const dataset = {
            isSelectable: 'true',
            takeParent: 'true',
        };
        const datasetParent = {
            takeParent: 'false',
        };

        Object.defineProperty(target, 'dataset', {value: dataset});
        Object.defineProperty(target, 'parentElement', {value: parent});
        Object.defineProperty(parent, 'dataset', {value: datasetParent});
        Object.defineProperty(event, 'target', {value: target});

        expect(selector.selectAtPosition(event)).toEqual(parent);
    });

    it('selectAtPosition should return the null if the target element is selectable and takeParent is set to true \
    and his parent is null', () => {
        const event = new MouseEvent('mousedown');
        const target = jasmine.createSpyObj('SVGElement', ['']);
        const parent = jasmine.createSpyObj('SVGElement', ['']);
        const dataset = {
            isSelectable: 'true',
            takeParent: 'true',
        };
        const datasetParent = {
            takeParent: 'true',
        };

        Object.defineProperty(target, 'dataset', {value: dataset});
        Object.defineProperty(target, 'parentElement', {value: parent});

        Object.defineProperty(parent, 'dataset', {value: datasetParent});
        Object.defineProperty(parent, 'parentElement', {value: null});

        Object.defineProperty(event, 'target', {value: target});

        expect(selector.selectAtPosition(event)).toBeNull();
    });

    it('selectAtPosition should return the top parentof all the target Element', () => {
        const event = new MouseEvent('mousedown');
        const target = jasmine.createSpyObj('SVGElement', ['']);
        const dataset = {
            isSelectable: 'true',
            takeParent: 'true',
        };

        let parent = jasmine.createSpyObj('SVGElement', ['']);

        Object.defineProperty(event, 'target', {value: target});

        Object.defineProperty(target, 'parentElement', {value: parent});
        Object.defineProperty(target, 'dataset', {value: dataset});

        // create a hierarchie of parent linked
        // tslint:disable-next-line: no-magic-numbers
        for (let i = 0; i < 7; ++i ) {
            const newParent = jasmine.createSpyObj('SVGElement', ['']);
            const datasetParent = {
                takeParent: 'true',
            };
            Object.defineProperty(parent, 'dataset', {value: datasetParent});
            Object.defineProperty(parent, 'parentElement', {value: newParent});

            parent = newParent;
        }

        Object.defineProperty(parent, 'dataset', {value: {takeParent: undefined}});
        expect(selector.selectAtPosition(event)).toEqual(parent);
    });
});
