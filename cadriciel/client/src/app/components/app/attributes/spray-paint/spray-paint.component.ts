import { Component } from '@angular/core';
import { SprayPaintService } from 'src/app/services/attributes/spray-paint/spray-paint.service';

@Component({
  selector: 'app-sprayPaint',
  templateUrl: './spray-paint.component.html',
  styleUrls: ['./spray-paint.component.css']
})
export class SprayPaintComponent {
  protected service: SprayPaintService;

  constructor(service: SprayPaintService) {
    this.service = service;
  }
}
