// tslint:disable: no-any
// tslint:disable: no-string-literal
// tslint:disable: no-magic-numbers

import { Color } from 'src/app/color-palette/color/color';
import { BLACK } from 'src/app/color-palette/constant';
import { Coordinate } from '../coordinate';
import { SprayPaint } from './spray-paint';
import * as json from './spray-paint.json';

const config = json.default;

describe('spray-paint class', () => {
    const radius: number = 10;
    const color: Color = BLACK;

    let sprayPaint: SprayPaint;
    let rendererSpy: any;

    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj('Renderer2', ['setAttribute', 'createElement', 'appendChild']);
        sprayPaint = new SprayPaint(color, radius, rendererSpy);
    });

    it('Should create', () => {
        expect(sprayPaint).toBeTruthy();
    });

    it('constructor should create an svg groupElement with the right attributes', () => {
        expect(rendererSpy.createElement).toHaveBeenCalledWith(config.element.group, config.element.from);
    });

    it('constructor should set the right attribute to the group element', () => {
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(sprayPaint['svg'], config.fill.opacity, `${color.a}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(sprayPaint['svg'], config.fill.color, `#${color.rgb_hexa}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(sprayPaint['svg'], config.selectable.identifier, config.selectable.true);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(sprayPaint['svg'], config.takeParent.identifier, config.takeParent.false);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(sprayPaint['svg'], config.eraserPreview, config.fill.color);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(sprayPaint['svg'], config.mainColor, config.fill.color);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(sprayPaint['svg'], config.secondaryColor, config.none);
    });

    it('addPaintSpeck should create a new circle to the group svg', () => {
        const posistion: Coordinate = {x: 2, y: 7};
        sprayPaint.addPaintSpeck(posistion);

        expect(rendererSpy.createElement).toHaveBeenCalledWith(config.element.circle, config.element.from);
        expect(rendererSpy.appendChild).toHaveBeenCalled();
    });

    it('addPaintSpeck should set the right attribut the the new DOT created', () => {
        const posistion: Coordinate = {x: 2, y: 7};
        const dot: SVGElement = jasmine.createSpyObj('SVGElement', ['']);
        const dotDistance = 3;
        const angle = 0.6;

        const angleInRad = angle * 2 * Math.PI;
        const xVar = Math.cos(angleInRad) * dotDistance;
        const yVar = Math.sin(angleInRad) * dotDistance;
        const dotPosition = {x: posistion.x + xVar, y: posistion.y + yVar};

        spyOn<any>(sprayPaint, 'getDotDistanceToCursor').and.returnValue(dotDistance);
        spyOn(Math, 'random').and.returnValue(angle);
        rendererSpy.createElement.and.returnValue(dot);

        sprayPaint.addPaintSpeck(posistion);

        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(dot, config.circle.position.x, `${dotPosition.x}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(dot, config.circle.position.y, `${dotPosition.y}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(dot, config.circle.radius, config.circle.radiusSize);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(dot, config.selectable.identifier, config.selectable.true);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(dot, config.takeParent.identifier, config.takeParent.true);
    });

    it('getDotDistanceToCursor should alway return a value under sprayRadius', () => {
        const random = spyOn(Math, 'random');

        for (let i = 0; i <= 10; ++i ) {
            random.and.returnValue(i / 10);
            expect(sprayPaint['getDotDistanceToCursor']()).toBeLessThanOrEqual(radius);
        }
    });
});
