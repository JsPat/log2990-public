// /* tslint:disable:no-unused-variable */
// import { NO_ERRORS_SCHEMA, Renderer2 } from '@angular/core';
// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { SprayPaintComponent } from './spray-paint.component';
// import { CommandInvokerService } from 'src/app/services/command-invoker/command-invoker.service';
// import { SVGChildService } from 'src/app/services/svg-child/svg-child.service';
// import { ColorService } from 'src/app/color-palette/color-service/color.service';
// import { ShapesHolderService } from 'src/app/services/shapes-holder/shapes-holder.service';

// describe('SprayPaintComponent', () => {
//   let component: SprayPaintComponent;
//   let fixture: ComponentFixture<SprayPaintComponent>;
//   let event: MockMouseEvent;
//   let renderSpy : Renderer2;

//   // tslint:disable-next-line: max-classes-per-file
//   class MockMouseEvent extends MouseEvent {
//     mockoffsetX: number;
//     mockoffsetY: number;

//     constructor() {
//       super('mousedown');
//     }
//   }

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [ SprayPaintComponent ],
//       schemas: [NO_ERRORS_SCHEMA]
//     })
//     .compileComponents();
//   }));
//   beforeEach(() => {
//     fixture = TestBed.createComponent(SprayPaintComponent);
//     component = fixture.componentInstance;
//     renderSpy = jasmine.createSpyObj('Renderer2',['createElement','setAttribute','appendChild']);
//     component = new SprayPaintComponent(renderSpy,
//                                         new CommandInvokerService(),
//                                         new SVGChildService(new ShapesHolderService()),
//                                         new ColorService());
//     fixture.detectChanges();
//     event = new MockMouseEvent();
//     jasmine.clock().install();
//   });
//   afterEach(() => {
//     jasmine.clock().uninstall();
//   });
//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
//   it('mouseUp,mouseLeave & toolSwap all stop the current spray', () => {
//     spyOn(global,'clearInterval');
//     component.mouseLeave();
//     expect(clearInterval).toHaveBeenCalledTimes(1);
//     component.toolSwap();
//     expect(clearInterval).toHaveBeenCalledTimes(2);
//     component.mouseUp();
//     expect(clearInterval).toHaveBeenCalledTimes(3);
//   });
// });
