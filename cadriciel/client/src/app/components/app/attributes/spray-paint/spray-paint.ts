import { Renderer2 } from '@angular/core';
import { Color } from 'src/app/color-palette/color/color';
import { Coordinate } from '../coordinate';
import { SVGHolder } from '../svg-holder';
import * as JSON from './spray-paint.json';

const config = JSON.default;

export class SprayPaint extends SVGHolder {
    private readonly THREE_STANDARD_DEVIATIONS: number = 3;
    protected svg: SVGGElement;
    constructor(private color: Color,
                private sprayRadius: number,
                private renderer: Renderer2) {
        super();
        this.svg = this.renderer.createElement(config.element.group, config.element.from);
        this.renderer.setAttribute(this.svg, config.fill.opacity, `${this.color.a}`);
        this.renderer.setAttribute(this.svg, config.fill.color, `#${this.color.rgb_hexa}`);
        this.renderer.setAttribute(this.svg, config.selectable.identifier, config.selectable.true);
        this.renderer.setAttribute(this.svg, config.takeParent.identifier, config.takeParent.false);
        this.renderer.setAttribute(this.svg, config.eraserPreview, config.fill.color);

        this.renderer.setAttribute(this.svg, config.mainColor, config.fill.color);
        this.renderer.setAttribute(this.svg, config.secondaryColor, config.none);
    }

    addPaintSpeck(currentCursorPosition: Coordinate): void {
        const distanceDotToCursor = this.getDotDistanceToCursor();

        const angleInRad = Math.random() * 2 * Math.PI;
        const xVar = Math.cos(angleInRad) * distanceDotToCursor;
        const yVar = Math.sin(angleInRad) * distanceDotToCursor;
        const dotPosition = {x: currentCursorPosition.x + xVar, y: currentCursorPosition.y + yVar};
        const dot = this.renderer.createElement(config.element.circle, config.element.from);

        this.renderer.setAttribute(dot, config.circle.position.x, `${dotPosition.x}`);
        this.renderer.setAttribute(dot, config.circle.position.y, `${dotPosition.y}`);
        this.renderer.setAttribute(dot, config.circle.radius, config.circle.radiusSize);
        this.renderer.setAttribute(dot, config.selectable.identifier, config.selectable.true);
        this.renderer.setAttribute(dot, config.takeParent.identifier, config.takeParent.true);
        this.renderer.appendChild(this.svg, dot);
    }

    private getDotDistanceToCursor(): number {
        let distanceDotToCursor = 0;
        do {
            // Use the Box-Muller transform to get a normal distribution on a variable (centered on 0)
            let u1 = Math.random();
            const u2 = Math.random();

            // Since a value of 0 to u1 will mess up the next step (log(0)=-Infinity), we replace that value with 1 if it occurs
            u1 = (u1 === 0) ? 1 : u1;

            // We take the absolute value of the result of the transform to get a positive distance
            const z0 = Math.abs(Math.sqrt(-2 * Math.log(u1)) * Math.cos(2 * Math.PI * u2));

            // Value is between 0 and -Inf, but the effective range of the value is ~3 times its standard variation (1*3 in this case)
            // By dividing z0 by 3, we therefore get a value around 0 to ~1, but not quite, so we may loop again to prevent this
            distanceDotToCursor = (z0 / this.THREE_STANDARD_DEVIATIONS) * this.sprayRadius;

        } while (distanceDotToCursor > this.sprayRadius);

        return distanceDotToCursor;
    }
}
