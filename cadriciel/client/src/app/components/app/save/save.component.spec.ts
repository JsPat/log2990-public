/* tslint:disable:no-unused-variable */
// tslint:disable: no-any
// tslint:disable: no-string-literal

import { HttpClient } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Observable, of, Subject } from 'rxjs';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { ToolsService } from 'src/app/services/attributes/toolsService/tools-service';
import { DatabaseDrawingsService } from 'src/app/services/database-drawings/database-drawings.service';
import { DrawSheetService } from 'src/app/services/draw-sheet/draw-sheet.service';
import { HandleRequestErrorService } from 'src/app/services/handle-request-error/handle-request-error.service';
import { Drawing } from '../../../../../../common/drawing';
import { SaveComponent } from './save.component';

// tslint:disable-next-line: max-classes-per-file
class DatabaseDrawingsServiceMock extends DatabaseDrawingsService {
  addDrawing(drawing: Drawing): Observable<Drawing> {
    return new Subject<Drawing>().asObservable();
  }
}
// tslint:disable-next-line: max-classes-per-file
class DrawSheetServiceMock extends DrawSheetService {
  svgToString(): string {
    return '<svg></svg>';
  }
}
// tslint:disable-next-line: max-classes-per-file
class HTMLInputElementMock {
  value: string;
}

describe('SaveComponent', () => {
  let component: SaveComponent;
  let databaseDrawingsServiceMock: DatabaseDrawingsServiceMock;
  let drawSheetServiveMock: DrawSheetServiceMock;
  // tslint:disable-next-line: prefer-const because can't initialize http if it's const
  let http: HttpClient;
  // tslint:disable-next-line: prefer-const because can't initialize HTMLInputElementMock if it's const
  let hTMLInputElementMock: HTMLInputElementMock;
  let matSnackBarMock: any;
  let fixture: ComponentFixture<SaveComponent>;

  const mockContext = {
    drawImage: () => { // simulating drawImage function
    }
  } as unknown as CanvasRenderingContext2D;
  const mockCanvas = {
    toDataURL: () => { // simulating drawImage function
    }
  } as unknown as HTMLCanvasElement;
  beforeEach(async(() => {
    matSnackBarMock = jasmine.createSpyObj('MatSnackBar', ['']);
    databaseDrawingsServiceMock = new DatabaseDrawingsServiceMock(http, new HandleRequestErrorService(matSnackBarMock));
    drawSheetServiveMock = new DrawSheetServiceMock(new ToolsService(), new ColorService());

    TestBed.configureTestingModule({
      declarations: [ SaveComponent ],
      providers: [
        { provide: MatSnackBar, useValue: matSnackBarMock },
        { provide: DrawSheetService, useValue: drawSheetServiveMock },
        { provide: HTMLInputElement, useValue: hTMLInputElementMock},
        { provide: DatabaseDrawingsService, useValue: databaseDrawingsServiceMock },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(async () => {
    fixture = TestBed.createComponent(SaveComponent);
    component = fixture.componentInstance;

    await fixture.whenStable();
    fixture.detectChanges();

    component['drawingName'] = new FormControl('', [
      Validators.required
    ]);
    component['drawingName'].setValue('Dessin');
  });

  it('should create', async () => {
    expect(component).toBeTruthy();
  });

  // addDrawing
  it('addDrawing() should update currentDrawing if drawingName not empty and validateAllTags', () => {
    const drawingName = 'name';
    component['drawingName'].setValue(drawingName);
    component['currentDrawing'].name = 'nameToBeReplaced';
    component['currentDrawing'].tags = new Array<string>('a', 'b');
    component['addDrawing']();
    expect(component['currentDrawing'].name).toEqual(drawingName);
  });

  it('addDrawing() should call databaseDrawingsService.addDrawing if drawingName not empty and validateAllTags', async(() => {
    const drawingName = 'name';
    component['drawingName'].setValue(drawingName);
    component['currentDrawing'].name = 'nameToBeReplaced';
    component['currentDrawing'].tags = new Array<string>('a', 'b');
    spyOn(databaseDrawingsServiceMock, 'addDrawing').and.returnValue(of(component['currentDrawing']));
    spyOn<any>(component, 'addDrawing').and.callThrough();
    component['addDrawing']();
    fixture.detectChanges();
    // tslint:disable-next-line: no-string-literal
    expect(component['databaseDrawingsService'].addDrawing).toHaveBeenCalled();
  }));

  it('addDrawing() should not call databaseDrawingsService.addDrawing if drawingName empty', async(() => {
    const drawingName = '';
    component['drawingName'].setValue(drawingName);
    component['currentDrawing'].name = 'nameToBeReplaced';
    component['currentDrawing'].tags = new Array<string>('a', 'b');
    spyOn(databaseDrawingsServiceMock, 'addDrawing').and.returnValue(of(component['currentDrawing']));
    spyOn<any>(component, 'addDrawing').and.callThrough();
    component['addDrawing']();
    fixture.detectChanges();
    // tslint:disable-next-line: no-string-literal
    expect(component['databaseDrawingsService'].addDrawing).not.toHaveBeenCalled();
  }));

  it('addDrawing() should not call databaseDrawingsService.addDrawing if not validateAllTags', async(() => {
    const drawingName = 'name';
    component['drawingName'].setValue(drawingName);
    component['currentDrawing'].name = 'nameToBeReplaced';
    component['currentDrawing'].tags = new Array<string>('notWorkingTag$$$$');
    spyOn(databaseDrawingsServiceMock, 'addDrawing').and.returnValue(of(component['currentDrawing']));
    spyOn<any>(component, 'addDrawing').and.callThrough();
    component['addDrawing']();
    fixture.detectChanges();
    // tslint:disable-next-line: no-string-literal
    expect(component['databaseDrawingsService'].addDrawing).not.toHaveBeenCalled();
  }));

  // addTag
  it('addTag() should call databaseDrawingsService.addTag', () => {
    const eventInput = new HTMLInputElementMock();
    eventInput.value = 'eventInputvalue';
    const eventValue = 'eventValue';
    // tslint:disable-next-line: no-any
    const event: any = {input: eventInput, value: eventValue};
    spyOn<any>(component, 'addTag').and.callThrough();
    // tslint:disable-next-line: no-string-literal
    spyOn(component['databaseDrawingsService'], 'addTag');
    component['addTag'](event);
    // tslint:disable-next-line: no-string-literal
    expect(component['databaseDrawingsService'].addTag).toHaveBeenCalled();
  });

  // removeTag
  it('removeTag() should call databaseDrawingsService.removeTag', () => {
    const tag = 'a';
    component['currentDrawing'].tags = new Array<string>('a', 'b', 'c');
    spyOn<any>(component, 'removeTag').and.callThrough();
    // tslint:disable-next-line: no-string-literal
    spyOn(component['databaseDrawingsService'], 'removeTag');
    component['removeTag'](tag);
    // tslint:disable-next-line: no-string-literal
    expect(component['databaseDrawingsService'].removeTag).toHaveBeenCalled();
  });

  it('loadPreview should call createImage', () => {
    spyOn(component['canvas'], 'getContext').and.returnValue(mockContext);
    component['loadPreview']();

    expect(component['canvas'].getContext).toHaveBeenCalled();
  });

  it('imageOnLoad should call drawImage', () => {
    component['canvasContext'] = mockContext;
    component['canvas'] = mockCanvas;

    spyOn<any>(component['canvasContext'], 'drawImage');
    spyOn<any>(component['canvas'], 'toDataURL');
    component['imageOnLoad']();

    expect(component['canvasContext'].drawImage).toHaveBeenCalled();
    expect(component['canvas'].toDataURL).toHaveBeenCalled();
  });

  it('imageOnLoad should not call drawImage when context is null', () => {
    const drawImageSpy = spyOn<any>(component['canvasContext'], 'drawImage');
    component['canvasContext'] = null;
    component['imageOnLoad']();

    expect(drawImageSpy).toHaveBeenCalledTimes(0);
  });

});
