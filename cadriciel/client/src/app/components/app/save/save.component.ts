import { ENTER, SPACE } from '@angular/cdk/keycodes';
import { Component, Renderer2 } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import * as JSON from 'src/app/components/app/export/export.json';
import { DatabaseDrawingsService } from 'src/app/services/database-drawings/database-drawings.service';
import { DrawSheetService } from 'src/app/services/draw-sheet/draw-sheet.service';
import { IdGenerator } from 'src/app/services/id-generator/id-generator';
import { Drawing } from '../../../../../../common/drawing';

const config = JSON.default;

@Component({
  selector: 'app-save',
  templateUrl: './save.component.html',
  styleUrls: ['./save.component.css']
})
export class SaveComponent  {

  protected readonly SEPARATOR_KEYS_CODES: number[] = [ENTER, SPACE];
  private readonly ERROR_NAME_REQUIRED: string = 'required';

  protected currentDrawing: Drawing;
  protected drawingName: FormControl = new FormControl('', [
    Validators.required
  ]);
  protected preview: string;
  private image: HTMLImageElement;
  private canvas: HTMLCanvasElement;
  private canvasContext: CanvasRenderingContext2D | null;

  private imageOnLoad = () => {
    if (this.canvasContext) {
      this.canvasContext.drawImage(this.image, 0, 0);
      this.preview = this.canvas.toDataURL(config.image + config.png);
      this.currentDrawing.pngContent = this.preview;
    }
  }

  constructor(private databaseDrawingsService: DatabaseDrawingsService, private idGenerator: IdGenerator,
              private data: DrawSheetService, private renderer: Renderer2) {
    this.currentDrawing = new Drawing(this.idGenerator.generateId(), data.svgToString());
    this.canvas = this.renderer.createElement(config.canvas);
    this.loadPreview();
  }

  protected addDrawing(): void  {
    if (!this.drawingName.hasError(this.ERROR_NAME_REQUIRED) && this.databaseDrawingsService.validateAllTags(this.currentDrawing)) {
      this.currentDrawing.name = this.drawingName.value;
      this.databaseDrawingsService.addDrawing(this.currentDrawing).subscribe();
    }
  }

  protected addTag(event: MatChipInputEvent): void {
    this.databaseDrawingsService.addTag(event, this.currentDrawing.tags);
  }

  protected removeTag(tag: string): void {
    this.databaseDrawingsService.removeTag(this.currentDrawing.tags, tag);
  }

  private loadPreview(): void {
    this.canvasContext = this.canvas.getContext(config.twoDimensions);

    this.canvas.width = this.data.dataDrawSheet.width;
    this.canvas.height = this.data.dataDrawSheet.height;

    const blop = new Blob([this.currentDrawing.svgContent], {type: config.type});
    const url = window.URL.createObjectURL(blop);
    this.image = new Image();
    this.image.src = url;

    this.image.onload = this.imageOnLoad;
  }
}
