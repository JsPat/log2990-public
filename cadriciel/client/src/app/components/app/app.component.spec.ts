// tslint:disable: no-string-literal
import { HttpClientModule } from '@angular/common/http';
import { async, TestBed } from '@angular/core/testing';
import { NavigationError, NavigationStart, Router, RouterEvent } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ReplaySubject } from 'rxjs';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { ToolsService } from 'src/app/services/attributes/toolsService/tools-service';
import { AutoSaveService } from 'src/app/services/auto-save/auto-save.service';
import { DrawSheetService } from 'src/app/services/draw-sheet/draw-sheet.service';
import { AppComponent } from './app.component';

// tslint:disable-next-line: max-classes-per-file
class DrawSheetServiceMock extends DrawSheetService {
    svgToString(): string {
      return '<svg></svg>';
    }
}

describe('AppComponent', () => {

    let component: AppComponent;
    let drawSheetDataSpy: DrawSheetService;
    let toolsServiceSpy: ToolsService;
    let colorSpy: ColorService;
    let autoSaveSpy: AutoSaveService;

    const eventSubject = new ReplaySubject<RouterEvent>(1);
    const routerMock = {
        navigate: jasmine.createSpy('navigate'),
        events: eventSubject.asObservable(),
        url: 'test/url'
    };

    beforeEach(async(() => {
        colorSpy = new ColorService();
        toolsServiceSpy = new ToolsService();
        drawSheetDataSpy = new DrawSheetServiceMock(toolsServiceSpy, colorSpy);
        autoSaveSpy = jasmine.createSpyObj('AutoSave', ['save']);
        TestBed.configureTestingModule({
            imports: [RouterTestingModule, HttpClientModule],
            declarations: [AppComponent],
            providers: [    { provide: ToolsService, useValue: toolsServiceSpy },
                            { provide: Router, useValue: routerMock },
                            { provide: DrawSheetService, useValue: drawSheetDataSpy },
                            { provide: AutoSaveService, useValue: autoSaveSpy } ],
        });
    }));

    beforeEach(() => {
        const fixture = TestBed.createComponent(AppComponent);
        component = fixture.componentInstance;
      });

    it('should create the app', () => {
        expect(component).toBeTruthy();
    });

    it('ngOnInit() should subscribe navigationStartEvent', () => {
        spyOn(component['data'], 'resizePage');
        spyOn(component['data'], 'changeDataDrawSheet');

        component.ngOnInit();
        const ns = new NavigationStart(0, 'http://localhost:4200/drawing');
        eventSubject.next(ns);

        expect(component['data'].resizePage).toHaveBeenCalled();
        expect(component['data'].changeDataDrawSheet).toHaveBeenCalled();
    });

    it('ngOnInit() should subscribe navigationStartEvent and do nothing', () => {
        spyOn(component['data'], 'resizePage');
        spyOn(component['data'], 'changeDataDrawSheet');

        // tslint:disable-next-line: no-any
        spyOn<any>(component['router'].events, 'subscribe').and.returnValue(new Event(''));

        component.ngOnInit();
        const ns = new NavigationError(0, 'http://localhost:4200/drawing', 'error');
        eventSubject.next(ns);

        expect(component['data'].resizePage).not.toHaveBeenCalled();
        expect(component['data'].changeDataDrawSheet).not.toHaveBeenCalled();
    });

});
