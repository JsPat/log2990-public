/* tslint:disable:no-unused-variable */
// tslint:disable: no-any
// tslint:disable: no-string-literal

import { NO_ERRORS_SCHEMA, Renderer2 } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpClient } from '@angular/common/http';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Observable, Subject } from 'rxjs';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { WHITE } from 'src/app/color-palette/constant';
import { ToolsService } from 'src/app/services/attributes/toolsService/tools-service';
import { DrawSheetService } from 'src/app/services/draw-sheet/draw-sheet.service';
import { EmailService } from 'src/app/services/email/email.service';
import { HandleRequestErrorService } from 'src/app/services/handle-request-error/handle-request-error.service';
import { MatDialogMock } from '../home-page/home-page.component.spec';
import { DrawSheetData } from '../new-drawing/draw-sheet-model';
import { DataToEmail } from './../../../../../../common/data-to-email';
import { ExportComponent } from './export.component';

class DrawSheetServiceMock extends DrawSheetService {
  svgToString(): string {
    return '<svg></svg>';
  }
}
// tslint:disable-next-line: max-classes-per-file
class EmailServiceMock extends EmailService {
  sendEmail(): Observable<DataToEmail> {
    return new Subject<DataToEmail>().asObservable();
  }
}

describe('ExportComponent', () => {
  let component: ExportComponent;
  let fixture: ComponentFixture<ExportComponent>;

  let drawSheetServiveMock: DrawSheetServiceMock;

  let rendererSpy: Renderer2;
  let emailServiceMock: EmailServiceMock;
  // tslint:disable-next-line: prefer-const because can't initialize http if it's const
  let http: HttpClient;
  let matSnackBarMock: any;

  const mockContext = {
    drawImage: () => { // simulating drawImage function
    }
  } as unknown as CanvasRenderingContext2D;

  beforeEach(async (() => {
    drawSheetServiveMock = new DrawSheetServiceMock(new ToolsService(), new ColorService());
    rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute']);
    matSnackBarMock = jasmine.createSpyObj('MatSnackBar', ['']);
    emailServiceMock = new EmailServiceMock(http, new HandleRequestErrorService(matSnackBarMock));

    TestBed.configureTestingModule({
      declarations: [ ExportComponent ],
      providers:  [
        { provide: MatSnackBar, useValue: matSnackBarMock },
        { provide: EmailService, useValue: emailServiceMock},
        { provide: Renderer2, usevalue: rendererSpy},
        { provide: DrawSheetService, useValue: drawSheetServiveMock },
        { provide: MatDialog, useClass: MatDialogMock}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component['renderer'] = rendererSpy;
    component['drawingName'] = new FormControl('', [
      Validators.required
    ]);
    component['drawingName'].setValue('Dessin');
    component['emailAddress'] = new FormControl('', [
      Validators.email,
      Validators.required
    ]);
    component['emailAddress'].setValue('a@a.a');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit() should subscribe drawSheetData', () => {
    const subject = new Subject<DrawSheetData>();
    spyOnProperty(drawSheetServiveMock, 'currentDataDrawSheet', 'get').and.returnValue(subject.asObservable());

    const data = new DrawSheetData(0, 0, WHITE);

    component.ngOnInit();

    subject.next(data);

    expect(component['drawSheetData']).toEqual(data);
  });

  it('loadPreview should call createImage', () => {
    spyOn<any>(component, 'createImage');
    component['loadPreview']();

    expect(component['createImage']).toHaveBeenCalled();
    expect(component['renderer'].setAttribute).toHaveBeenCalled();
  });

  it('createImage should call getContext', () => {
    spyOn(component['canvas'], 'getContext').and.returnValue(mockContext);
    spyOn(component['canvas'], 'toDataURL');
    component['createImage']();

    expect(component['canvas'].getContext).toHaveBeenCalled();
  });

  it('createImage with no context should do nothing', () => {
    spyOn(component['canvas'], 'getContext').and.returnValue(null);
    spyOn(component['canvas'], 'toDataURL');
    component['createImage']();

    expect(component['canvas'].getContext).toHaveBeenCalled();
    expect(component['canvas'].toDataURL).toHaveBeenCalledTimes(0);
    expect(component['renderer'].setAttribute).toHaveBeenCalledTimes(0);
  });

  it('imageOnLoad should call toDataURL, setAttribute', () => {
    spyOn(component['canvas'], 'toDataURL');
    component['imageOnLoad']();

    expect(component['canvas'].toDataURL).toHaveBeenCalled();
  });

  it('imageOnLoad should not call toDataURL, setAttribute when context is null', () => {
    component['canvasContext'] = null;
    spyOn(component['canvas'], 'toDataURL');
    component['imageOnLoad']();

    expect(component['canvas'].toDataURL).toHaveBeenCalledTimes(0);
  });

  it('download should call click', () => {
    spyOn(component['downloadTag'], 'click');
    const data = '<svg></svg>';
    const format = 'svg';
    component['download'](data, format);

    expect(component['downloadTag'].click).toHaveBeenCalled();
  });

  it('download with empty fileName should call it dessin and call click', () => {
    spyOn(component['downloadTag'], 'click');
    const data = '<svg></svg>';
    const format = 'svg';
    component['download'](data, format);

    expect(component['downloadTag'].click).toHaveBeenCalled();
  });

  it('export should call download if mail = false', () => {
    spyOn<any>(component, 'download');
    const data = 'dessin';
    const format = 'png';
    component['mail'] = false;
    component['export'](data, format);

    expect(component['download']).toHaveBeenCalled();
  });

  it('export should call toEmail if mail = true', () => {
    spyOn<any>(component, 'toEmail');
    const data = 'dessin';
    const format = 'png';
    component['mail'] = true;
    component['export'](data, format);

    expect(component['toEmail']).toHaveBeenCalled();
  });

  it('toEmail should call sendEmail if validateEmailAddress return true', () => {
    spyOn<any>(component, 'toEmail').and.callThrough();
    spyOn<any>(component, 'validateEmailAddress').and.returnValue(true);
    spyOn<any>(emailServiceMock, 'sendEmail').and.returnValue(new Subject<DataToEmail>().asObservable());
    component['toEmail']('a', 'a');
    expect(emailServiceMock.sendEmail).toHaveBeenCalled();
  });

  it('toEmail should not call sendEmail if validateEmailAddress return false', () => {
    spyOn<any>(component, 'toEmail').and.callThrough();
    spyOn<any>(component, 'validateEmailAddress').and.returnValue(false);
    spyOn<any>(emailServiceMock, 'sendEmail').and.returnValue(new Subject<DataToEmail>().asObservable());
    component['toEmail']('a', 'a');
    expect(emailServiceMock.sendEmail).not.toHaveBeenCalled();
  });

  it('openConfirmPanel should do nothing if validateDrawingName return false', () => {
    spyOn<any>(component, 'openConfirmPanel').and.callThrough();
    spyOn<any>(component, 'validateDrawingName').and.returnValue(false);

    const dialogSpy = spyOn<any>(component, 'export');
    const action = 'exporter';
    const type = 'jpg';

    component['openConfirmPanel'](action, type);

    expect(dialogSpy).not.toHaveBeenCalled();
  });

  it('openConfirmPanel(exporter,svg) should open a dialog openConfirmPanel and call export with svg if mail true', () => {
    const subject = new Subject<any>();
    const mathDialogSpy = jasmine.createSpyObj('MatDialogRef', ['afterClosed']);
    const dialogSpy = spyOn<any>(component, 'export');
    mathDialogSpy.afterClosed.and.returnValue(subject.asObservable());
    spyOn<any>(component, 'openConfirmPanel').and.callThrough();
    spyOn(component.dialog, 'open').and.returnValue(mathDialogSpy);

    const action = 'exporter';
    const type = 'svg';
    component['mail'] = true;

    component['openConfirmPanel'](action, type);
    subject.next(type);

    expect(dialogSpy).toHaveBeenCalled();
  });

  it('openConfirmPanel(exporter,svg) should open a dialog openConfirmPanel and call export with svg if mail false', () => {
    const subject = new Subject<any>();
    const mathDialogSpy = jasmine.createSpyObj('MatDialogRef', ['afterClosed']);
    const dialogSpy = spyOn<any>(component, 'export');
    mathDialogSpy.afterClosed.and.returnValue(subject.asObservable());
    spyOn<any>(component, 'openConfirmPanel').and.callThrough();
    spyOn(component.dialog, 'open').and.returnValue(mathDialogSpy);

    const action = 'exporter';
    const type = 'svg';
    component['mail'] = false;

    component['openConfirmPanel'](action, type);
    subject.next(type);

    expect(dialogSpy).toHaveBeenCalled();
  });

  it('openConfirmPanel(exporter,png) should open a dialog openConfirmPanel and call export with png', () => {
    const subject = new Subject<any>();
    const mathDialogSpy = jasmine.createSpyObj('MatDialogRef', ['afterClosed']);
    const dialogSpy = spyOn<any>(component, 'export');
    mathDialogSpy.afterClosed.and.returnValue(subject.asObservable());
    spyOn<any>(component, 'openConfirmPanel').and.callThrough();
    spyOn(component.dialog, 'open').and.returnValue(mathDialogSpy);

    const action = 'exporter';
    const type = 'png';

    component['openConfirmPanel'](action, type);
    subject.next(type);

    expect(dialogSpy).toHaveBeenCalled();
  });

  it('openConfirmPanel(exporter,jpg) should open a dialog openConfirmPanel and call export with jpg', () => {
    const subject = new Subject<any>();
    const mathDialogSpy = jasmine.createSpyObj('MatDialogRef', ['afterClosed']);
    const dialogSpy = spyOn<any>(component, 'export');
    mathDialogSpy.afterClosed.and.returnValue(subject.asObservable());
    spyOn<any>(component, 'openConfirmPanel').and.callThrough();
    spyOn(component.dialog, 'open').and.returnValue(mathDialogSpy);

    const action = 'exporter';
    const type = 'jpg';

    component['openConfirmPanel'](action, type);
    subject.next(type);

    expect(dialogSpy).toHaveBeenCalled();
  });

  it('validateEmailAddress should return true if no ERROR_EMAIL_FORMAT and no ERROR_REQUIRED', () => {
    component['emailAddress'].setValue('a@a.a');
    const result = component['validateEmailAddress']();
    expect(result).toBeTruthy();
  });

  it('validateEmailAddress should return false if ERROR_EMAIL_FORMAT and no ERROR_REQUIRED', () => {
    component['emailAddress'].setValue('a');
    const result = component['validateEmailAddress']();
    expect(result).toBeFalsy();
  });

  it('validateEmailAddress should return false if ERROR_EMAIL_FORMAT and ERROR_REQUIRED', () => {
    component['emailAddress'].setValue('');
    const result = component['validateEmailAddress']();
    expect(result).toBeFalsy();
  });

  it('validateDrawingName should return true if no ERROR_REQUIRED', () => {
    component['drawingName'].setValue('a');
    const result = component['validateDrawingName']();
    expect(result).toBeTruthy();
  });

  it('validateDrawingName should return false if ERROR_REQUIRED', () => {
    component['drawingName'].setValue('');
    const result = component['validateDrawingName']();
    expect(result).toBeFalsy();
  });

});
