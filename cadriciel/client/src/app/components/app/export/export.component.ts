import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { WHITE } from 'src/app/color-palette/constant';
import { DrawSheetService } from 'src/app/services/draw-sheet/draw-sheet.service';
import { EmailService } from 'src/app/services/email/email.service';
import { ConfirmPanelComponent } from '../confirm-panel/confirm-panel.component';
import { DrawSheetData } from '../new-drawing/draw-sheet-model';
import * as JSON from './export.json';

const config = JSON.default;

@Component({
  selector: 'app-export',
  templateUrl: './export.component.html',
  styleUrls: ['./export.component.css'],
})
export class ExportComponent implements OnInit {

  private readonly FILE_TYPE_SEPARATOR: string = '.';
  private readonly BASIC_DRAWING_NAME: string = 'Dessin';
  private readonly ERROR_EMAIL_FORMAT: string = 'email';
  private readonly ERROR_REQUIRED: string = 'required';

  protected drawSheetData: DrawSheetData = new DrawSheetData(0, 0, WHITE);
  protected preview: string;
  private downloadTag: HTMLAnchorElement;
  private svg: SVGSVGElement;
  private image: HTMLImageElement;
  private canvas: HTMLCanvasElement;

  protected filter: string = config.noFilter;
  protected mail: boolean = false;
  protected emailAddress: FormControl = new FormControl('', [
    Validators.email,
    Validators.required
  ]);
  protected drawingName: FormControl = new FormControl('', [
    Validators.required
  ]);

  private canvasContext: CanvasRenderingContext2D | null;
  private svgString: string;
  private imageSVG: string;
  private imagePNG: string;
  private imageJPG: string;

  private imageOnLoad = () => {
    if (this.canvasContext) {
      this.canvasContext.drawImage(this.image, 0, 0);
      this.imageSVG = this.data.svgToString();
      this.imagePNG = this.canvas.toDataURL(config.image + config.png);
      this.imageJPG = this.canvas.toDataURL(config.image + config.jpg);
      this.preview = this.imagePNG;
      this.renderer.setAttribute(this.svg, config.filter, `url(#${config.noFilter})`);
    }
  }

  constructor(public dialog: MatDialog, private emailService: EmailService,
              private data: DrawSheetService, private renderer: Renderer2) {
    this.drawSheetData = this.data.dataDrawSheet;
    this.svg = data.svg;
    this.downloadTag = this.renderer.createElement(config.a);
    this.canvas = this.renderer.createElement(config.canvas);
    this.createImage();
    this.drawingName.setValue(this.BASIC_DRAWING_NAME);
  }

  ngOnInit(): void {
    this.data.currentDataDrawSheet.subscribe((result) => this.drawSheetData = result);
    this.data.currentDataDrawSheet.subscribe((drawSheetData) => {this.drawSheetData = drawSheetData; });
  }

  protected loadPreview(): void {
    this.svg = this.data.svg;
    this.renderer.setAttribute(this.svg, config.filter, `url(#${this.filter})`);
    this.createImage();
  }

  protected openConfirmPanel(action: string, type: string): void {
    if (!this.mail && this.validateDrawingName() || this.validateEmailAddress() && this.validateDrawingName()) {
      const dialogRef = this.dialog.open(ConfirmPanelComponent, {
        data: {action, name: this.drawingName.value + this.FILE_TYPE_SEPARATOR + type }
      });
      dialogRef.afterClosed().subscribe( (_) => {
        let data: string;
        if (type === config.svg) {
          data = this.mail ? this.imageSVG : this.svgString;
        } else if (type === config.png) {
          data = this.imagePNG;
        } else {
          data = this.imageJPG;
        }
        this.export(data, type);
      });
    }
  }

  // Inspired by https://gist.github.com/curran/7cf9967028259ea032e8
  private download(data: string, format: string): void {
    const downloadName = this.drawingName.value + this.FILE_TYPE_SEPARATOR + format;
    this.downloadTag.href = data;
    this.downloadTag.download = downloadName;
    this.downloadTag.click();
  }

  private toEmail(data: string, format: string): void {
    if (this.validateEmailAddress()) {
      this.emailService.sendEmail(this.emailAddress.value, this.drawingName.value, format, data).subscribe();
    }
  }

  private export(data: string, format: string): void {
    if (this.mail) {
      this.toEmail(data, format);
    } else {
      this.download(data, format);
    }
  }

  private createImage(): void {
    this.svgString = this.data.svgToString();

    this.canvasContext = this.canvas.getContext(config.twoDimensions);

    this.canvas.width = this.data.dataDrawSheet.width;
    this.canvas.height = this.data.dataDrawSheet.height;

    const blob = new Blob([this.svgString], {type: config.type});
    this.svgString = window.URL.createObjectURL(blob);
    this.image = new Image();
    this.image.src = this.svgString;
    this.image.onload = this.imageOnLoad;
  }

  private validateEmailAddress(): boolean {
    return !this.emailAddress.hasError(this.ERROR_EMAIL_FORMAT) && !this.emailAddress.hasError(this.ERROR_REQUIRED);
  }

  private validateDrawingName(): boolean {
    return !this.drawingName.hasError(this.ERROR_REQUIRED);
  }

}
