import { Component, HostListener, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { WHITE } from 'src/app/color-palette/constant';
import { DrawSheetService } from 'src/app/services/draw-sheet/draw-sheet.service';
import * as JSON from './new-drawing.json';

const config = JSON.default;

@Component({
  selector: 'app-new-drawing',
  templateUrl: './new-drawing.component.html',
  styleUrls: ['./new-drawing.component.css']
})
export class NewDrawingComponent implements OnInit {

  protected drawNotNull: boolean;

  constructor(protected  data: DrawSheetService, public dialog: MatDialog ) {
    this.data.resizePage();
    this.data.color.sendBaseColor(WHITE);
  }

  ngOnInit(): void {
    this.drawNotNull =  this.data.buttonContinue;
  }

  changePageSize(height: number, width: number): void {
    height = Math.abs(height);
    width = Math.abs(width);

    height = Math.min(height, this.data.MAX_HEIGHT);
    width = Math.min(width, this.data.MAX_WIDTH);

    this.data.changeSize = false;
    this.data.pageHeight = (this.data.pageHeight === null ? 0 : height);
    this.data.pageWidth = (this.data.pageWidth === null ? 0 : width);
  }

  newDrawSheetData(): void {
    this.data.changeDataDrawSheet();
  }

  @HostListener(config.resize, [config.event]) onresizePage(): void {
    this.data.resizePage();
  }

}
