// tslint:disable: no-string-literal
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatDialogModule, MatFormFieldModule } from '@angular/material';
import { ColorPaletteModule } from 'src/app/color-palette/color-palette.module';
import { NewDrawingComponent } from './new-drawing.component';

describe('NewDrawingComponent', () => {
  let component: NewDrawingComponent;
  let fixture: ComponentFixture<NewDrawingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewDrawingComponent ],
      imports: [
        FormsModule,
        ColorPaletteModule,
        MatDialogModule,
        MatFormFieldModule
      ],
    })
    .compileComponents();
  }));

  beforeEach(async () => {
    fixture = TestBed.createComponent(NewDrawingComponent);
    component = fixture.componentInstance;

    component.ngOnInit();
    await fixture.whenStable();
    // fixture.detectchangePageSizes();
  });

  // ngOnInit
  it('draw not null is updated by ng-onInit', () => {
    component.ngOnInit();

    // tslint:disable-next-line: no-any
    expect(component['drawNotNull']).toBe((component as any).data.buttonContinue);
  });

  // changePageSizePageSize
  it('pageHeight should be updated', () => {
    const height = 500;
    const width = 800;
    component.changePageSize(height, width);
    expect(component['data'].pageHeight).toBe(height);
  });

  it('pageWidth should be updated', () => {
    const height = 500;
    const width = 800;
    component.changePageSize(height, width);
    expect(component['data'].pageWidth).toBe(width);
  });

  it('pageHeight should be zero if the input is null', () => {
    component['data'].pageHeight = null;
    const height = 500;
    const width = 800;
    component.changePageSize(height, width);
    expect(component['data'].pageHeight).not.toBeNull();
  });

  it('pageWidth should be zero if the input is null', () => {
    const height = 500;
    const width = 800;
    component['data'].pageWidth = null;
    component.changePageSize(height, width);
    expect(component['data'].pageHeight).not.toBeNull();
  });

  // resizePage
  it('resizePage should be called on the hostlistiner', () => {
    spyOn(component['data'], 'resizePage');
    window.dispatchEvent(new Event('resize'));
    expect(component['data'].resizePage).toHaveBeenCalled();
  });

  it('pageHeight is not modified if changePageSize size is false', () => {
    component['data'].changeSize = false;
    // tslint:disable-next-line: no-magic-numbers
    component['data'].pageHeight = 500;
    // tslint:disable-next-line: no-magic-numbers
    spyOnProperty(window, 'innerWidth').and.returnValue(760);
    component['data'].resizePage();
    // tslint:disable-next-line: no-magic-numbers
    expect(component['data'].pageHeight).toBe(500);

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('newDrawSheetData() should call data.changeDataDrawSheet(DrawSheetData)', () => {
    spyOn(component, 'newDrawSheetData').and.callThrough();
    // we disable ts lint beacause qe want to access private attribute via string literal
    // tslint:disable-next-line: no-string-literal
    spyOn(component['data'], 'changeDataDrawSheet');
    component.newDrawSheetData();
    // we disable ts lint beacause qe want to access private attribute via string literal
    // tslint:disable-next-line: no-string-literal
    expect((component['data'].changeDataDrawSheet()));
    expect(component.newDrawSheetData).toHaveBeenCalled();
  });
});
