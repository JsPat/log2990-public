import { async, TestBed } from '@angular/core/testing';
import { Color } from 'src/app/color-palette/color/color';
import { DrawSheetData } from './draw-sheet-model';

describe('DrawSheetData ', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({})
    .compileComponents();
  }));
  it('new DrawSheetData() should create a new object of DrawSheetData', () => {
    const  heightGiven = 69;
    const  widthGiven = 69;
    // tslint:disable-next-line: no-magic-numbers
    const  colorGiven  = new Color(255, 255, 255, 0);
    const svgGiven = '<svg></svg>';
    const drawSheetData = new DrawSheetData(heightGiven, widthGiven, colorGiven, svgGiven);
    expect(drawSheetData).toBeDefined();
    expect(heightGiven).toBe(drawSheetData.height);
    expect(widthGiven).toBe(drawSheetData.width);
    expect(colorGiven).toBe(drawSheetData.color);
    expect(svgGiven).toBe(drawSheetData.svg);

  });

  it('new DrawSheetData() should not set svg if svg is undefined', () => {
    const  heightGiven = 69;
    const  widthGiven = 69;
    // tslint:disable-next-line: no-magic-numbers
    const  colorGiven  = new Color(255, 255, 255, 0);
    const drawSheetData = new DrawSheetData(heightGiven, widthGiven, colorGiven);
    expect(drawSheetData).toBeDefined();
    expect(heightGiven).toBe(drawSheetData.height);
    expect(widthGiven).toBe(drawSheetData.width);
    expect(colorGiven).toBe(drawSheetData.color);
    expect(drawSheetData.svg).not.toBeDefined();

  });
});
