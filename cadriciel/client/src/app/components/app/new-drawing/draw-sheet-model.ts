import { Color } from 'src/app/color-palette/color/color';

export class DrawSheetData {
    height: number;
    width: number;
    color: Color;
    svg: string;

    constructor(height: number, width: number, color: Color, svg?: string) {
        this.height = height;
        this.width = width;
        this.color = color;
        if ( svg !== undefined ) {
            this.svg = svg;
        }
    }

}
