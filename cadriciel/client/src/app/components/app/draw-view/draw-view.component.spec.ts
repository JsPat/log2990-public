// tslint:disable: no-unused-variable
// tslint:disable: no-any
// tslint:disable: no-string-literal
// tslint:disable: max-file-line-count

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatDialog } from '@angular/material';
import { Subject } from 'rxjs';
import { of } from 'rxjs/internal/observable/of';
import { SelectorService } from 'src/app/services/attributes/selector/selector.service';
import { PossibleTabs } from 'src/app/services/draw-view/draw-view.service';
import { DrawSheetComponent} from '../draw-sheet/draw-sheet.component';
import { DrawViewComponent } from './draw-view.component';

class MatDialogMock {
  // tslint:disable-next-line: typedef
  open() {
    return {
      afterClosed: () => of({action: true})
    };
  }
}

describe('DrawViewComponent', () => {
  let component: DrawViewComponent;
  let fixture: ComponentFixture<DrawViewComponent>;
  let selectorSpy: SelectorService;

  beforeEach(async(() => {

    const rendererFactorySpy = jasmine.createSpyObj('RendererFactory2', ['createRenderer']);
    const svgServiceSpy = jasmine.createSpyObj('SVGChildService', ['']);
    const drawViewServiceSpy = jasmine.createSpyObj('DrawViewService', ['']);
    const shapesHolderServiceSpy = jasmine.createSpyObj('ShapesHolderService', ['']);
    const matrixGeneratorServiceSpy = jasmine.createSpyObj('MatrixGeneratorService', ['']);
    const commandInvokerServiceSpy = jasmine.createSpyObj('CommandInvokerService', ['']);
    const drawSheetServiceSpy = jasmine.createSpyObj('DrawSheetService', ['']);
    const idGeneratorSpy = jasmine.createSpyObj('IdGenerator', ['']);
    const rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement']);
    rendererFactorySpy.createRenderer.and.returnValue(rendererSpy);
    // tslint:disable-next-line: max-line-length
    selectorSpy = new SelectorService(rendererFactorySpy, svgServiceSpy, drawViewServiceSpy,
      shapesHolderServiceSpy, matrixGeneratorServiceSpy, commandInvokerServiceSpy, drawSheetServiceSpy, idGeneratorSpy);

    TestBed.configureTestingModule({
      declarations: [ DrawViewComponent , DrawSheetComponent],
      providers : [
        { provide : MatDialog, useClass : MatDialogMock },
        { provide : selectorSpy, useClass :  SelectorService},
      ],
      schemas: [NO_ERRORS_SCHEMA],
      }).compileComponents();
    }));

  beforeEach(async () => {
    fixture = TestBed.createComponent(DrawViewComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    await fixture.whenStable();
    fixture.detectChanges();

  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('toggle function should shift the active state with New(1) and call the correct dialog', () => {
    component['state'] = component['PossibleTabs'].ATTRIBUT;
    spyOn(component, 'openNewDraw');
    spyOn(component, 'openGallery');
    spyOn(component, 'openSave');
    spyOn(component, 'openGuide');
    component.toggle(component['PossibleTabs'].NEW);
    expect(component['state'].valueOf).toEqual(component['PossibleTabs'].NEW.valueOf);
    expect(component.openNewDraw).toHaveBeenCalledTimes(1);
    expect(component.openGallery).toHaveBeenCalledTimes(0);
    expect(component.openSave).toHaveBeenCalledTimes(0);
    expect(component.openGuide).toHaveBeenCalledTimes(0);
  });
  it('toggle function should shift the active state with Gallery(2) and call the correct dialog', () => {
    component['state'] = component['PossibleTabs'].ATTRIBUT;
    spyOn(component, 'openNewDraw');
    spyOn(component, 'openGallery');
    spyOn(component, 'openSave');
    spyOn(component, 'openGuide');
    component.toggle(component['PossibleTabs'].GALLERY);
    expect(component['state'].valueOf).toEqual(component['PossibleTabs'].GALLERY.valueOf);
    expect(component.openNewDraw).toHaveBeenCalledTimes(0);
    expect(component.openGallery).toHaveBeenCalledTimes(1);
    expect(component.openSave).toHaveBeenCalledTimes(0);
    expect(component.openGuide).toHaveBeenCalledTimes(0);
  });
  it('toggle function should shift the active state with Save(2) and call the correct dialog', () => {
    component['state'] = component['PossibleTabs'].ATTRIBUT;
    spyOn(component, 'openNewDraw');
    spyOn(component, 'openGallery');
    spyOn(component, 'openSave');
    spyOn(component, 'openGuide');
    component.toggle(component['PossibleTabs'].SAVE);
    expect(component['state'].valueOf).toEqual(component['PossibleTabs'].SAVE.valueOf);
    expect(component.openNewDraw).toHaveBeenCalledTimes(0);
    expect(component.openGallery).toHaveBeenCalledTimes(0);
    expect(component.openSave).toHaveBeenCalledTimes(1);
    expect(component.openGuide).toHaveBeenCalledTimes(0);
  });
  it('toggle function should shift the active state with Guide(4) and call the correct dialog', () => {
    component['state'] = PossibleTabs.ATTRIBUT;
    spyOn(component, 'openNewDraw');
    spyOn(component, 'openGallery');
    spyOn(component, 'openSave');
    spyOn(component, 'openGuide');
    component.toggle(PossibleTabs.HELP);
    expect(component['state'].valueOf).toEqual(PossibleTabs.HELP.valueOf);
    expect(component.openNewDraw).toHaveBeenCalledTimes(0);
    expect(component.openGallery).toHaveBeenCalledTimes(0);
    expect(component.openSave).toHaveBeenCalledTimes(0);
    expect(component.openGuide).toHaveBeenCalledTimes(1);
  });
  it('toggle function should shift the active state with Export(4) and call the correct dialog', () => {
    component['state'] = PossibleTabs.ATTRIBUT;
    spyOn(component, 'openNewDraw');
    spyOn(component, 'openGallery');
    spyOn(component, 'openSave');
    spyOn(component, 'openExport');
    spyOn(component, 'openGuide');
    component.toggle(PossibleTabs.EXPORT);
    expect(component['state'].valueOf).toEqual(PossibleTabs.EXPORT.valueOf);
    expect(component.openNewDraw).toHaveBeenCalledTimes(0);
    expect(component.openGallery).toHaveBeenCalledTimes(0);
    expect(component.openSave).toHaveBeenCalledTimes(0);
    expect(component.openExport).toHaveBeenCalledTimes(1);
    expect(component.openGuide).toHaveBeenCalledTimes(0);
  });
  it('toggle function on active state should unactivate it', () => {
    component['state'] = PossibleTabs.ATTRIBUT;
    spyOn(component, 'openNewDraw');
    spyOn(component, 'openGallery');
    spyOn(component, 'openSave');
    spyOn(component, 'openGuide');
    component.toggle(PossibleTabs.ATTRIBUT);
    expect(component['state'].valueOf).toEqual(PossibleTabs.NONE.valueOf);
    expect(component.openNewDraw).toHaveBeenCalledTimes(0);
    expect(component.openGallery).toHaveBeenCalledTimes(0);
    expect(component.openSave).toHaveBeenCalledTimes(0);
    expect(component.openGuide).toHaveBeenCalledTimes(0);
  });
  it('toggleGrid should set GridStatus to the opposite state (active)', () => {
    component['gridStatus'] = true;
    component.toggleGrid();
    expect(component['gridStatus']).toBeFalsy();
  });
  it('toggleGrid should set GridStatus to the opposite state (not-active)', () => {
    component['gridStatus'] = false;
    component.toggleGrid();
    expect(component['gridStatus']).toBeTruthy();
  });
  it('toggleGrid should not change the GridStatus to the opposite state if shortcut are not enable', () => {
    component['gridStatus'] = false;
    component['shortcutsEnabled'] = false;
    component.toggleGrid();
    expect(component['gridStatus']).toBeFalsy();
  });
  it('openNewDraw() should open a dialog NewDrawingComponent', () => {
    component['shortcutsEnabled'] = false;
    const subject = new Subject<any>();
    const mathDialogSpy = jasmine.createSpyObj('MatDialogRef', ['afterClosed']);
    mathDialogSpy.afterClosed.and.returnValue(subject.asObservable());
    spyOn(component.dialog, 'open').and.returnValue(mathDialogSpy);
    component.openNewDraw();
    subject.next(null);
    expect(component['shortcutsEnabled']).toBeTruthy();
  });
  it('openGallery() should open a dialog GalleryComponent', () => {
    component['shortcutsEnabled'] = false;
    const subject = new Subject<any>();
    const mathDialogSpy = jasmine.createSpyObj('MatDialogRef', ['afterClosed']);
    mathDialogSpy.afterClosed.and.returnValue(subject.asObservable());
    spyOn(component.dialog, 'open').and.returnValue(mathDialogSpy);
    component.openGallery();
    subject.next(null);
    expect(component['shortcutsEnabled']).toBeTruthy();
  });
  it('openSave() should open a dialog SaveComponent', () => {
    component['shortcutsEnabled'] = false;
    const subject = new Subject<any>();
    const mathDialogSpy = jasmine.createSpyObj('MatDialogRef', ['afterClosed']);
    mathDialogSpy.afterClosed.and.returnValue(subject.asObservable());
    spyOn(component.dialog, 'open').and.returnValue(mathDialogSpy);
    component.openSave();
    subject.next(null);
    expect(component['shortcutsEnabled']).toBeTruthy();
  });
  it('openExport() should open a dialog ExportComponent', () => {
    component['shortcutsEnabled'] = false;
    const subject = new Subject<any>();
    const mathDialogSpy = jasmine.createSpyObj('MatDialogRef', ['afterClosed']);
    mathDialogSpy.afterClosed.and.returnValue(subject.asObservable());
    spyOn(component.dialog, 'open').and.returnValue(mathDialogSpy);
    component.openExport();
    subject.next(null);
    expect(component['shortcutsEnabled']).toBeTruthy();
  });
  it('openGuide() should open a dialog GuideComponent', () => {
    component['shortcutsEnabled'] = false;
    const subject = new Subject<any>();
    const mathDialogSpy = jasmine.createSpyObj('MatDialogRef', ['afterClosed']);
    mathDialogSpy.afterClosed.and.returnValue(subject.asObservable());
    spyOn(component.dialog, 'open').and.returnValue(mathDialogSpy);
    component.openGuide();
    subject.next(null);
    expect(component['shortcutsEnabled']).toBeTruthy();
  });
  it('Pushing the control and o button down simultaniously should call openNewDraw if shortcuts are enabled', () => {
    spyOn(component, 'openNewDraw');
    const mockKeyboard = new KeyboardEvent('keydown.control.o');
    component['shortcutsEnabled'] = true;
    component.openNewDrawShortCut(mockKeyboard);
    expect(component.openNewDraw).toHaveBeenCalledTimes(1);
  });
  it('Pushing the control and o button down simultaniously should call nothing when shortcut is disabled', () => {
    spyOn(component, 'openNewDraw');
    const mockKeyboard = new KeyboardEvent('keydown.control.o');
    component['shortcutsEnabled'] = false;
    component.openNewDrawShortCut(mockKeyboard);
    expect(component.openNewDraw).toHaveBeenCalledTimes(0);
  });
  it('Pushing the control and s button down simultaniously should call openSave if shortcuts are enabled', () => {
    spyOn(component, 'openSave');
    const mockKeyboard = new KeyboardEvent('keydown.control.s');
    component['shortcutsEnabled'] = true;
    component.openSaveShortCut(mockKeyboard);
    expect(component.openSave).toHaveBeenCalledTimes(1);
  });
  it('Pushing the control and s button down simultaniously should call nothing when shortcut is disabled', () => {
    spyOn(component, 'openSave');
    const mockKeyboard = new KeyboardEvent('keydown.control.s');
    component['shortcutsEnabled'] = false;
    component.openSaveShortCut(mockKeyboard);
    expect(component.openSave).toHaveBeenCalledTimes(0);
  });
  it('Pushing the control and g button down simultaniously should call nothing when shortcut is disabled', () => {
    spyOn(component, 'openGallery');
    const mockKeyboard = new KeyboardEvent('keydown.control.g');
    component['shortcutsEnabled'] = false;
    component.openGalleryShortCut(mockKeyboard);
    expect(component.openGallery).toHaveBeenCalledTimes(0);
  });
  it('Pushing the control and g button down simultaniously should call openGalleryShortCut if shortcuts are enabled', () => {
    spyOn(component, 'openGallery');
    const mockKeyboard = new KeyboardEvent('keydown.control.g');
    component['shortcutsEnabled'] = true;
    component.openGalleryShortCut(mockKeyboard);
    expect(component.openGallery).toHaveBeenCalledTimes(1);
  });
  it('Pushing the control and e button down simultaniously should call openExport if shortcuts are enabled', () => {
    spyOn(component, 'openExport');
    const mockKeyboard = new KeyboardEvent('keydown.control.e');
    component['shortcutsEnabled'] = true;
    component.openExportShortCut(mockKeyboard);
    expect(component.openExport).toHaveBeenCalledTimes(1);
  });
  it('Pushing the control and e button down simultaniously should call nothing when shortcut is disabled', () => {
    spyOn(component, 'openExport');
    const mockKeyboard = new KeyboardEvent('keydown.control.e');
    component['shortcutsEnabled'] = false;
    component.openExportShortCut(mockKeyboard);
    expect(component.openExport).toHaveBeenCalledTimes(0);
  });
  it('ngOnInit() should subscribe CommandInvoker', () => {
    const width = 1000;
    const height = 500;
    component['navBar'].undoDisabled = false;
    component['navBar'].redoDisabled = false;
    spyOn<any>(component['commandInvoker'], 'hasCommandDone').and.returnValue(false);
    spyOn<any>(component['commandInvoker'], 'hasCommandUndone').and.returnValue(false);
    component.ngOnInit(width, height);
    component['commandInvoker']['emptyCommand'].next();
    expect(component['navBar'].undoDisabled).toBeTruthy();
    expect(component['navBar'].redoDisabled).toBeTruthy();
  });
  it('Pushing the control and x button down simultaniously should call cutSelection of toolHolder', () => {
    spyOn(selectorSpy, 'cutSelection');
    component['toolHolder']['currentTool'] = selectorSpy;
    component['shortcutsEnabled'] = true;
    component.cut();
    expect(selectorSpy.cutSelection).toHaveBeenCalled();
  });
  it('Pushing the control and c button down simultaniously should call copySelection of toolHolder', () => {
    spyOn(selectorSpy, 'copySelection');
    component['toolHolder']['currentTool'] = selectorSpy;
    component['shortcutsEnabled'] = true;
    component.copy();
    expect(selectorSpy.copySelection).toHaveBeenCalled();
  });
  it('Pushing the delete button down simultaniously should call deleteSelection of toolHolder', () => {
    spyOn(selectorSpy, 'deleteSelection');
    component['toolHolder']['currentTool'] = selectorSpy;
    component['shortcutsEnabled'] = true;
    component.delete();
    expect(selectorSpy.deleteSelection).toHaveBeenCalled();
  });
  it('Pushing the control and d button down simultaniously should call duplicateSelection of toolHolder', () => {
    spyOn(selectorSpy, 'duplicateSelection');
    component['toolHolder']['currentTool'] = selectorSpy;
    component['shortcutsEnabled'] = true;
    const mockKeyboard = new KeyboardEvent('keydown.control.d');
    component.duplicate(mockKeyboard);
    expect(selectorSpy.duplicateSelection).toHaveBeenCalled();
  });
  it('Pushing the control and v button down simultaniously should call pasteClipboard of toolHolder', () => {
    spyOn(selectorSpy, 'pasteClipboard');
    component['toolHolder']['currentTool'] = selectorSpy;
    component['shortcutsEnabled'] = true;
    component.paste();
    expect(selectorSpy.pasteClipboard).toHaveBeenCalled();
  });
  it('Pushing the control and a button down simultaniously should call selectAll of toolHolder', () => {
    spyOn(selectorSpy, 'selectAll');
    component['toolHolder']['currentTool'] = selectorSpy;
    component['shortcutsEnabled'] = true;
    const mockKeyboard = new KeyboardEvent('keydown.control.a');
    component.selectAll(mockKeyboard);
    expect(selectorSpy.selectAll).toHaveBeenCalled();
  });
  it('Pushing the control and x button down simultaniously should call cutSelection of toolHolder', () => {
    spyOn(selectorSpy, 'cutSelection');
    component['toolHolder']['currentTool'] = selectorSpy;
    component['shortcutsEnabled'] = false;
    component.cut();
    expect(selectorSpy.cutSelection).not.toHaveBeenCalled();
  });
  it('Pushing the control and c button down simultaniously should call copySelection of toolHolder', () => {
    spyOn(selectorSpy, 'copySelection');
    component['toolHolder']['currentTool'] = selectorSpy;
    component['shortcutsEnabled'] = false;
    component.copy();
    expect(selectorSpy.copySelection).not.toHaveBeenCalled();
  });
  it('Pushing the delete button down simultaniously should call deleteSelection of toolHolder', () => {
    spyOn(selectorSpy, 'deleteSelection');
    component['toolHolder']['currentTool'] = selectorSpy;
    component['shortcutsEnabled'] = false;
    component.delete();
    expect(selectorSpy.deleteSelection).not.toHaveBeenCalled();
  });
  it('Pushing the control and d button down simultaniously should call duplicateSelection of toolHolder', () => {
    spyOn(selectorSpy, 'duplicateSelection');
    component['toolHolder']['currentTool'] = selectorSpy;
    component['shortcutsEnabled'] = false;
    const mockKeyboard = new KeyboardEvent('keydown.control.d');
    component.duplicate(mockKeyboard);
    expect(selectorSpy.duplicateSelection).not.toHaveBeenCalled();
  });
  it('Pushing the control and v button down simultaniously should call pasteClipboard of toolHolder', () => {
    spyOn(selectorSpy, 'pasteClipboard');
    component['toolHolder']['currentTool'] = selectorSpy;
    component['shortcutsEnabled'] = false;
    component.paste();
    expect(selectorSpy.pasteClipboard).not.toHaveBeenCalled();
  });
  it('Pushing the control and a button down simultaniously should call selectAll of toolHolder', () => {
    spyOn(selectorSpy, 'selectAll');
    component['toolHolder']['currentTool'] = selectorSpy;
    component['shortcutsEnabled'] = false;
    const mockKeyboard = new KeyboardEvent('keydown.control.a');
    component.selectAll(mockKeyboard);
    expect(selectorSpy.selectAll).not.toHaveBeenCalled();
  });
});
