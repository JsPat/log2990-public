import { Component,  HostListener, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SelectorService } from 'src/app/services/attributes/selector/selector.service';
import { ToolHolderService } from 'src/app/services/attributes/tools-holder/tools-holder.service';
import { AutoSaveService } from 'src/app/services/auto-save/auto-save.service';
import { CommandInvokerService } from 'src/app/services/command-invoker/command-invoker.service';
import { DrawViewService, PossibleTabs } from 'src/app/services/draw-view/draw-view.service';
import { GuideComponent } from '../../guide/guide.component';
import { ExportComponent } from '../export/export.component';
import { GalleryComponent } from '../gallery/gallery.component';
import { NewDrawingComponent } from '../new-drawing/new-drawing.component';
import { SaveComponent } from '../save/save.component';
import * as JSON from './draw-view.json';

const config = JSON.default;

@Component({
  selector: 'app-drawView',
  templateUrl: './draw-view.component.html',
  styleUrls: ['./draw-view.component.css']
})
export class DrawViewComponent implements OnInit {
  protected state: PossibleTabs;
  protected gridStatus: boolean = false;
  // tslint:disable-next-line: typedef because the enum has no specific type
  protected readonly PossibleTabs = PossibleTabs;

  private shortcutsEnabled: boolean = true;

  constructor(public dialog: MatDialog,
              private commandInvoker: CommandInvokerService,
              protected navBar: DrawViewService,
              private toolHolder: ToolHolderService,
              private autoSave: AutoSaveService) {
    this.state = PossibleTabs.ATTRIBUT;
  }

  ngOnInit(width: number = window.innerWidth, height: number = window.innerHeight): void {
    this.commandInvoker.emptyCommandObs.subscribe((_) => {
      this.navBar.undoDisabled = !this.commandInvoker.hasCommandDone();
      this.navBar.redoDisabled = !this.commandInvoker.hasCommandUndone();
    });
  }

  toggle(elem: PossibleTabs): void {
    this.state = (this.state !== elem) ? elem : PossibleTabs.NONE;
    this.navBar.toggle(this.state);
    switch (this.state) {
      case (PossibleTabs.NEW):
        this.openNewDraw();
        break;
      case (PossibleTabs.GALLERY):
        this.openGallery();
        break;
      case (PossibleTabs.SAVE):
        this.openSave();
        break;
      case (PossibleTabs.EXPORT):
        this.openExport();
        break;
      case (PossibleTabs.HELP):
        this.openGuide();
        break;
      default:
        break;
    }
  }

  openNewDraw(): void {
    const dialogRef = this.dialog.open(NewDrawingComponent);
    this.shortcutsEnabled = false;
    dialogRef.afterClosed().subscribe(() => {
      this.shortcutsEnabled = true;
      this.state = PossibleTabs.NONE;
      this.navBar.toggle(this.state);
    });
  }

  openGallery(): void {
    const dialogRef2 = this.dialog.open(GalleryComponent, { width: config.width, maxWidth: config.maxWidthGallery });
    this.shortcutsEnabled = false;
    dialogRef2.afterClosed().subscribe(() => {
      this.shortcutsEnabled = true;
      this.state = PossibleTabs.NONE;
      this.navBar.toggle(this.state);
    });
  }

  openSave(): void {
    const dialogRef3 = this.dialog.open(SaveComponent);
    this.shortcutsEnabled = false;
    dialogRef3.afterClosed().subscribe(() => {
      this.shortcutsEnabled = true;
      this.state = PossibleTabs.NONE;
      this.navBar.toggle(this.state);
    });
  }

  openGuide(): void {
    const dialogRef4 = this.dialog.open(GuideComponent, { maxWidth: config.maxWidthGuide});
    this.shortcutsEnabled = false;
    dialogRef4.afterClosed().subscribe(() => {
      this.shortcutsEnabled = true;
      this.state = PossibleTabs.NONE;
      this.navBar.toggle(this.state);
    });
  }

  openExport(): void {
    const dialogRef5 = this.dialog.open(ExportComponent);
    this.shortcutsEnabled = false;
    dialogRef5.afterClosed().subscribe(() => {
      this.shortcutsEnabled = true;
      this.state = PossibleTabs.NONE;
      this.navBar.toggle(this.state);
    });
  }

  undo(): void {
    this.toolHolder.activeTool.toolSwap();
    this.commandInvoker.undo();
    this.autoSave.save();
  }

  redo(): void {
    this.toolHolder.activeTool.toolSwap();
    this.commandInvoker.redo();
    this.autoSave.save();
  }

  @HostListener(config.keyDownControlO, [config.event]) openNewDrawShortCut(event: KeyboardEvent): void {
    event.preventDefault();
    if (this.shortcutsEnabled) {
      this.openNewDraw();
      this.state = PossibleTabs.NONE;
      this.navBar.toggle(this.state);
    }
  }

  @HostListener(config.keyDownControlS, [config.event]) openSaveShortCut(event: KeyboardEvent): void {
    event.preventDefault();
    if (this.shortcutsEnabled) {
      this.openSave();
      this.state = PossibleTabs.NONE;
      this.navBar.toggle(this.state);
    }
  }

  @HostListener(config.keyDownControlE, [config.event]) openExportShortCut(event: KeyboardEvent): void {
    event.preventDefault();
    if (this.shortcutsEnabled) {
      this.openExport();
      this.state = PossibleTabs.NONE;
      this.navBar.toggle(this.state);
    }
  }

  @HostListener(config.keyDownControlG, [config.event]) openGalleryShortCut(event: KeyboardEvent): void {
    event.preventDefault();
    if (this.shortcutsEnabled) {
      this.openGallery();
      this.state = PossibleTabs.NONE;
      this.navBar.toggle(this.state);
    }
  }

  @HostListener(config.keyDownG, [config.event])
  toggleGrid(): void {
    if (this.shortcutsEnabled) {
      this.gridStatus = !this.gridStatus;
    }
  }

  @HostListener(config.keyDownControlC, [config.event]) copy(): void {
    if (this.toolHolder.activeTool instanceof SelectorService && this.shortcutsEnabled) {
      this.toolHolder.activeTool.copySelection();
    }
  }

  @HostListener(config.keyDownControlX, [config.event]) cut(): void {
    if (this.toolHolder.activeTool instanceof SelectorService && this.shortcutsEnabled) {
      this.toolHolder.activeTool.cutSelection();
      this.autoSave.save();
    }
  }

  @HostListener(config.keyDownControlV, [config.event]) paste(): void {
    if (this.toolHolder.activeTool instanceof SelectorService && this.shortcutsEnabled) {
      this.toolHolder.activeTool.pasteClipboard();
      this.autoSave.save();
    }
  }

  @HostListener(config.keyDownControlD, [config.event]) duplicate(event: KeyboardEvent): void {
    event.preventDefault();
    if (this.toolHolder.activeTool instanceof SelectorService && this.shortcutsEnabled) {
      this.toolHolder.activeTool.duplicateSelection();
      this.autoSave.save();
    }
  }

  @HostListener(config.keyDownDelete, [config.event]) delete(): void {
    if (this.toolHolder.activeTool instanceof SelectorService && this.shortcutsEnabled) {
      this.toolHolder.activeTool.deleteSelection();
    }
  }

  @HostListener(config.keyDownControlA, [config.event]) selectAll(event: KeyboardEvent): void {
    event.preventDefault();
    if (this.toolHolder.activeTool instanceof SelectorService && this.shortcutsEnabled) {
      this.toolHolder.activeTool.selectAll();
    }
  }
}
