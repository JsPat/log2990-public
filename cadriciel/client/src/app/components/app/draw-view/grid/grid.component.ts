import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { GridService } from 'src/app/services/grid/grid.service';
import * as JSON from './grid.json';

const config = JSON.default;

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit, OnDestroy {
  private readonly MAX_WIDTH: number = 100;
  private readonly MIN_WIDTH: number = 5;
  private readonly STEP_WIDTH: number = 5;
  private readonly ADD_CHAR: string = '+';
  private readonly SOUS_CHAR: string = '-';

  protected service: GridService;

  constructor(service: GridService) {
    this.service = service;
  }

  ngOnInit(): void {
    this.service.displayGrid();
  }

  ngOnDestroy(): void {
    this.service.hideGrid();
  }

  @HostListener(config.keyDown, [config.event])
  handleKeyEvent(event: KeyboardEvent): void {
    if (event.key === this.ADD_CHAR) {
      this.increment();
    } else if (event.key === this.SOUS_CHAR) {
      this.decrement();
    }
  }

  private increment(): void {
    if (this.service.gridWidth < this.MAX_WIDTH - this.STEP_WIDTH) {
      this.service.gridWidth += this.STEP_WIDTH;
    } else {
      this.service.gridWidth = this.MAX_WIDTH;
    }
    this.service.redrawGrid();
  }

  private decrement(): void {
    if (this.service.gridWidth > this.MIN_WIDTH + this.STEP_WIDTH) {
      this.service.gridWidth -= this.STEP_WIDTH;
    } else {
      this.service.gridWidth = this.MIN_WIDTH;
    }
    this.service.redrawGrid();
  }

}
