/* tslint:disable:no-unused-variable */
// tslint:disable: no-string-literal
// tslint:disable: no-any
// tslint:disable: no-magic-numbers
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { GridService } from 'src/app/services/grid/grid.service';
import { GridComponent } from './grid.component';

describe('GridComponent', () => {
  let component: GridComponent;
  let fixture: ComponentFixture<GridComponent>;

  let serviceSpy: GridService;

  beforeEach(async(() => {

    serviceSpy = jasmine.createSpyObj('GridService', ['displayGrid', 'hideGrid', 'redrawGrid']);

    TestBed.configureTestingModule({
      declarations: [ GridComponent ],
      providers: [
        { provide: GridService, useValue: serviceSpy },
      ],
      imports: [
        FormsModule
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit should display the grid', () => {
    component.ngOnInit();
    expect(serviceSpy.displayGrid).toHaveBeenCalled();
  });

  it('ngOnDestroy should hide the grid', () => {
    component.ngOnDestroy();

    expect(serviceSpy.hideGrid).toHaveBeenCalled();
  });

  it('handleKeyEvent should do nothing if we pass a wrong key', () => {
    const event = new KeyboardEvent('keydown', { key: 'a' });
    spyOn<any>(component, 'increment');
    spyOn<any>(component, 'decrement');
    component.handleKeyEvent(event);

    expect(component['increment']).not.toHaveBeenCalled();
    expect(component['decrement']).not.toHaveBeenCalled();
  });

  it('handleKeyEvent should do increment if we pass the key \'+\'', () => {
    const event = new KeyboardEvent('keydown', { key: '+' });
    spyOn<any>(component, 'increment');
    component.handleKeyEvent(event);

    expect(component['increment']).toHaveBeenCalled();
  });

  it('handleKeyEvent should do decrement if we pass the key \'-\'', () => {
    const event = new KeyboardEvent('keydown', { key: '-' });
    spyOn<any>(component, 'decrement');
    component.handleKeyEvent(event);

    expect(component['decrement']).toHaveBeenCalled();
  });

  it('increment should add STEP_WIDTH if the gridWidth is under MAX_WIDTH - STEP_WIDTH (95)', () => {
    component['service'].gridWidth = 18;
    component['increment']();

    expect(component['service'].gridWidth).toBe(18 + component['STEP_WIDTH']);
  });

  it('increment should set the gridWidth to MAX_WIDTH if the gridWidth is not under MAX_WIDTH - STEP_WIDTH (95)', () => {
    component['service'].gridWidth = 98;
    component['increment']();

    expect(component['service'].gridWidth).toBe(component['MAX_WIDTH']);
  });

  it('increment should call reDraw()', () => {
    component['increment']();
    expect(serviceSpy.redrawGrid).toHaveBeenCalled();
  });

  it('decrement should subtract STEP_WIDTH if the gridWidth is over MIN_WIDTH + STEP_WIDTH (10)', () => {
    component['service'].gridWidth = 67;
    component['decrement']();

    expect(component['service'].gridWidth).toBe(67 - component['STEP_WIDTH']);
  });

  it('decrement should set the gridWidth to MIN_WIDTH if the gridWidth is not over MIN_WIDTH + STEP_WIDTH (10)', () => {
    component['service'].gridWidth = 2;
    component['decrement']();
    expect(component['service'].gridWidth).toBe(component['MIN_WIDTH']);
  });

  it('decrement should call reDraw()', () => {
    component['decrement']();
    expect(serviceSpy.redrawGrid).toHaveBeenCalled();
  });

});
