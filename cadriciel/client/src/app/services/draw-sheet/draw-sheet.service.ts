import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { WHITE } from 'src/app/color-palette/constant';
import { DrawSheetData } from 'src/app/components/app/new-drawing/draw-sheet-model';
import { ToolsService } from '../attributes/toolsService/tools-service';
import { PossibleTools } from '../constants';

@Injectable(
  {  providedIn: 'root' }
)
export class DrawSheetService {
  readonly MAX_HEIGHT: number = 5000;
  readonly MAX_WIDTH: number = 5000;
  private readonly SIDE_NAV_WIDTH: number = 310;
  private readonly SCROLL_BAR: number = 5;

  svg: SVGSVGElement;
  pageHeight: number | null;
  pageWidth: number | null;
  changeSize: boolean = true;

  private dataDrawSheetSource: Subject<DrawSheetData> = new Subject<DrawSheetData>();
  private currentDrawData: DrawSheetData = new DrawSheetData(0, 0, WHITE);
  private enableButton: boolean = false ;
  private serializer: XMLSerializer;

  constructor(private toolsService: ToolsService, public color: ColorService) {
    this.serializer = new XMLSerializer();
  }

  get buttonContinue(): boolean {
    return this.enableButton;
  }

  get dataDrawSheet(): DrawSheetData {
    return this.currentDrawData;
  }

  get toggleObs(): Observable<PossibleTools> {
    return this.toolsService.toggleObs;
  }

  get currentDataDrawSheet(): Observable<DrawSheetData> {
    return this.dataDrawSheetSource.asObservable();
  }

  svgToString(): string {
    return this.serializer.serializeToString(this.svg);
  }

  changeDataDrawSheet(svg?: string): void {
    const drawData = new DrawSheetData(this.pageHeight as number, this.pageWidth as number, this.color.color, svg );
    this.dataDrawSheetSource.next(drawData);
    this.currentDrawData = drawData;
    this.enableButton = true;
  }

  resizePage(): void {
    if (this.changeSize) {
      this.pageWidth = window.innerWidth - this.SIDE_NAV_WIDTH;
      this.pageHeight =  window.innerHeight - this.SCROLL_BAR;
    }
  }

}
