export class DownloadData {

    canvasContext: CanvasRenderingContext2D | null;
    imagePNG: string;
    imageJPG: string;

    constructor(canvasContext: CanvasRenderingContext2D | null, imagePNG: string, imageJPG: string) {
     this.canvasContext = canvasContext;
     this.imagePNG = imagePNG;
     this.imageJPG = imageJPG;
    }

}
