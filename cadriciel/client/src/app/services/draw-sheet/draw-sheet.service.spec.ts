// tslint:disable: no-magic-numbers
// tslint:disable: no-string-literal
// tslint:disable: no-any
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed } from '@angular/core/testing';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { Color } from 'src/app/color-palette/color/color';
import { DrawSheetData } from 'src/app/components/app/new-drawing/draw-sheet-model';
import { ToolsService } from '../attributes/toolsService/tools-service';
import { DrawSheetService } from './draw-sheet.service';

describe('DrawSheetService', () => {
  let service: DrawSheetService;
  let toolsService: ToolsService;
  let color: ColorService;
  let serializerSpy: XMLSerializer;

  beforeEach(async(() => {
    toolsService = new ToolsService();
    color = new ColorService();
    serializerSpy = jasmine.createSpyObj('XMLSerializer', ['serializeToString']);

    service = new DrawSheetService(toolsService, color);
    // tslint:disable-next-line: no-string-literal
    service['serializer'] = serializerSpy;

    TestBed.configureTestingModule({
      providers: [
        DrawSheetService,
          { provide: ToolsService, useValue: toolsService },
          { provide: ColorService, useValue: color},
        ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  }));

  // beforeEach(async () => {
  //   const svg = '<svg></svg>';
  //   await service.createImage(svg);
  // });

  it('dataDrawSheet getter should return the dataDrawSheet', () => {
    // we disable ts lint beacause qe want to access private attribute via string literal
    // tslint:disable-next-line: no-string-literal
    expect(service.dataDrawSheet).toBe(service['dataDrawSheet']);
  });
  it('buttonContinue getter should return the buttonContinue', () => {
    // we disable ts lint beacause qe want to access private attribute via string literal
    // tslint:disable-next-line: no-string-literal
    expect(service.buttonContinue).toBe(service['buttonContinue']);
  });
  it('toggleObs getter should return the ToolsService', () => {
    // we disable ts lint beacause qe want to access private attribute via string literal
    // tslint:disable-next-line: no-string-literal
    expect(service.toggleObs).toBe(toolsService.toggleObs);
  });
  it('currentDataDrawSheet getter should return the currentDataDrawSheet', () => {
    // we disable ts lint beacause qe want to access private attribute via string literal
    // tslint:disable-next-line: no-string-literal
    expect(service.currentDataDrawSheet).toEqual(service['currentDataDrawSheet']);
  });
  it('should be able to subscribe to DrawSheetData changes', () => {
    // tslint:disable-next-line: no-magic-numbers
    const colorGiven = new Color(255, 255, 255, 255);
    service['color'].sendBaseColor(colorGiven);
    service.pageWidth = 500;
    service.pageHeight = 500;

    const resultColor = new Color(69, 69, 69, 1);
    const drawSheetDataGiven = new DrawSheetData(500, 500, colorGiven);
    // tslint:disable-next-line: no-magic-numbers
    let result = new DrawSheetData(69, 69, resultColor);

    service.currentDataDrawSheet.subscribe((drawSheetData) => {
      result = drawSheetData;
    });

    service.changeDataDrawSheet();
    expect(result).toEqual(drawSheetDataGiven);
  });

  it('SVGtoString should call serializeToString', () => {
    service.svgToString();

    expect(serializerSpy.serializeToString).toHaveBeenCalled();
  });

  it('resizePage sets the PageSizes minus always displayed features', () => {
    service.changeSize = true;
    // tslint:disable-next-line: no-magic-numbers
    // service.pageHeight = 500;
    // tslint:disable-next-line: no-magic-numbers
    spyOnProperty(window, 'innerWidth').and.returnValue(810);
    spyOnProperty(window, 'innerHeight').and.returnValue(505);
    service.resizePage();

    expect(service.pageWidth).toBe(500);
    expect(service.pageHeight).toBe(500);

  });

  it('resizePage does nothing if changeSize is false', () => {
    service.changeSize = false;

    service.pageWidth = 500;
    service.pageHeight = 500;

    spyOnProperty(window, 'innerWidth').and.returnValue(1000);
    spyOnProperty(window, 'innerHeight').and.returnValue(1000);

    service.resizePage();

    expect(service.pageWidth).toBe(500);
    expect(service.pageHeight).toBe(500);
  });
});
