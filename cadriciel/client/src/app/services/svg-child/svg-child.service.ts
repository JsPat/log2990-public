import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { SVGHolder as SVGHolder } from 'src/app/components/app/attributes/svg-holder';
import { ShapesHolderService } from '../shapes-holder/shapes-holder.service';

@Injectable({
  providedIn: 'root'
})
export class SVGChildService {

  private appendSource: Subject<SVGElement>;
  private removeSource: Subject<SVGElement>;
  private appendOnTopSource: Subject<SVGElement>;
  private replaceSource: Subject<[SVGElement, SVGElement]>;

  constructor(private shapesHolder: ShapesHolderService) {
    this.appendSource = new Subject<SVGElement>();
    this.removeSource = new Subject<SVGElement>();
    this.appendOnTopSource = new Subject<SVGElement>();
    this.replaceSource = new Subject<[SVGElement, SVGElement]>();
  }

  get appendObs(): Observable<SVGElement> {
    return this.appendSource.asObservable();
  }

  get removeObs(): Observable<SVGElement> {
    return this.removeSource.asObservable();
  }

  get appendOnTopObs(): Observable<SVGElement> {
    return this.appendOnTopSource.asObservable();
  }

  get replaceObs(): Observable<[SVGElement, SVGElement]> {
    return this.replaceSource.asObservable();
  }

  addNewSVG(shape: SVGHolder | SVGGraphicsElement): void {
    if (shape instanceof SVGHolder) {
      this.shapesHolder.addShape(shape.svgDrawElement);
      this.appendSource.next(shape.svgDrawElement);
    } else {
      this.shapesHolder.addShape(shape);
      this.appendSource.next(shape);
    }
  }

  removeSVG(shape: SVGHolder | SVGGraphicsElement): void {
    if ( shape instanceof SVGHolder ) {
      this.shapesHolder.removeShape(shape.svgDrawElement);
      this.removeSource.next(shape.svgDrawElement);
    } else {
      this.shapesHolder.removeShape(shape);
      this.removeSource.next(shape);
    }
  }

  appendNewSVG(svgRef: SVGElement): void {
    this.appendSource.next(svgRef);
  }

  replaceSvg(newSVG: SVGGraphicsElement, oldSVG: SVGGraphicsElement): void {
    this.replaceSource.next([newSVG, oldSVG]);
    if (!(newSVG instanceof SVGDefsElement)) {
      this.shapesHolder.addShape(newSVG);
    } else {
      this.shapesHolder.removeShape(oldSVG);
    }
  }

  appendSVGOnTop(shape: SVGElement): void {
    this.appendOnTopSource.next(shape);
  }
}
