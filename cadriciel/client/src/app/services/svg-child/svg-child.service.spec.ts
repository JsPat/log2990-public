// tslint:disable: no-string-literal

import { SVGHolder } from 'src/app/components/app/attributes/svg-holder';
import { ShapesHolderService } from '../shapes-holder/shapes-holder.service';
import { SVGChildService } from './svg-child.service';

class SVGHolderMock extends SVGHolder {
  constructor() {
    super();
  }
}

describe('SVGChildService', () => {
  let svgChild: SVGChildService;
  let shapesHolderSpy: ShapesHolderService;
  let svgHolderSpy: SVGHolder;

  beforeEach(() => {
      shapesHolderSpy = jasmine.createSpyObj('ShapesHolderService', ['addShape', 'removeShape']);
      svgHolderSpy = jasmine.createSpyObj('SVGHolder', ['svgDrawElement']);
      svgChild = new SVGChildService(shapesHolderSpy);
    });

  it('can create', () => {
    expect(svgChild).toBeTruthy();
  });

  it('we can get an observable of the append source', () => {

    // we disable ts lint because we ant make sure it is link with the private attribute;
    // tslint:disable-next-line: no-string-literal
    expect(svgChild.appendObs).toEqual(svgChild['appendSource'].asObservable());
  });

  it('if we are subscribe to the append observer, we should get an update evrytime the addNewSVG is called', () => {
    let subscribe = false;

    svgChild.appendObs.subscribe((_) => {
      subscribe = true;
    });

    svgChild.addNewSVG(svgHolderSpy);

    expect(subscribe).toBeTruthy();

  });

  it('addNewSVG should add the shape to the shape holder', () => {
    svgChild.addNewSVG(svgHolderSpy);
    expect(shapesHolderSpy.addShape).toHaveBeenCalledWith(svgHolderSpy);
  });

  it('if we are subscribe to the append observer, we should get an update evrytime the appendNewSVG is called', () => {
    let subscribe = false;
    const expectedValue = jasmine.createSpyObj('SVGElement', ['']);

    svgChild.appendObs.subscribe((_) => {
      subscribe = true;
    });

    svgChild.appendNewSVG(expectedValue);

    expect(subscribe).toBeTruthy();
  });

  it('we can get an observable of the remove source', () => {

    // we disable ts lint because we ant make sure it is link with the private attribute;
    // tslint:disable-next-line: no-string-literal
    expect(svgChild.removeObs).toEqual(svgChild['removeSource'].asObservable());
  });

  it('if we are subscribe to the remove observer, we should get an update evrytime the removeSVG is called', () => {
    let subscribe = false;

    svgChild.removeObs.subscribe((_) => {
      subscribe = true;
    });

    svgChild.removeSVG(svgHolderSpy);

    expect(subscribe).toBeTruthy();
  });

  it('if we are subscribe to the remove observer, we should get an update evrytime the removeSVG is called with an SVGElement', () => {
    let subscribe = false;

    svgChild.removeObs.subscribe((_) => {
      subscribe = true;
    });

    svgChild.removeSVG(jasmine.createSpyObj('SVGElement', ['']));

    expect(subscribe).toBeTruthy();

  });

  it('we can get an observable of the appendOnTop source', () => {

    // we disable ts lint because we ant make sure it is link with the private attribute;
    // tslint:disable-next-line: no-string-literal
    expect(svgChild.appendOnTopObs).toEqual(svgChild['appendOnTopSource'].asObservable());
  });

  it('if we are subscribe to the appendOnTop observer, we should get an update evrytime the appendSVGOnTop is called', () => {
    let subscribe;
    const expectedValue = jasmine.createSpyObj('SVGElement', ['']);

    svgChild.appendOnTopObs.subscribe((value) => {
      subscribe = value;
    });

    svgChild.appendSVGOnTop(expectedValue);

    expect(subscribe).toEqual(expectedValue);

  });

  it('remove should remove the shape to the shape holder', () => {
    // tslint:disable-next-line: no-any
    const svgHolderMock = new SVGHolderMock();
    svgChild.removeSVG(svgHolderMock);
    expect(shapesHolderSpy.removeShape).toHaveBeenCalledWith(svgHolderMock.svgDrawElement);
  });

  it('remove should call next if shape is not SVGHolder', () => {
    spyOn(svgChild, 'removeSVG').and.callThrough();
    spyOn(svgChild['removeSource'], 'next');
    svgChild.removeSVG(svgHolderSpy);
    expect(svgChild['removeSource'].next).toHaveBeenCalled();
  });

  it('we can get an observable of the replaceObs source', () => {

    // we disable ts lint because we ant make sure it is link with the private attribute;
    // tslint:disable-next-line: no-string-literal
    expect(svgChild.replaceObs).toEqual(svgChild['replaceSource'].asObservable());
  });

  it('if we are subscribe to the append observer, we should get an update evrytime the replaceSvg is called', () => {
    let subscribe = false;
    const newExpectedValue = jasmine.createSpyObj('SVGElement', ['']);
    const oldExpectedValue = jasmine.createSpyObj('SVGElement', ['']);

    svgChild.replaceObs.subscribe((_) => {
      subscribe = true;
    });

    svgChild.replaceSvg(newExpectedValue, oldExpectedValue);

    expect(subscribe).toBeTruthy();
  });

  it('replaceSvg should remove the old svg if the new svg to the shape holder is a SVGDefElement', () => {
    const newSvg = document.createElementNS('http://www.w3.org/2000/svg', 'defs');
    const oldSvg = document.createElementNS('http://www.w3.org/2000/svg', 'rect');

    svgChild.replaceSvg(newSvg, oldSvg);
    expect(shapesHolderSpy.removeShape).toHaveBeenCalledWith(oldSvg);

  });

  it('replaceSvg should add the new svg if the new svg to the shape holder is a not SVGDefElement', () => {
    const newSvg = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
    const oldSvg = document.createElementNS('http://www.w3.org/2000/svg', 'defs');

    svgChild.replaceSvg(newSvg, oldSvg);
    expect(shapesHolderSpy.addShape).toHaveBeenCalledWith(newSvg);
  });

});
