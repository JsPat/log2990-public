import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Observable, of } from 'rxjs';

const SNACK_BAR_ACCEPT: string = 'OK';
const SNACK_BAR_DURATION: number = 4000;

@Injectable({
  providedIn: 'root'
})
export class HandleRequestErrorService {

  constructor(private snackBar: MatSnackBar) { }

  handleError<T>(errorMessage: string, result?: T): (error: Error) => Observable<T> {
    return (error: Error): Observable<T> => {
      this.openSnackBar(errorMessage);
      return of(result as T);
    };
  }

  private openSnackBar(message: string): void {
    this.snackBar.open(message, SNACK_BAR_ACCEPT, {
      duration: SNACK_BAR_DURATION,
    });
  }

}
