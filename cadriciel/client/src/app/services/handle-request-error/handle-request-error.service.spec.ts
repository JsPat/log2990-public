/* tslint:disable:no-unused-variable */
// tslint:disable: no-any
// tslint:disable: no-string-literal

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material';
import { Drawing } from '../../../../../common/drawing';
import { DatabaseDrawingsService } from '../database-drawings/database-drawings.service';
import { HandleRequestErrorService } from './handle-request-error.service';

describe('Service: HandleRequestError', () => {
  const BASE_URL: string = 'http://localhost:3000/api/database/drawings';
  const ROUTING_GET: string = '/get/';

  let handleRequestErrorService: HandleRequestErrorService;
  let matSnackBarSpy: MatSnackBar;
  let databaseDrawingsService: DatabaseDrawingsService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        HandleRequestErrorService,
        { provide: MatSnackBar, useValue: matSnackBarSpy },
      ]
    });
    handleRequestErrorService = TestBed.get(HandleRequestErrorService);
    matSnackBarSpy = jasmine.createSpyObj('MatSnackBar', ['open']);
    httpMock = TestBed.get(HttpTestingController);
    databaseDrawingsService = TestBed.get(DatabaseDrawingsService);
    databaseDrawingsService['handleRequestErrorService'] = handleRequestErrorService;
  });

  it('should inject', inject([HandleRequestErrorService], (service: HandleRequestErrorService) => {
    expect(service).toBeTruthy();
  }));

  // handleError
  it('handleError should handle http error safely and call openSnackBar', () => {
    spyOn<any>(handleRequestErrorService, 'handleError').and.callThrough();
    spyOn<any>(handleRequestErrorService, 'openSnackBar');

    const drawing: Drawing = new Drawing('0', '<svg>');
    databaseDrawingsService.getCurrentDrawing(drawing).subscribe((response: Drawing) => {
      expect(response).toEqual(drawing);
    }, fail);
    // tslint:disable-next-line: no-string-literal
    const testURL = BASE_URL + ROUTING_GET + drawing.id;
    const req = httpMock.expectOne(testURL);
    expect(req.request.method).toBe('GET');
    req.error(new ErrorEvent('Random error occured'));

    expect(handleRequestErrorService['openSnackBar']).toHaveBeenCalled();
  });

  // openSnackBar
  it('openSnackBar should call open', () => {
    spyOn<any>(handleRequestErrorService, 'openSnackBar').and.callThrough();
    handleRequestErrorService['snackBar'] = matSnackBarSpy;
    handleRequestErrorService['openSnackBar']('message');
    expect(matSnackBarSpy.open).toHaveBeenCalled();
  });
});
