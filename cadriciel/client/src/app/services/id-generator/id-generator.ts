import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class IdGenerator {
    private allId: Set<string> = new Set<string>();

    get ids(): Set<string> {
        return this.allId;
    }

    generateId(): string {
        const d = new Date();
        // this format is needed because we don't want TABS characters in the string
        let id = `\
${d.getUTCFullYear()}\
${d.getUTCMonth()}\
${d.getUTCDay()}\
${d.getUTCHours()}\
${d.getUTCMinutes()}\
${d.getUTCSeconds()}\
${d.getUTCMilliseconds()}`;

        while (this.allId.has(id)) {
            id = (parseInt(id, 10) + 1).toString();
        }
        this.allId.add(id);
        return id;
    }
}
