import { async } from '@angular/core/testing';
import { IdGenerator } from './id-generator';

describe('IdGenerator', () => {

    let generator: IdGenerator;

    beforeEach(() => {
        generator = new IdGenerator();
    });

    it('all the id generated should be in the memory of the service', async(() => {
        const allId = new Array<string>();
        // tslint:disable-next-line: no-magic-numbers
        for ( let i = 0; i < 100; ++i) {
            allId.push(generator.generateId());
        }

        const generatedId = generator.ids;
        // tslint:disable-next-line: no-magic-numbers
        for ( let i = 0; i < 100; ++i) {
            expect(generatedId.has(allId[i])).toBeTruthy();
        }

    }));

    it('2 id cannot be the same', async(() => {
        const firstId = generator.generateId();
        const secondId = generator.generateId();

        expect(firstId).not.toBe(secondId);
    }));
});
