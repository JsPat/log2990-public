import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ShapesHolderService {

  private allShapes: Set<SVGGraphicsElement>;

  constructor() {
    this.allShapes = new Set<SVGGraphicsElement>();
  }

  get iterator(): IterableIterator<SVGGraphicsElement> {
    return this.allShapes.values();
  }

  get nOfShapes(): number {
    return this.allShapes.size;
  }

  addShape(shape: SVGGraphicsElement): void {
    this.allShapes.add(shape);
  }

  removeShape(shape: SVGGraphicsElement): void {
    this.allShapes.delete(shape);
  }

  contain(shape: SVGGraphicsElement): boolean {
    return this.allShapes.has(shape);
  }

  clear(): void {
    this.allShapes.clear();
  }
}
