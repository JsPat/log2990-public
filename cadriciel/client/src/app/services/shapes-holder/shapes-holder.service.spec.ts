/* tslint:disable: no-magic-numbers
   tslint:disable: no-string-literal
  */
import { async, inject, TestBed } from '@angular/core/testing';
import { ShapesHolderService } from './shapes-holder.service';

describe('Service: ShapesHolder', () => {
  let service: ShapesHolderService;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
        providers: [ ShapesHolderService ]
    });
  }));

  beforeEach(inject([ShapesHolderService], (injected: ShapesHolderService) => {
    service = injected;
  }));

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('a shape can be add using the addShape methods', () => {
    const testObj = jasmine.createSpyObj('SVGGraphicsElement', ['']);

    service.addShape(testObj);

    expect(service['allShapes'].has(testObj)).toBeTruthy();
  });

  it('an added shape can be removed using the remove methods', () => {
    const testObj = jasmine.createSpyObj('SVGGraphicsElement', ['']);

    service.addShape(testObj);

    service.removeShape(testObj);

    expect(service['allShapes'].has(testObj)).toBeFalsy();
  });

  it('we can get an iterator to iterate over all the shapes', () => {
    for (let i = 0; i < 5; ++i) {
      service.addShape(jasmine.createSpyObj('SVGGraphicsElement', ['']));
    }
    const iterator = service.iterator;

    for (let i = 0; i < 5; ++i) {
      expect(service['allShapes'].has(iterator.next().value)).toBeDefined();
    }
  });

  it('nOfShape should be 0 when the object just got created', () => {
    expect(service.nOfShapes).toBe(0);
  });

  it('nOfShapes Should increment when we are adding shapes', () => {
    for (let i = 0; i < 5; ++i) {
      service.addShape(jasmine.createSpyObj('SVGGraphicsElement', ['']));
      expect(service.nOfShapes).toBe(i + 1);
    }
  });

  it('nOfShapes Should decrement when we are removing shapes', () => {
    const object = new Array<SVGGraphicsElement>();
    for (let i = 0; i < 5; ++i) {
      object.push(jasmine.createSpyObj('SVGGraphicsElement', ['']));
      service.addShape(object[i]);
    }
    for (let i = 4; i > 0; --i) {
      service.removeShape(object[i]);
      expect(service.nOfShapes).toBe(i);
    }
  });

  it('contain should return true if the shape is contained in the shapeHolder', () => {
    const el = jasmine.createSpyObj('SVGGraphicsElement', ['']);

    service.addShape(el);

    expect(service.contain(el)).toBeTruthy(el);
  });

  it('contain should return false if the shape is not contained in the shapeHolder', () => {
    const el = jasmine.createSpyObj('SVGGraphicsElement', ['']);

    expect(service.contain(el)).toBeFalsy(el);
  });

  it('clear should clear the object that contain all shapes',  () => {
    for (let i = 0; i < 5; ++i) {
      service.addShape(jasmine.createSpyObj('SVGGraphicsElement', ['']));
    }
    service.clear();
    expect(service.nOfShapes).toEqual(0);
  });

});
