/* tslint:disable:no-unused-variable */
/* tslint:disable:no-magic-numbers */
/* tslint:disable:max-line-length */

import { inject, TestBed } from '@angular/core/testing';
import { CoordinateTransformer } from './coordinate-transformer';

describe('Service: CoordinateTransformer', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CoordinateTransformer]
    });
  });

  it('should ...', inject([CoordinateTransformer], (service: CoordinateTransformer) => {
    expect(service).toBeTruthy();
  }));
  it('applyTransformation should return the same point if no transformation was applied', inject([CoordinateTransformer], (service: CoordinateTransformer) => {
    const mockPoint = {x: 5, y: 23};
    const returnedPoint = service.applyTransformation(mockPoint, null);
    expect(returnedPoint).toEqual(mockPoint);
  }));
  it('applyTransform should return a point modified by the transformation matrix if it was provided', inject([CoordinateTransformer], (service: CoordinateTransformer) => {
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg') as SVGSVGElement;
    const matrix = svg.createSVGTransform();
    matrix.setTranslate(2, 3);
    const mockPoint = {x: 5, y: 23};
    const returnedPoint = service.applyTransformation(mockPoint, matrix);
    expect(returnedPoint).toEqual({x: 7, y: 26});
  }));
});
