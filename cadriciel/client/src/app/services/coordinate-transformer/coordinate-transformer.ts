import { Coordinate } from 'src/app/components/app/attributes/coordinate';

export class CoordinateTransformer {

  applyTransformation(point: Coordinate, svgMatrix: SVGTransform | null): Coordinate {
    // If svgMatrix is null, then there was no transformation to apply and so we return the point itself
    if (svgMatrix === null) {
      return point;
    }
    // Otherwise we apply the single transformation to the given point
    const matrix = svgMatrix.matrix;
    const tempX = point.x;
    point.x = matrix.a * point.x + matrix.c * point.y + matrix.e;
    point.y = matrix.b * tempX + matrix.d * point.y + matrix.f;
    return point;
  }
}
