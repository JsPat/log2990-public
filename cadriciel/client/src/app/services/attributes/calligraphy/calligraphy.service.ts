import { Injectable } from '@angular/core';
import { Tool } from 'src/app/components/app/attributes/tool';

@Injectable({
  providedIn: 'root'
})
export class CalligraphyService extends Tool {

  calligraphySize: number = 2;
  calligraphyAngle: number = 45;

  constructor() { super(); }

}
