/* tslint:disable: no-unused-variable
   tslint:disable: no-any
   tslint:disable: no-string-literal
   tslint:disable: no-magic-numbers*/

import { RendererFactory2 } from '@angular/core';
import { async, inject, TestBed } from '@angular/core/testing';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { Color } from 'src/app/color-palette/color/color';
import { BLACK } from 'src/app/color-palette/constant';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { SVGChildService } from '../../svg-child/svg-child.service';
import { SprayPaintService } from './spray-paint.service';

describe('Service: SprayPaint', () => {
    let service: SprayPaintService;
    let rendererFactorySpy: any;
    let commandInvokerSpy: any;
    let svgServiceSpy: any;
    let colorServiceSpy: any;
    let rendererSpy: any;
    let event: MouseEvent;

    const leftClick = 0;

    beforeEach(async(() => {
        commandInvokerSpy = jasmine.createSpyObj('CommandInvokerService', ['execute']);
        rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute', 'appendChild']);
        svgServiceSpy = jasmine.createSpyObj('SVGChildService', ['removeSVG', 'addNewSVG']);
        event = jasmine.createSpyObj('MouseEvent', ['']);

        rendererFactorySpy = {
          createRenderer(_0: null, _1: null): any {
            return rendererSpy;
          }
        };

        colorServiceSpy = {
            get mainColor(): Color {
                return BLACK;
            }
        };

        TestBed.configureTestingModule({
        providers: [
            SprayPaintService,
            { provide: CommandInvokerService, useValue: commandInvokerSpy},
            { provide: ColorService, useValue: colorServiceSpy },
            { provide: SVGChildService, useValue: svgServiceSpy },
            { provide: RendererFactory2, useValue: rendererFactorySpy}
        ]
        });
    }));

    beforeEach(inject([SprayPaintService], (injected: SprayPaintService) => {
        service = injected;
        jasmine.clock().install();
    }));

    afterEach(() => {
        jasmine.clock().uninstall();
    });

    it('should create', () => {
        expect(service).toBeTruthy();
    });

    it('should create renderer', () => {
        expect(service['renderer']).toEqual(rendererSpy);
    });

    it('mouseDown should do nothing if the event given is not the leftClick', () => {
        Object.defineProperty(event, 'button', {value: leftClick + 1});
        const cursorPostion = {x: 2, y: 3};
        const sprayPaintInstance = jasmine.createSpyObj('SprayPaint', ['']);
        const intervalId = 3;

        service['currentCursorPosition'] = cursorPostion;
        service['sprayPaintInstance'] = sprayPaintInstance;
        service['intervalID'] = intervalId;

        service.mouseDown(event);

        expect(service['currentCursorPosition']).toEqual(cursorPostion);
        expect(service['sprayPaintInstance']).toEqual(sprayPaintInstance);
        expect(service['intervalID']).toEqual(intervalId);
        expect(commandInvokerSpy.execute).not.toHaveBeenCalled();
    });

    it('mouseDown should do update his attributes and execute a new command if the event given is the leftClick', () => {
        Object.defineProperty(event, 'button', {value: leftClick});
        const offset = {x: 6, y: 12};
        Object.defineProperty(event, 'offsetX', {value: offset.x});
        Object.defineProperty(event, 'offsetY', {value: offset.y});

        const sprayPaintInstance = jasmine.createSpyObj('SprayPaint', ['']);
        const intervalId = 6;

        service['sprayPaintInstance'] = sprayPaintInstance;
        service['intervalID'] = intervalId;

        service.mouseDown(event);

        expect(service['currentCursorPosition']).toEqual(offset);
        expect(service['sprayPaintInstance']).not.toEqual(sprayPaintInstance);
        expect(service['intervalID']).not.toEqual(intervalId);
        expect(commandInvokerSpy.execute).toHaveBeenCalled();
    });

    it('if mouse is pressed on the left click, sprayPaint should be call evry time the timer reashed the asked frequency', () => {
        const frequency = 5;
        service['sprayPower'] = 1000 / frequency;
        Object.defineProperty(event, 'button', {value: leftClick});

        spyOn(service, 'sprayPaint');

        service.mouseDown(event);

        for ( let i = 1; i < 100; ++i) {
            jasmine.clock().tick(frequency);
            expect(service.sprayPaint).toHaveBeenCalledTimes(i);
        }
    });

    it('if mouse is not pressed, sprayPaint should not be call evry time the timer reashed the asked frequency', () => {
        const frequency = 5;
        service['sprayPower'] = 1000 / frequency;
        spyOn(service, 'sprayPaint');

        for ( let i = 1; i < 100; ++i) {
            jasmine.clock().tick(frequency);
            expect(service.sprayPaint).not.toHaveBeenCalled();
        }
    });

    it('sprayPaint should call addPaintSpeck on the SprayPaint with the current location',  () => {
        const sprayPaintSpy = jasmine.createSpyObj('SprayPaint', ['addPaintSpeck']);
        service['sprayPaintInstance'] = sprayPaintSpy;
        const cursorPostion = {x: 7, y: 3};
        service['currentCursorPosition'] = cursorPostion;

        service.sprayPaint();
        expect(sprayPaintSpy.addPaintSpeck).toHaveBeenCalledWith(cursorPostion);
    });

    it('mouse up should stop any spray to be bind to the clock', () => {
        const frequency = 5;
        service['sprayPower'] = 1000 / frequency;
        Object.defineProperty(event, 'button', {value: leftClick});

        spyOn(service, 'sprayPaint');

        service.mouseDown(event);

        for ( let i = 1; i < 100; ++i) {
            jasmine.clock().tick(frequency);
            i < 50 ? expect(service.sprayPaint).toHaveBeenCalledTimes(i) : expect(service.sprayPaint).toHaveBeenCalledTimes(50);

            if (i === 50) { service.mouseUp(); }
        }
    });

    it('mouseDrag should update the cursor position', () => {
        const offset = {x: 9, y: 1};
        Object.defineProperty(event, 'offsetX', {value: offset.x});
        Object.defineProperty(event, 'offsetY', {value: offset.y});

        service.mouseDrag(event);

        expect(service['currentCursorPosition']).toEqual(offset);
    });

    it('mouseLeave should call mouseUp', () => {
        spyOn(service, 'mouseUp');
        service.mouseLeave();

        expect(service.mouseUp).toHaveBeenCalled();
    });

    it('toolsSwap should call mouseUp', () => {
        spyOn(service, 'mouseUp');
        service.toolSwap();

        expect(service.mouseUp).toHaveBeenCalled();
    });
});
