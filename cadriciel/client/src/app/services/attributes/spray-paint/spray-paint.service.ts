import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { CreateShapeCommand } from 'src/app/components/app/attributes/command/create-shape-command';
import { Coordinate } from 'src/app/components/app/attributes/coordinate';
import { SprayPaint } from 'src/app/components/app/attributes/spray-paint/spray-paint';
import { BASIC_WIDTH, Tool } from 'src/app/components/app/attributes/tool';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { SVGChildService } from '../../svg-child/svg-child.service';

const BASIC_SPRAY_POWER: number = 40;
const MILLISECONDS_IN_A_SECOND: number = 1000;

@Injectable({
  providedIn: 'root'
})
export class SprayPaintService extends Tool {

  private sprayPower: number = BASIC_SPRAY_POWER;
  private sprayDiameter: number = BASIC_WIDTH;

  private intervalID: number;
  private currentCursorPosition: Coordinate;
  private sprayPaintInstance: SprayPaint;
  private renderer: Renderer2;

  constructor(rendererFactory: RendererFactory2,
              private commandInvoker: CommandInvokerService,
              private svgService: SVGChildService,
              private colorService: ColorService) {
    super();
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  mouseDown(event: MouseEvent): void {
    if (event.button === this.LEFT_CLICK) {
      this.currentCursorPosition = {x: event.offsetX, y: event.offsetY};
      this.sprayPaintInstance = new SprayPaint(this.colorService.mainColor, this.sprayDiameter / 2, this.renderer);
      this.commandInvoker.execute(new CreateShapeCommand(this.sprayPaintInstance, this.svgService));
      this.intervalID = setInterval(this.sprayPaint.bind(this) as TimerHandler, MILLISECONDS_IN_A_SECOND / this.sprayPower);
    }
  }

  sprayPaint(): void {
    this.sprayPaintInstance.addPaintSpeck(this.currentCursorPosition);
  }

  mouseUp(): void {
    clearInterval(this.intervalID);
  }

  mouseDrag(event: MouseEvent): void {
    this.currentCursorPosition = {x: event.offsetX, y: event.offsetY};
  }

  mouseLeave(): void {
    this.mouseUp();
  }

  toolSwap(): void {
    this.mouseUp();
  }

}
