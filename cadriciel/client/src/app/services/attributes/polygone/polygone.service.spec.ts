/* tslint:disable: no-unused-variable
   tslint:disable: no-any
   tslint:disable: no-string-literal
   tslint:disable: no-magic-numbers */

import { NO_ERRORS_SCHEMA, RendererFactory2 } from '@angular/core';
import { async, inject, TestBed } from '@angular/core/testing';

import { Color } from 'src/app/color-palette/color/color';
import { Polygone } from 'src/app/components/app/attributes/polygone/polygone';
import { RectanglePreview } from 'src/app/components/app/attributes/rectangle/rectangle-preview';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { SVGChildService } from '../../svg-child/svg-child.service';
import { PolygoneService } from './polygone.service';

describe('PolygoneService', () => {
  let service: PolygoneService;

  let color: Color;
  let strokeColor: Color;
  let event: MockMouseEvent;
  let svgServiceSpy: SVGChildService;
  let commandInvokerSpy: CommandInvokerService;

  let rendererSpy: any;
  let rendererFactorySpy: any;

  let polygone: any;

  class MockColor extends Color {
    constructor() {
        super(0, 0, 0);
    }
  }
  // tslint:disable-next-line: max-classes-per-file
  class MockMouseEvent extends MouseEvent {
    mockoffsetX: number;
    mockoffsetY: number;

    constructor() {
      super('mousedown');
    }
  }

  beforeEach(async(() => {

    rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute', 'appendChild']);
    svgServiceSpy = jasmine.createSpyObj('SVGChildService', ['removeSVG', 'appendNewSVG', 'removeSVG']);
    commandInvokerSpy = jasmine.createSpyObj('CommandInvoker', ['execute', 'removeLastExecuted']);

    rendererFactorySpy = {
      createRenderer(_0: null, _1: null): any {
        return rendererSpy;
      }
    };

    TestBed.configureTestingModule({
      providers: [
        PolygoneService,
        { provide: Color, useClass: MockColor},
        { provide: MouseEvent, useClass: MockMouseEvent},
        { provide: RendererFactory2, useValue: rendererFactorySpy},
        { provide: CommandInvokerService, useValue: commandInvokerSpy},
        { provide: SVGChildService, useValue: svgServiceSpy},
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  }));
  beforeEach(inject([PolygoneService], (injected: PolygoneService) => {
    service = injected;
    polygone = jasmine.createSpyObj('Polygone', ['drawShape', 'updateShape', 'isNull']);

    service['mousePosition'] = {x: 0, y: 0};
    service['start'] = {x: 0, y: 0};
    service['polygone'] = polygone;
    service['previewShape'] = new RectanglePreview(service['mousePosition'], rendererSpy);

    color = new MockColor();
    strokeColor = new MockColor();
    event = new MockMouseEvent();
}));

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  // toolSwap
  it('toolSwap calls mouseUp', () => {
    spyOn(service, 'mouseUp');
    service['toolSwap']();
    expect(service.mouseUp).toHaveBeenCalled();
  });

  // createPreview()
  it('createPreview should add a shape to the SVGChildService', () => {
    service['createPreview']();
    expect(svgServiceSpy.appendNewSVG).toHaveBeenCalled();
  });

  it('createPreview should reassign previewShape to a new Rectangle', () => {
    service['start'] = {x: 5, y: 5};
    service['previewShape']['width'] = 3;
    service['previewShape']['height'] = 4;
    service['previewShape']['position'].x = 10;
    service['previewShape']['position'].y = 10;
    service['createPreview']();
    expect(service['previewShape']['position'].x).toEqual(service['start'].x);
    expect(service['previewShape']['position'].y).toEqual(service['start'].y);
    expect(service['previewShape']['width']).toEqual(service['previewShape']['width']);
    expect(service['previewShape']['height']).toEqual(service['previewShape']['height']);
  });

  // mouseDown()
  it('mouseDown should send a polygone via a commands', () => {
    service.mouseDown(event);
    expect(commandInvokerSpy.execute).toHaveBeenCalled();
  });

  it('mouseDown should only work if triggered by a left click', () => {
    spyOnProperty(event, 'button').and.returnValue(2);
    service.mouseDown(event);
    expect(commandInvokerSpy.execute).not.toHaveBeenCalled();
  });

  it('mouseDown should reassign polygone to a new Polygone', () => {
    service['polygone'] = new Polygone(color, strokeColor, 5, {x: 0, y: 0}, true, true, 5, rendererSpy);
    service['strokeWidth'] = 1;
    service.mouseDown(event);
    expect(service['previewShape']['position']).toEqual({x: event.offsetX, y: event.offsetY});
    expect(service['previewShape']['strokeWidth']).toEqual(service['strokeWidth']);
  });

  // mouseUp()
  it('mouseUp should only assigned mousePressed (not remove the preview) if polygone is undefined', () => {
    spyOn(service, 'mouseUp').and.callThrough();
    service['polygone'] = undefined;
    service.mouseUp();
    expect(svgServiceSpy.removeSVG).not.toHaveBeenCalled();
  });

  it('mouseUp should assigned mousePressed, polygone and remove the preview if polygone is defined', () => {
    spyOn(service, 'mouseUp').and.callThrough();
    service.mouseUp();
    expect(svgServiceSpy.removeSVG).toHaveBeenCalled();
    expect(service['polygone']).toBeUndefined();
  });

  it('mouseUp should remove the polygone if the polygone is null', () => {
    polygone.isNull.and.returnValue(true);
    service.mouseUp();
    expect(commandInvokerSpy.removeLastExecuted).toHaveBeenCalled();
    expect(svgServiceSpy.removeSVG).toHaveBeenCalledWith(polygone);
  });

  it('mouseUp should not remove the polygone if the polygone is not null', () => {
    polygone.isNull.and.returnValue(false);
    service.mouseUp();
    expect(commandInvokerSpy.removeLastExecuted).not.toHaveBeenCalled();
    expect(svgServiceSpy.removeSVG).not.toHaveBeenCalledWith(polygone);
  });

  // mouseLeave()
  it('mouseLeave should call mouseUp', () => {
    spyOn(service, 'mouseLeave').and.callThrough();
    spyOn(service, 'mouseUp');
    service.mouseLeave();
    expect(service.mouseUp).toHaveBeenCalled();
  });

  // mouseDrag()
  it('mouseDrag should always return undefined', () => {
    const result = service.mouseDrag(event);
    expect(result).toBeUndefined();
  });

  it('mouseDrag should call polygone.updateShape and change mousePosition if polygone is defined', () => {
    spyOn(service, 'mouseDrag').and.callThrough();
    service['mousePosition'] = {x: 2, y: 2};
    service.mouseDrag(event);
    if (service['polygone']) {
      expect(polygone.updateShape).toHaveBeenCalled();
    }
    expect(service['mousePosition']).toEqual({x: event.offsetX, y: event.offsetY});
  });

  it('mouseDrag should call do nothing if polygone is undefined', () => {
    spyOn(service, 'mouseDrag').and.callThrough();
    service['polygone'] = undefined;
    service['mousePosition'] = {x: 6, y: 10};
    service.mouseDrag(event);
    expect(service['mousePosition']).toEqual({x: 6, y: 10});
  });

  it('mouseDrag should call 1 time updateShape, change mousePosition if polygone is defined', () => {
    service['polygone'] = new Polygone(color, strokeColor, 5, {x: 0, y: 0}, true, true, 5, rendererSpy);
    spyOn(service['polygone'], 'updateShape');
    spyOn(service['previewShape'], 'updateShape');
    // ['mousePosition'] is different than {0,0}
    service['mousePosition'] = {x: 2, y: 2};
    service.mouseDrag(event);
    expect(service['polygone'].updateShape).toHaveBeenCalledTimes(1);
    expect(service['previewShape'].updateShape).toHaveBeenCalled();
    // the event.offset will set the currentposition to {0,0}
    // because event.offset is readonly and can't be set to another value for testing
    expect(service['mousePosition']).toEqual({x: event.offsetX, y: event.offsetY});
  });
});
