import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { CreateShapeCommand } from 'src/app/components/app/attributes/command/create-shape-command';
import { Coordinate } from 'src/app/components/app/attributes/coordinate';
import { Polygone } from 'src/app/components/app/attributes/polygone/polygone';
import { RectanglePreview } from 'src/app/components/app/attributes/rectangle/rectangle-preview';
import { BASIC_WIDTH, Tool } from 'src/app/components/app/attributes/tool';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { SVGChildService } from '../../svg-child/svg-child.service';

const BASIC_SIDE_NUMBER: number = 5;

@Injectable({
  providedIn: 'root'
})
export class PolygoneService extends Tool {

  borderEnabled: boolean = true;
  fillEnabled: boolean = true;
  private strokeWidth: number = BASIC_WIDTH;
  private sidesNumber: number = BASIC_SIDE_NUMBER;

  private polygone: Polygone | undefined;
  private mousePosition: Coordinate;
  private start: Coordinate;
  private previewShape: RectanglePreview;

  private renderer: Renderer2;

  constructor(private colorService: ColorService, rendererFactory: RendererFactory2,
              private svgService: SVGChildService, private commandInvoker: CommandInvokerService) {
    super();
    this.start = {x: 0, y: 0};
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  mouseDown(event: MouseEvent): void {
    if (event.button === this.LEFT_CLICK) {
      this.mousePosition = {x: event.offsetX, y: event.offsetY};
      this.start = this.mousePosition;
      this.polygone = new Polygone(
        this.colorService.mainColor,
        this.colorService.secondaryColor,
        this.strokeWidth,
        this.start,
        this.borderEnabled,
        this.fillEnabled,
        this.sidesNumber,
        this.renderer
      );
      this.polygone.updateShape(this.mousePosition);
      this.commandInvoker.execute(new CreateShapeCommand(this.polygone, this.svgService));
      this.createPreview();
    }
  }

  mouseLeave(): void {
    this.mouseUp();
  }

  mouseUp(): void {
    if (this.polygone) {
      if (this.polygone.isNull()) {
        this.svgService.removeSVG(this.polygone);
        this.commandInvoker.removeLastExecuted();
      }
      this.polygone = undefined;
      this.svgService.removeSVG(this.previewShape);
    }
  }

  toolSwap(): void {
    this.mouseUp();
  }

  mouseDrag(event: MouseEvent): void {
    if (this.polygone) {
      this.mousePosition = {x: event.offsetX, y: event.offsetY};
      this.polygone.updateShape(this.mousePosition);
      this.previewShape.updateShape(this.mousePosition, false);
    }
  }

  private createPreview(): void {
    this.previewShape = new RectanglePreview(this.start, this.renderer);
    this.previewShape.updateShape(this.mousePosition, false);
    this.svgService.appendNewSVG(this.previewShape.svgDrawElement);
  }
}
