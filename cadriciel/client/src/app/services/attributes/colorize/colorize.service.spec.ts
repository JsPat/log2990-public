/* tslint:disable:no-unused-variable
   tslint:disable: no-string-literal
   tslint:disable-next-line: no-any*/

import { async, inject, TestBed } from '@angular/core/testing';
import { BLACK, WHITE } from 'src/app/color-palette/constant';
import { ChangeColorCommand } from 'src/app/components/app/attributes/command/change-color-command';
import { MultiCommand } from 'src/app/components/app/attributes/command/multi-command';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import * as json from './colorize.json';
import { ColorizeService } from './colorize.service';

const config = json.default;

describe('Service: Colorize', () => {

    let service: ColorizeService;
    let selector: any;
    let event: any;
    let commandInvokerSpy: any;
    let svgElementSpy: any;

    beforeEach(async(() => {

        commandInvokerSpy = jasmine.createSpyObj('CommandInvokerService', ['execute']);

        TestBed.configureTestingModule({
            providers: [
                ColorizeService,
                {provide: CommandInvokerService, useValue: commandInvokerSpy},
            ]
        });
      }));

    beforeEach(inject([ColorizeService], (injected: ColorizeService) => {
       service = injected;
       selector = jasmine.createSpyObj('SVGSelector', ['selectAtPosition']);
       event = jasmine.createSpyObj('MouseEvent', ['preventDefault']);
       svgElementSpy = jasmine.createSpyObj('SVGElement', ['getAttribute']);

       service['selector'] = selector;
    }));

    it('should create', () => {
        expect(service).toBeTruthy();
    });

    // click
    it('click should not send a new command if the svg selector retrun null', () => {
        selector.selectAtPosition.and.returnValue(null);

        service.click(event);

        expect(commandInvokerSpy.execute).not.toHaveBeenCalled();
    });

    it('click should throw an error if the element return undefined for his main color', () => {
        const dataset = {
            mainColor: undefined,
        };

        Object.defineProperty(svgElementSpy, 'dataset', {value: dataset});

        selector.selectAtPosition.and.returnValue(svgElementSpy);

        expect(() => { service.click(event); } ).toThrowError();
    });

    it('click should not send a new command if get command return null', () => {
        const dataset = {
            mainColor: 'fill',
        };

        Object.defineProperty(svgElementSpy, 'dataset', {value: dataset});

        selector.selectAtPosition.and.returnValue(svgElementSpy);
        spyOn<any>(service, 'getCommand').and.returnValue(null);

        service.click(event);

        expect(commandInvokerSpy.execute).not.toHaveBeenCalled();

    });

    it('click should send a new command if get command return a command', () => {
        const dataset = {
            mainColor: 'fill',
        };
        const command = jasmine.createSpyObj('Command', ['do', 'undo']);

        Object.defineProperty(svgElementSpy, 'dataset', {value: dataset});

        selector.selectAtPosition.and.returnValue(svgElementSpy);
        spyOn<any>(service, 'getCommand').and.returnValue(command);

        service.click(event);
        expect(commandInvokerSpy.execute).toHaveBeenCalled();

    });

    // rightClick
    it('rightClick should not send a new command if the svg selector retrun null', () => {
        selector.selectAtPosition.and.returnValue(null);

        service.rightClick(event);

        expect(commandInvokerSpy.execute).not.toHaveBeenCalled();
    });

    it('rightClick should throw an error if the element return undefined for his secondaryColor', () => {
        const dataset = {
            secondaryColor: undefined,
        };

        Object.defineProperty(svgElementSpy, 'dataset', {value: dataset});

        selector.selectAtPosition.and.returnValue(svgElementSpy);

        expect(() => { service.rightClick(event); } ).toThrowError();
    });

    it('rightClick should not send a new command if get command return null', () => {
        const dataset = {
            secondaryColor: 'fill',
        };

        Object.defineProperty(svgElementSpy, 'dataset', {value: dataset});

        selector.selectAtPosition.and.returnValue(svgElementSpy);
        spyOn<any>(service, 'getCommand').and.returnValue(null);

        service.rightClick(event);

        expect(commandInvokerSpy.execute).not.toHaveBeenCalled();

    });

    it('rightClick should send a new command if get command return a command', () => {
        const dataset = {
            secondaryColor: 'fill',
        };
        const command = jasmine.createSpyObj('Command', ['do', 'undo']);

        Object.defineProperty(svgElementSpy, 'dataset', {value: dataset});

        selector.selectAtPosition.and.returnValue(svgElementSpy);
        spyOn<any>(service, 'getCommand').and.returnValue(command);

        service.rightClick(event);
        expect(commandInvokerSpy.execute).toHaveBeenCalled();

    });

    // getCommand
    it('getCommand should trhow if the tag is none of the possible value', () => {
        expect(() => { service['getCommand']('not-defined', svgElementSpy, BLACK); }).toThrowError();
    });

    it('getCommand should return a ChangeColorCommand if the tag is fill and isColorEqual return false', () => {
        spyOn<any>(service, 'isColorEqual').and.returnValue(false);
        expect( service['getCommand'](config.fill.identifier, svgElementSpy, BLACK) instanceof ChangeColorCommand ).toBeTruthy();
    });

    it('getCommand should return null if the tag is fill but isColorEqual return true', () => {
        spyOn<any>(service, 'isColorEqual').and.returnValue(true);
        expect( service['getCommand'](config.fill.identifier, svgElementSpy, BLACK)).toBeNull();
    });

    it('getCommand should return a ChangeColorCommand if the tag is stroke and isColorEqual return false', () => {
        spyOn<any>(service, 'isColorEqual').and.returnValue(false);
        expect( service['getCommand'](config.stroke.identifier, svgElementSpy, BLACK) instanceof ChangeColorCommand ).toBeTruthy();
    });

    it('getCommand should return null if the tag is stroke but isColorEqual return true', () => {
        spyOn<any>(service, 'isColorEqual').and.returnValue(true);
        expect( service['getCommand'](config.stroke.identifier, svgElementSpy, BLACK)).toBeNull();
    });

    it('getCommand should return a multiCommand if the tag is fill&stroke', () => {
        expect( service['getCommand'](config.both, svgElementSpy, BLACK) instanceof MultiCommand ).toBeTruthy();
    });

    it('getCommand should return a null if the tag is fill&stroke and stroke color is equal', () => {
        spyOn<any>(service, 'isColorEqual').and.returnValue(true);
        expect( service['getCommand'](config.both, svgElementSpy, BLACK)).toBeNull();
    });

    it('getCommand should return a null if the tag is fill&stroke and fill color is equal', () => {
        spyOn<any>(service, 'isColorEqual').and.returnValues(false, true);
        expect( service['getCommand'](config.both, svgElementSpy, BLACK)).toBeNull();
    });

    it('getCommand should return null if the tag none', () => {
        expect( service['getCommand'](config.none, svgElementSpy, BLACK)).toBeNull();
    });

    // isColorEqual
    it('isColorEqual should retrun true if the current color is the same as the new color', () => {
        svgElementSpy.getAttribute.and.returnValues(`#${BLACK.rgb_hexa}`, `${BLACK.a}`);
        expect(service['isColorEqual']('', '', svgElementSpy, BLACK)).toBeTruthy();
    });

    it('isColorEqual should retrun false if the current color rgb is not the same as the new color', () => {
        svgElementSpy.getAttribute.and.returnValues(`#${WHITE.rgb_hexa}`, `${BLACK.a}`);
        expect(service['isColorEqual']('', '', svgElementSpy, BLACK)).toBeFalsy();
    });

    it('isColorEqual should retrun false if the current color a is not the same as the new color', () => {
        svgElementSpy.getAttribute.and.returnValues(`#${BLACK.rgb_hexa}`, `${BLACK.a / 2}`);
        expect(service['isColorEqual']('', '', svgElementSpy, BLACK)).toBeFalsy();
    });
});
