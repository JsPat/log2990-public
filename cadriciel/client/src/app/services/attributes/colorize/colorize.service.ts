import { Injectable } from '@angular/core';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { Color } from 'src/app/color-palette/color/color';
import { ChangeColorCommand } from 'src/app/components/app/attributes/command/change-color-command';
import { MultiCommand } from 'src/app/components/app/attributes/command/multi-command';
import { SVGSelector } from 'src/app/components/app/attributes/svg-selector/svg-selector';
import { Tool } from 'src/app/components/app/attributes/tool';
import { Command } from '../../command-invoker/command';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import * as JSON from './colorize.json';

const config = JSON.default;

@Injectable({
  providedIn: 'root'
})
export class ColorizeService extends Tool {

  private selector: SVGSelector;

  constructor(private colorService: ColorService, private commandInvoker: CommandInvokerService) {
    super();
    this.selector = new SVGSelector();
  }

  click(event: MouseEvent): void {
    const el = this.selector.selectAtPosition(event);
    if (el === null) { return; }

    const tag = el.dataset.mainColor;
    if (tag === undefined) {
      throw new Error(config.error.mainColor + `${el}`);
    }

    const command = this.getCommand(tag, el, this.colorService.mainColor);

    if (command !== null) {
      this.commandInvoker.execute(command);
    }
  }

  rightClick(event: MouseEvent): void {
    event.preventDefault();
    const el = this.selector.selectAtPosition(event);
    if (el === null) { return; }

    const tag = el.dataset.secondaryColor;
    if (tag === undefined) {
      throw new Error(config.error.secondaryColor + `${el}`);
    }

    const command = this.getCommand(tag, el, this.colorService.secondaryColor);

    if (command !== null) {
      this.commandInvoker.execute(command);
    }
  }

  private getCommand(tag: string, el: SVGGraphicsElement, color: Color): Command | null {

    switch (tag) {
      case config.fill.identifier: {
        const colorTag = config.fill.color;
        const transparencyTag = config.fill.transparency;

        if (this.isColorEqual(colorTag, transparencyTag, el, color)) {
          return null;
        } else {
          return  new ChangeColorCommand(el, colorTag, transparencyTag, color);
        }
      }
      case config.stroke.identifier: {
        const colorTag = config.stroke.color;
        const transparencyTag = config.stroke.transparency;

        if (this.isColorEqual(colorTag, transparencyTag, el, color)) {
          return null;
        } else {
          return  new ChangeColorCommand(el, colorTag, transparencyTag, color);
        }
      }
      case config.both: {

        const command1 = this.getCommand(config.stroke.identifier, el, color);
        if (command1 === null) { return null; }

        const command2 = this.getCommand(config.fill.identifier, el, color);
        if (command2 === null) { return null; }

        return new MultiCommand([command1, command2]);
      }
      case config.none: {
        return null;
      }
      default:
        throw new Error(`${tag} ${config.error.tag}: ${el}`);
    }
  }

  private isColorEqual(colorTag: string, transparencyTag: string, el: SVGGraphicsElement, color: Color): boolean {
    return el.getAttribute(colorTag) === `#${color.rgb_hexa}` && el.getAttribute(transparencyTag) === `${color.a}`;
  }
}
