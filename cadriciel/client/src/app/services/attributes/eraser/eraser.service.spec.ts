// tslint:disable: no-magic-numbers
// tslint:disable:no-unused-variable
// tslint:disable: no-string-literal
// tslint:disable: no-any
// tslint:disable: max-file-line-count

import { NO_ERRORS_SCHEMA, Renderer2, RendererFactory2 } from '@angular/core';
import { async, inject, TestBed } from '@angular/core/testing';
import { Color } from 'src/app/color-palette/color/color';
import { MultiCommand } from 'src/app/components/app/attributes/command/multi-command';
import { Coordinate } from 'src/app/components/app/attributes/coordinate';
import { Eraser } from '../../../../app/components/app/attributes/eraser/eraser';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { SVGChildService } from '../../svg-child/svg-child.service';
import { EraserService } from './eraser.service';

describe('EraserService', () => {
  let service: EraserService;

  let rendererSpy: any;
  let rendererFactorySpy: any;
  let svgServiceSpy: any;
  let svgGraphicElementSpy: any;
  let selectorSpy: any;
  let eraseRadiusReturn: any[];
  let commandInvokerSpy: any;

  // let color: Color;
  let mousePosition: Coordinate = {x: 5, y: 5};
  let event: any;
  let svgSvgSpy: any;

  class MockColor extends Color {
    constructor() {
        super(0, 0, 0);
    }
  }

  beforeEach(async(() => {
    rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute', 'appendChild']);
    svgServiceSpy = jasmine.createSpyObj('SVGChildService', ['removeSVG', 'appendNewSVG']);
    svgGraphicElementSpy = jasmine.createSpyObj('SVGGraphicsElement', ['getAttribute', 'setAttribute']);
    event = jasmine.createSpyObj('MouseEvent', ['']);
    commandInvokerSpy = jasmine.createSpyObj('CommandInvokerService', ['execute']);
    selectorSpy = jasmine.createSpyObj('SVGSelector', ['getParentIfneeded', 'isSelectable']);

    rendererFactorySpy = {
      createRenderer(_0: null, _1: null): any {
        return rendererSpy;
      }
    };

    TestBed.configureTestingModule({
      providers: [
        EraserService,
        { provide: Renderer2, useValue: rendererSpy},
        { provide: Color, useClass: MockColor},
        { provide: RendererFactory2, useValue: rendererFactorySpy},
        { provide: SVGChildService, useValue: svgServiceSpy},
        { provide: CommandInvokerService, useValue: commandInvokerSpy}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  }));

  beforeEach(inject([EraserService], (injected: EraserService) => {

    service = injected;

    eraseRadiusReturn = [];
    for (let _ = 0;  _ < 6; ++_) {
      eraseRadiusReturn.push(jasmine.createSpyObj('SVGElement', ['']));
    }

    spyOn<any>(service, 'eraseRadius').and.returnValue(eraseRadiusReturn);

    mousePosition = {x: 1, y: 1};
    service['eraser'] = new Eraser(mousePosition, 5, rendererSpy);
    service['selector'] = selectorSpy;
    svgSvgSpy = jasmine.createSpyObj('SVGSVGSelector', ['getIntersectionList', 'createSVGRect']);
    const rectSpy = {
      x: 0,
      y: 0,
      height: 0,
      width: 0,
    };

    svgSvgSpy.createSVGRect.and.returnValue(rectSpy);

    spyOn(document, 'querySelector').and.returnValue(svgSvgSpy);
    Object.defineProperty(event, 'button', {value: 0});
  }));

  it('mouseLeave should remove the eraser if eraser is defined and restore color', () => {
    const restoreAllColor = spyOn<any>(service, 'restoreAllColor');
    service.mouseLeave();
    expect(service['eraser']).toBeUndefined();
    expect(svgServiceSpy.removeSVG).toHaveBeenCalled();
    expect(restoreAllColor).toHaveBeenCalled();
  });

  it('mouseLeave should not remove the eraser and restore color if eraser is undefined', () => {
    const restoreAllColor = spyOn<any>(service, 'restoreAllColor');
    service['eraser'] = undefined;
    service.mouseLeave();

    expect(svgServiceSpy.removeSVG).not.toHaveBeenCalled();
    expect(restoreAllColor).not.toHaveBeenCalled();
  });

  // toolSwap
  it('toolSwap should call mouse Leave', () => {
    const mouseLeave = spyOn<any>(service, 'mouseLeave');
    service.toolSwap();
    expect(mouseLeave).toHaveBeenCalled();
  });

  // click
  it('click should set multipleEraseCommand to undefined', () => {
    service['multipleEraseCommand'] = jasmine.createSpyObj('MultiCommand', ['']);
    spyOn<any>(service, 'checkIfContained').and.returnValue(false);
    service.click();
    expect(service['multipleEraseCommand']).toBeUndefined();
    expect(service['checkIfContained']).toHaveBeenCalled();
  });

  it('click should not call checkIfContained if eraser is undefined', () => {
    const checkIfContained = spyOn<any>(service, 'checkIfContained');
    service['eraser'] = undefined;

    service.click();

    expect(checkIfContained).not.toHaveBeenCalled();
  });

  it('click should not call eraseOne if checkIfContained return false', () => {
    const eraseOne = spyOn<any>(service, 'eraseOne');
    spyOn<any>(service, 'checkIfContained').and.returnValue(false);

    service.click();

    expect(eraseOne).not.toHaveBeenCalled();
  });

  it('click should call eraseOne one time if checkIfContained return true', () => {
    const eraseOne = spyOn<any>(service, 'eraseOne');
    spyOn<any>(service, 'checkIfContained').and.returnValue(true);

    service.click();

    expect(eraseOne).toHaveBeenCalledTimes(1);
  });

  // mouse move
  it('mouseMouve should call create new if eraser is undefined',  () => {
    const createNew = spyOn<any>(service, 'createNew');
    service['eraser'] = undefined;

    service.mouseMove(event);

    expect(createNew).toHaveBeenCalled();
  });

  it('mouseMouve should not call create new if eraser is not undefined',  () => {
    const createNew = spyOn<any>(service, 'createNew');
    spyOn<any>(service, 'checkIfContained').and.returnValue(false);

    service.mouseMove(event);

    expect(createNew).not.toHaveBeenCalled();
  });

  it('mouseMouve should call restoreAllColor and updateShape if eraser is not undefined',  () => {
    const restoreAllColor = spyOn<any>(service, 'restoreAllColor');
    const updateShape = spyOn<any>(service['eraser'], 'updateShape');
    spyOn<any>(service, 'checkIfContained').and.returnValue(false);

    service.mouseMove(event);

    expect(restoreAllColor).toHaveBeenCalled();
    expect(updateShape).toHaveBeenCalled();
  });

  it('mouseMouve should not call changeColor if checkIfContained return false', () => {
    const changeColor = spyOn<any>(service, 'changeColor');
    spyOn<any>(service, 'checkIfContained').and.returnValue(false);

    service.mouseMove(event);

    expect(changeColor).toHaveBeenCalledTimes(0);
  });

  it('mouseMouve should call changeColor with all element except first if checkIfContained return true', () => {
    const changeColor = spyOn<any>(service, 'changeColor');
    spyOn<any>(service, 'checkIfContained').and.returnValue(true);

    service.mouseMove(event);

    for (let i = 1;  i < 6; ++i) {
      expect(changeColor).toHaveBeenCalledWith(service['selector'].getParentIfneeded(eraseRadiusReturn[i]));
    }
  });

  // mouseDrag
  it('mouseDrag should not trigger if click event was not LEFT_CLICK', () => {
    const checkIfContained = spyOn<any>(service, 'checkIfContained');
    Object.defineProperty(event, 'button', {value: 2});

    service.mouseDrag(event);

    expect(checkIfContained).not.toHaveBeenCalled();
  });

  it('mouseDrag should not call checkIfContained if eraser is undefined', () => {
    const checkIfContained = spyOn<any>(service, 'checkIfContained');
    service['eraser'] = undefined;

    service.mouseDrag(event);

    expect(checkIfContained).not.toHaveBeenCalled();
  });

  it('mouseDrag should not call eraseShapeDrag if  checkIfContained return false', () => {
    spyOn<any>(service, 'checkIfContained').and.returnValue(false);
    const eraseShapeDrag = spyOn<any>(service, 'eraseShapeDrag');

    service.mouseDrag(event);

    expect(eraseShapeDrag).not.toHaveBeenCalled();
  });

  it('mouseDrag should call eraseShapeDrag with all element except first if checkIfContained return true', () => {
    const changeColor = spyOn<any>(service, 'eraseShapeDrag');
    spyOn<any>(service, 'checkIfContained').and.returnValue(true);

    service.mouseDrag(event);

    for (let i = 1;  i < 6; ++i) {
      expect(changeColor).toHaveBeenCalledWith(service['selector'].getParentIfneeded(eraseRadiusReturn[i]));
    }
  });

  // createNew
  it('createNew assigns the eraser to a newErase and append it to the svg ', () => {
    service['eraser'] = undefined;
    service['createNew']();
    expect(service['eraser']).toBeDefined();
    expect(svgServiceSpy.appendNewSVG).toHaveBeenCalled();
  });

  // checkIfContained

  it('checkIfContained return false if isSelectable return false', () => {
    selectorSpy.isSelectable.and.returnValue(false);
    const element = document.createElementNS('http://www.w3.org/2000/svg', 'path') as SVGPathElement;
    const result = service['checkIfContained'](element);
    expect(result).toBeFalsy();
  });

  it('checkIfContained return false if fill is set', () => {
    selectorSpy.isSelectable.and.returnValue(true);
    const element = document.createElementNS('http://www.w3.org/2000/svg', 'path') as SVGPathElement;
    element.setAttribute('fill', '#FFFFFF');
    element.setAttribute('points', '1,1');
    const result = service['checkIfContained'](element);
    expect(result).toBeFalsy();
  });

  it('checkIfContained return true if point is null', () => {
    const element = document.createElementNS('http://www.w3.org/2000/svg', 'path') as SVGPathElement;
    spyOn(element, 'getAttribute').and.returnValue(null);
    selectorSpy.isSelectable.and.returnValue(true);

    const result = service['checkIfContained'](element);
    expect(result).toBeTruthy();
  });

  it('checkIfContained return true if path is instance of SVGPolygoneElment', () => {
    const element = document.createElementNS('http://www.w3.org/2000/svg', 'polygon') as any;
    element.setAttribute('points', '1,1');
    element.setAttribute('fill', '#FFFFFF');
    selectorSpy.isSelectable.and.returnValue(true);

    const result = service['checkIfContained'](element);
    expect(result).toBeTruthy();
  });

  it('checkIfContained return true if intersectWithEraser return true', () => {
    const element = document.createElementNS('http://www.w3.org/2000/svg', 'path') as any;
    element.setAttribute('points', '1,1 2,1 3,1 4,2');
    element.setAttribute('fill', '#FFFFFF');
    selectorSpy.isSelectable.and.returnValue(true);

    spyOn<any>(service, 'intersectWithEraser').and.returnValue(true);

    const result = service['checkIfContained'](element);
    expect(result).toBeTruthy();
  });

  it('checkIfContained return false if intersectWithEraser return false', () => {
    selectorSpy.isSelectable.and.returnValue(true);
    const element = document.createElementNS('http://www.w3.org/2000/svg', 'path') as any;
    element.setAttribute('points', '1,1 2,1 3,1 4,2');
    element.setAttribute('fill', '#FFFFFF');

    spyOn<any>(service, 'intersectWithEraser').and.returnValue(false);

    const result = service['checkIfContained'](element);
    expect(result).toBeFalsy();
  });

  it('checkIfContained return false if point is undefined', () => {
    selectorSpy.isSelectable.and.returnValue(true);
    const element = document.createElementNS('http://www.w3.org/2000/svg', 'path') as any;
    element.setAttribute('fill', '#FFFFFF');
    const elementPath = {
      split(_: string): undefined { return undefined; }
    };

    spyOn(element, 'getAttribute').and.returnValue(elementPath);

    spyOn<any>(service, 'intersectWithEraser').and.returnValue(false);

    const result = service['checkIfContained'](element);
    expect(result).toBeFalsy();
  });

  it('eraseRadius should call intersection list of the svg and the rect created', () => {
    service = new EraserService(svgServiceSpy, commandInvokerSpy, rendererFactorySpy);
    const constructRect = spyOn<any>(service, 'constructRect');

    service['eraseRadius']();

    expect(document.querySelector).toHaveBeenCalled();
    expect(constructRect).toHaveBeenCalled();
    expect(svgSvgSpy.getIntersectionList).toHaveBeenCalled();
  });

  // changeColorOfElement
  it('changeColorOfElement should change the fill color if the element has fill in his dataset under eraserPreview', () => {
    const element = jasmine.createSpyObj('SVGElement', ['setAttribute']);
    const dataset = {
      eraserPreview: 'fill'
    };
    Object.defineProperty(element, 'dataset', {value: dataset});

    const color = 'YELLOW';

    service['changeColorOfElement'](element, color, 'none');

    expect(element.setAttribute).toHaveBeenCalledWith('fill', color);
  });

  it('changeColorOfElement should change the stroke color and width if the element has stroke in his dataset under eraserPreview', () => {
    const element = jasmine.createSpyObj('SVGElement', ['setAttribute']);
    const dataset = {
      eraserPreview: 'stroke'
    };
    Object.defineProperty(element, 'dataset', {value: dataset});

    const color = 'YELLOW';
    const width = 'width';

    service['changeColorOfElement'](element, color, width);

    expect(element.setAttribute).toHaveBeenCalledWith('stroke', color);
    expect(element.setAttribute).toHaveBeenCalledWith('stroke-width', width);
  });

  it('changeColorOfElement should change the fill and stroke color \
  if the element has fill&stroke in his dataset under eraserPreview', () => {
    const element = jasmine.createSpyObj('SVGElement', ['setAttribute']);
    const dataset = {
      eraserPreview: 'fill&stroke'
    };
    Object.defineProperty(element, 'dataset', {value: dataset});

    const color = 'YELLOW';
    const width = 'width';

    service['changeColorOfElement'](element, color, width);

    expect(element.setAttribute).toHaveBeenCalledWith('fill', color);
    expect(element.setAttribute).toHaveBeenCalledWith('stroke', color);
    expect(element.setAttribute).toHaveBeenCalledWith('stroke-width', width);
  });

  it('changeColorOfElement should not change the color of the element eraserPreview is undefined', () => {
    const element = jasmine.createSpyObj('SVGElement', ['setAttribute']);
    const dataset = {
      eraserPreview: undefined
    };
    Object.defineProperty(element, 'dataset', {value: dataset});

    const color = 'YELLOW';
    const width = 'width';

    service['changeColorOfElement'](element, color, width);

    expect(element.setAttribute).not.toHaveBeenCalledWith('fill', color);
    expect(element.setAttribute).not.toHaveBeenCalledWith('stroke', color);
    expect(element.setAttribute).not.toHaveBeenCalledWith('stroke-width', width);
  });

  // constructRect
  it('constructRect sets the rect property', () => {
    const rectangle = {
      x: 0,
      y: 0,
      width: 0,
      height: 0
    };
    const svg = {
      createSVGRect: () => {
        return rectangle;
      }
    } as unknown as SVGSVGElement;
    service['eraser'] = new Eraser(mousePosition, 5, rendererSpy);
    const rect = service['constructRect'](svg);
    expect(rect.x).toBe(service['eraser'].position.x);
    expect(rect.y).toBe(service['eraser'].position.y);
    expect(rect.width).toBe(service['strokeWidth']);
    expect(rect.height).toBe(service['strokeWidth']);
  });

  it('if eraser is undefined, the rect will nect attributs will not be updated', () => {
    const rectangle = {
      x: 0,
      y: 0,
      width: 0,
      height: 0
    };
    const svg = {
      createSVGRect: () => {
        return rectangle;
      }
    } as unknown as SVGSVGElement;
    service['eraser'] = undefined;
    const rect = service['constructRect'](svg);
    expect(rect.x).toBe(0);
    expect(rect.y).toBe(0);
    expect(rect.width).toBe(0);
    expect(rect.height).toBe(0);
  });

  // changeColor
  it('changeColor should return if colorSaved has the parametre', () => {
    spyOn<any>(service['colorSaved'], 'has').and.returnValue(true);
    spyOn<any>(service['colorSaved'], 'set');

    const dataset = {
      eraserPreview: ''
    };

    Object.defineProperty(svgGraphicElementSpy, 'dataset', {value: dataset});

    service['changeColor'](svgGraphicElementSpy);
    expect(service['colorSaved'].has).toHaveBeenCalledTimes(1);
    expect(service['colorSaved'].set).toHaveBeenCalledTimes(0);
  });

  it('changeColor should add the parametre to colorSaved and call changeColorOfElement if it is red', () => {
    svgGraphicElementSpy.getAttribute.and.returnValues('#FF0000', '2');
    spyOn<any>(service['colorSaved'], 'has').and.returnValue(false);
    spyOn<any>(service['colorSaved'], 'set');
    spyOn<any>(service, 'changeColorOfElement');

    const dataset = {
      eraserPreview: 'stroke'
    };

    Object.defineProperty(svgGraphicElementSpy, 'dataset', {value: dataset});

    service['changeColor'](svgGraphicElementSpy);
    expect(service['colorSaved'].has).toHaveBeenCalledTimes(1);
    expect(service['colorSaved'].set).toHaveBeenCalledTimes(1);
    expect(service['changeColorOfElement']).toHaveBeenCalledWith(svgGraphicElementSpy, '#800000', '7');
  });

  it('changeColor should add the parametre to colorSaved and call changeColorOfElement if it is not red', () => {
    svgGraphicElementSpy.getAttribute.and.returnValues('#000000', null);
    spyOn<any>(service['colorSaved'], 'has').and.returnValue(false);
    spyOn<any>(service, 'changeColorOfElement');

    const dataset = {
      eraserPreview: 'fill'
    };

    Object.defineProperty(svgGraphicElementSpy, 'dataset', {value: dataset});

    service['changeColor'](svgGraphicElementSpy);
    expect(service['colorSaved'].has).toHaveBeenCalledTimes(1);
    expect(service['changeColorOfElement']).toHaveBeenCalledWith(svgGraphicElementSpy, '#FF0000', '5');
  });

  // restoreColor
  it('restoreColor should call change color with the value saved', () => {
    const changeColor = spyOn<any>(service, 'changeColorOfElement');
    selectorSpy.getParentIfneeded.and.returnValue(svgGraphicElementSpy);

    const width = 'myWidth';
    const color = 'myColor';

    service['colorSaved'].set(svgGraphicElementSpy, [color, width]);
    service['restoreColor'](svgGraphicElementSpy);
    expect(changeColor).toHaveBeenCalledWith(svgGraphicElementSpy, color, width);
  });

  // restoreAllColor
  it('restoreAllColor should call restoreColor', () => {
    spyOn<any>(service, 'restoreAllColor').and.callThrough();
    spyOn<any>(service, 'restoreColor');
    service['colorSaved'].set(svgGraphicElementSpy, ['', '']);
    service['restoreAllColor']();
    expect(service['restoreColor']).toHaveBeenCalledTimes(1);
  });

  // eraseOne
  it('eraseOne should call restore and execute if element is defined', () => {
    spyOn<any>(service, 'restoreColor');
    service['eraseOne'](svgGraphicElementSpy);
    expect(service['restoreColor']).toHaveBeenCalled();
    expect(service['commandInvoker'].execute).toHaveBeenCalled();
  });

  it('eraseOne should not call restore and execute if element is null', () => {
    spyOn<any>(service, 'restoreColor');
    service['eraseOne'](null);
    expect(service['restoreColor']).not.toHaveBeenCalled();
    expect(service['commandInvoker'].execute).not.toHaveBeenCalled();
  });

  // intersect
  it('intersect should return false if det == 0', () => {
    const dot: Coordinate = {x: 0, y: 0};
    const result = service['intersect'](dot, dot, dot, dot);
    expect(result).toBeFalsy();
  });

  it('intersect should return false if (0 < lambda && lambda < 1) && (0 < gamma && gamma < 1) is false', () => {
    const firstDot: Coordinate = {x: 5, y: 8};
    const secondDot: Coordinate = {x: 10, y: 6};
    const thirdDot: Coordinate = {x: 3, y: 4};
    const fourthDot: Coordinate = {x: 2, y: 1};
    const result = service['intersect'](firstDot, secondDot, thirdDot, fourthDot);
    expect(result).toBeFalsy();
  });

  it('intersect should return true if (0 < lambda && lambda < 1) && (0 < gamma && gamma < 1) is true', () => {
    const firstDot: Coordinate = {x: 1, y: 1};
    const secondDot: Coordinate = {x: 8, y: 6};
    const thirdDot: Coordinate = {x: 3, y: 4};
    const fourthDot: Coordinate = {x: 20, y: 10};
    const result = service['intersect'](firstDot, secondDot, thirdDot, fourthDot);
    expect(result).toBeTruthy();
  });

  // intersectWithEraser
  it('intersectWithEraser should return false if eraser is undefined', () => {
    const firstDot: Coordinate = {x: 1, y: 1};
    const secondDot: Coordinate = {x: 8, y: 6};
    service['eraser'] = undefined;
    const result = service['intersectWithEraser'](firstDot, secondDot);
    expect(result).toBeFalsy();
  });

  it('intersectWithEraser should return false if intersect return false', () => {
    const firstDot: Coordinate = {x: 1, y: 1};
    const secondDot: Coordinate = {x: 8, y: 6};
    spyOn<any>(service, 'intersect').and.returnValue(false);
    const result = service['intersectWithEraser'](firstDot, secondDot);
    expect(result).toBeFalsy();
  });

  it('intersectWithEraser should return true if intersect return true', () => {
    const firstDot: Coordinate = {x: 1, y: 1};
    const secondDot: Coordinate = {x: 8, y: 6};
    spyOn<any>(service, 'intersect').and.returnValue(true);
    const result = service['intersectWithEraser'](firstDot, secondDot);
    expect(result).toBeTruthy();
  });

  it('eraseShapeDrag should call execute if multipleEraseCommand is undefined', () => {
    service['multipleEraseCommand'] = undefined;
    service['eraseShapeDrag'](null);
    expect(service['commandInvoker'].execute).toHaveBeenCalled();
  });

  it('eraseShapeDrag should call addCommandToDo if element is defined', () => {
    service['multipleEraseCommand'] = new MultiCommand([]);
    spyOn<any>(service['multipleEraseCommand'], 'addCommandToDo');
    service['eraseShapeDrag'](svgGraphicElementSpy);
    expect((service['multipleEraseCommand'] as MultiCommand).addCommandToDo).toHaveBeenCalled();
  });

  // should Create test
  it('should create', () => {
    expect(service).toBeTruthy();
  });
});
