import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { MultiCommand } from 'src/app/components/app/attributes/command/multi-command';
import { RemoveShapeCommand } from 'src/app/components/app/attributes/command/remove-shape-command';
import { Coordinate } from 'src/app/components/app/attributes/coordinate';
import { Eraser } from 'src/app/components/app/attributes/eraser/eraser';
import { SVGSelector } from 'src/app/components/app/attributes/svg-selector/svg-selector';
import { Tool } from 'src/app/components/app/attributes/tool';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { CoordinateTransformer } from '../../coordinate-transformer/coordinate-transformer';
import { SVGChildService } from '../../svg-child/svg-child.service';
import * as JSON from './eraser-service.json';

const config = JSON.default;
const BASIC_STROKE_WIDTH: number = 50;
const WIDTH_BONUS = 5;
const N_CORNER_IN_RECTANGLE: number = 4;

@Injectable({
  providedIn: 'root'
})
export class EraserService extends Tool {

  private strokeWidth: number = BASIC_STROKE_WIDTH;
  private selector: SVGSelector;
  private colorSaved: Map<SVGGraphicsElement, [string, string]>;
  private renderer: Renderer2;
  private coodinateTransformer: CoordinateTransformer;

  private eraser: Eraser | undefined;

  private multipleEraseCommand: MultiCommand | undefined;

  constructor(private svgService: SVGChildService,
              private commandInvoker: CommandInvokerService, rendererFactory: RendererFactory2) {
    super();
    this.renderer = rendererFactory.createRenderer(null, null);
    this.selector = new SVGSelector();
    this.colorSaved = new Map<SVGGraphicsElement, [string, string]>();
    this.coodinateTransformer = new CoordinateTransformer();

  }

  click(): void {
    const intersected = this.eraseRadius();
    this.multipleEraseCommand = undefined;

    if (this.eraser) {
      for (let i = intersected.length - 1; i > 0 ; --i) {
        const contained = this.checkIfContained(intersected[i] as SVGPathElement);
        if (contained) {
          this.eraseOne(this.selector.getParentIfneeded(intersected[i] as SVGGraphicsElement));
          return;
        }
      }
    }
  }

  toolSwap(): void {
    this.mouseLeave();
  }

  mouseMove(event: MouseEvent): void {
    if (this.eraser === undefined ) {
      this.createNew();
    }
    if (this.eraser) {
      this.restoreAllColor();
      const intersected = this.eraseRadius();
      for (let i = 1; i < intersected.length; i++) {
        let contained = true;
        contained = this.checkIfContained(intersected[i] as SVGPathElement);
        if (contained) {
          this.changeColor(this.selector.getParentIfneeded(intersected[i] as SVGGraphicsElement) as SVGGraphicsElement);
        }
      }
      this.eraser.updateShape({x: event.offsetX, y: event.offsetY}, this.strokeWidth);
    }
  }

  mouseDrag(event: MouseEvent): void {
    if (event.button !== this.LEFT_CLICK) { return; }
    const intersected = this.eraseRadius();
    if (this.eraser) {
      for (let i = 1; i < intersected.length; i++) {
        const contained = this.checkIfContained(intersected[i] as SVGPathElement);
        if (contained) {
          this.eraseShapeDrag(this.selector.getParentIfneeded(intersected[i] as SVGGraphicsElement));
        }
      }
      this.eraser.updateShape({x: event.offsetX, y: event.offsetY}, this.strokeWidth);
    }
  }

  mouseLeave(): void {
    if (this.eraser) {
      this.svgService.removeSVG(this.eraser.svgDrawElement);
      this.eraser = undefined;
      this.restoreAllColor();
    }
  }

  private createNew(): void {
    this.eraser = new Eraser({x: 0, y: 0}, this.strokeWidth, this.renderer);
    this.svgService.appendNewSVG(this.eraser.svgDrawElement);
  }

  private checkIfContained(path: SVGPathElement): boolean {
    if (!this.selector.isSelectable(path)) { return false; }
    const elementPath = path.getAttribute(config.attribute.points);

    if (elementPath === null
      || !path.getAttribute(config.attribute.fill)
      || path instanceof SVGPolygonElement) {
      return true;
    }

    const points = elementPath.split(' ');
    if (points && this.eraser) {
      // We create a copy of the element to avoid modifying the original's SVGTransformList
      const matrix: SVGTransform | null = (path.cloneNode(false) as SVGGraphicsElement).transform.baseVal.consolidate();
      // Matrix is null if the list was empty, or a single SVGTransform if there were transformations applied on the element
      for (let i = 0; i < points.length - 1; ++i) {
        const point1 = points[i].split(',', 2);
        const point2 = points[i + 1].split(',', 2);
        const firstDotOfSecondLine = {x: parseFloat(point1[0]), y: parseFloat(point1[1]) };
        const secondDotOfSecondLine = {x: parseFloat(point2[0]), y: parseFloat(point2[1]) };

        if (this.intersectWithEraser(
          this.coodinateTransformer.applyTransformation(firstDotOfSecondLine, matrix),
          this.coodinateTransformer.applyTransformation(secondDotOfSecondLine, matrix),
          )) {
          return true;
        }
      }
    }
    return false;
  }

  private eraseRadius(): NodeListOf<SVGElement> {
    const svg = document.querySelector(config.drawingSelector) as SVGSVGElement;
    const rect = this.constructRect(svg);
    return svg.getIntersectionList(rect, svg);
  }

  private constructRect(svg: SVGSVGElement): DOMRect {
    const rect = svg.createSVGRect();
    if (this.eraser) {
      rect.x = this.eraser.position.x;
      rect.y = this.eraser.position.y;
      rect.width = this.strokeWidth;
      rect.height = this.strokeWidth;
    }
    return rect;
  }

  private changeColor(element: SVGGraphicsElement): void {
    const preview = element.dataset.eraserPreview;
    if (this.colorSaved.has(element) || preview === undefined) { return; }

    const colorId = (preview === config.attribute.fill) ?  config.attribute.fill :  config.attribute.stroke;

    const color = element.getAttribute(colorId) as string;
    let borderWidth = element.getAttribute(config.attribute.strokeWidth);
    borderWidth = borderWidth === null ? '0' : borderWidth;

    this.colorSaved.set(element, [color, borderWidth]);
    let width = Number(borderWidth);

    if (color === config.value.red) {
      width += WIDTH_BONUS;
      this.changeColorOfElement(element, config.value.darkRed, width.toString());
    } else {
      width = Math.max(width, WIDTH_BONUS);
      this.changeColorOfElement(element, config.value.red, width.toString());
    }
  }

  private changeColorOfElement(element: SVGGraphicsElement, color: string, borderWidth: string): void {
    const preview = element.dataset.eraserPreview;
    if (preview === undefined) { return; }

    if (preview.includes( config.attribute.stroke)) {
      element.setAttribute(config.attribute.stroke,  color);
      element.setAttribute(config.attribute.strokeWidth, borderWidth);
    }
    if (preview.includes( config.attribute.fill)) {
      element.setAttribute(config.attribute.fill, color);
    }
  }

  private restoreAllColor(): void {
    for (const element of this.colorSaved.keys()) {
     this.restoreColor(element);
    }
    this.colorSaved.clear();
  }

  private restoreColor(element: SVGGraphicsElement): void {
    element = this.selector.getParentIfneeded(element) as SVGGraphicsElement;

    const [color, borderWidth] = this.colorSaved.get(element) as [string, string];

    this.changeColorOfElement(element, color, borderWidth);
  }

  private eraseOne(element: SVGGraphicsElement | null): void {
    if (element) {
      this.restoreColor(element);
      const command = new RemoveShapeCommand(this.svgService, this.renderer, element);
      this.commandInvoker.execute(command);
    }
  }

  private eraseShapeDrag(element: SVGGraphicsElement | null): void {
    if (this.multipleEraseCommand === undefined) {
      this.multipleEraseCommand = new MultiCommand([]);
      this.commandInvoker.execute(this.multipleEraseCommand);
    }
    if (element) {
      this.multipleEraseCommand.addCommandToDo(new RemoveShapeCommand(this.svgService, this.renderer, element));
    }
  }

  private intersectWithEraser(firstDotOfSecondLine: Coordinate, secondDotOfSecondLine: Coordinate): boolean {
    if (this.eraser === undefined) { return false; }

    let firstCornerOfEraser: Coordinate = {x: this.eraser.position.x, y: this.eraser.position.y};
    let secondCornerOfEraser: Coordinate = {x: this.eraser.position.x, y: this.eraser.position.y};

    for (let i = 0; i < N_CORNER_IN_RECTANGLE ; ++i) {
      // check each side
      firstCornerOfEraser = {x: this.eraser.position.x, y: this.eraser.position.y};
      secondCornerOfEraser = {x: this.eraser.position.x, y: this.eraser.position.y};
      switch (i) {
        case 2:
          secondCornerOfEraser.y += this.strokeWidth;
          break;
        case 1:
          secondCornerOfEraser.x += this.strokeWidth;
          break;
        case 0:
          firstCornerOfEraser.x += this.strokeWidth;
          firstCornerOfEraser.y += this.strokeWidth;
          secondCornerOfEraser.y += this.strokeWidth;
          break;
        // tslint:disable-next-line: no-magic-numbers
        case 3:
          firstCornerOfEraser.x += this.strokeWidth;
          firstCornerOfEraser.y += this.strokeWidth;
          secondCornerOfEraser.x += this.strokeWidth;
          break;
      }
      if (this.intersect(firstCornerOfEraser, secondCornerOfEraser, firstDotOfSecondLine, secondDotOfSecondLine)) {
        return true;
      }
    }
    return false;
  }

  // returns true if the line from (a,b)->(c,d) intersects with (p,q)->(r,s)
  // from https://stackoverflow.com/questions/9043805/test-if-two-lines-intersect-javascript-function
  private intersect(firstDot: Coordinate, secondDot: Coordinate, thirdDot: Coordinate, fourthDot: Coordinate): boolean {
    let det: number;
    let gamma: number;
    let lambda: number;
    det = (secondDot.x - firstDot.x) * (fourthDot.y - thirdDot.y) - (fourthDot.x - thirdDot.x) * (secondDot.y - firstDot.y);
    if (det === 0) {
      return false;
    } else {
      lambda = ((fourthDot.y - thirdDot.y) * (fourthDot.x - firstDot.x) + (thirdDot.x - fourthDot.x) * (fourthDot.y - firstDot.y)) / det;
      gamma = ((firstDot.y - secondDot.y) * (fourthDot.x - firstDot.x) + (secondDot.x - firstDot.x) * (fourthDot.y - firstDot.y)) / det;
      return (0 < lambda && lambda < 1) && (0 < gamma && gamma < 1);
    }
  }

}
