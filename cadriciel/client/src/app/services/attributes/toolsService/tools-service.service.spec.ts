import { PossibleTools } from '../../constants';
import { ToolsService } from './tools-service';

describe('ToolsService', () => {
    let toolsService: ToolsService;

    beforeEach(() => {
        toolsService = new ToolsService();
    });

    it('should create', () => {
        expect(toolsService).toBeTruthy();
    });

    it('Should be able to subscribe to the toggle observable and be updated when a new Tools is sent', () => {
        let currentTool: PossibleTools = PossibleTools.PEN;

        toolsService.toggleObs.subscribe((tool: PossibleTools) => {
            currentTool = tool;
        });

        toolsService.toggle(PossibleTools.POLYGONE);
        expect(currentTool.valueOf).toEqual(PossibleTools.POLYGONE.valueOf);
    });

    it('the currentTool in the service should be the last tool sent by the tooggle function', () => {
        for (let i = 0; i < PossibleTools.N_TOOLS; ++i) {
            toolsService.toggle(i as PossibleTools);
            expect(toolsService.currentTool).toEqual( i as PossibleTools);
        }
    });

    it('the currentTool by default should be the select', () => {
        expect(toolsService.currentTool).toBe(PossibleTools.SELECT);
    });

});
