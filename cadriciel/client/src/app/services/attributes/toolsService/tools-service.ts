import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { PossibleTools } from '../../constants';

@Injectable({
  providedIn: 'root'
})
export class ToolsService {
    private toggleSource: Subject<PossibleTools> = new Subject<PossibleTools>();
    private toggleObs$: Observable<PossibleTools> = this.toggleSource.asObservable();
    private currentToolValue: PossibleTools = PossibleTools.SELECT;

    get toggleObs(): Observable<PossibleTools> {
        return this.toggleObs$;
    }

    get currentTool(): PossibleTools {
        return this.currentToolValue;
    }

    toggle(tool: PossibleTools): void {
        this.toggleSource.next(tool);
        this.currentToolValue = tool;
    }
}
