/* tslint:disable: no-unused-variable
   tslint:disable: no-any
   tslint:disable: no-string-literal
   tslint:disable: no-magic-numbers */

import { BLUE } from 'src/app/color-palette/constant';
import { Pixel } from './pixel';
import { PotatoShape } from './potato-shape';
import * as JSON from './potato-shape.json';

const config = JSON.default;

describe('PotatoShape Class', () => {
    const color = BLUE;

    let potatoShape: PotatoShape;
    let rendererSpy: any;
    let idGeneratorSpy: any;
    let points: Pixel[][];
    let createSVG: any;
    const id = 'myId';
    const nMask = 2;

    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute', 'appendChild']);
        idGeneratorSpy = jasmine.createSpyObj('IdGenerator', ['generateId']);
        idGeneratorSpy.generateId.and.returnValue(id);

        points = [];

        for (let i = 0; i < nMask + 1; ++i ) {
            const tmp = [];
            for (let j = 0; j < 20; ++j ) {
                tmp.push(new Pixel(i, j));
            }
            points.push(tmp);
        }

        createSVG = spyOn<any>(PotatoShape.prototype, 'createSVG');
        potatoShape = new PotatoShape(rendererSpy, idGeneratorSpy, color, points);
        (PotatoShape.prototype['createSVG']as any).isSpy = false;

    });

    it('constructor should store the id and then create the potatoShape', () => {
        expect(potatoShape['maskId']).toEqual(id);
        expect(createSVG).toHaveBeenCalled();
    });

    it('createSVG should create the mask from the inner shape, the shape from the outer point and add both to the main svg', () => {
        jasmine.getEnv().allowRespy(true);
        createSVG = spyOn<any>(PotatoShape.prototype, 'createSVG').and.callThrough();

        const mainSVG = jasmine.createSpyObj('SVGElement', ['']);
        rendererSpy.createElement.and.returnValue(mainSVG);
        const outer: any[] = [];
        const inner: any[][] = [[], []];
        const mask = jasmine.createSpyObj('SVGMaskElement', ['']);
        const shape = jasmine.createSpyObj('SVGPathElement', ['']);

        spyOn<any>(potatoShape, 'findOuterShape').and.returnValue([outer, inner]);
        const findOuterBoxSize = spyOn<any>(potatoShape, 'findOuterBoxSize');
        const createMask = spyOn<any>(potatoShape, 'createMask').and.returnValue(mask);
        const createShape = spyOn<any>(potatoShape, 'createShape').and.returnValue(shape);

        (potatoShape['createSVG']as any).isSpy = false;
        potatoShape['createSVG'](color, points);

        expect(findOuterBoxSize).toHaveBeenCalledWith(outer);
        expect(createMask).toHaveBeenCalledWith(inner);
        expect(createShape).toHaveBeenCalledWith(outer);
        expect(rendererSpy.appendChild).toHaveBeenCalledWith(mainSVG, mask);
        expect(rendererSpy.appendChild).toHaveBeenCalledWith(mainSVG, shape);
    });

    it('createSVG should set the right attribute to the SVGGelement', () => {
        jasmine.getEnv().allowRespy(true);
        createSVG = spyOn<any>(PotatoShape.prototype, 'createSVG').and.callThrough();
        const mainSVG = jasmine.createSpyObj('SVGElement', ['']);
        rendererSpy.createElement.and.returnValue(mainSVG);

        const outer: any[] = [];
        const inner: any[][] = [[], []];

        spyOn<any>(potatoShape, 'findOuterShape').and.returnValue([outer, inner]);
        spyOn<any>(potatoShape, 'findOuterBoxSize');
        spyOn<any>(potatoShape, 'createMask');
        spyOn<any>(potatoShape, 'createShape');

        potatoShape['createSVG'](color, points);

        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(mainSVG, config.attributes.border.color, `#${color.rgb_hexa}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(mainSVG, config.attributes.color, `#${color.rgb_hexa}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(mainSVG, config.attributes.opacity, `${color.a}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(mainSVG, config.attributes.mainColor, config.attributes.fillAndStroke);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(mainSVG, config.attributes.secondaryColor, config.none);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(mainSVG, config.attributes.type, config.type);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(mainSVG, config.attributes.eraserPreview, config.attributes.border.color);
    });

    it('findOuterShape should return the array with the biggest x as outer and all other as inner', () => {
        points[1][2].x = Infinity;
        const [outer, inner] = potatoShape['findOuterShape'](points);

        expect(outer).toEqual(points[1]);
        for (let i = 0; i < points.length; ++i) {
            if ( i === 1 ) { continue; }
            expect(inner.includes(points[i])).toBeTruthy();
        }
        expect(inner.includes(points[1])).toBeFalsy();
    });

    it('findOuterBoxSized should return the size of the box containing all the point', () => {
        points[1][0].x = 0;
        points[1][2].y = 0;
        points[1][5].x = 100;
        points[1][14].y = 200;

        potatoShape['findOuterBoxSize'](points[1]);
        expect(potatoShape['height']).toEqual(200 + 1);
        expect(potatoShape['width']).toEqual(100 + 1);

    });

    it('createShape should return a new shape with the needed attributes', () => {
        const shape = jasmine.createSpyObj('SVGPathElement', ['']);
        const path = 'this is my path';

        rendererSpy.createElement.and.returnValue(shape);
        spyOn<any>(potatoShape, 'findPathForElement').and.returnValue(path);

        expect(potatoShape['createShape'](points[0])).toEqual(shape);

        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(shape, config.attributes.path, path);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(shape, config.attributes.border.width, config.default.borderWidth);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith
                        (shape, config.attributes.mask.identifier, `${config.attributes.mask.url}(#${id})`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(shape, config.attributes.isSelectable, config.true);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(shape, config.attributes.takeParent, config.true);

    });

    it('createMask should return a new shape array with all the mask and the visible rectangle', () => {
        const mask = jasmine.createSpyObj('SVGPathElement', ['setAttribute']);
        const rect = jasmine.createSpyObj('SVGElement', ['']);
        const maskEl = [
            jasmine.createSpyObj('SVGElement', ['']),
            jasmine.createSpyObj('SVGElement', ['']),
            jasmine.createSpyObj('SVGElement', ['']),
            jasmine.createSpyObj('SVGElement', [''])
        ];

        rendererSpy.createElement.and.returnValue(mask);
        spyOn<any>(potatoShape, 'createVisibleRectangle').and.returnValue(rect);
        spyOn<any>(potatoShape, 'createMaskedElement').and.returnValues(maskEl);

        expect(potatoShape['createMask'](points)).toEqual(mask);
        expect(mask.setAttribute).toHaveBeenCalledWith(config.attributes.mask.id, id);
        expect(rendererSpy.appendChild).toHaveBeenCalledWith(mask, rect);

        for (const element of maskEl) {
            expect(rendererSpy.appendChild).toHaveBeenCalledWith(mask, element);
        }

    });

    it('createVisibleRectangle should return a new white rectangle whit all needed attribute rectangle', () => {
        const rect = jasmine.createSpyObj('SVGElement', ['']);
        rendererSpy.createElement.and.returnValue(rect);
        potatoShape['start'] = new Pixel(4, 6);
        potatoShape['width'] = 20;
        potatoShape['height'] = 26;

        expect(potatoShape['createVisibleRectangle']()).toEqual(rect);

        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(rect, config.attributes.x, `${4 - 2}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(rect, config.attributes.y, `${6 - 2}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(rect, config.attributes.width, `${20 + 4}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(rect, config.attributes.height, `${26 + 4}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(rect, config.attributes.color, config.attributes.mask.type.visible);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(rect, config.attributes.isSelectable, config.false);
    });

    it('createMaskedElement should create a mask for each element given', () => { // changer pour svgPAth
        const paths = ['this is my path1', 'this is my path2', 'this is my path3'];
        const mask = [
            jasmine.createSpyObj('SVGMaskElement', ['']),
            jasmine.createSpyObj('SVGMaskElement', ['']),
            jasmine.createSpyObj('SVGMaskElement', ['']),
        ];
        spyOn<any>(potatoShape, 'findPathForElement').and.returnValues(paths[0], paths[1], paths[2]);
        rendererSpy.createElement.and.returnValues(mask[0], mask[1], mask[2]);

        const maskedEl = potatoShape['createMaskedElement'](points);

        for (let i = 0; i < points.length; ++i) {
            expect(rendererSpy.setAttribute).toHaveBeenCalledWith(mask[i], config.attributes.path, `${paths[i]}`);
            expect(rendererSpy.setAttribute).toHaveBeenCalledWith(mask[i], config.attributes.color, config.attributes.mask.type.masked);
            expect(rendererSpy.setAttribute).toHaveBeenCalledWith(mask[i], config.attributes.border.position, config.default.position);
            expect(rendererSpy.setAttribute).toHaveBeenCalledWith
                (mask[i], config.attributes.border.color, config.attributes.mask.type.visible);
            expect(rendererSpy.setAttribute).toHaveBeenCalledWith(mask[i], config.attributes.border.width, config.default.borderWidth);
            expect(rendererSpy.setAttribute).toHaveBeenCalledWith(mask[i], config.attributes.isSelectable, config.false);

            expect(maskedEl.includes(mask[i])).toBeTruthy();
        }
    });

    it('findPathForElement should return the path of all pixel given in parameter', () => {
        const pixels = [];
        for (let i = 0; i < 10; ++i) {
            pixels.push(new Pixel(i, 10 - i));
        }
        const path = 'M0 10 L1.5 9.5 2.5 8.5 3.5 7.5 4.5 6.5 5.5 5.5 6.5 4.5 7.5 3.5 8.5 2.5 9.5 1.5 Z';
        expect(potatoShape['findPathForElement'](pixels)).toEqual(path);
    });

    it('findPathForElement should return empty string if the array of pixel contain 1 pixel or less', () => {
        const pixels = [];
        pixels.push(new Pixel(0, 10));
        expect(potatoShape['findPathForElement'](pixels)).toEqual('');
    });

    it('changeMaskId should', () => {
        const newId = 'newId';
        idGeneratorSpy.generateId.and.returnValue(newId);
        const mask = document.createElementNS('http://www.w3.org/2000/svg', 'mask');
        const path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        const group = document.createElementNS('http://www.w3.org/2000/svg', 'g');
        const buffer = document.createElementNS('http://www.w3.org/2000/svg', 'rect');

        group.appendChild(mask);
        group.appendChild(path);
        group.appendChild(buffer); // just for the coverage

        PotatoShape.changeMaskId(group, idGeneratorSpy);

        expect(mask.getAttribute(config.attributes.mask.id)).toEqual(newId);
        expect(path.getAttribute(config.attributes.mask.identifier)).toEqual(`${config.attributes.mask.url}(#${newId})`);
    });
});
