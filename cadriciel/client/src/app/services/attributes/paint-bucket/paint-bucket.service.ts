import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { Color } from 'src/app/color-palette/color/color';
import { MAX_COLOR_VALUE } from 'src/app/color-palette/constant';
import { CreateShapeCommand } from 'src/app/components/app/attributes/command/create-shape-command';
import { Tool } from 'src/app/components/app/attributes/tool';
import * as JSON from 'src/app/components/app/export/export.json';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { Direction, PERCENTILE } from '../../constants';
import { DrawSheetService } from '../../draw-sheet/draw-sheet.service';
import { IdGenerator } from '../../id-generator/id-generator';
import { SVGChildService } from '../../svg-child/svg-child.service';
import { Pixel } from './pixel';
import { PotatoShape } from './potato-shape';

const config = JSON.default;
const N_COLOR_PER_PIXEL = 4;

const FOUR_DIRECTION = [
  [Direction.UP],
  [Direction.DOWN],
  [Direction.LEFT],
  [Direction.RIGHT],
];

const EIGHT_DIRECTION = FOUR_DIRECTION.concat([
  [Direction.UP, Direction.LEFT],    // top left corner
  [Direction.UP, Direction.RIGHT],   // top right corner
  [Direction.DOWN, Direction.LEFT],  // bottom left corner
  [Direction.DOWN, Direction.RIGHT], // bottom right corner
]);

@Injectable({
  providedIn: 'root'
})
export class PaintBucketService extends Tool {

  private tolerance: number = 5;

  private renderer: Renderer2;
  private canvas: HTMLCanvasElement;
  private context: CanvasRenderingContext2D;
  private image: HTMLImageElement;
  private event: MouseEvent;
  private oldColor: Color;
  private pixelColors: Uint8ClampedArray;

  // lauch after the image is loaded
  private imageOnLoad = () => {
    this.context.drawImage(this.image, 0, 0);

    this.pixelColors = this.context.getImageData(0, 0, this.canvas.width, this.canvas.height).data;
    const startingPixel = new Pixel(this.event.offsetX, this.event.offsetY);
    this.oldColor = this.getPixelColor(startingPixel);

    if (this.oldColor.isRGBequal(this.colorService.mainColor)) { return; }

    const potatoShape = this.getAllPotatoShapes(startingPixel);
    this.drawPotatoShapes(potatoShape);
  }

  constructor(private sheet: DrawSheetService,  private commandInvoker: CommandInvokerService,
              private svgService: SVGChildService, rendererFactory: RendererFactory2,
              private idGenerator: IdGenerator, private colorService: ColorService) {
    super();
    this.renderer = rendererFactory.createRenderer(null, null);
    this.canvas = this.renderer.createElement(config.canvas);

  }

  click(event: MouseEvent): void {
    this.fillShape(event);
  }

  private fillShape(event: MouseEvent): void {
    const svgString = this.sheet.svgToString();

    const tmp = this.canvas.getContext(config.twoDimensions);
    if (tmp === null) { return; }

    this.context = tmp;

    this.canvas.width = this.sheet.dataDrawSheet.width;
    this.canvas.height = this.sheet.dataDrawSheet.height;

    const blop = new Blob([svgString], {type: config.type});
    const url = window.URL.createObjectURL(blop);
    this.image = new Image();
    this.image.src = url;

    this.event = event;
    this.image.onload = this.imageOnLoad;
  }

  // given the starting pixel and all color of the pixel in the canvas, find the border to colorize
  private getAllPotatoShapes(startingPixel: Pixel): PotatoShape {
    const pixelToVisit: Pixel[] = [startingPixel];
    const vistedPixel = new Set<string>();
    const edgePixel = new Map<string, Pixel>();

    // for all pixel in pixel to visit
    for (let pixel = pixelToVisit.pop(); pixel !== undefined; pixel = pixelToVisit.pop()) {
      let border = false;

      for (const direction of FOUR_DIRECTION) {
        border = border || this.visitNeighbour(pixel, direction, vistedPixel, pixelToVisit);
      }
      if (border) {
        edgePixel.set(pixel.toString(), pixel);
      }
    }
    return this.createPotatoShape(edgePixel);
  }

  // visit the nieghbour in the given direction
  // return true if the pixel in the direction cannot be fill
  private visitNeighbour(
    pixel: Pixel,
    directions: Direction[],
    visitedPixel: Set<string>,
    pixelToVisit: Pixel[]
  ): boolean {

    let neighbour = pixel;

    for (const direction of directions) {
      neighbour = pixel.getPixelInDirection(direction);
    }

    if (!visitedPixel.has(neighbour.toString()) ) {
      if (this.pixelCanBeFill(neighbour)) {
        visitedPixel.add(neighbour.toString());
        pixelToVisit.push(neighbour);
      } else {
        return true;
      }
    }
    return false;
  }

  private pixelCanBeFill(pixel: Pixel): boolean {
    if (this.pixelOnCanva(pixel)) {
      const color = this.getPixelColor(pixel);
      return this.oldColor.isRGBequal(color, (this.tolerance / PERCENTILE) * MAX_COLOR_VALUE);
    }
    return false;
  }

  private pixelOnCanva(pixel: Pixel): boolean {
    const width = this.canvas.width;
    const height = this.canvas.height;
    return pixel.x >= 0 && pixel.y >= 0 && pixel.x < width && pixel.y < height;
  }

  private createPotatoShape(edgePixel: Map<string, Pixel>): PotatoShape {
    const potatoShape: Pixel[][] = [];
    while (edgePixel.size !== 0) {
      potatoShape.push(this.getOnePotatoShape(edgePixel));
    }
    return new PotatoShape(this.renderer, this.idGenerator, this.colorService.mainColor, potatoShape);
  }

  // find all pixel in the edgesPixel that form a potatoShape. return all the pixel of that potatoShape in order
  private getOnePotatoShape(edgePixel: Map<string, Pixel>): Pixel[] {
    const shape: Pixel[] = [];

    const first = edgePixel.values().next().value as Pixel;

    let currentPixel: Pixel = first;
    let result: boolean = false;

    do {
      for (const direction of EIGHT_DIRECTION) {
        [result, currentPixel] = this.neighbourInTheEdge(currentPixel, direction, shape, edgePixel);
        if (result) {
          edgePixel.delete(currentPixel.toString());
          break;
        }
      }

      // all of the pixel neighbour are not in the edge, lets find the closest one
      if (!result) {
        currentPixel = this.findClosestNeighbour(currentPixel, edgePixel.values());
        shape.push(currentPixel);
        edgePixel.delete(currentPixel.toString());
       }

    } while (first.toString() !== currentPixel.toString());

    edgePixel.delete(first.toString());

    return shape;
  }

  // check if the neighbour of the given pixel is on the edge
  // return true and the new pixel if the neighbour is on the edge
  // return false and the current pixel if not
  private neighbourInTheEdge(
    currentPixel: Pixel,
    directions: Direction[],
    shape: Pixel[],
    edgePixel: Map<string, Pixel>
  ): [boolean, Pixel] {

    let pixel = currentPixel;
    for (const direction of directions) {
      pixel = pixel.getPixelInDirection(direction);
    }

    if (edgePixel.has(pixel.toString())) {
        shape.push(pixel);
        return [true, pixel];
    }

    return [false, currentPixel];
  }

  private drawPotatoShapes(potatoShape: PotatoShape): void {
    this.commandInvoker.execute(new CreateShapeCommand(potatoShape, this.svgService));
  }

  // return the pixel color read on the canvas
  private getPixelColor(pixel: Pixel): Color {
    const index = (pixel.x + pixel.y * this.canvas.width) * N_COLOR_PER_PIXEL;
    return new Color(this.pixelColors[index], this.pixelColors[index + 1], this.pixelColors[index + 2]);
  }

  // return the closest neighbour of the pixel that is on the edged
  private findClosestNeighbour(pixel: Pixel, edgePixel: IterableIterator<Pixel>): Pixel {
    let distance = Infinity;
    let closestPixel = new Pixel(Infinity, Infinity);

    for (const pix of edgePixel) {
      const dist = pixel.squareDistance(pix);
      if (dist < distance) {
        distance = dist;
        closestPixel = pix;
      }
    }
    return closestPixel;
  }

}
