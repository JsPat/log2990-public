// tslint:disable: no-magic-numbers
import { Direction } from '../../constants';
import { Pixel } from './pixel';

describe('Pixel', () => {
    let pixel: Pixel;
    const x = 2;
    const y = 5;

    beforeEach(() => {
        pixel = new Pixel(x, y);
    });

    it('can create', () => {
        expect(pixel).toBeTruthy();
    });

    it('we can get the pixel in string()', () => {
        expect(pixel.toString()).toEqual('2 5');
    });

    it('should get the pixel in UP direction', () => {
        const neightbour = pixel.getPixelInDirection(Direction.UP);
        expect(neightbour.x).toEqual(x);
        expect(neightbour.y).toEqual(y - 1);
    });

    it('should get the pixel in DOWN direction', () => {
        const neightbour = pixel.getPixelInDirection(Direction.DOWN);
        expect(neightbour.x).toEqual(x);
        expect(neightbour.y).toEqual(y + 1);
    });

    it('should get the pixel in LEFT direction', () => {
        const neightbour = pixel.getPixelInDirection(Direction.LEFT);
        expect(neightbour.x).toEqual(x - 1);
        expect(neightbour.y).toEqual(y);
    });

    it('should get the pixel in RIGHT direction', () => {
        const neightbour = pixel.getPixelInDirection(Direction.RIGHT);
        expect(neightbour.x).toEqual(x + 1);
        expect(neightbour.y).toEqual(y);
    });

    it('should raise an error if the direction is none of the 4 (UP, DOWN, LEFT, RIGHT)', () => {
        expect( () => pixel.getPixelInDirection(Direction.N_DIRECTIONS)).toThrowError();
    });

    it('SquareDistance should return the square of the distance with the given pixel', () => {
        const secondPixel = new Pixel(10, 2);
        const result = (10 - x) * (10 - x ) + (2 - y) * (2 - y);
        expect(pixel.squareDistance(secondPixel)).toEqual(result);
    });

});
