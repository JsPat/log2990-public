import { Coordinate } from '../../../components/app/attributes/coordinate';
import { Direction } from '../../constants';

export class Pixel extends Coordinate {
    constructor(x: number, y: number) {
        super();
        this.x = x;
        this.y = y;
    }

    toString(): string {
        return `${this.x} ${this.y}`;
    }

    getPixelInDirection(direction: Direction): Pixel {
        switch (direction) {
            case Direction.UP : {
                // the origin is in the top left so to go up we reduce y
                return new Pixel(this.x, this.y - 1);
            }
            case Direction.DOWN : {
                // the origin is in the top left so to go down we augment y
                return new Pixel(this.x, this.y + 1);
            }
            case Direction.LEFT : {
                return new Pixel(this.x - 1, this.y);
            }
            case Direction.RIGHT : {
                return new Pixel(this.x + 1, this.y);
            }
            default : {
                throw new Error('getCordinateInDirection can only take UP, DOWN, LEFT, RIGHT');
            }
        }
    }

    squareDistance(pixel: Pixel): number {
        return Math.pow(pixel.x - this.x, 2) + Math.pow(pixel.y - this.y, 2);
    }
}
