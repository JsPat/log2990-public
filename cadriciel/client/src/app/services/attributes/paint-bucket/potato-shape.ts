import { Renderer2 } from '@angular/core';
import { Color } from '../../../color-palette/color/color';
import { SVGHolder } from '../../../components/app/attributes/svg-holder';
import { IdGenerator } from '../../id-generator/id-generator';
import { Pixel } from './pixel';
import * as JSON from './potato-shape.json';

const config = JSON.default;
const START_OFFSET = 2;
const DIMENSION_OFFSET = 4;
const POINTS_OFFSET = 0.5;

export class PotatoShape extends SVGHolder {
    private start: Pixel;
    private width: number;
    private height: number;
    private maskId: string;

    // change the unique id of the mask of the given potatoShape
    // used with copy
    static changeMaskId(svg: SVGGElement, idGenerator: IdGenerator): void {
        const id  = idGenerator.generateId();
        svg.childNodes.forEach( (el) => {
            if (el instanceof SVGMaskElement) {
                el.setAttribute(config.attributes.mask.id, id);
            } else if (el instanceof SVGPathElement) {
                el.setAttribute(config.attributes.mask.identifier, `${config.attributes.mask.url}(#${id})`);
            }
        });
    }

    constructor(private renderer: Renderer2, idGenerator: IdGenerator, color: Color, points: Pixel[][]) {
        super();
        this.maskId = idGenerator.generateId();
        this.createSVG(color, points);
    }

    private createSVG(color: Color, points: Pixel[][]): void {

        const [outer, inner] = this.findOuterShape(points);
        this.findOuterBoxSize(outer);

        this.svg = this.renderer.createElement(config.svg.group, config.svg.svg);
        const mask = this.createMask(inner);
        this.renderer.appendChild(this.svg, mask);

        const shape = this.createShape(outer);
        this.renderer.appendChild(this.svg, shape);

        this.renderer.setAttribute(this.svg, config.attributes.border.color, `#${color.rgb_hexa}`);
        this.renderer.setAttribute(this.svg, config.attributes.color, `#${color.rgb_hexa}`);
        this.renderer.setAttribute(this.svg, config.attributes.opacity, `${color.a}`);
        this.renderer.setAttribute(this.svg, config.attributes.mainColor, config.attributes.fillAndStroke);
        this.renderer.setAttribute(this.svg, config.attributes.secondaryColor, config.none);
        this.renderer.setAttribute(this.svg, config.attributes.type, config.type);
        this.renderer.setAttribute(this.svg, config.attributes.eraserPreview, config.attributes.border.color);
    }

    // given all pixel array(1 pixel array = 1 shape), find the most outer shape
    // return the most outer shape and all other shapes
    private findOuterShape(shapes: Pixel[][]): [Pixel[], Pixel[][]] {
        let max: number = 0;
        let outer: Pixel[] = [];
        const inner = new Set<Pixel[]>(); // we use a set here to simplified the delete function we need later

        for (const shape of shapes) {
            inner.add(shape);
            for (const point of shape) {
                if (point.x > max) {
                    max = point.x;
                    outer = shape;
                }
            }
        }
        inner.delete(outer);
        return [outer, Array.from(inner)];
    }

    private findOuterBoxSize(points: Pixel[]): void {
        this.start = new Pixel(Infinity, Infinity);
        const max = new Pixel(0, 0);

        for (const point of points) {
            max.x = Math.max(point.x, max.x);
            max.y = Math.max(point.y, max.y);
            this.start.x = Math.min(this.start.x, point.x);
            this.start.y = Math.min(this.start.y, point.y);
        }

        this.width = max.x - this.start.x + 1;
        this.height = max.y - this.start.y + 1;
    }

    // shape = the outer most shape that contain all other insaide
    private createShape(points: Pixel[]): SVGPathElement {

        const shape =  this.renderer.createElement(config.svg.path, config.svg.svg);

        this.renderer.setAttribute(shape, config.attributes.path, this.findPathForElement(points));
        this.renderer.setAttribute(shape, config.attributes.border.width, config.default.borderWidth);
        this.renderer.setAttribute(shape, config.attributes.mask.identifier, `${config.attributes.mask.url}(#${this.maskId})`);
        this.renderer.setAttribute(shape, config.attributes.isSelectable, config.true);
        this.renderer.setAttribute(shape, config.attributes.takeParent, config.true);

        return shape;
    }

    private createMask(elements: Pixel[][]): SVGMaskElement {
        const mask = this.renderer.createElement(config.svg.mask, config.svg.svg);
        mask.setAttribute(config.attributes.mask.id, this.maskId);

        const rect = this.createVisibleRectangle();
        const maskedElement = this.createMaskedElement(elements);

        this.renderer.appendChild(mask, rect);

        for (const element of maskedElement) {
            this.renderer.appendChild(mask, element);
        }
        return mask;
    }

    // we need a visble rectangle when using a mask. it determine the max rectangle we can see
    private createVisibleRectangle(): SVGRectElement {
        const rect = this.renderer.createElement(config.svg.rectangle, config.svg.svg);

        this.renderer.setAttribute(rect, config.attributes.x, `${this.start.x - START_OFFSET}`);
        this.renderer.setAttribute(rect, config.attributes.y, `${this.start.y - START_OFFSET}`);
        this.renderer.setAttribute(rect, config.attributes.width, `${this.width + DIMENSION_OFFSET}`);
        this.renderer.setAttribute(rect, config.attributes.height, `${this.height + DIMENSION_OFFSET}`);
        this.renderer.setAttribute(rect, config.attributes.color, config.attributes.mask.type.visible);
        this.renderer.setAttribute(rect, config.attributes.isSelectable, config.false);

        return rect;
    }

    private createMaskedElement(elements: Pixel[][]): SVGPathElement[] { // changer pour svgPAth

        const maskedElement: SVGPathElement[] = [];

        for (const element of elements) {
            const path = this.findPathForElement(element);
            const mask = this.renderer.createElement(config.svg.path, config.svg.svg);
            this.renderer.setAttribute(mask, config.attributes.path, `${path}`);
            this.renderer.setAttribute(mask, config.attributes.color, config.attributes.mask.type.masked);
            this.renderer.setAttribute(mask, config.attributes.border.position, config.default.position);
            this.renderer.setAttribute(mask, config.attributes.border.color, config.attributes.mask.type.visible);
            this.renderer.setAttribute(mask, config.attributes.border.width, config.default.borderWidth);
            this.renderer.setAttribute(mask, config.attributes.isSelectable, config.false);
            maskedElement.push(mask);
        }

        return maskedElement;
    }

    private findPathForElement(points: Pixel[]): string {
        if (points.length <= 1) { return ''; }
        let path: string = `${config.path.moveTo}${points[0].toString()} ${config.path.lineTo}`;

        for (let i = 1; i < points.length; ++i) {
            path = path + `${points[i].x + POINTS_OFFSET} ${points[i].y + POINTS_OFFSET} `;
        }

        path = path + config.path.close; // fermer le path

        return path;
    }
}
