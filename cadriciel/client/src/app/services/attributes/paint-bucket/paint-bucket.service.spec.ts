/* tslint:disable: no-unused-variable
   tslint:disable: no-any
   tslint:disable: no-string-literal
   tslint:disable: no-magic-numbers */

import { RendererFactory2 } from '@angular/core';
import { async, inject, TestBed } from '@angular/core/testing';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import * as JSON from 'src/app/components/app/export/export.json';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { Direction } from '../../constants';
import { DrawSheetService } from '../../draw-sheet/draw-sheet.service';
import { IdGenerator } from '../../id-generator/id-generator';
import { SVGChildService } from '../../svg-child/svg-child.service';
import { PaintBucketService } from './paint-bucket.service';
import { Pixel } from './pixel';
import { PotatoShape } from './potato-shape';

describe('Service: PaintBucket', () => {

    const config = JSON.default;
    const width = 12;
    const height = 23;
    const data = {
        width,
        height,
    };

    let service: PaintBucketService;
    let drawSheetServiceSpy: any;
    let commandInvokerSpy: any;
    let svgServiceSpy: any;
    let rendererFactorySpy: any;
    let idGeneratorSpy: any;
    let colorServiceSpy: any;
    let rendererSpy: any;
    let event: any;
    let canvasSpy: any;
    let pixelSpy: any;

    beforeEach(async(() => {

        drawSheetServiceSpy = jasmine.createSpyObj('DrawSheetService', ['svgToString']);
        commandInvokerSpy = jasmine.createSpyObj('CommandInvokerService', ['execute']);
        svgServiceSpy = jasmine.createSpyObj('SVGChildService', ['']);
        rendererFactorySpy = jasmine.createSpyObj('RendererFactory2', ['createRenderer']);
        idGeneratorSpy = jasmine.createSpyObj('IdGenerator', ['generateId']);
        colorServiceSpy = jasmine.createSpyObj('ColorService', ['']);
        rendererSpy = jasmine.createSpyObj('Renderer2', ['setAttribute', 'createElement', 'appendChild']);
        canvasSpy = jasmine.createSpyObj('HTMLCanvasElement', ['getContext']);

        drawSheetServiceSpy = {
            ...drawSheetServiceSpy,
            dataDrawSheet: data
        };

        pixelSpy = jasmine.createSpyObj('Pixel', ['toString', 'getPixelInDirection']);
        pixelSpy.toString.and.returnValue('1, 2');
        pixelSpy = {
            ...pixelSpy,
            x: 1,
            y: 2,
        };

        Object.defineProperty(canvasSpy, 'width', {value: 0,  writable: true});
        Object.defineProperty(canvasSpy, 'height', {value: 0,  writable: true});

        rendererFactorySpy.createRenderer.and.returnValue(rendererSpy);
        rendererSpy.createElement.and.returnValue(canvasSpy);

        TestBed.configureTestingModule({
            providers: [ PaintBucketService,
                { provide: DrawSheetService, useValue: drawSheetServiceSpy},
                { provide: CommandInvokerService, useValue: commandInvokerSpy},
                { provide: SVGChildService, useValue: svgServiceSpy},
                { provide: RendererFactory2, useValue: rendererFactorySpy},
                { provide: IdGenerator, useValue: idGeneratorSpy},
                { provide: ColorService, useValue: colorServiceSpy},
            ]
        });
    }));

    beforeEach(inject([PaintBucketService], (injected: PaintBucketService) => {
        service = injected;
        event = jasmine.createSpyObj('Event', ['']);
        Object.defineProperty(event, 'offsetX', {value: 2});
        Object.defineProperty(event, 'offsetY', {value: 8});
    }));

    it('should create', () => {
        expect(service).toBeTruthy();
    });

    it('constructor should create renderer and a canvas', () => {
        expect(rendererFactorySpy.createRenderer).toHaveBeenCalled();
        expect(rendererSpy.createElement).toHaveBeenCalledWith(config.canvas);
        expect(service['canvas']).toBe(canvasSpy);
    });

    it('click should call fillShape', () => {
        const fillShape = spyOn<any>(service, 'fillShape');
        service.click(event);
        expect(fillShape).toHaveBeenCalled();
    });

    it('fillShape should do nothing if the canvas return null for his context', () => {
        (service['canvas'].getContext as any).and.returnValue(null);
        service['fillShape'](event);
        expect(service['image']).toBeUndefined();
    });

    it('fillShape should create a new image and set all the needed value for this image', () => {
        const context = jasmine.createSpyObj('CanvasRenderingContext2D', ['']);
        (service['canvas'].getContext as any).and.returnValue(context);

        service['fillShape'](event);

        expect(service['canvas'].width).toEqual(width);
        expect(service['canvas'].height).toEqual(height);
        expect(service['image']).toBeDefined();
        expect(service['image'].onload).toEqual(service['imageOnLoad']);
    });

    it('getAllPotatoShapes should visit all 4 neighbour of each pixel to visit',  () => {
        spyOn<any>(service, 'createPotatoShape');
        const visitNeighbour = spyOn<any>(service, 'visitNeighbour');

        service['getAllPotatoShapes'](pixelSpy);

        expect(visitNeighbour).toHaveBeenCalledTimes(4);
    });

    it('getAllPotatoShapes should add the pixel to edges if it is in the bprder',  () => {
        let edges: Map<string, any> = new Map<any, any>();
        spyOn<any>(service, 'createPotatoShape').and.callFake((edgesPixel: Map<any, any>) => {
            edges = edgesPixel;
        });
        spyOn<any>(service, 'visitNeighbour').and.returnValues(true, false, false, false);

        const startingPixel = jasmine.createSpyObj('Pixel', ['']);
        service['getAllPotatoShapes'](startingPixel);
        expect(edges.size).toBe(1);
    });

    it('visitNeightbour should return false if the given pixel has already has been visited', () => {
        const visitedPixel: Set<string> = new Set<string>();
        spyOn(visitedPixel, 'has').and.returnValues(true);
        pixelSpy.getPixelInDirection.and.returnValue(pixelSpy);

        expect(service['visitNeighbour'](pixelSpy, [Direction.UP], visitedPixel, [])).toBeFalsy();
    });

    it('visitNeightbour should return false if the given pixel has not been visited but it can be Fill', () => {
        const visitedPixel: Set<string> = new Set<string>();
        spyOn(visitedPixel, 'has').and.returnValues(false);
        spyOn<any>(service, 'pixelCanBeFill').and.returnValue(true);
        pixelSpy.getPixelInDirection.and.returnValue(pixelSpy);

        expect(service['visitNeighbour'](pixelSpy, [Direction.UP], visitedPixel, [])).toBeFalsy();
    });

    it('visitNeightbour should return true if the given pixel has not been visited and it cannot be Fill', () => {
        const visitedPixel: Set<string> = new Set<string>();
        spyOn(visitedPixel, 'has').and.returnValues(false);
        spyOn<any>(service, 'pixelCanBeFill').and.returnValue(false);
        pixelSpy.getPixelInDirection.and.returnValue(pixelSpy);

        expect(service['visitNeighbour'](pixelSpy, [Direction.UP], visitedPixel, [])).toBeTruthy();
    });

    it('pixelCanBeFill should return false is the pixel is not on the canvas', () => {
        spyOn<any>(service, 'pixelOnCanva').and.returnValue(false);
        expect(service['pixelCanBeFill'](pixelSpy)).toBeFalsy();
    });

    it('pixelCanBeFill should return false is the pixel color is not close to the given one', () => {
        const colorSpy = jasmine.createSpyObj('Color', ['isRGBequal']);
        colorSpy.isRGBequal.and.returnValue(false);
        service['oldColor'] = colorSpy;
        spyOn<any>(service, 'getPixelColor').and.returnValue(jasmine.createSpyObj('Color', ['']));
        spyOn<any>(service, 'pixelOnCanva').and.returnValue(true);

        expect(service['pixelCanBeFill'](pixelSpy)).toBeFalsy();
    });

    it('pixelCanBeFill should return true is the pixel color is close to the given one', () => {
        const colorSpy = jasmine.createSpyObj('Color', ['isRGBequal']);
        colorSpy.isRGBequal.and.returnValue(true);
        service['oldColor'] = colorSpy;
        spyOn<any>(service, 'getPixelColor').and.returnValue(jasmine.createSpyObj('Color', ['']));
        spyOn<any>(service, 'pixelOnCanva').and.returnValue(true);

        expect(service['pixelCanBeFill'](pixelSpy)).toBeTruthy();
    });

    it('pixelOnCanvas should return false if the x coordinate is under 0', () => {
        pixelSpy.x = -1;
        expect(service['pixelOnCanva'](pixelSpy)).toBeFalsy();
    });

    it('pixelOnCanvas should return false if the y coordinate is under 0', () => {
        pixelSpy.y = -1;
        expect(service['pixelOnCanva'](pixelSpy)).toBeFalsy();
    });

    it('pixelOnCanvas should return false if the x coordinate is over canva width', () => {
        service['canvas'].width = 10;
        pixelSpy.x = 11;
        expect(service['pixelOnCanva'](pixelSpy)).toBeFalsy();
    });

    it('pixelOnCanvas should return false if the y coordinate is over canva height', () => {
        service['canvas'].height = 12;
        pixelSpy.y = 13;
        expect(service['pixelOnCanva'](pixelSpy)).toBeFalsy();
    });

    it('pixelOnCanvas should return true if the pixel is inside the canvas', () => {
        service['canvas'].height = 12;
        service['canvas'].width = 10;
        pixelSpy.y = 1;
        pixelSpy.x = 2;
        expect(service['pixelOnCanva'](pixelSpy)).toBeTruthy();
    });

    it('createPotatoShape should return a new patatoid', () => {
        rendererSpy.createElement.and.returnValue(jasmine.createSpyObj('SVGElement', ['setAttribute']));
        const edgesPixel = new Map<any, any>();
        edgesPixel.set(pixelSpy.toString(), pixelSpy);

        spyOn<any>(PotatoShape.prototype, 'createSVG');

        spyOn<any>(service, 'getOnePotatoShape').and.callFake(() => {
            edgesPixel.clear();
        });
        expect(service['createPotatoShape'](edgesPixel)).toBeTruthy();
    });

    it('getOnePotatoShape should return empty array of only the first if neighbourInTheEdge keep returning false', () => {
        const edgesPixel = new Map<string, any>();
        const first = jasmine.createSpyObj('Pixel', ['toString']);
        const firstKey = 'first';
        first.toString.and.returnValue(firstKey);
        edgesPixel.set(firstKey, first);

        spyOn<any>(service, 'neighbourInTheEdge').and.returnValue([false, undefined]);
        spyOn<any>(service, 'findClosestNeighbour').and.returnValue(first);
        expect(service['getOnePotatoShape'](edgesPixel).length).toBe(1);
    });

    it('getOnePotatoShape should return the array given in neighbourInTheEdge', () => {
        const edgesPixel = new Map<string, any>();
        const first = jasmine.createSpyObj('Pixel', ['toString']);
        const firstKey = 'first';
        first.toString.and.returnValue(firstKey);
        edgesPixel.set(firstKey, first);

        const second = jasmine.createSpyObj('Pixel', ['toString']);
        const secondKey = 'second';
        second.toString.and.returnValue(secondKey);
        edgesPixel.set(secondKey, second);

        spyOn<any>(service, 'neighbourInTheEdge').and.callFake((_1: any, _2: any, shapes: any, _3: any) => {
            for (let i = 0; i < 10; ++i) {
                shapes.push(jasmine.createSpyObj('Pixel', ['toString']));
            }
            return[true, first];
        });
        expect(service['getOnePotatoShape'](edgesPixel).length).toBe(10);
    });

    it('neighbourInTheEdge should return false and the current pixel if the given pixel is not in the edge', () => {
        const edgePixel = new Map<string, any>();
        spyOn(edgePixel, 'has').and.returnValue(false);

        expect(service['neighbourInTheEdge'](pixelSpy, [], [], edgePixel)).toEqual([false, pixelSpy]);
    });

    it('neighbourInTheEdge should return true and the oldPixel if the given pixel is in the edge', () => {
        const edgePixel = new Map<string, any>();
        spyOn(edgePixel, 'has').and.returnValue(true);
        const newPixel = {
            ...pixelSpy,
            x: 6,
            y: 4,
        };

        pixelSpy.getPixelInDirection.and.returnValue(newPixel);
        expect(service['neighbourInTheEdge'](pixelSpy, [Direction.UP], [], edgePixel)).toEqual([true, newPixel]);
    });

    it('drawPotatoShapes should execute a command with the new patatoid', () => {
        service['drawPotatoShapes'](jasmine.createSpyObj('Patatoide', ['']));
        expect(commandInvokerSpy.execute).toHaveBeenCalled();
    });

    it('getPixelColor should return a new color of the given pixel', () => {
        pixelSpy.x = 0;
        pixelSpy.y = 0;

        service['pixelColors'] = new Uint8ClampedArray([123, 2, 89]);

        const color = service['getPixelColor'](pixelSpy);
        expect(color.r).toBe(123);
        expect(color.g).toBe(2);
        expect(color.b).toBe(89);
    });

    it('imageOnload should get all needed data then draw the patatoides', () => {
        const colorData = jasmine.createSpyObj('Uint8ClampedArray', ['']);
        const imageData = {
            data: colorData,
        };
        const oldColor = jasmine.createSpyObj('Color', ['isRGBequal']);
        oldColor.isRGBequal.and.returnValue(false);
        const context = jasmine.createSpyObj('CanvasRenderingContext2D', ['drawImage', 'getImageData']);
        const allPatatoides = [[]];
        context.getImageData.and.returnValue(imageData);

        service['context'] = context;
        service['event'] = event;

        spyOn<any>(service, 'getPixelColor').and.returnValue(oldColor);
        spyOn<any>(service, 'getAllPotatoShapes').and.returnValue(allPatatoides);
        const drawPotatoShapes = spyOn<any>(service, 'drawPotatoShapes');

        service['imageOnLoad']();

        expect(service['pixelColors']).toEqual(colorData);
        expect(service['oldColor']).toEqual(oldColor);
        expect(drawPotatoShapes).toHaveBeenCalledWith(allPatatoides);
    });

    it('imageOnload should get return before finding all patatoide if the color is the same as the old', () => {
        const colorData = jasmine.createSpyObj('Uint8ClampedArray', ['']);
        const imageData = {
            data: colorData,
        };
        const oldColor = jasmine.createSpyObj('Color', ['isRGBequal']);
        oldColor.isRGBequal.and.returnValue(true);
        const context = jasmine.createSpyObj('CanvasRenderingContext2D', ['drawImage', 'getImageData']);
        const allPatatoides = [[]];
        context.getImageData.and.returnValue(imageData);

        service['context'] = context;
        service['event'] = event;

        spyOn<any>(service, 'getPixelColor').and.returnValue(oldColor);
        spyOn<any>(service, 'getAllPotatoShapes').and.returnValue(allPatatoides);
        const drawPotatoShapes = spyOn<any>(service, 'drawPotatoShapes');

        service['imageOnLoad']();

        expect(drawPotatoShapes).not.toHaveBeenCalled();
    });

    it('find closest neighbour should return the pixelInTheEdge closest to the given one', () => {
        const edgePixel = [];
        for (let i = 0; i < 8; i++) {
            edgePixel.push(new Pixel(100 + i, 200));
        }

        const closest = new Pixel(5, 6);
        edgePixel.push(closest);
        const pixel = new Pixel(0, 0);

        expect(service['findClosestNeighbour'](pixel, edgePixel.values())).toEqual(closest);
    });

// tslint:disable-next-line: max-file-line-count
});
