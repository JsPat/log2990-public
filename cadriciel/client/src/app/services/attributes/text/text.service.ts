import { Injectable } from '@angular/core';
import { Tool } from 'src/app/components/app/attributes/tool';

@Injectable({
  providedIn: 'root'
})
export class TextService extends Tool {

  constructor() { super(); }

}
