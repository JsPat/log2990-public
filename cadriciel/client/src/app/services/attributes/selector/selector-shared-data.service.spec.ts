/* tslint:disable:no-unused-variable */

import { inject, TestBed } from '@angular/core/testing';
import { SelectorSharedDataService } from './selector-shared-data.service';

describe('Service: SelectorSharedData', () => {
    let service: SelectorSharedDataService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                SelectorSharedDataService
            ]
        });
    });

    beforeEach(inject([SelectorSharedDataService], (injected: SelectorSharedDataService) => {
        service = injected;
    }));

    it('should ...', inject([SelectorSharedDataService], (serviceToTest: SelectorSharedDataService) => {
        expect(serviceToTest).toBeTruthy();
    }));

    it('selectionIsEmpty should return true if this.selectedShapes.length === 0', () => {
        service.selectedShapes.length = 0;
        const result = service.selectionIsEmpty();
        expect(result).toBeTruthy();
    });

    it('selectionIsEmpty should return false if this.selectedShapes.length !== 0', () => {
        service.selectedShapes.length = 1;
        const result = service.selectionIsEmpty();
        expect(result).toBeFalsy();
    });

});
