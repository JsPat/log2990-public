/* tslint:disable:no-unused-variable */
// tslint:disable: no-string-literal
// tslint:disable: no-magic-numbers
// tslint:disable: no-any

import { NO_ERRORS_SCHEMA, Renderer2 } from '@angular/core';
import { async, inject, TestBed } from '@angular/core/testing';
import { SelectorRectangle } from 'src/app/components/app/attributes/selector/selector-rectangle';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { DrawSheetService } from '../../draw-sheet/draw-sheet.service';
import { IdGenerator } from '../../id-generator/id-generator';
import { MatrixGeneratorService } from '../../matrix-generator/matrix-generator.service';
import { SVGChildService } from '../../svg-child/svg-child.service';
import { PotatoShape } from '../paint-bucket/potato-shape';
import { SelectorManipulationService } from './selector-manipulation.service';
import { SelectorRotationService } from './selector-rotation.service';
import { SelectorSharedDataService } from './selector-shared-data.service';

describe('Service: SelectorManipulation', () => {
  let service: SelectorManipulationService;
  let rendererSpy: Renderer2;
  let commandInvokerSpy: any;
  let drawSheetServiceSpy: any;
  let matrixGeneratorServiceSpy: any;
  let selectorRectangleSpy: any;
  let selectorSharedDataServiceSpy: any;
  let svgServiceSpy: any;
  let idGeneratorSpy: any;

  beforeEach(async(() => {
    rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement']);
    commandInvokerSpy = jasmine.createSpyObj('CommandInvokerService', ['execute', 'removeLastExecuted']);
    drawSheetServiceSpy = jasmine.createSpyObj('DrawSheetService', ['']);
    matrixGeneratorServiceSpy = jasmine.createSpyObj('MatrixGeneratorService', ['createTranslationMatrixFromVariation']);
    selectorRectangleSpy = jasmine.createSpyObj('SelectorRectangle', ['redraw', 'getCenterOfRectangle']);
    selectorSharedDataServiceSpy = jasmine.createSpyObj('SelectorSharedDataServiceMock', ['']);
    svgServiceSpy = jasmine.createSpyObj('SVGChildService', ['removeSVG', 'appendNewSVG']);
    idGeneratorSpy = jasmine.createSpyObj('IdGenerator', ['']);

    TestBed.configureTestingModule({
      providers: [
        SelectorManipulationService,
        { provide: Renderer2, useValue: rendererSpy },
        { provide: SelectorSharedDataService, useValue: selectorSharedDataServiceSpy },
        { provide: CommandInvokerService, useValue: commandInvokerSpy },
        { provide: SVGChildService, useValue: svgServiceSpy },
        { provide: MatrixGeneratorService, useValue: matrixGeneratorServiceSpy },
        { provide: DrawSheetService, useValue: drawSheetServiceSpy },
        { provide: SelectorRectangle, useValue: selectorRectangleSpy },
        { provide: IdGenerator, useValue: idGeneratorSpy }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  }));

  beforeEach(inject([SelectorManipulationService], (injected: SelectorManipulationService) => {
    service = injected;
  }));

  it('should ...', inject([SelectorManipulationService], (serviceToTest: SelectorRotationService) => {
    expect(serviceToTest).toBeTruthy();
  }));

  // deleteSelection
  it('deleteSelection should not call commandInvoker.execute if this.sharedData.selectedShapes.length === 0', () => {
    service['sharedData'].selectedShapes = [];
    service['drawSheet'].pageHeight = null;
    service['drawSheet'].pageWidth = null;
    service.deleteSelection();
    expect(service['commandInvoker'].execute).not.toHaveBeenCalled();
  });

  it('deleteSelection Should set the sharedData.selectedShapes to a new array and call commandInvoker.execute', () => {
    service['drawSheet'].pageHeight = null;
    service['drawSheet'].pageWidth = null;
    service['sharedData'].selectedShapes = [];
    service['sharedData'].selectedShapes[0] = document.createElementNS('http://www.w3.org/2000/svg', 'path') as SVGGraphicsElement;
    service.deleteSelection();
    expect(service['commandInvoker'].execute).toHaveBeenCalled();
  });

  // copySelectionToClipboard

  it('copySelectionToClipboard should not call numberOfPossiblePastes if this.sharedData.selectedShapes.length === 0', () => {
    service['sharedData'].selectedShapes = [];
    service['drawSheet'].pageHeight = null;
    service['drawSheet'].pageWidth = null;
    spyOn<any>(service, 'numberOfPossiblePastes');
    service.copySelectionToClipboard();
    expect(service['numberOfPossiblePastes']).not.toHaveBeenCalled();
  });

  it('copySelectionToClipboard Should set the clipboard and the numberOfPastes and call the numberOfPossiblePastes', () => {
    service['drawSheet'].pageHeight = null;
    service['drawSheet'].pageWidth = null;
    service['sharedData'].selectedShapes = [];
    service['sharedData'].selectedShapes[0] = document.createElementNS('http://www.w3.org/2000/svg', 'path') as SVGGraphicsElement;
    spyOn<any>(service, 'numberOfPossiblePastes');
    service.copySelectionToClipboard();
    expect(service['numberOfPossiblePastes']).toHaveBeenCalled();
    expect(service['clipboard']).not.toEqual([]);
    expect(service['numberOfPastes']).toBe(0);
  });

  // cutSelectionToClipboard

  it('cutSelectionToClipboard should call copySelectionToClipboard and deleteSelection', () => {
    spyOn(service, 'deleteSelection');
    spyOn(service, 'copySelectionToClipboard');
    service.cutSelectionToClipboard();
    expect(service.copySelectionToClipboard).toHaveBeenCalled();
    expect(service.deleteSelection).toHaveBeenCalled();
  });

  // duplicateSelection

  it('duplicateSelection should not call pasteClipboard if this.sharedData.selectedShapes.length === 0', () => {
    service['sharedData'].selectedShapes = [];
    service['drawSheet'].pageHeight = null;
    service['drawSheet'].pageWidth = null;
    spyOn(service, 'pasteClipboard');
    service.duplicateSelection();
    expect(service.pasteClipboard).not.toHaveBeenCalled();
  });

  it('duplicateSelection should call pasteClipboard if this.sharedData.selectedShapes.length !== 0', () => {
    service['drawSheet'].pageHeight = null;
    service['drawSheet'].pageWidth = null;
    service['sharedData'].selectedShapes = [];
    service['sharedData'].selectedShapes[0] = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    spyOn(service, 'pasteClipboard');
    service.duplicateSelection();
    expect(service.pasteClipboard).toHaveBeenCalled();
    expect(service['numberOfPastes']).toBe(0);
    expect(service['possiblePastes']).toBe(-1);
  });

  // pasteClipboard
  it('pasteClipboard should not call determineOffset if shapes is undefined and clipboard.length is 0', () => {
    spyOn<any>(service, 'determineOffset');
    service['clipboard'].length = 0;
    service['pasteClipboard'](undefined);
    expect(service['determineOffset']).not.toHaveBeenCalled();
  });

  it('pasteClipboard should call determineOffset if clipboard.length is not 0', () => {
    service['clipboard'] = [];
    service['clipboard'].length = 1;
    spyOn<any>(service, 'determineOffset').and.returnValue(5);
    service['pasteClipboard'](undefined);
    expect(service['determineOffset']).toHaveBeenCalled();
  });

  it('pasteClipboard should call determineOffset if shapes is defined', () => {
    const shapes: SVGGraphicsElement[]  = [];
    shapes.length = 1;
    spyOn<any>(service, 'determineOffset');
    service['clipboard'].length = 0;
    service['pasteClipboard'](shapes);
    expect(service['determineOffset']).toHaveBeenCalled();
  });

  // isWithinDrawing
  it('isWhitinDrawing should return false if drawSheet.pageHeight is null', () => {
    service['drawSheet'].pageWidth = 5;
    service['drawSheet'].pageHeight = null;
    const within = service['isWithinDrawing']({x: 5, y: 5});
    expect(within).toBeFalsy();
  });

  it('isWhitinDrawing should return false if drawSheet.pageWidth is null', () => {
    service['drawSheet'].pageWidth = null;
    service['drawSheet'].pageHeight = 5;
    const within = service['isWithinDrawing']({x: 5, y: 5});
    expect(within).toBeFalsy();
  });

  it('isWhitinDrawing should return false if point.x is smaller then 0', () => {
    service['drawSheet'].pageWidth = 5;
    service['drawSheet'].pageHeight = 5;
    const within = service['isWithinDrawing']({x: -5, y: 3});
    expect(within).toBeFalsy();
  });

  it('isWhitinDrawing should return false if point.x is bigger then drawSheet.pageWidth', () => {
    service['drawSheet'].pageWidth = 5;
    service['drawSheet'].pageHeight = 5;
    const within = service['isWithinDrawing']({x: 10, y: 3});
    expect(within).toBeFalsy();
  });

  it('isWhitinDrawing should return false if point.y is smaller then 0', () => {
    service['drawSheet'].pageWidth = 5;
    service['drawSheet'].pageHeight = 5;
    const within = service['isWithinDrawing']({x: 3, y: -5});
    expect(within).toBeFalsy();
  });

  it('isWhitinDrawing should return false if point.y is bigger then drawSheet.pageHeight', () => {
    service['drawSheet'].pageWidth = 5;
    service['drawSheet'].pageHeight = 5;
    const within = service['isWithinDrawing']({x: 3, y: 10});
    expect(within).toBeFalsy();
  });

  it('isWhitinDrawing should return true if point.x and y is between pageWidth and height and 0', () => {
    service['drawSheet'].pageWidth = 5;
    service['drawSheet'].pageHeight = 5;
    const within = service['isWithinDrawing']({x: 3, y: 3});
    expect(within).toBeTruthy();
  });

  // numberOfPossiblePastes
  it('numberOfPossiblePastes should return -1 if drawSheet.pageHeight is null', () => {
    service['drawSheet'].pageHeight = 5;
    service['drawSheet'].pageWidth = null;
    const possiblePastes = service['numberOfPossiblePastes']();
    expect(possiblePastes).toBe(-1);
  });

  it('numberOfPossiblePastes should return -1 if originalDot.x > drawSheet.pageWidth', () => {
    service['selectionRectangle'].position = {x: 5, y: 5};
    service['drawSheet'].pageHeight = 6;
    service['drawSheet'].pageWidth = 2;
    const possiblePastes = service['numberOfPossiblePastes']();
    expect(possiblePastes).toBe(-1);
  });

  it('numberOfPossiblePastes should return -1 if originalDot.y > drawSheet.pageHeight', () => {
    service['selectionRectangle'].position = {x: 5, y: 5};
    service['drawSheet'].pageHeight = 2;
    service['drawSheet'].pageWidth = 6;
    const possiblePastes = service['numberOfPossiblePastes']();
    expect(possiblePastes).toBe(-1);
  });

  // tslint:disable-next-line: max-line-length
  it('numberOfPossiblePastes should return the min between the floor of vertical and horizontal offset if originalDot.y < drawSheet.pageHeight and originalDot.x < drawSheet.pageWidth', () => {
    service['selectionRectangle'].position = {x: 5, y: 5};
    service['drawSheet'].pageHeight = 6;
    service['drawSheet'].pageWidth = 6;
    const possiblePastes = service['numberOfPossiblePastes']();
    // tslint:disable-next-line: max-line-length
    const numberOfPossibleVerticleOffsets = Math.floor((service['drawSheet'].pageHeight - service['selectionRectangle'].position.y) / service['OFFSET_LENGTH']);
    // tslint:disable-next-line: max-line-length
    const numberOfPossibleHorizontalOffsets = Math.floor((service['drawSheet'].pageWidth - service['selectionRectangle'].position.x) / service['OFFSET_LENGTH']);
    const tempPastes = Math.min(numberOfPossibleVerticleOffsets, numberOfPossibleHorizontalOffsets);
    expect(possiblePastes).toBe(tempPastes);
  });

  // determineOffset
  it('determineOffset should return numberOfPastes * OFFSET_LENGTH if possiblePastes equals -1', () => {
    service['possiblePastes'] = -1;
    service['numberOfPastes'] = 1;
    const determine = service['determineOffset']();
    expect(determine).toBe(5);
  });

  it('determineOffset should return 0 if possiblePastes equals 0', () => {
    service['possiblePastes'] = 0;
    const determine = service['determineOffset']();
    expect(determine).toBe(0);
  });

  it('determineOffset should return (numberOfPastes % possiblePastes) * OFFSET_LENGTH if possiblePastes equals is not -1 or 0', () => {
    service['possiblePastes'] = 5;
    service['numberOfPastes'] = 1;
    const determine = service['determineOffset']();
    expect(determine).toBe((service['numberOfPastes'] % service['possiblePastes']) * service['OFFSET_LENGTH']);
  });

  // createCopiedShapes
  it('createCopiedShapes should return an array of shapes if a shapes argument is passed', () => {
    const shapes: SVGGraphicsElement[]  = [];
    shapes[0] = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    const newShape: SVGGraphicsElement[] = [];
    newShape[0] = shapes[0].cloneNode(true) as SVGGraphicsElement;
    const copied = service['createCopiedShapes'](shapes);
    expect(copied).toEqual(newShape);
  });

  it('createCopiedShapes should call changeMaskId if newShape.dataset.type is patatoide and a shapes argument is passed', () => {
    const shapes: SVGGraphicsElement[]  = [];
    shapes[0] = document.createElementNS('http://www.w3.org/2000/svg', 'path') as SVGGraphicsElement;
    shapes[0].dataset.type = 'patatoide';
    spyOn<any>(shapes[0], 'cloneNode').and.returnValue(shapes[0]);
    spyOn(PotatoShape, 'changeMaskId');
    service['createCopiedShapes'](shapes);
    expect(PotatoShape.changeMaskId).toHaveBeenCalled();
  });

  it('createCopiedShapes should return an array of shapes if no shapes argument is passed', () => {
    service['clipboard'] = [];
    service['clipboard'][0] = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    const newShape: SVGGraphicsElement[] = [];
    newShape[0] = service['clipboard'][0].cloneNode(true) as SVGGraphicsElement;
    const copied = service['createCopiedShapes']();
    expect(copied).toEqual(newShape);
  });

  it('createCopiedShapes should call changeMaskId if newShape.dataset.type is patatoide and a shapes argument is passed', () => {
    service['clipboard'] = [];
    service['clipboard'][0] = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    service['clipboard'][0].dataset.type = 'patatoide';
    spyOn<any>(service['clipboard'][0], 'cloneNode').and.returnValue(service['clipboard'][0]);
    spyOn(PotatoShape, 'changeMaskId');
    service['createCopiedShapes']();
    expect(PotatoShape.changeMaskId).toHaveBeenCalled();
  });

  // manipulateNumberOfPastes
  it('manipulateNumberOfPastes should add one to the numberOfPastes if the argument is true', () => {
    service['numberOfPastes'] = 0;
    service.manipulateNumberOfPastes(true);
    expect(service['numberOfPastes']).toBe(1);
  });

  it('manipulateNumberOfPastes should substract one to the numberOfPastes if the argument is false', () => {
    service['numberOfPastes'] = 2;
    service.manipulateNumberOfPastes(false);
    expect(service['numberOfPastes']).toBe(1);
  });

  // tslint:disable-next-line: max-line-length
  it('manipulateNumberOfPastes should set to numberOfPastes to 0, if it becomes negative while the argument is true (should normaly never happen when true)', () => {
    service['numberOfPastes'] = -5;
    service.manipulateNumberOfPastes(true);
    expect(service['numberOfPastes']).toBe(0);
  });

  it('manipulateNumberOfPastes should set to numberOfPastes to 0, if it becomes negative while the argument is false', () => {
    service['numberOfPastes'] = 0;
    service.manipulateNumberOfPastes(false);
    expect(service['numberOfPastes']).toBe(0);
  });
});
