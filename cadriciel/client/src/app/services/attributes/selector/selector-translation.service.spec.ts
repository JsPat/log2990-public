/* tslint:disable:no-unused-variable */
// tslint:disable: no-any
// tslint:disable: no-string-literal
// tslint:disable: no-magic-numbers

import { NO_ERRORS_SCHEMA, Renderer2, RendererFactory2 } from '@angular/core';
import { async, inject, TestBed } from '@angular/core/testing';
import { TranslateShapesCommand } from 'src/app/components/app/attributes/command/translate-shapes-command';
import { SelectorRectangle } from 'src/app/components/app/attributes/selector/selector-rectangle';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { MatrixGeneratorService } from '../../matrix-generator/matrix-generator.service';
import { SelectorSharedDataService } from './selector-shared-data.service';
import { SelectorTranslationService } from './selector-translation.service';

describe('Service: SelectorTranslation', () => {
  let service: SelectorTranslationService;
  let selectorRectangleSpy: any;
  let commandInvokerSpy: any;
  let matrixGeneratorSpy: any;
  let rendererSpy: Renderer2;
  let rendererFactorySpy: RendererFactory2;

  beforeEach(async(() => {
    rendererFactorySpy = jasmine.createSpyObj('RendererFactory2', ['createRenderer']);
    rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement']);
    selectorRectangleSpy = jasmine.createSpyObj('SelectionAreaRectangle', ['redraw']);
    commandInvokerSpy = jasmine.createSpyObj('CommandInvokerService', ['execute']);
    matrixGeneratorSpy = jasmine.createSpyObj('MatrixGeneratorService', [
      'createTranslationMatrixFromPoints', 'createTranslationMatrixFromVariation']);
    TestBed.configureTestingModule({
      providers: [
        SelectorTranslationService,
        SelectorSharedDataService,
        { provide: Renderer2, useValue: rendererSpy },
        { provide: RendererFactory2, useValue: rendererFactorySpy },
        { provide: SelectorRectangle, useValue: selectorRectangleSpy },
        { provide: CommandInvokerService, useValue: commandInvokerSpy },
        { provide: MatrixGeneratorService, useValue: matrixGeneratorSpy },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  }));

  beforeEach(inject([SelectorTranslationService], (injected: SelectorTranslationService) => {
    service = injected;
    service['sharedData'] = new SelectorSharedDataService();
    clearInterval(service['intervalID']);
    clearTimeout(service['timeoutID']);
  }));

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  // createOrModifyTranslationMatrix
  it('createOrModifyTranslationMatrix should call createTranslationMatrixFromPoints if currentTranslationCommand is defined', () => {
    const paramSvgTransform = matrixGeneratorSpy.createTranslationMatrixFromVariation(1, 1);
    service['currentTranslationCommand'] = new TranslateShapesCommand(new Array<SVGGraphicsElement>(), paramSvgTransform);
    service.createOrModifyTranslationMatrix({x: 0, y: 0});
    expect(matrixGeneratorSpy.createTranslationMatrixFromPoints).toHaveBeenCalledTimes(1);
  });

  // tslint:disable-next-line: max-line-length
  it('createOrModifyTranslationMatrix should call createTranslationMatrixFromPoints and execute if currentTranslationCommand is undefined', () => {
    service['currentTranslationCommand'] = undefined;
    service.createOrModifyTranslationMatrix({x: 0, y: 0});
    expect(matrixGeneratorSpy.createTranslationMatrixFromPoints).toHaveBeenCalledTimes(1);
    expect(commandInvokerSpy.execute).toHaveBeenCalled();
  });

  // setKeyAsDown
  it('setKeyAsDown should set arrowKeyState to upArrowMask if ArrowUp', () => {
    const event = new KeyboardEvent('ArrowUp', {key: 'ArrowUp'});
    service.arrowKeyState = 0;
    service.setKeyAsDown(event);
    expect(service.arrowKeyState).toEqual(service['UP_ARROW_MASK']);
  });

  it('setKeyAsDown should set arrowKeyState to rightArrowMask if ArrowRight', () => {
    const event = new KeyboardEvent('ArrowRight', {key: 'ArrowRight'});
    service.arrowKeyState = 0;
    service.setKeyAsDown(event);
    expect(service.arrowKeyState).toEqual(service['RIGHT_ARROW_MASK']);
  });

  it('setKeyAsDown should set arrowKeyState to downArrowMask if ArrowDown', () => {
    const event = new KeyboardEvent('ArrowDown', {key: 'ArrowDown'});
    service.arrowKeyState = 0;
    service.setKeyAsDown(event);
    expect(service.arrowKeyState).toEqual(service['DOWN_ARROW_MASK']);
  });

  it('setKeyAsDown should set arrowKeyState to leftArrowMask if ArrowLeft', () => {
    const event = new KeyboardEvent('ArrowLeft', {key: 'ArrowLeft'});
    service.arrowKeyState = 0;
    service.setKeyAsDown(event);
    expect(service.arrowKeyState).toEqual(service['LEFT_ARROW_MASK']);
  });

  it('setKeyAsDown should call computeArrowKeyMovement, execute, ... if createCommand is true', () => {
    const event = new KeyboardEvent('ArrowLeft', {key: 'ArrowLeft'});
    const element = document.createElementNS('http://www.w3.org/2000/svg', 'rect') as SVGRectElement;
    service.arrowKeyState = 0;
    service['sharedData'].selectedShapes.push(element);
    spyOn(service, 'computeArrowKeyMovement').and.returnValue({x: 0, y: 0});
    service.setKeyAsDown(event);
    clearInterval(service['intervalID']);
    clearTimeout(service['timeoutID']);
    expect(service.computeArrowKeyMovement).toHaveBeenCalled();
    expect(matrixGeneratorSpy.createTranslationMatrixFromVariation).toHaveBeenCalled();
    expect(commandInvokerSpy.execute).toHaveBeenCalled();
    expect(selectorRectangleSpy.redraw).toHaveBeenCalled();
  });

  it('setKeyAsDown should not call computeArrowKeyMovement, execute, ... if createCommand is false', () => {
    const event = new KeyboardEvent('ArrowLeft', {key: 'ArrowLeft'});
    service.arrowKeyState = 1;
    service['sharedData'] = new SelectorSharedDataService();
    spyOn(service, 'setKeyAsDown').and.callThrough();
    spyOn(service, 'computeArrowKeyMovement').and.returnValue({x: 0, y: 0});
    service.setKeyAsDown(event);
    expect(service.computeArrowKeyMovement).not.toHaveBeenCalled();
    expect(matrixGeneratorSpy.createTranslationMatrixFromVariation).not.toHaveBeenCalled();
    expect(commandInvokerSpy.execute).not.toHaveBeenCalled();
    expect(selectorRectangleSpy.redraw).not.toHaveBeenCalled();
  });

  // setKeyAsDown
  it('setKeyAsUp should set arrowKeyState to upArrowMask if ArrowUp', () => {
    const event = new KeyboardEvent('ArrowUp', {key: 'ArrowUp'});
    service.arrowKeyState = 0b1111;
    service.setKeyAsUp(event);
    expect(service.arrowKeyState).toEqual(0b1110);
  });

  it('setKeyAsUp should set arrowKeyState to rightArrowMask if ArrowRight', () => {
    const event = new KeyboardEvent('ArrowRight', {key: 'ArrowRight'});
    service.arrowKeyState = 0b1111;
    service.setKeyAsUp(event);
    expect(service.arrowKeyState).toEqual(0b1101);
  });

  it('setKeyAsUp should set arrowKeyState to downArrowMask if ArrowDown', () => {
    const event = new KeyboardEvent('ArrowDown', {key: 'ArrowDown'});
    service.arrowKeyState = 0b1111;
    service.setKeyAsUp(event);
    expect(service.arrowKeyState).toEqual(0b1011);
  });

  it('setKeyAsUp should set arrowKeyState to leftArrowMask if ArrowLeft', () => {
    const event = new KeyboardEvent('ArrowLeft', {key: 'ArrowLeft'});
    // tslint:disable-next-line: no-bitwise
    service.arrowKeyState = 0b1111;
    service.setKeyAsUp(event);
    expect(service.arrowKeyState).toEqual(0b0111);
  });

  it('setKeyAsUp should call clearTimeout if arrowKeyState === 0', () => {
    const event = new KeyboardEvent('ArrowLeft', {key: 'ArrowLeft'});
    // tslint:disable-next-line: no-bitwise
    service.arrowKeyState = 0;
    spyOn(window, 'clearTimeout').and.callThrough();
    service.setKeyAsUp(event);
    expect(window.clearTimeout).toHaveBeenCalled();
  });

  it('setKeyAsUp should not call clearTimeout if arrowKeyState !== 0', () => {
    const event = new KeyboardEvent('ArrowLeft', {key: 'ArrowLeft'});
    // tslint:disable-next-line: no-bitwise
    service.arrowKeyState = 1;
    spyOn(window, 'clearTimeout').and.callThrough();
    service.setKeyAsUp(event);
    expect(window.clearTimeout).not.toHaveBeenCalled();
  });

  // resetArrowState
  it('resetArrowState should resets the arrowState variable to 0', () => {
    service.resetArrowState();
    expect(service.arrowKeyState).toEqual(0);
  });

  // startContinuousTranslation
  it('startContinuousTranslation should call setInterval', () => {
    spyOn(window, 'setInterval').and.callThrough();
    service.startContinuousTranslation();
    expect(window.setInterval).toHaveBeenCalled();
  });

  // computeArrowKeyMovement
  it('computeArrowKeyMovement should return good variation if (this.arrowKeyState & this.UP_ARROW_MASK) !== 0', () => {
    service.arrowKeyState = 1;
    const result = service.computeArrowKeyMovement();
    expect(result).toEqual({x: 0, y: -service['DISPLACEMENT_FOR_ARROWS']});
  });

  it('computeArrowKeyMovement should return good variation if (this.arrowKeyState & this.RIGHT_ARROW_MASK) !== 0', () => {
    service.arrowKeyState = 2;
    const result = service.computeArrowKeyMovement();
    expect(result).toEqual({x: service['DISPLACEMENT_FOR_ARROWS'], y: 0});
  });

  it('computeArrowKeyMovemen shouldt return good variation if (this.arrowKeyState & this.DOWN_ARROW_MASK) !== 0', () => {
    service.arrowKeyState = 4;
    const result = service.computeArrowKeyMovement();
    expect(result).toEqual({x: 0, y: service['DISPLACEMENT_FOR_ARROWS']});
  });

  it('computeArrowKeyMovement should return good variation if (this.arrowKeyState & this.LEFT_ARROW_MASK) !== 0', () => {
    service.arrowKeyState = 8;
    const result = service.computeArrowKeyMovement();
    expect(result).toEqual({x: -service['DISPLACEMENT_FOR_ARROWS'], y: 0});
  });

  // resetTranslationCommand
  it('resetTranslationCommand should set currentTranslationCommand to undefined', () => {
    service.resetTranslationCommand();
    expect(service['currentTranslationCommand']).toBeUndefined();
  });

  // modifyTranslationWithKeys
  it('modifyTranslationWithKeys should not call createTranslationMatrixFromVariation if  currentTranslationCommand is undefined', () => {
    service['currentTranslationCommand'] = undefined;
    service.modifyTranslationWithKeys();
    expect(matrixGeneratorSpy.createTranslationMatrixFromVariation).not.toHaveBeenCalled();
  });

  it('modifyTranslationWithKeys should call redraw if currentTranslationCommand is defined', () => {
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg') as SVGSVGElement;
    service['currentTranslationCommand'] = new TranslateShapesCommand(new Array<SVGGraphicsElement>(), svg.createSVGTransform());
    const spy = spyOn<any>(service['currentTranslationCommand'], 'combineTranslations');
    service.modifyTranslationWithKeys();
    expect(spy).toHaveBeenCalled();
    expect(selectorRectangleSpy.redraw).toHaveBeenCalled();
  });
});
