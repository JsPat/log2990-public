import { Injectable } from '@angular/core';
import { MultiCommand } from 'src/app/components/app/attributes/command/multi-command';
import { RotateShapesCommand } from 'src/app/components/app/attributes/command/rotate-shapes-command';
import { Coordinate } from 'src/app/components/app/attributes/coordinate';
import { SelectorRectangle } from 'src/app/components/app/attributes/selector/selector-rectangle';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { DrawViewService } from '../../draw-view/draw-view.service';
import { MatrixGeneratorService } from '../../matrix-generator/matrix-generator.service';
import { SelectorSharedDataService } from './selector-shared-data.service';

@Injectable({
  providedIn: 'root'
})
export class SelectorRotationService {
  private readonly REGULAR_ROTATION_STEP: number = 15;

  constructor(private sharedData: SelectorSharedDataService,
              private selectionRectangle: SelectorRectangle,
              private commandInvoker: CommandInvokerService,
              private matrixGenerator: MatrixGeneratorService,
              private drawView: DrawViewService) {

  }

  executeNewRotation(event: WheelEvent): void {
    // Selector already checked that there was at least 1 selected shape so we omit checking it here
    let step = (event.altKey) ? 1 : this.REGULAR_ROTATION_STEP;
    // We rotate counter-clockwise if deltaY is negative
    step *= (Math.sign(event.deltaY) >= 0) ? 1 : -1;
    if (event.shiftKey) {
      const rotationCommandArray = new Array<RotateShapesCommand>();
      this.sharedData.selectedShapes.forEach( (shape) => {
        const shapeArray = new Array<SVGGraphicsElement>();
        shapeArray.push(shape);
        const matrix = this.matrixGenerator.createRotationMatrix(step, this.getCenterOfShape(shape));
        rotationCommandArray.push(new RotateShapesCommand(shapeArray, matrix));
      });
      this.commandInvoker.execute(new MultiCommand(rotationCommandArray));
      // We need to create a multiCommand of rotationCommands as the center of rotation is different for each element
    } else {
      const matrix = this.matrixGenerator.createRotationMatrix(step, this.getCenterOfSelectionRectangle());
      const array = this.sharedData.selectedShapes.slice();
      this.commandInvoker.execute(new RotateShapesCommand(array, matrix));
    }
    this.selectionRectangle.redraw();
  }

  getCenterOfSelectionRectangle(): Coordinate {
    return this.selectionRectangle.getCenterOfRectangle();
  }

  getCenterOfShape(shape: SVGGraphicsElement): Coordinate {
    const boundingBox = shape.getBoundingClientRect() as DOMRect;
    const alignedTopLeft = this.drawView.alignCoordinateToDrawView({x : boundingBox.left, y : boundingBox.top});
    const alignedBottomRight = this.drawView.alignCoordinateToDrawView({x : boundingBox.right, y : boundingBox.bottom});
    const midX = alignedTopLeft.x + ( (alignedBottomRight.x - alignedTopLeft.x) / 2 );
    const midY = alignedTopLeft.y + ( (alignedBottomRight.y - alignedTopLeft.y) / 2 );
    return {x: midX, y: midY};
  }
}
