import { Injectable } from '@angular/core';
import { Coordinate } from 'src/app/components/app/attributes/coordinate';

@Injectable({
  providedIn: 'root'
})
export class SelectorSharedDataService {
  // Used to store and modify all attributes that is used modified by 2 or more services in selector
  // SelectedShapes, which contains all selectedShapes
  // AnchorPoint, which specifies where mouseDown was last triggered
  selectedShapes: SVGGraphicsElement[] = new Array<SVGGraphicsElement>();
  anchorPoint: Coordinate = {x: 0, y: 0};

  selectionIsEmpty(): boolean {
    return this.selectedShapes.length === 0;
  }
}
