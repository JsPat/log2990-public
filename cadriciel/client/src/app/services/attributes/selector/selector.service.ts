import { Injectable, RendererFactory2 } from '@angular/core';
import { SelectionAreaRectangle } from 'src/app/components/app/attributes/selector/selection-area-rectangle';
import { SelectorRectangle } from 'src/app/components/app/attributes/selector/selector-rectangle';
import { SVGSelector } from 'src/app/components/app/attributes/svg-selector/svg-selector';
import { Tool } from 'src/app/components/app/attributes/tool';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { DrawSheetService } from '../../draw-sheet/draw-sheet.service';
import { DrawViewService } from '../../draw-view/draw-view.service';
import { IdGenerator } from '../../id-generator/id-generator';
import { MatrixGeneratorService } from '../../matrix-generator/matrix-generator.service';
import { ShapesHolderService } from '../../shapes-holder/shapes-holder.service';
import { SVGChildService } from '../../svg-child/svg-child.service';
import { SelectorManipulationService } from './selector-manipulation.service';
import { SelectorRotationService } from './selector-rotation.service';
import { SelectorSharedDataService } from './selector-shared-data.service';
import { SelectorTranslationService } from './selector-translation.service';

export enum ActionOnDrag {
  SELECT, // Create a selection rectangle
  INVERSE_SELECT, // Create an inverted selection rectangle
  TRANSLATE, // Translate the current selection
  SCALE // Scale the current selection
}

@Injectable({
  providedIn: 'root'
})
export class SelectorService extends Tool {

  private readonly BOTH_MOUSE_BUTTONS: number = 3;
  private readonly LEFT_RIGHT_BUTTONS_BITMASK: number = 4;

  private selector: SVGSelector = new SVGSelector();
  private actionOnDrag: number = ActionOnDrag.SELECT;
  private selectionRectangle: SelectorRectangle;
  private selectionArea: SelectionAreaRectangle;
  private translationService: SelectorTranslationService;
  private rotationService: SelectorRotationService;
  private manipulationService: SelectorManipulationService;
  private sharedData: SelectorSharedDataService;

  constructor(rendererFactory: RendererFactory2,
              svgService: SVGChildService,
              drawView: DrawViewService,
              private shapesHolder: ShapesHolderService,
              matrixGenerator: MatrixGeneratorService,
              commandInvoker: CommandInvokerService,
              drawSheet: DrawSheetService,
              idGenerator: IdGenerator) {
    super();
    this.sharedData = new SelectorSharedDataService();
    const renderer = rendererFactory.createRenderer(null, null);
    this.selectionRectangle = new SelectorRectangle(this.sharedData, renderer, drawView, svgService);
    this.selectionArea = new SelectionAreaRectangle(this.sharedData, renderer, shapesHolder, drawView, svgService);
    this.translationService = new SelectorTranslationService(this.sharedData, this.selectionRectangle, commandInvoker, matrixGenerator);
    this.rotationService = new SelectorRotationService(this.sharedData, this.selectionRectangle, commandInvoker, matrixGenerator, drawView);
    this.manipulationService = new SelectorManipulationService( this.sharedData,
                                                                renderer,
                                                                commandInvoker,
                                                                svgService,
                                                                matrixGenerator,
                                                                drawSheet,
                                                                this.selectionRectangle,
                                                                idGenerator);
  }

  mouseDown(event: MouseEvent): void {
    // Edge case when we try to click left or right when the other button is already down
    if (event.buttons % this.LEFT_RIGHT_BUTTONS_BITMASK === this.BOTH_MOUSE_BUTTONS) {
      this.finalizeDragAction();
    } else if (event.button === this.LEFT_CLICK || event.button === this.RIGHT_CLICK) {
        const el = this.selector.selectAtPosition(event);
        this.sharedData.anchorPoint = {x: event.offsetX, y: event.offsetY};
        if (event.button === this.LEFT_CLICK) {
          if (this.selectionRectangle.isAControlPoint(event.target)) {
            this.actionOnDrag = ActionOnDrag.SCALE;
          } else if (el === null) {
            this.actionOnDrag = (this.selectionRectangle.isWithinArea(event)) ? ActionOnDrag.TRANSLATE : ActionOnDrag.SELECT;
          } else {
            this.actionOnDrag = (!this.sharedData.selectedShapes.includes(el)) ? ActionOnDrag.SELECT : ActionOnDrag.TRANSLATE;
          }
        } else {
          this.actionOnDrag = ActionOnDrag.INVERSE_SELECT;
        }
    }
  }

  mouseDrag(event: MouseEvent): void {
    // Only consider the drag if either right mouse button is held or the left mouse button is held
    if (event.buttons === 1 || event.buttons === 2) {
      const currentCursorPosition = {x: event.offsetX, y: event.offsetY};
      switch (this.actionOnDrag) {
        case ActionOnDrag.SELECT:
        case ActionOnDrag.INVERSE_SELECT:
          this.selectionArea.drawShape(currentCursorPosition, this.sharedData.anchorPoint);
          if (this.actionOnDrag === ActionOnDrag.SELECT) {
            this.sharedData.selectedShapes = this.selectionArea.intersectingElements;
          }
          this.selectionArea.redrawSelectionArea();
          this.prepareSelectedElementList();
          break;
        case ActionOnDrag.TRANSLATE:
          this.translationService.createOrModifyTranslationMatrix(currentCursorPosition);
          this.selectionRectangle.redraw();
          break;
        case ActionOnDrag.SCALE: break;
      }
    }
  }

  toolSwap(): void {
    this.finalizeDragAction();
    this.resetTool();
  }

  arrowKeyDown(event: KeyboardEvent): void {
    // We stop the drag action if a key press is sent
    this.finalizeDragAction();
    this.translationService.setKeyAsDown(event);
  }

  arrowKeyUp(event: KeyboardEvent): void {
    this.translationService.setKeyAsUp(event);
    if (this.translationService.arrowKeyState === 0) {
      this.translationService.resetTranslationCommand();
      this.selectionRectangle.redraw();
    }
    // Last redraw to make sure we aren't off be 3 px
  }

  mouseLeave(): void {
    // Destroy the current selection rectangle if it exists
    // Does not unselect selected shapes
    this.finalizeDragAction();
  }

  click(event: MouseEvent): void {
    if (this.selectionArea.isDrawn) {
      this.sharedData.selectedShapes = this.selectionArea.intersectingElements;
    } else {
      const selectedElement = this.selector.selectAtPosition(event);
      if (this.actionOnDrag === ActionOnDrag.TRANSLATE || this.actionOnDrag === ActionOnDrag.SCALE) {
        this.translationService.resetTranslationCommand();
      } else {
        this.sharedData.selectedShapes = new Array<SVGGraphicsElement>();
        if (selectedElement !== null) {
          this.sharedData.selectedShapes.push(selectedElement);
        }
      }
    }
    this.finalizeSelection();
  }

  rightClick(event: MouseEvent): void {
    if (this.selectionArea.isDrawn) {
      this.sharedData.selectedShapes = this.selectionArea.invertedAreaSelection();
    } else {
      const selectedElement = this.selector.selectAtPosition(event);
      if (selectedElement !== null) {
        this.invertSelectionState(selectedElement);
      }
    }
    this.finalizeSelection();
  }

  selectAll(): void {
    const shapeIterator = this.shapesHolder.iterator;
    let elem = shapeIterator.next();
    this.sharedData.selectedShapes = new Array<SVGGraphicsElement>();
    while (!elem.done) {
      this.sharedData.selectedShapes.push(elem.value);
      elem = shapeIterator.next();
    }
    this.selectionRectangle.redraw();
  }

  wheel(event: WheelEvent): void {
    this.finalizeDragAction();
    if (this.sharedData.selectedShapes.length !== 0) {
      this.rotationService.executeNewRotation(event);
    }
  }

  deleteSelection(): void {
    this.manipulationService.deleteSelection();
    this.selectionRectangle.redraw();
  }

  copySelection(): void {
    this.manipulationService.copySelectionToClipboard();
  }

  cutSelection(): void {
    this.manipulationService.cutSelectionToClipboard();
    this.selectionRectangle.redraw();
  }

  duplicateSelection(): void {
    this.manipulationService.duplicateSelection();
    this.selectionRectangle.redraw();
  }

  pasteClipboard(): void {
    this.manipulationService.pasteClipboard();
    this.selectionRectangle.redraw();
  }

  private invertSelectionState(element: SVGGraphicsElement): void {
    (this.sharedData.selectedShapes.includes(element)) ? this.sharedData.selectedShapes.splice(
                                                          this.sharedData.selectedShapes.indexOf(element), 1) :
                                                        this.sharedData.selectedShapes.push(element);
  }

  private prepareSelectedElementList(): void {
    this.selectionRectangle.redraw((this.actionOnDrag === ActionOnDrag.INVERSE_SELECT) ?
      this.selectionArea.invertedAreaSelection() : undefined);
  }

  private finalizeDragAction(): void {
    if (this.selectionArea.isDrawn) {
      if (this.actionOnDrag === ActionOnDrag.INVERSE_SELECT) {
        this.sharedData.selectedShapes = this.selectionArea.invertedAreaSelection();
      } else {
        this.sharedData.selectedShapes = this.selectionArea.intersectingElements;
      }
      this.finalizeSelection();
    } else if (this.actionOnDrag === ActionOnDrag.TRANSLATE) {
      this.translationService.resetTranslationCommand();
    }
    // We reset the action on drag to its default state
    this.actionOnDrag = ActionOnDrag.SELECT;
  }

  private removeSelectionElements(): void {
    this.selectionArea.removeArea();
    this.selectionRectangle.removeFromDrawing();
  }

  private resetTool(): void {
    this.removeSelectionElements();
    this.translationService.resetArrowState();
    this.selectionArea.intersectingElements = new Array<SVGGraphicsElement>();
    this.sharedData.selectedShapes = new Array<SVGGraphicsElement>();
    this.selectionRectangle.removeFromDrawing();
  }

  private finalizeSelection(): void {
    this.selectionArea.removeArea();
    this.selectionRectangle.redraw();
  }

}
