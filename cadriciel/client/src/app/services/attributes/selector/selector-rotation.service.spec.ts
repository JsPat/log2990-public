/* tslint:disable:no-unused-variable */
// tslint:disable: no-any
// tslint:disable: no-string-literal

import { async, inject, TestBed } from '@angular/core/testing';
import { Coordinate } from 'src/app/components/app/attributes/coordinate';
import { SelectorRectangle } from 'src/app/components/app/attributes/selector/selector-rectangle';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { DrawViewService } from '../../draw-view/draw-view.service';
import { MatrixGeneratorService } from '../../matrix-generator/matrix-generator.service';
import { SelectorRotationService } from './selector-rotation.service';
import { SelectorSharedDataService } from './selector-shared-data.service';

class SelectorSharedDataServiceMock extends SelectorSharedDataService {
    constructor(shape: SVGGraphicsElement) {
        super();
        this.selectedShapes = [];
        this.selectedShapes.push(shape);
    }
}

describe('Service: SelectorRotation', () => {
    let service: SelectorRotationService;
    let commandInvokerSpy: any;
    let drawViewServiceSpy: any;
    let matrixGeneratorServiceSpy: any;
    let selectorRectangleSpy: any;
    let selectorSharedDataServiceSpy: any;
    let shape: any;

    beforeEach(async(() => {
        commandInvokerSpy = jasmine.createSpyObj('CommandInvokerService', ['execute', 'removeLastExecuted']);
        drawViewServiceSpy = jasmine.createSpyObj('DrawViewService', ['alignCoordinateToDrawView']);
        matrixGeneratorServiceSpy = jasmine.createSpyObj('MatrixGeneratorService', ['createRotationMatrix']);
        selectorRectangleSpy = jasmine.createSpyObj('SelectorRectangle', ['redraw', 'getCenterOfRectangle']);
        selectorSharedDataServiceSpy = jasmine.createSpyObj('SelectorSharedDataServiceMock', ['']);

        shape = jasmine.createSpyObj('SVGGraphicsElementMock', ['getBoundingClientRect']);

        TestBed.configureTestingModule({
            providers: [
                SelectorRotationService,
                { provide: CommandInvokerService, useValue: commandInvokerSpy},
                { provide: DrawViewService, useValue: drawViewServiceSpy},
                { provide: MatrixGeneratorService, useValue: matrixGeneratorServiceSpy},
                { provide: SelectorRectangle, useValue: selectorRectangleSpy},
                { provide: SelectorSharedDataService, useValue: selectorSharedDataServiceSpy},
                { provide: SVGGraphicsElement, useValue: shape},
            ]
        });
    }));

    beforeEach(inject([SelectorRotationService], (injected: SelectorRotationService) => {
        service = injected;
        service['sharedData'].selectedShapes = [];
    }));

    it('should ...', inject([SelectorRotationService], (serviceToTest: SelectorRotationService) => {
        expect(serviceToTest).toBeTruthy();
    }));

    it('executeNewRotation should call execute if event.shiftKey is true', () => {
        spyOn(service, 'executeNewRotation').and.callThrough();
        const event =  new WheelEvent('', {shiftKey: true});
        service.executeNewRotation(event);
        expect(commandInvokerSpy.execute).toHaveBeenCalled();
    });

    it('executeNewRotation should call createRotationMatrix if event.shiftKey is true and selectedShapes is not empty', () => {
        spyOn(service, 'executeNewRotation').and.callThrough();
        const event =  new WheelEvent('', {shiftKey: true});
        shape.getBoundingClientRect.and.returnValue({
            x: 5,
            y: 5,
            height: 5,
            width: 5
        } as DOMRect);
        drawViewServiceSpy.alignCoordinateToDrawView.and.returnValue({
            x: 10,
            y: 10
        } as Coordinate);

        service['sharedData'] = new SelectorSharedDataServiceMock(shape);

        service.executeNewRotation(event);
        expect(matrixGeneratorServiceSpy.createRotationMatrix).toHaveBeenCalled();
    });

    it('executeNewRotation should assign the right value to step if event.altkey is true', () => {
        spyOn(service, 'executeNewRotation').and.callThrough();
        spyOn<any>(Math, 'sign');
        const event =  new WheelEvent('', {altKey: true});
        service.executeNewRotation(event);
        expect(Math.sign).toHaveBeenCalled();
    });

    it('executeNewRotation should assign the right value to step if event.altkey is false', () => {
        spyOn(service, 'executeNewRotation').and.callThrough();
        spyOn<any>(Math, 'sign');
        const event =  new WheelEvent('', {altKey: true});
        service.executeNewRotation(event);
        expect(Math.sign).toHaveBeenCalled();
    });

    it('executeNewRotation should assign the right value to step if event.deltaY is positive', () => {
        spyOn(service, 'executeNewRotation').and.callThrough();
        spyOn<any>(Math, 'sign');
        const event =  new WheelEvent('', {deltaY: 1});
        service.executeNewRotation(event);
        expect(Math.sign).toHaveBeenCalled();
    });

    it('executeNewRotation should assign the right value to step if event.deltaY is negative', () => {
        spyOn(service, 'executeNewRotation').and.callThrough();
        spyOn<any>(Math, 'sign');
        const event =  new WheelEvent('', {deltaY: -1});
        service.executeNewRotation(event);
        expect(Math.sign).toHaveBeenCalled();
    });

    it('executeNewRotation should call getCenterOfSelectionRectangle if event.shiftKey is false', () => {
        spyOn(service, 'executeNewRotation').and.callThrough();
        spyOn(service, 'getCenterOfSelectionRectangle');
        const event =  new WheelEvent('', {shiftKey: false});
        service.executeNewRotation(event);
        expect(service.getCenterOfSelectionRectangle).toHaveBeenCalled();
    });

    it('getCenterOfSelectionRectangle should call getCenterOfRectangle', () => {
        spyOn(service, 'getCenterOfSelectionRectangle').and.callThrough();
        service.getCenterOfSelectionRectangle();
        expect(selectorRectangleSpy.getCenterOfRectangle).toHaveBeenCalled();
    });

    it('getCenterOfShape should call alignCoordinateToDrawView', () => {
        spyOn(service, 'getCenterOfShape').and.callThrough();
        shape.getBoundingClientRect.and.returnValue(new DOMRect());
        drawViewServiceSpy.alignCoordinateToDrawView.and.returnValue({x: 0, y: 0});
        service.getCenterOfShape(shape);
        expect(drawViewServiceSpy.alignCoordinateToDrawView).toHaveBeenCalled();
    });

});
