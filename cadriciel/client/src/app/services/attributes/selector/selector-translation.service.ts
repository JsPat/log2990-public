/* tslint:disable:no-bitwise */
import { Injectable } from '@angular/core';
import { TranslateShapesCommand } from 'src/app/components/app/attributes/command/translate-shapes-command';
import { Coordinate } from 'src/app/components/app/attributes/coordinate';
import { SelectorRectangle } from 'src/app/components/app/attributes/selector/selector-rectangle';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { MatrixGeneratorService } from '../../matrix-generator/matrix-generator.service';
import { SelectorSharedDataService } from './selector-shared-data.service';

@Injectable({
  providedIn: 'root'
})
export class SelectorTranslationService {
  private readonly UP_ARROW_MASK: number = 1;
  private readonly RIGHT_ARROW_MASK: number = this.UP_ARROW_MASK << 1;
  private readonly DOWN_ARROW_MASK: number = this.RIGHT_ARROW_MASK << 1;
  private readonly LEFT_ARROW_MASK: number = this.DOWN_ARROW_MASK << 1;
  private readonly TIMEOUT_DURATION_IN_MS: number = 500;
  private readonly INTERVAL_DURATION_IN_MS: number = 100;
  private readonly DISPLACEMENT_FOR_ARROWS: number = 3;
  private readonly EVENT_ARROW_UP: string = 'ArrowUp';
  private readonly EVENT_ARROW_RIGHT: string = 'ArrowRight';
  private readonly EVENT_ARROW_DOWN: string = 'ArrowDown';
  private readonly EVENT_ARROW_LEFT: string = 'ArrowLeft';

  arrowKeyState: number = 0;

  private currentTranslationCommand: TranslateShapesCommand | undefined;
  private timeoutID: number;
  private intervalID: number;

  // Using bits to store the state of all arrows, we can effectively have this variable be 4 booleans at once
  constructor(private sharedData: SelectorSharedDataService,
              private selectionRectangle: SelectorRectangle,
              private commandInvoker: CommandInvokerService,
              private matrixGenerator: MatrixGeneratorService) {

  }

  createOrModifyTranslationMatrix(currentCursorPosition: Coordinate): void {
    if (this.currentTranslationCommand) {
      // If the translation command already exists, we just replace the matrix with a new matrix
      this.currentTranslationCommand.replaceCurrentTranslationMatrix(
        this.matrixGenerator.createTranslationMatrixFromPoints(this.sharedData.anchorPoint, currentCursorPosition));
    } else {
      // Otherwise we create the translation matrix, its command and pass the matrix to the command
      this.currentTranslationCommand = new TranslateShapesCommand(this.sharedData.selectedShapes.slice(),
        this.matrixGenerator.createTranslationMatrixFromPoints(this.sharedData.anchorPoint, currentCursorPosition));
      this.commandInvoker.execute(this.currentTranslationCommand);
    }
  }

  setKeyAsDown(event: KeyboardEvent): void {
    // We only make a new translation command if we were originally at rest and we have a selection to move
    const createCommand = (this.arrowKeyState === 0 && this.sharedData.selectedShapes.length !== 0);
    // Sets the arrowKeyState to show which arrowKeyIsPressed
    switch (event.key) {
      case this.EVENT_ARROW_UP:     this.arrowKeyState |= this.UP_ARROW_MASK;
                                    break;
      case this.EVENT_ARROW_RIGHT:  this.arrowKeyState |= this.RIGHT_ARROW_MASK;
                                    break;
      case this.EVENT_ARROW_DOWN:   this.arrowKeyState |= this.DOWN_ARROW_MASK;
                                    break;
      case this.EVENT_ARROW_LEFT:   this.arrowKeyState |= this.LEFT_ARROW_MASK;
                                    break;
    }
    if (createCommand) {
      const variation = this.computeArrowKeyMovement();
      this.currentTranslationCommand = new TranslateShapesCommand(this.sharedData.selectedShapes.slice(),
                                        this.matrixGenerator.createTranslationMatrixFromVariation(variation.x, variation.y));
      this.commandInvoker.execute(this.currentTranslationCommand);
      this.selectionRectangle.redraw();
      // Set a timeout of 500ms before checking if the arrowKeyState is back to 0
      this.timeoutID = setTimeout(this.startContinuousTranslation.bind(this) as TimerHandler, this.TIMEOUT_DURATION_IN_MS);
    }
  }

  setKeyAsUp(event: KeyboardEvent): void {
    switch (event.key) {
      case this.EVENT_ARROW_UP:     this.arrowKeyState &= ~this.UP_ARROW_MASK;
                                    break;
      case this.EVENT_ARROW_RIGHT:  this.arrowKeyState &= ~this.RIGHT_ARROW_MASK;
                                    break;
      case this.EVENT_ARROW_DOWN:   this.arrowKeyState &= ~this.DOWN_ARROW_MASK;
                                    break;
      case this.EVENT_ARROW_LEFT:   this.arrowKeyState &= ~this.LEFT_ARROW_MASK;
                                    break;
    }
    if (this.arrowKeyState === 0) {
      clearTimeout(this.timeoutID);
      clearInterval(this.intervalID);
    }
    // If the arrowKeyState is set back to 0, we stop the interval function
    // We also stop the timeout function from executing, which prevent someone from double tapping within 500ms
    // And triggering the first timeout
  }

  resetArrowState(): void {
    this.arrowKeyState = 0;
  }

  // Function called every 100ms while arrowKeyState !== 0
  startContinuousTranslation(): void {
    this.intervalID = setInterval(this.modifyTranslationWithKeys.bind(this) as TimerHandler, this.INTERVAL_DURATION_IN_MS);
  }

  computeArrowKeyMovement(): Coordinate {
    const variation = {x: 0, y: 0};
    if ( (this.arrowKeyState & this.UP_ARROW_MASK) !== 0) {
      variation.y -= this.DISPLACEMENT_FOR_ARROWS;
    }
    if ( (this.arrowKeyState & this.RIGHT_ARROW_MASK) !== 0) {
      variation.x += this.DISPLACEMENT_FOR_ARROWS;
    }
    if ( (this.arrowKeyState & this.DOWN_ARROW_MASK) !== 0) {
      variation.y += this.DISPLACEMENT_FOR_ARROWS;
    }
    if ( (this.arrowKeyState & this.LEFT_ARROW_MASK) !== 0) {
      variation.x -= this.DISPLACEMENT_FOR_ARROWS;
    }
    return variation;
  }

  resetTranslationCommand(): void {
    this.currentTranslationCommand = undefined;
  }

  modifyTranslationWithKeys(): void {
    if (this.currentTranslationCommand) {
      const variation = this.computeArrowKeyMovement();
      this.currentTranslationCommand.combineTranslations(
        this.matrixGenerator.createTranslationMatrixFromVariation(variation.x, variation.y));
      this.selectionRectangle.redraw();
    }
  }
}
