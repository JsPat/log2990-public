/* tslint:disable:no-unused-variable */
/* tslint:disable:no-any */
/* tslint:disable:no-string-literal */

import { async, inject, TestBed} from '@angular/core/testing';
import { ActionOnDrag, SelectorService } from './selector.service';

describe('Service: Selector', () => {
  let service: SelectorService;
  let mouseEvent: any;
  let keyBoardEvent: any;
  let element: SVGRectElement;
  beforeEach(async(() => {

    TestBed.configureTestingModule({
        providers: [
            SelectorService,
        ]
    });
  }));
  beforeEach(inject([SelectorService], (injected: SelectorService) => {
    service = injected;
    mouseEvent = jasmine.createSpyObj('MouseEvent', ['preventDefault']);
    keyBoardEvent = jasmine.createSpyObj('KeyBoardEvent', ['']);
    element = document.createElementNS('http://www.w3.org/2000/svg', 'rect') as SVGRectElement;
    element.setAttributeNS('http://www.w3.org/2000/svg', 'data-is-selectable', 'true');
    element.setAttributeNS('http://www.w3.org/2000/svg', 'data-take-parent', 'false');
  }));
  it('should create', () => {
    expect(service).toBeTruthy();
  });
  it('swapping tools should finalize any drag action & reset the tool to its default state', () => {
    const resetTool = spyOn<any>(service, 'resetTool').and.callThrough();
    const finalizeDragAction = spyOn<any>(service, 'finalizeDragAction');
    service.toolSwap();
    expect(resetTool).toHaveBeenCalled();
    expect(finalizeDragAction).toHaveBeenCalled();
  });
  it('leaving the drawsheet should finalize any drag action', () => {
    const finalizeDragAction = spyOn<any>(service, 'finalizeDragAction');
    service.mouseLeave();
    expect(finalizeDragAction).toHaveBeenCalled();
  });
  it('selectAll should get all shapes from the shapesHolder and use them as the selection', () => {
    spyOn<any>(service['shapesHolder'], 'iterator').and.callThrough();
    service['shapesHolder'].addShape(element);
    service.selectAll();
    const array = new Array<SVGGraphicsElement>();
    array.push(element);
    expect(service['sharedData'].selectedShapes).toEqual(array);
  });
  it('mouseDown should finalize the dragAction if both left/right buttons are triggered on mousedown', () => {
    const spy = spyOn<any>(service, 'finalizeDragAction').and.callThrough();
    Object.defineProperty(mouseEvent, 'buttons', {value : 3});
    service.mouseDown(mouseEvent);
    expect(spy).toHaveBeenCalled();
  });
  it('mouseDown should do nothing if the button pressed was not left/right', () => {
    const spy = spyOn<any>(service, 'finalizeDragAction');
    Object.defineProperty(mouseEvent, 'button', {value : 3});
    service.mouseDown(mouseEvent);
    expect(spy).not.toHaveBeenCalled();
  });
  it('mouseDown should set the dragAction to inverse select if the right button was used', () => {
    Object.defineProperty(mouseEvent, 'button', {value : 2});
    Object.defineProperty(mouseEvent, 'target', {value : element});
    service.mouseDown(mouseEvent);
    expect(service['actionOnDrag']).toEqual(ActionOnDrag.INVERSE_SELECT);
  });
  it('mouseDown should set the dragAction to select if a clicked element is not part of the selection', () => {
    Object.defineProperty(mouseEvent, 'button', {value : 0});
    Object.defineProperty(mouseEvent, 'target', {value : element});
    service.mouseDown(mouseEvent);
    expect(service['actionOnDrag']).toEqual(ActionOnDrag.SELECT);
  });
  it('mouseDown should set the dragAction to translate if a clicked element is part of the selection', () => {
    Object.defineProperty(mouseEvent, 'button', {value : 0});
    Object.defineProperty(mouseEvent, 'target', {value : element});
    service['sharedData'].selectedShapes.push(element);
    service.mouseDown(mouseEvent);
    expect(service['actionOnDrag']).toEqual(ActionOnDrag.TRANSLATE);
  });
  it('mouseDown should set the dragAction to scale if a clicked element is a control point and we pressed the left mouse button', () => {
    const spy = spyOn<any>(service['selectionRectangle'], 'isAControlPoint').and.returnValue(true);
    Object.defineProperty(mouseEvent, 'button', {value : 0});
    Object.defineProperty(mouseEvent, 'target', {value : element});
    service.mouseDown(mouseEvent);
    expect(spy).toHaveBeenCalled();
    expect(service['actionOnDrag']).toEqual(ActionOnDrag.SCALE);
  });
  it('mouseDown should set the dragAction to select we clicked an unselected element outside the selection rectangle', () => {
    const spy = spyOn<any>(service['selectionRectangle'], 'isAControlPoint').and.returnValue(false);
    const spy2 = spyOn<any>(service['selectionRectangle'], 'isWithinArea').and.returnValue(false);
    Object.defineProperty(mouseEvent, 'button', {value : 0});
    Object.defineProperty(mouseEvent, 'target', {value : null});
    service.mouseDown(mouseEvent);
    expect(spy).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
    expect(service['actionOnDrag']).toEqual(ActionOnDrag.SELECT);
  });
  it('mouseDrag only executes if either the left or right button is down', () => {
    const spy = spyOn<any>(service['selectionArea'], 'drawShape');
    Object.defineProperty(mouseEvent, 'buttons', {value : 1});
    service['actionOnDrag'] = ActionOnDrag.SELECT;
    service.mouseDrag(mouseEvent);
    expect(spy).toHaveBeenCalled();
  });
  it('mouseDrag does nothing if we are on a SCALE actionOnDrag', () => {
    Object.defineProperty(mouseEvent, 'buttons', {value : 1});
    service['actionOnDrag'] = ActionOnDrag.SCALE;
    service.mouseDrag(mouseEvent);
    expect().nothing();
  });
  it('mouseDrag creates or modifies the translation if we are on a TRANSLATE actionOnDrag', () => {
    const spy = spyOn<any>(service['translationService'], 'createOrModifyTranslationMatrix');
    Object.defineProperty(mouseEvent, 'buttons', {value : 1});
    service['actionOnDrag'] = ActionOnDrag.TRANSLATE;
    service.mouseDrag(mouseEvent);
    expect(spy).toHaveBeenCalled();
  });
  it('arrowKeyDown should finalize any currently ongoing action', () => {
    const spy = spyOn<any>(service, 'finalizeDragAction').and.callThrough();
    service.arrowKeyDown(keyBoardEvent);
    expect(spy).toHaveBeenCalled();
  });
  it('arrowKeyUp should call on the translation service to modify the current arrowKeyState', () => {
    const spy = spyOn<any>(service['translationService'], 'setKeyAsUp');
    service.arrowKeyUp(keyBoardEvent);
    expect(spy).toHaveBeenCalled();
  });
  it('click should finalize the selection', () => {
    const spy = spyOn<any>(service, 'finalizeSelection').and.callThrough();
    Object.defineProperty(mouseEvent, 'button', {value : 0});
    Object.defineProperty(mouseEvent, 'target', {value : element});
    service['sharedData'].selectedShapes.push(element);
    service.click(mouseEvent);
    expect(spy).toHaveBeenCalled();
  });
  it('click resets the translation command when actionOnDrag is TRANSLATE', () => {
    const spy = spyOn<any>(service['translationService'], 'resetTranslationCommand');
    service['selectionArea'].isDrawn = false;
    service['actionOnDrag'] = ActionOnDrag.TRANSLATE;
    Object.defineProperty(mouseEvent, 'button', {value : 0});
    Object.defineProperty(mouseEvent, 'target', {value : element});
    service.click(mouseEvent);
    expect(spy).toHaveBeenCalled();
  });
  it('click sets the selected elements to the elements contained within the area of selection if it is drawn', () => {
    service['selectionArea'].isDrawn = true;
    service['selectionArea'].intersectingElements = new Array<SVGGraphicsElement>();
    service['sharedData'].selectedShapes.push(element);
    Object.defineProperty(mouseEvent, 'button', {value : 0});
    Object.defineProperty(mouseEvent, 'target', {value : element});
    service.click(mouseEvent);
    expect(service['sharedData'].selectedShapes).toEqual(service['selectionArea'].intersectingElements);
  });
  it('rightClick should finalize the selection', () => {
    const spy = spyOn<any>(service, 'finalizeSelection').and.callThrough();
    Object.defineProperty(mouseEvent, 'button', {value : 0});
    Object.defineProperty(mouseEvent, 'target', {value : element});
    service['sharedData'].selectedShapes.push(element);
    service.rightClick(mouseEvent);
    expect(spy).toHaveBeenCalled();
  });
  it('rightClick changes the selection if an area of selection is drawn', () => {
    service['selectionArea'].isDrawn = true;
    const spy = spyOn<any>(service['selectionArea'], 'invertedAreaSelection').and.callThrough();
    Object.defineProperty(mouseEvent, 'button', {value : 0});
    Object.defineProperty(mouseEvent, 'target', {value : element});
    service['sharedData'].selectedShapes.push(element);
    const mockArray = new Array<SVGGraphicsElement>();
    mockArray.push(element);
    service.rightClick(mouseEvent);
    expect(spy).toHaveBeenCalled();
  });
  it('arrowKeyDown should remove the drawn area selection if it is drawn to the sheet', () => {
    const spy = spyOn<any>(service, 'finalizeSelection');
    service['selectionArea'].isDrawn = true;
    service.arrowKeyDown(keyBoardEvent);
    expect(spy).toHaveBeenCalled();
  });
  it('finalizeDragAction ends the translation if actionOnDrag is set to TRANSLATE', () => {
    const spy = spyOn<any>(service['translationService'], 'resetTranslationCommand');
    service['selectionArea'].isDrawn = false;
    service['actionOnDrag'] = ActionOnDrag.TRANSLATE;
    service['finalizeDragAction']();
    expect(spy).toHaveBeenCalled();
  });
  it('finalizeDragAction inverts the selection state of elements when on INVERSE_SELECT', () => {
    const spy = spyOn<any>(service['selectionArea'], 'invertedAreaSelection').and.returnValue(new Array<SVGGraphicsElement>());
    service['selectionArea'].isDrawn = true;
    service['actionOnDrag'] = ActionOnDrag.INVERSE_SELECT;
    service['finalizeDragAction']();
    expect(spy).toHaveBeenCalled();
  });
  it('prepareSelectedElementList should only call on invertedAreaSelection if the ActionOnDrag is INVERSE_SELECT', () => {
    const spy = spyOn<any>(service['selectionArea'], 'invertedAreaSelection');
    service['actionOnDrag'] = ActionOnDrag.INVERSE_SELECT;
    service['prepareSelectedElementList']();
    service['actionOnDrag'] = ActionOnDrag.SELECT;
    service['prepareSelectedElementList']();
    expect(spy).toHaveBeenCalledTimes(1);
  });
  it('invertSelectionState should add or remove an element from the selectedShapes array ', () => {
    service['sharedData'].selectedShapes.push(element);
    service['invertSelectionState'](element);
    expect(service['sharedData'].selectedShapes.length).toEqual(0);
    service['invertSelectionState'](element);
    expect(service['sharedData'].selectedShapes.length).toEqual(1);
  });
  it('rightClick should do nothing if the click was on an unselectable element', () => {
    const spy = spyOn<any>(service['selector'], 'selectAtPosition').and.returnValue(null);
    const spy2 = spyOn<any>(service, 'invertSelectionState');
    Object.defineProperty(mouseEvent, 'button', {value : 0});
    Object.defineProperty(mouseEvent, 'target', {value : element});
    service['selectionArea'].isDrawn = false;
    service.rightClick(mouseEvent);
    expect(spy).toHaveBeenCalled();
    expect(spy2).not.toHaveBeenCalled();
  });
  it('click should set the selectedShapes array to 0 and not add an element if the click was on an unselectable element', () => {
    const spy = spyOn<any>(service['selector'], 'selectAtPosition').and.returnValue(null);
    Object.defineProperty(mouseEvent, 'button', {value : 0});
    Object.defineProperty(mouseEvent, 'target', {value : element});
    service['selectionArea'].isDrawn = false;
    service['sharedData'].selectedShapes.push(element);
    service.click(mouseEvent);
    expect(spy).toHaveBeenCalled();
    expect(service['sharedData'].selectedShapes.length).toEqual(0);
  });
  it('arrowKeyUp should only reset the translation command if the arrowKeyState gets set back to 0', () => {
    const spy = spyOn<any>(service['translationService'], 'setKeyAsUp');
    const spy2 = spyOn<any>(service['translationService'], 'resetTranslationCommand');
    service['translationService'].arrowKeyState = 2;
    service.arrowKeyUp(keyBoardEvent);
    expect(spy).toHaveBeenCalled();
    expect(spy2).not.toHaveBeenCalled();
  });
  it('mouseDrag should change the selectedShapes array only if we are on ActionOnDrag.SELECT', () => {
    const spy = spyOn<any>(service['selectionArea'], 'drawShape');
    Object.defineProperty(mouseEvent, 'buttons', {value : 1});
    service['actionOnDrag'] = ActionOnDrag.INVERSE_SELECT;
    service['selectionArea'].intersectingElements.push(element);
    service.mouseDrag(mouseEvent);
    expect(spy).toHaveBeenCalled();
    expect(service['sharedData'].selectedShapes.length).toEqual(0);
  });
  it('mouseDrag does nothing if neither left or right mouse buttons are pressed', () => {
    Object.defineProperty(mouseEvent, 'buttons', {value : 0});
    service.mouseDrag(mouseEvent);
    expect().nothing();
  });
  it('mouseDown set the actionOnDrag to TRANSLATE if we click inside the selection rectangle', () => {
    const spy = spyOn<any>(service['selectionRectangle'], 'isWithinArea').and.returnValue(true);
    const spy2 = spyOn<any>(service['selector'], 'selectAtPosition').and.returnValue(null);
    Object.defineProperty(mouseEvent, 'button', {value : 0});
    service.mouseDown(mouseEvent);
    expect(spy).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
    expect(service['actionOnDrag']).toEqual(ActionOnDrag.TRANSLATE);
  });

  it('wheel should call executeNewRotation if this.sharedData.selectedShapes.length !== 0', () => {
    spyOn<any>(service, 'wheel').and.callThrough();
    const spy = spyOn<any>(service['rotationService'], 'executeNewRotation');
    const event =  new WheelEvent('');
    service['sharedData'].selectedShapes.length = 1;
    service.wheel(event);
    expect(spy).toHaveBeenCalled();
  });

  it('wheel should not call executeNewRotation if this.sharedData.selectedShapes.length === 0', () => {
    spyOn<any>(service, 'wheel').and.callThrough();
    const spy = spyOn<any>(service['rotationService'], 'executeNewRotation');
    const event =  new WheelEvent('');
    service['sharedData'].selectedShapes.length = 0;
    service.wheel(event);
    expect(spy).not.toHaveBeenCalled();
  });

  it('deleteSelection should call deleteSelection', () => {
    spyOn<any>(service, 'deleteSelection').and.callThrough();
    const spy = spyOn<any>(service['manipulationService'], 'deleteSelection');
    service.deleteSelection();
    expect(spy).toHaveBeenCalled();
  });

  it('copySelection should call copySelectionToClipboard', () => {
    spyOn<any>(service, 'copySelection').and.callThrough();
    const spy = spyOn<any>(service['manipulationService'], 'copySelectionToClipboard');
    service.copySelection();
    expect(spy).toHaveBeenCalled();
  });

  it('cutSelection should call cutSelectionToClipboard', () => {
    spyOn<any>(service, 'cutSelection').and.callThrough();
    const spy = spyOn<any>(service['manipulationService'], 'cutSelectionToClipboard');
    service.cutSelection();
    expect(spy).toHaveBeenCalled();
  });

  it('duplicateSelection should call duplicateSelection', () => {
    spyOn<any>(service, 'duplicateSelection').and.callThrough();
    const spy = spyOn<any>(service['manipulationService'], 'duplicateSelection');
    service.duplicateSelection();
    expect(spy).toHaveBeenCalled();
  });

  it('pasteClipboard should call pasteClipboard', () => {
    spyOn<any>(service, 'pasteClipboard').and.callThrough();
    const spy = spyOn<any>(service['manipulationService'], 'pasteClipboard');
    service.pasteClipboard();
    expect(spy).toHaveBeenCalled();
  });

 });
