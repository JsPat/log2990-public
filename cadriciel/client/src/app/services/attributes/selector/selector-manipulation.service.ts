import { Injectable, Renderer2 } from '@angular/core';
import { MultiCommand } from 'src/app/components/app/attributes/command/multi-command';
import { PasteSelectionCommand } from 'src/app/components/app/attributes/command/paste-selection-command';
import { RemoveShapeCommand } from 'src/app/components/app/attributes/command/remove-shape-command';
import { Coordinate } from 'src/app/components/app/attributes/coordinate';
import { SelectorRectangle } from 'src/app/components/app/attributes/selector/selector-rectangle';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { DrawSheetService } from '../../draw-sheet/draw-sheet.service';
import { IdGenerator } from '../../id-generator/id-generator';
import { MatrixGeneratorService } from '../../matrix-generator/matrix-generator.service';
import { SVGChildService } from '../../svg-child/svg-child.service';
import { PotatoShape } from '../paint-bucket/potato-shape';
import { SelectorSharedDataService } from './selector-shared-data.service';

@Injectable({
  providedIn: 'root'
})
export class SelectorManipulationService {
  private readonly OFFSET_LENGTH: number = 5;
  private readonly POTATO_SHAPE_TYPE: string = 'patatoide';
  private clipboard: SVGGraphicsElement[] = new Array<SVGGraphicsElement>();
  private numberOfPastes: number = 0;
  private possiblePastes: number = 0;

constructor(private sharedData: SelectorSharedDataService,
            private renderer: Renderer2,
            private commandInvoker: CommandInvokerService,
            private svgService: SVGChildService,
            private matrixGenerator: MatrixGeneratorService,
            private drawSheet: DrawSheetService,
            private selectionRectangle: SelectorRectangle,
            private idGenerator: IdGenerator) {
            }

  deleteSelection(): void {
    if (this.sharedData.selectedShapes.length !== 0) {
      const commandArray = new Array<RemoveShapeCommand>();
      this.sharedData.selectedShapes.forEach( (elem) => {
        const deleteCommand = new RemoveShapeCommand(this.svgService, this.renderer, elem);
        commandArray.push(deleteCommand);
      });
      this.commandInvoker.execute(new MultiCommand(commandArray));
      // Empty the selectedShapes array after removing all shapes
      this.sharedData.selectedShapes = new Array<SVGGraphicsElement>();
    }
  }

  copySelectionToClipboard(): void {
    if (this.sharedData.selectedShapes.length !== 0) {
      // We first empty the clipboard
      this.clipboard = new Array<SVGGraphicsElement>();
      // This prevents the clipboard shapes from being updated when we move the original shapes
      this.sharedData.selectedShapes.forEach( (elem) => {
        this.clipboard.push( elem.cloneNode(true) as SVGGraphicsElement);
      });
      this.numberOfPastes = 0;
      this.possiblePastes = this.numberOfPossiblePastes();
      // Knowing the selection and its placement, we find the number of times we can offset by 5 px vertically/horizontally
      // And save this value for a modulo operation later
    }
  }

  cutSelectionToClipboard(): void {
    this.copySelectionToClipboard();
    this.deleteSelection();
  }

  duplicateSelection(): void {
    if (this.sharedData.selectedShapes.length !== 0) {
      this.numberOfPastes = 0;
      this.possiblePastes = this.numberOfPossiblePastes();
      this.pasteClipboard(this.sharedData.selectedShapes.slice());
    }
  }

  // If shapes is undefined, we use the clipboard content, otherwise we use shapes as the pasted material
  pasteClipboard(shapes?: SVGGraphicsElement[]): void {
    if (shapes !== undefined || this.clipboard.length !== 0) {
      this.numberOfPastes++;
      const offset = this.determineOffset();
      const offsetMatrix = this.matrixGenerator.createTranslationMatrixFromVariation(offset, offset);
      if (shapes) {
        const copiedShapes = this.createCopiedShapes(shapes);
        this.commandInvoker.execute(new PasteSelectionCommand(copiedShapes, this.svgService, offsetMatrix, this));
        this.sharedData.selectedShapes = copiedShapes;
      } else {
        // We select the pasted shapes
        const copiedShapes = this.createCopiedShapes();
        this.commandInvoker.execute(new PasteSelectionCommand(copiedShapes, this.svgService, offsetMatrix, this));
        this.sharedData.selectedShapes = copiedShapes;
      }
    }
  }

  isWithinDrawing(point: Coordinate): boolean {
    if (this.drawSheet.pageHeight === null || this.drawSheet.pageWidth === null) {
      return false;
    } else {
      return  point.x > 0 && point.x < this.drawSheet.pageWidth &&
              point.y > 0 && point.y < this.drawSheet.pageHeight;
    }
  }

  manipulateNumberOfPastes(increment: boolean): void {
    this.numberOfPastes += (increment) ? 1 : -1;
    // If we find ourselves with a negative number of pastes, we set it to 0
    this.numberOfPastes =  Math.max(this.numberOfPastes, 0);
  }

  // Returns the number of possible pastes before we need to start back on the original position
  private numberOfPossiblePastes(): number {
    const originalDot = this.selectionRectangle.position;
    // If we are outside of the drawing by default we are in an edgecase, that we represent by returning -1
    if (this.drawSheet.pageHeight === null || this.drawSheet.pageWidth === null) {
      return -1;
    } else if ( originalDot.x > this.drawSheet.pageWidth || originalDot.y > this.drawSheet.pageHeight) {
      return -1;
    } else {
      const numberOfPossibleVerticleOffsets = Math.floor((this.drawSheet.pageHeight - originalDot.y) / this.OFFSET_LENGTH);
      const numberOfPossibleHorizontalOffsets = Math.floor((this.drawSheet.pageWidth - originalDot.x) / this.OFFSET_LENGTH);
      return Math.min(numberOfPossibleVerticleOffsets, numberOfPossibleHorizontalOffsets);
    }
  }

  private determineOffset(): number {
    if (this.possiblePastes === -1) {
      return this.numberOfPastes * this.OFFSET_LENGTH;
    } else {
      if (this.possiblePastes === 0) {
        return 0;
      }
      return (this.numberOfPastes % this.possiblePastes) * this.OFFSET_LENGTH;
    }
  }

  private createCopiedShapes(shapes?: SVGGraphicsElement[]): SVGGraphicsElement[] {
    const copiedShapes: SVGGraphicsElement[] = new Array<SVGGraphicsElement>();
    if (shapes) {
      shapes.forEach((elem) => {
        const newShape = elem.cloneNode(true) as SVGGraphicsElement;
        // Add check for shapes that require new IDS
        if (newShape.dataset.type === this.POTATO_SHAPE_TYPE) {
          PotatoShape.changeMaskId(newShape, this.idGenerator);
        }
        copiedShapes.push(newShape);
      });
    } else {
      this.clipboard.forEach((elem) => {
        // Add check for shapes that require new IDS
        const newShape = elem.cloneNode(true) as SVGGraphicsElement;
        // Add check for shapes that require new IDS
        if (newShape.dataset.type === this.POTATO_SHAPE_TYPE) {
          PotatoShape.changeMaskId(newShape, this.idGenerator);
        }
        copiedShapes.push(newShape as SVGGraphicsElement);
      });
    }
    return copiedShapes;
  }

}
