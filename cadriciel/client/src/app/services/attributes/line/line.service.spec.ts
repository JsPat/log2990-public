// tslint:disable: no-string-literal
// tslint:disable: no-any
// tslint:disable: no-magic-numbers

import { NO_ERRORS_SCHEMA, RendererFactory2 } from '@angular/core';
import { async, inject, TestBed } from '@angular/core/testing';
import { Color } from 'src/app/color-palette/color/color';
import { Line } from 'src/app/components/app/attributes/line/line';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { LineService } from '../line/line.service';

describe('LineService', () => {
  let service: LineService;

  let color: Color;
  let line: Line;
  let event: MockMouseEvent;
  let rendererSpy: any;
  let rendererFactorySpy: any;
  let commandInvokerSpy: any;

  class MockColor extends Color {
    constructor() {
        super(0, 0, 0);
    }
  }
  // tslint:disable-next-line: max-classes-per-file
  class MockMouseEvent extends MouseEvent {
    mockoffsetX: number;
    mockoffsetY: number;

    constructor() {
      super('mousedown');
    }
  }

  beforeEach(async(() => {

    rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute', 'appendChild']);
    commandInvokerSpy = jasmine.createSpyObj('CommandInvokerService', ['execute', 'removeLastExecuted']);

    rendererFactorySpy = {
      createRenderer(_0: null, _1: null): any {
        return rendererSpy;
      }
    };

    TestBed.configureTestingModule({
      providers: [
        LineService,
        { provide: Color, useClass: MockColor},
        { provide: MouseEvent, useClass: MockMouseEvent},
        { provide: RendererFactory2, useValue: rendererFactorySpy},
        { provide: CommandInvokerService, useValue: commandInvokerSpy}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  }));

  beforeEach(inject([LineService], (injected: LineService)  => {

    service = injected;

    color = new MockColor();
    line = new Line(color, {x: 5, y: 5}, 5, rendererSpy, false);
    event = new MockMouseEvent();
    service['line'] = line;
  }));

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('shiftDown function updates the previewLine if line is defined', () => {
    service['line'] = line;
    const updatePreviewLine = spyOn<any>(service['line'], 'updatePreviewLine');
    service.shiftDown();
    expect(updatePreviewLine).toHaveBeenCalledTimes(1);
  });

  it('shiftDown function does not updates the previewLine if line is undefined', () => {
    service['line'] = line;
    const updatePreviewLine = spyOn(service['line'], 'updatePreviewLine');
    service['line'] = undefined;
    service.shiftDown();
    expect(updatePreviewLine).not.toHaveBeenCalledTimes(1);
  });

  it('shiftUp function updates the previewLine uf line is defined', () => {
    if (service['line']) {
      spyOn(service['line'], 'updatePreviewLine');
      service.shiftUp();
      expect(service['line'].updatePreviewLine).toHaveBeenCalledTimes(1);
    }
  });

  it('shiftUp should do nothing if line is undefined', () => {
    service['line'] = line;
    const updatePreviewLine = spyOn(service['line'], 'updatePreviewLine');
    service['line'] = undefined;

    service.shiftUp();

    expect(updatePreviewLine).not.toHaveBeenCalled();
  });

  it('backspace function calls removePreviousDot if line is defined', () => {
    if (service['line']) {
      spyOn(service['line'], 'removePreviousDot');
      service.backSpaceDown();
      expect(service['line'].removePreviousDot).toHaveBeenCalledTimes(1);
    }
  });

  it('backspaceDown should do nothing if line is undefined', () => {
    const removePreviousDot = spyOn<any>(service['line'], 'removePreviousDot');
    service['line'] = undefined;

    service.backSpaceDown();

    expect(removePreviousDot).not.toHaveBeenCalled();
  });

  it('escape function removes the line and sets it as undefined, if line is defined', () => {
    service['line'] = line;
    service.escapeDown();
    expect(commandInvokerSpy.removeLastExecuted).toHaveBeenCalled();
    expect(service['line']).toBeUndefined();
  });

  it('escapeDown should do nothing if line is undefined', () => {
    service['line'] = undefined;

    service.escapeDown();

    expect(commandInvokerSpy.removeLastExecuted).not.toHaveBeenCalled();
  });

  it('click adds a new dot to the line if line is not undefined and return undefined', () => {
    if (service['line']) {
      spyOn(service['line'], 'addNewDotToLine');
      const result = service.click(event);
      expect(service['line'].addNewDotToLine).toHaveBeenCalled();
      expect(result).toBeUndefined();
    }
  });

  it('mouseDrag calls mouseMove', () => {
    spyOn(service, 'mouseMove');
    service.mouseDrag(event);
    expect(service.mouseMove).toHaveBeenCalled();
  });

  it('mouseLeave sets line to undefined if line is defined', () => {
    service['line'] = line;
    if (service['line']) {
      service.mouseLeave(event);
      expect(service['line']).toBeUndefined();
    }
  });

  it('click returns any[] if line is undefined', () => {
    service['line'] = undefined;
    service.click(event);
    expect(service['commandInvoker']).toBeDefined();
  });

  it('click generates a line with a dottedEdgeDiameter parameter if junctionsEnabled is true', () => {
    service['line'] = undefined;
    service['dottedEdgeDiameter'] = 13;
    service['junctionsEnabled'] = true;
    service.click(event);
    // casting as any because we know it is not undefined but the compiler does not
    expect((service['line'] as any).dottedEdgeDiameter).toBe(13);
  });

  it('click generates a line with a 0 as dottedEdgeDiameter parameter if junctionsEnabled is false', () => {
    service['line'] = undefined;
    service['junctionsEnabled'] = false;
    service.click(event);
    // casting as any because we know it is not undefined but the compiler does not
    expect((service['line'] as any).dottedEdgeDiameter).toBe(0);
  });

  it('dblClick sets line to undefined if line is defined', () => {
    service.dblClick(event);
    expect(service['line']).toBeUndefined();
  });

  it('dblClick completes the line if the start and end of the line are within 5 px of each other', () => {
    service['line'] = undefined;
    service.dblClick(event); // x: 0, y: 0
    expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(16);
    service['line'] = new Line(color, {x: 5, y: 5}, 5, rendererSpy, false);
    service['line'].dots[0] = {x: 500, y: 500};
    service.dblClick(event); // x: 0, y: 0
    expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(32);
  });

  it('dblClick should do nothing if line is undefined', () => {
    service['line'] = line;
    const completeLine = spyOn(service['line'], 'completeLine');
    service['line'] = undefined;

    service.dblClick(event);

    expect(completeLine).not.toHaveBeenCalled();
  });

  it('mouseMove updates the preview if line is defined', () => {
    service['line'] = line;
    spyOn(service['line'], 'updatePreviewLine');
    service.mouseMove(event);
    expect(service['line'].updatePreviewLine).toHaveBeenCalled();
  });

  it('mouseMove uses lineService to compute the shifted preview line if shift is pressed', () => {
    service['line'] = line;
    service['line'].addNewDotToLine({x: 15, y: 5});
    service.mouseMove(new MouseEvent('mousemove', {bubbles: false, cancelable: false, composed: false, shiftKey: true}));
    expect(service['line'].getCursorDot()).toEqual({x: 0, y: 0});
  });

  it('mouseMove should do nothing if line is undefined', () => {
    service['line'] = line;
    const updatePreviewLine = spyOn(service['line'], 'updatePreviewLine');
    service['line'] = undefined;

    service.mouseMove(event);

    expect(updatePreviewLine).not.toHaveBeenCalled();
  });

  it('toolSwap sets line as undefined', () => {
    service.toolSwap();
    expect(service['line']).toBeUndefined();
  });
});
