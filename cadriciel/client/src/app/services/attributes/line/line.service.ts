import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { CreateShapeCommand } from 'src/app/components/app/attributes/command/create-shape-command';
import { Coordinate } from 'src/app/components/app/attributes/coordinate';
import { Line } from 'src/app/components/app/attributes/line/line';
import { BASIC_WIDTH, Tool } from 'src/app/components/app/attributes/tool';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { SVGChildService } from '../../svg-child/svg-child.service';

@Injectable({
  providedIn: 'root'
})
export class LineService extends Tool {

  private junctionsEnabled: boolean = true;
  private strokeSize: number = BASIC_WIDTH;
  private dottedEdgeDiameter: number = BASIC_WIDTH * 2;

  private line: Line | undefined;
  private renderer: Renderer2;

  constructor(rendererFactory: RendererFactory2,
              private colorService: ColorService,
              private svgService: SVGChildService,
              private commandInvoker: CommandInvokerService ) {
      super();
      this.renderer = rendererFactory.createRenderer(null, null);
  }

  mouseMove(event: MouseEvent): void {
      if (this.line) {
          const newCursorPos = {x: event.offsetX, y: event.offsetY};
          this.line.cursorLocation = newCursorPos;
          const newDot: Coordinate = (event.shiftKey) ?
                          this.line.computeShiftedEndPoint(newCursorPos) :
                          newCursorPos;
          this.line.updatePreviewLine(newDot);
      }
  }

  mouseDrag(event: MouseEvent): void {
      this.mouseMove(event);
  }

  dblClick(event: MouseEvent): void {
      if (this.line) {
          if (this.line.connectsToStartPoint({x: event.offsetX, y: event.offsetY})) {
              this.line.completeLine();
          }
          this.line = undefined;
      }
  }

  click(event: MouseEvent): void {
      if (this.line) {
          this.line.addNewDotToLine({x: event.offsetX, y: event.offsetY});
      } else {
          this.line = new Line (
              this.colorService.mainColor,
              {x: event.offsetX, y: event.offsetY},
              this.strokeSize,
              this.renderer,
              this.junctionsEnabled,
              (this.junctionsEnabled) ? this.dottedEdgeDiameter : undefined
          );
          this.line.drawShape();
          this.commandInvoker.execute(new CreateShapeCommand(this.line, this.svgService));
      }
  }

  shiftDown(): void {
      if (this.line) {
          this.line.updatePreviewLine(this.line.computeShiftedEndPoint(this.line.getCursorDot()));
      }
  }

  shiftUp(): void {
      if (this.line) {
          this.line.updatePreviewLine();
      }
  }

  escapeDown(): void {
      if (this.line) {
          this.commandInvoker.removeLastExecuted();
          this.svgService.removeSVG(this.line);
          this.line = undefined;
      }
  }

  backSpaceDown(): void {
      if (this.line) {
          this.line.removePreviousDot();
      }
  }

  mouseLeave(event: MouseEvent): void {
      this.line = undefined;
  }

  toolSwap(): void {
      this.line = undefined;
  }
}
