import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { CreateShapeCommand } from 'src/app/components/app/attributes/command/create-shape-command';
import { Stamp } from 'src/app/components/app/attributes/stamp/stamp';
import { Tool } from 'src/app/components/app/attributes/tool';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { SVGChildService } from '../../svg-child/svg-child.service';

@Injectable({
  providedIn: 'root'
})
export class StampService extends Tool {

  private readonly REGULAR_ROTATION_STEP: number = 15;
  private readonly MAX_DEGREES: number = 360;
  private readonly BASIC_FACTOR: number = 50;
  private readonly BASIC_SHAPE: string = 'heart';

  private factor: number = this.BASIC_FACTOR;
  private angle: number = 0;
  private shape: string = this.BASIC_SHAPE;

  private stamp: Stamp | undefined;
  private renderer: Renderer2;

  constructor(rendererFactory: RendererFactory2,
              private colorService: ColorService,
              private svgService: SVGChildService,
              private commandInvoker: CommandInvokerService) {
      super();
      this.renderer = rendererFactory.createRenderer(null, null);
  }

  click(event: MouseEvent): void {
    this.stamp = new Stamp (this.renderer,
                            this.colorService.mainColor,
                            this.colorService.secondaryColor,
                            {x: event.offsetX, y: event.offsetY},
                            this.factor,
                            this.angle,
                            this.shape);

    this.commandInvoker.execute(new CreateShapeCommand(this.stamp, this.svgService));
  }

  wheel(event: WheelEvent): void {
    let step = (event.altKey) ? 1 : this.REGULAR_ROTATION_STEP;
    step *= (Math.sign(event.deltaY) >= 0) ? -1 : 1;
    if ((this.angle + step) < 0) {
      this.angle = this.MAX_DEGREES + step - this.angle;
    } else {
      this.angle += step;
      this.angle %= this.MAX_DEGREES;
    }
  }

  toolSwap(): void {
    this.stamp = undefined;
  }

}
