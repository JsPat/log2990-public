/* tslint:disable:no-unused-variable */
// tslint:disable: no-string-literal
// tslint:disable: no-any
// tslint:disable: no-magic-numbers

import { NO_ERRORS_SCHEMA, RendererFactory2 } from '@angular/core';
import { async, inject, TestBed } from '@angular/core/testing';
import { BLACK, WHITE } from 'src/app/color-palette/constant';
import { Stamp } from 'src/app/components/app/attributes/stamp/stamp';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { StampService } from './stamp.service';

// tslint:disable-next-line: max-classes-per-file
class MockMouseEvent extends MouseEvent {
    mockoffsetX: number;
    mockoffsetY: number;

    constructor() {
      super('mousedown');
    }
  }

describe('StampService', () => {
  let service: StampService;

  const color = BLACK;
  const factor = 50;
  const angle = 0;
  const shape = 'heart';
  let rendererSpy: any;
  let rendererFactorySpy: any;
  let commandInvokerSpy: any;

  beforeEach(async(() => {

    rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute', 'appendChild']);
    commandInvokerSpy = jasmine.createSpyObj('CommandInvokerService', ['execute', 'removeLastExecuted']);

    rendererFactorySpy = {
      createRenderer(_0: null, _1: null): any {
        return rendererSpy;
      }
    };

    TestBed.configureTestingModule({
      providers: [
        StampService,
        { provide: MouseEvent, useClass: MockMouseEvent},
        { provide: RendererFactory2, useValue: rendererFactorySpy},
        { provide: CommandInvokerService, useValue: commandInvokerSpy}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  }));

  beforeEach(inject([StampService], (injected: StampService)  => {

    service = injected;
    service['stamp'] = new Stamp(service['renderer'], color, color, {x: 5, y: 5}, factor, angle, shape);
  }));

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('toolSwap should set stamp to undefined', () => {
    service.toolSwap();
    expect(service['stamp']).not.toBeDefined();
  });

  it('wheel should add 15 per mouse crank when altKey is up', () => {
    const eventSpy = jasmine.createSpyObj('WheelEvent', ['']);
    Object.defineProperty(eventSpy, 'altKey', {value: false});
    Object.defineProperty(eventSpy, 'deltaY', {value: 10});
    service.wheel(eventSpy);
    expect(service['angle']).toBe(345);
  });

  it('wheel should remove 15 per mouse crank when altKey is up', () => {
    const eventSpy = jasmine.createSpyObj('WheelEvent', ['']);
    Object.defineProperty(eventSpy, 'altKey', {value: false});
    Object.defineProperty(eventSpy, 'deltaY', {value: -10});
    service.wheel(eventSpy);
    expect(service['angle']).toBe(15);
  });

  it('wheel should add 15 per mouse crank when altKey is up and go back to starting numbers', () => {
    service['angle'] = 355;
    const eventSpy = jasmine.createSpyObj('WheelEvent', ['']);
    Object.defineProperty(eventSpy, 'altKey', {value: false});
    Object.defineProperty(eventSpy, 'deltaY', {value: -10});
    service.wheel(eventSpy);
    expect(service['angle']).toBe(10);
  });

  it('wheel should add 1 per mouse crank when altKey is down', () => {
    const eventSpy = jasmine.createSpyObj('WheelEvent', ['']);
    Object.defineProperty(eventSpy, 'altKey', {value: true});
    Object.defineProperty(eventSpy, 'deltaY', {value: -10});
    service.wheel(eventSpy);
    expect(service['angle']).toBe(1);
  });

  it('click should create a new stamp and call execute', () => {
    service['factor'] = 100;
    const eventSpy = jasmine.createSpyObj('MouseEvent', ['']);
    Object.defineProperty(eventSpy, 'offsetX', {value: 10});
    Object.defineProperty(eventSpy, 'offsetY', {value: 10});
    const stampTest = new Stamp(service['renderer'], color, WHITE, {x: 10, y: 10}, service['factor'], service['angle'], shape);
    service.click(eventSpy);
    expect(service['stamp']).toEqual(stampTest);
    expect(service['commandInvoker'].execute).toHaveBeenCalled();
  });

});
