// tslint:disable: no-any
// tslint:disable: no-string-literal
// tslint:disable: no-magic-numbers

import { NO_ERRORS_SCHEMA, RendererFactory2 } from '@angular/core';
import { async, inject, TestBed } from '@angular/core/testing';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { Color } from 'src/app/color-palette/color/color';
import { Coordinate } from 'src/app/components/app/attributes/coordinate';
import { PaintBrush } from 'src/app/components/app/attributes/paint-brush/paint-brush';
import { AutoSaveService } from '../../auto-save/auto-save.service';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { ShapesHolderService } from '../../shapes-holder/shapes-holder.service';
import { SVGChildService } from '../../svg-child/svg-child.service';
import { PaintBrushService } from './paint-brush.service';

describe('PaintBrushService', () => {
  let service: PaintBrushService;

  let rendererSpy: any;
  let rendererFactorySpy: any;

  let color: Color;
  let mousePosition: Coordinate;
  let shape: PaintBrush;
  let shapeHolderSpy: ShapesHolderService;

  class MockColor extends Color {
    constructor() {
        super(0, 0, 0);
    }
  }

  beforeEach(async(() => {

    rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute', 'appendChild']);
    shapeHolderSpy = new ShapesHolderService();

    rendererFactorySpy = {
      createRenderer(_0: null, _1: null): any {
        return rendererSpy;
      }
    };

    TestBed.configureTestingModule({
      providers: [
        PaintBrushService,
        { provide: Color, useClass: MockColor},
        { provide: RendererFactory2, useValue: rendererFactorySpy}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  }));

  beforeEach(inject([PaintBrushService], (injected: PaintBrushService) => {
    service = injected;

    mousePosition = {x: 1, y: 1};
    color = new MockColor();
    shape = new PaintBrush(mousePosition, color, 5, '@sapeli', rendererSpy );
    service['shape'] = shape;
  }));

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  // constructor
  it('constructor should initialise the attributs', () => {
    const colorService = new ColorService();
    const svgService = new SVGChildService(shapeHolderSpy);
    const mockAutoSave = {
    } as unknown as AutoSaveService;
    const commandInvoker = new CommandInvokerService(mockAutoSave);
    const newPaintBrush = new PaintBrushService(colorService, rendererFactorySpy, svgService, commandInvoker);
    expect(newPaintBrush).toBeDefined();
  });

  it('creatNewLine should send a new command', () => {
    const newDots = new Coordinate();
    spyOn<any>(service['commandInvoker'], 'execute');

    service['createNewLine'](newDots);
    expect(service['commandInvoker'].execute).toHaveBeenCalled();
  });

});
