import { Injectable, RendererFactory2 } from '@angular/core';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { CreateShapeCommand } from 'src/app/components/app/attributes/command/create-shape-command';
import { Coordinate } from 'src/app/components/app/attributes/coordinate';
import { PaintBrush } from 'src/app/components/app/attributes/paint-brush/paint-brush';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { SVGChildService } from '../../svg-child/svg-child.service';
import { PenService } from '../pen/pen.service';

const BASIC_FILTER: string = '@sapeli';

@Injectable({
  providedIn: 'root'
})
export class PaintBrushService extends PenService {
  private filter: string = BASIC_FILTER;

  constructor(protected colorService: ColorService, protected rendererFactory: RendererFactory2,
              protected svgService: SVGChildService, protected commandInvoker: CommandInvokerService) {
    super(colorService, rendererFactory, svgService, commandInvoker);
  }

  protected createNewLine(mousePosition: Coordinate): void {
    this.shape = new PaintBrush(
      mousePosition,
      this.colorService.mainColor,
      this.strokeWidth,
      this.filter,
      this.renderer
    );
    this.shape.pushDot(mousePosition);
    this.commandInvoker.execute(new CreateShapeCommand(this.shape, this.svgService));
  }

}
