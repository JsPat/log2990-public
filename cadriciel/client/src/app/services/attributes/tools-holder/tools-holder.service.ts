import { Injectable } from '@angular/core';
import { Tool } from 'src/app/components/app/attributes/tool';
import { PossibleTools } from '../../constants';
import { CalligraphyService } from '../calligraphy/calligraphy.service';
import { ColorizeService } from '../colorize/colorize.service';
import { EllipseService } from '../ellipse/ellipse.service';
import { EraserService } from '../eraser/eraser.service';
import { LineService } from '../line/line.service';
import { PaintBrushService } from '../paint-brush/paint-brush.service';
import { PaintBucketService } from '../paint-bucket/paint-bucket.service';
import { PenService } from '../pen/pen.service';
import { PipetteService } from '../pipette/pipette.service';
import { PolygoneService } from '../polygone/polygone.service';
import { RectangleService } from '../rectangle/rectangle.service';
import { SelectorService } from '../selector/selector.service';
import { SprayPaintService } from '../spray-paint/spray-paint.service';
import { StampService } from '../stamp/stamp.service';
import { TextService } from '../text/text.service';

@Injectable({
  providedIn: 'root'
})
export class ToolHolderService {
  private readonly tools: Tool[] = new Array<Tool>(PossibleTools.N_TOOLS);
  private currentTool: Tool;

  constructor(calligraphy: CalligraphyService,
              colorize: ColorizeService,
              ellipse: EllipseService,
              eraser: EraserService,
              line: LineService,
              paintBrush: PaintBrushService,
              paintBucket: PaintBucketService,
              pen: PenService,
              pipette: PipetteService,
              polygone: PolygoneService,
              rectangle: RectangleService,
              selector: SelectorService,
              sprayPaint: SprayPaintService,
              stamp: StampService,
              text: TextService) {
  this.tools[PossibleTools.CALLIGRAPHY] = calligraphy;
  this.tools[PossibleTools.COLORIZE] = colorize;
  this.tools[PossibleTools.ELLIPSE] = ellipse;
  this.tools[PossibleTools.ERASER] = eraser;
  this.tools[PossibleTools.LINE] = line;
  this.tools[PossibleTools.PAINTBRUSH] = paintBrush;
  this.tools[PossibleTools.PAINTBUCKET] = paintBucket;
  this.tools[PossibleTools.PEN] = pen;
  this.tools[PossibleTools.PIPETTE] = pipette;
  this.tools[PossibleTools.POLYGONE] = polygone;
  this.tools[PossibleTools.RECTANGLE] = rectangle;
  this.tools[PossibleTools.SELECT] = selector;
  this.tools[PossibleTools.SPRAYPAINT] = sprayPaint;
  this.tools[PossibleTools.STAMP] = stamp;
  this.tools[PossibleTools.TEXT] = text;

  this.currentTool = this.tools[PossibleTools.SELECT];

  }

  get activeTool(): Tool {
    return this.currentTool;
  }

  toolSwap(newTools?: PossibleTools): void {
    this.currentTool.toolSwap();
    if (newTools !== undefined) {
      this.currentTool = this.tools[newTools];
    }
  }
}
