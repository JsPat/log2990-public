/* tslint:disable:no-unused-variable
   tslint:disable: no-string-literal */

import { async, inject, TestBed } from '@angular/core/testing';
import { PossibleTools } from '../../constants';
import { ToolHolderService } from './tools-holder.service';

describe('Service: ToolHolder', () => {

    let service: ToolHolderService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            providers: [ToolHolderService]
          });
    }));

    beforeEach(inject([ToolHolderService], (injected: ToolHolderService) => {
        service = injected;
    }));

    it('should create', () => {
        expect(service).toBeTruthy();
    });

    it('toolSwap should still call currentTool.toolSwap if their is no parameter given', () => {
        const func = spyOn(service['currentTool'], 'toolSwap');
        const currentTool = service['currentTool'];
        service.toolSwap();
        expect(func).toHaveBeenCalled();
        expect(service['currentTool']).toEqual(currentTool);
    });

    it('toolSwap should call toolSwap of the current tool the change the current tool to the given value', () => {
        const func = spyOn(service['currentTool'], 'toolSwap');
        service['currentTool'] = service['tools'][PossibleTools.SELECT];
        service.toolSwap(PossibleTools.TEXT);
        expect(func).toHaveBeenCalled();
        expect(service['currentTool']).toEqual(service['tools'][PossibleTools.TEXT]);
    });

    it('activeTool getter should retrun the current tool of the holder', () => {
        service['currentTool'] = service['tools'][PossibleTools.ERASER];
        expect(service.activeTool).toEqual(service['tools'][PossibleTools.ERASER]);
    });
});
