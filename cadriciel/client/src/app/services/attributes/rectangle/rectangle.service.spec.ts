/* tslint:disable:no-unused-variable
   tslint:disable: no-any
   tslint:disable: no-string-literal
   tslint:disable: no-magic-numbers */

import { NO_ERRORS_SCHEMA, RendererFactory2 } from '@angular/core';
import { async, inject, TestBed } from '@angular/core/testing';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { SVGChildService } from '../../svg-child/svg-child.service';
import { RectangleService } from './rectangle.service';

describe('RectangleService', () => {
  let service: RectangleService;

  let rendererSpy: any;
  let rendererFactorySpy: any;

  let colorServiceSpy: any;
  let svgServiceSpy: any;
  let commandInvokerSpy: any;

  let rectangle: any;
  let preview: any;

  beforeEach(async(() => {

    commandInvokerSpy = jasmine.createSpyObj('CommandInvokerService', ['execute', 'removeLastExecuted']);
    rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute', 'appendChild']);
    colorServiceSpy = jasmine.createSpyObj('ColorService', ['']);
    svgServiceSpy = jasmine.createSpyObj('SVGChildService', ['removeSVG', 'addNewSVG']);

    rendererFactorySpy = {
      createRenderer(_0: null, _1: null): any {
        return rendererSpy;
      }
    };

    TestBed.configureTestingModule({
      providers: [
        RectangleService,
        { provide: CommandInvokerService, useValue: commandInvokerSpy},
        { provide: ColorService, useValue: colorServiceSpy },
        { provide: SVGChildService, useValue: svgServiceSpy },
        { provide: RendererFactory2, useValue: rendererFactorySpy}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  }));

  beforeEach(inject([RectangleService], (injected: RectangleService) => {

    service = injected;

    service['mousePosition'] = {x: 0, y: 0};
    rectangle = jasmine.createSpyObj('Rectangle', ['updateShape', 'makeInvisible', 'isNull']);
    preview = jasmine.createSpyObj('Rectangle', ['updateShape', 'makeInvisible']);

    spyOn<any>(service, 'constructRectangle').and.returnValue(rectangle);
    spyOn<any>(service, 'constructPreview').and.returnValue(preview);

  }));

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  // mouseDown
  it('MouseDown should store the current value of the mouse', () => {
    const event = new MouseEvent('mouseDown');
    spyOnProperty(event, 'offsetX').and.returnValue(10);
    spyOnProperty(event, 'offsetY').and.returnValue(12);
    service.mouseDown(event);

    expect(service['mousePosition'].x).toBe(10);
    expect(service['mousePosition'].y).toBe(12);
  });

  it('MouseDown should create both the preview and the rectangle', () => {
    const event = new MouseEvent('mouseDown');
    spyOn<any>(service, 'createRectangle');
    spyOn<any>(service, 'createPreview');
    service.mouseDown(event);

    expect(service['createRectangle']).toHaveBeenCalled();
    expect(service['createPreview']).toHaveBeenCalled();
  });

  it('MouseDown should create both the preview and the rectangle', () => {
    const event = new MouseEvent('mouseDown');
    spyOn<any>(service, 'createRectangle');
    spyOn<any>(service, 'createPreview');
    service.mouseDown(event);

    expect(service['createRectangle']).toHaveBeenCalled();
    expect(service['createPreview']).toHaveBeenCalled();
  });

  it('MouseDown should not create both the preview and the rectangle if buttons is not left click', () => {
    const event = new MouseEvent('mouseDown', {
      button: 3
    });
    spyOn<any>(service, 'createRectangle');
    spyOn<any>(service, 'createPreview');
    service.mouseDown(event);

    expect(service['createRectangle']).not.toHaveBeenCalled();
    expect(service['createPreview']).not.toHaveBeenCalled();
  });

  it('MouseDown should execute the command that create the recatngle', () => {
    const event = new MouseEvent('mouseDown');
    service.mouseDown(event);

    expect(commandInvokerSpy.execute).toHaveBeenCalled();
  });

  // MouseUp
  it('mouseUp reset the rectangle and preview shape if rectangle and preview are defined', () => {
    service['rectangle'] = rectangle;
    service['previewShape'] = preview;
    service.mouseUp();

    expect(service['rectangle']).toBeUndefined();
    expect(svgServiceSpy.removeSVG).toHaveBeenCalled();
    expect(service['previewShape']).toBeUndefined();
  });

  it('mouseUp do not reset the rectangle and preview shape if rectangle is undefined', () => {
    service['rectangle'] = undefined;
    service['previewShape'] = preview;
    service.mouseUp();

    expect(svgServiceSpy.removeSVG).not.toHaveBeenCalled();
  });

  it('mouseUp do not reset the rectangle and preview shape if preview is undefined', () => {
    service['rectangle'] = rectangle;
    service['previewShape'] = undefined;
    service.mouseUp();

    expect(svgServiceSpy.removeSVG).not.toHaveBeenCalled();
  });

  it('mouseUp should remove the rectangle if the rectangle is null', () => {
    rectangle.isNull.and.returnValue(true);
    service['rectangle'] = rectangle;
    service['previewShape'] = preview;
    service.mouseUp();
    expect(commandInvokerSpy.removeLastExecuted).toHaveBeenCalled();
    expect(svgServiceSpy.removeSVG).toHaveBeenCalledWith(rectangle);
  });

  it('mouseUp should not remove the rectangle if the rectangle is not null', () => {
    rectangle.isNull.and.returnValue(false);
    service['rectangle'] = rectangle;
    service['previewShape'] = preview;
    service.mouseUp();
    expect(commandInvokerSpy.removeLastExecuted).not.toHaveBeenCalled();
    expect(svgServiceSpy.removeSVG).not.toHaveBeenCalledWith(rectangle);
  });

  // mouseLeave
  it('mouseLeave should call mouseUp', () => {
    spyOn(service, 'mouseUp');
    service.mouseLeave();

    expect(service.mouseUp).toHaveBeenCalled();
  });

  // toolSwap
  it('toolSwap should call mouseUp', () => {
    spyOn(service, 'mouseUp');
    service.toolSwap();

    expect(service.mouseUp).toHaveBeenCalled();
  });

  // mouseDrag
  it('mouseDrag should do nothing (not call updateShape or change attributes) if rectangle is undefined', () => {
    spyOn<any>(service, 'updateShape');
    service['rectangle'] = undefined;

    const event = new MouseEvent('mouseDrag');

    service.mouseDrag(event);
    expect(service['updateShape']).not.toHaveBeenCalled();
  });

  it('mouseDrag should capture the new mouse position and call updateShape if rectangle is defined', () => {
    spyOn<any>(service, 'updateShape');
    service['rectangle'] = rectangle;

    const event = new MouseEvent('mouseDown');
    spyOnProperty(event, 'offsetX').and.returnValue(14);
    spyOnProperty(event, 'offsetY').and.returnValue(16);

    service.mouseDrag(event);

    expect(service['mousePosition']).toEqual({x: 14, y: 16});
    expect(service['updateShape']).toHaveBeenCalled();
  });

  // shiftup
  it('shiftUp should do nothing (not call updateShape and make invisibloe) if rectangle is undefined', () => {
    spyOn<any>(service, 'updateShape');
    service['rectangle'] = undefined;

    service.shiftUp();
    expect(service['updateShape']).not.toHaveBeenCalled();
    expect(preview.makeInvisible).not.toHaveBeenCalled();
  });

  it('shiftUp should do nothing (not call updateShape and makeInvisible) if preview is undefined', () => {
    spyOn<any>(service, 'updateShape');
    service['rectangle'] = rectangle;
    service['previewShape'] = undefined;

    service.shiftUp();
    expect(service['updateShape']).not.toHaveBeenCalled();
    expect(preview.makeInvisible).not.toHaveBeenCalled();
  });

  it('shiftUp should call updateShape and makeInvisible if preview and rectangle are defined', () => {
    spyOn<any>(service, 'updateShape');
    service['rectangle'] = rectangle;
    service['previewShape'] = preview;

    service.shiftUp();
    expect(service['updateShape']).toHaveBeenCalled();
    expect(preview.makeInvisible).toHaveBeenCalled();
  });

  // shiftDown
  it('shiftDown should do nothing (not call updateShape) if rectangle is undefined', () => {
    spyOn<any>(service, 'updateShape');
    service['rectangle'] = undefined;

    service.shiftDown();
    expect(service['updateShape']).not.toHaveBeenCalled();
  });

  it('shiftDown should call updateShape if rectangle is defined', () => {
    spyOn<any>(service, 'updateShape');
    service['rectangle'] = rectangle;

    service.shiftDown();
    expect(service['updateShape']).toHaveBeenCalled();
  });

  // update shape
  it('updateShape should do nothing (not calling update) on each triangle if rectangle is undefined', () => {
    service['rectangle'] = undefined;

    service['updateShape']();

    expect(rectangle.updateShape).not.toHaveBeenCalled();
    expect(preview.updateShape).not.toHaveBeenCalled();
  });

  it('updateShape should update the rectangle if it is defined', () => {
    service['rectangle'] = rectangle;

    service['updateShape']();

    expect(rectangle.updateShape).toHaveBeenCalled();
  });

  it('updateShape should not update the preview if it is undefined', () => {
    service['rectangle'] = rectangle;
    service['previewShape'] = undefined;

    service['updateShape']();

    expect(preview.updateShape).not.toHaveBeenCalled();
  });

  it('updateShape should not update the preview if shiftPressed is false', () => {
    service['rectangle'] = rectangle;
    service['shiftPressed'] = false;

    service['updateShape']();

    expect(preview.updateShape).not.toHaveBeenCalled();
  });

  it('updateShape should update the preview if it is defined and shiftPressed is true', () => {
    service['rectangle'] = rectangle;
    service['previewShape'] = preview;
    service['shiftPressed'] = true;

    service['updateShape']();

    expect(preview.updateShape).toHaveBeenCalled();
  });

  // construct
  it('construct rectangle should return a new Rectangle', () => {
    service = new RectangleService(rendererFactorySpy, svgServiceSpy, colorServiceSpy, commandInvokerSpy);
    expect(service['constructRectangle']()).toBeTruthy();
  });

  it('construct preview should return a new Rectangle', () => {
    service = new RectangleService(rendererFactorySpy, svgServiceSpy, colorServiceSpy, commandInvokerSpy);
    expect(service['constructPreview']()).toBeTruthy();
  });

  // create rectangle
  it('create rectangle should construct a new rectangle', () => {
    service['createRectangle']();
    expect(service['constructRectangle']).toHaveBeenCalled();
  });

  it('create rectangle should call the updtae shape of the rectangle', () => {
    service['createRectangle']();
    expect(rectangle.updateShape).toHaveBeenCalled();
  });

  it('create rectangle should send a new command', () => {
    service['createRectangle']();
    expect(commandInvokerSpy.execute).toHaveBeenCalled();
  });

  // create preview
  it('create preview should construct a new preview rectangle', () => {
    service['createPreview']();
    expect(service['constructPreview']).toHaveBeenCalled();
  });

  it('create preview should call the update shape of the preview', () => {
    service['createPreview']();
    expect(preview.updateShape).toHaveBeenCalled();
  });

  it('create preview should send the new preview to the svgChild', () => {
    service['createPreview']();
    expect(svgServiceSpy.addNewSVG).toHaveBeenCalled();
  });

});
