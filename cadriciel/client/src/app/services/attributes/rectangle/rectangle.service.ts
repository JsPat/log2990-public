import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { CreateShapeCommand } from 'src/app/components/app/attributes/command/create-shape-command';
import { Coordinate } from 'src/app/components/app/attributes/coordinate';
import { Rectangle } from 'src/app/components/app/attributes/rectangle/rectangle';
import { RectanglePreview } from 'src/app/components/app/attributes/rectangle/rectangle-preview';
import { BASIC_WIDTH, Tool } from 'src/app/components/app/attributes/tool';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { SVGChildService } from '../../svg-child/svg-child.service';

@Injectable({
  providedIn: 'root'
})
export class RectangleService extends Tool {

  borderEnabled: boolean = true;
  fillEnabled: boolean = true;

  private strokeWidth: number = BASIC_WIDTH;
  private renderer: Renderer2;
  private shiftPressed: boolean;
  private mousePosition: Coordinate;
  private rectangle: Rectangle | undefined;
  private previewShape: RectanglePreview | undefined;

  constructor( rendererFactory: RendererFactory2, private svgService: SVGChildService,
               private colorService: ColorService, private commandInvoker: CommandInvokerService) {
    super();
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  mouseDown(event: MouseEvent): void {
    if (event.button !== this.LEFT_CLICK) { return; }
    this.mousePosition = {x: event.offsetX, y: event.offsetY};

    this.createRectangle();
    this.createPreview();
  }

  mouseUp(): void {
    if (this.rectangle && this.previewShape) {
      if (this.rectangle.isNull()) {
        this.svgService.removeSVG(this.rectangle);
        this.commandInvoker.removeLastExecuted();
      }
      this.svgService.removeSVG(this.previewShape);
      this.rectangle = undefined;
      this.previewShape = undefined;
    }
  }

  mouseLeave(): void {
    this.mouseUp();
  }

  toolSwap(): void {
    this.mouseUp();
  }

  mouseDrag(event: MouseEvent): void {
    if (this.rectangle) {
      this.mousePosition = {x: event.offsetX, y: event.offsetY};
      this.updateShape();
    }
  }

  shiftDown(): void {
    this.shiftPressed = true;
    if (this.rectangle) {
      this.updateShape();
    }
  }

  shiftUp(): void {
    this.shiftPressed = false;
    if (this.rectangle) {
      if (this.previewShape) {
        this.updateShape();
        this.previewShape.makeInvisible();
      }
    }
  }

  private updateShape(): void {
    if (this.rectangle) {
      this.rectangle.updateShape( this.mousePosition, this.shiftPressed );

      if (this.shiftPressed && this.previewShape) {
        this.previewShape.updateShape( this.mousePosition, false );
      }
    }
  }

  private constructRectangle(): Rectangle {
    return new Rectangle(
      this.mousePosition,
      this.colorService.mainColor,
      this.strokeWidth,
      this.colorService.secondaryColor,
      this.borderEnabled,
      this.fillEnabled,
      this.renderer,
    );
  }

  private createRectangle(): void {
    this.rectangle = this.constructRectangle();
    this.rectangle.updateShape(this.mousePosition, this.shiftPressed);
    this.commandInvoker.execute(new CreateShapeCommand(this.rectangle, this.svgService));
  }

  private constructPreview(): RectanglePreview {
    return new RectanglePreview(this.mousePosition, this.renderer);
  }

  private createPreview(): void {
    this.previewShape = this.constructPreview();
    this.previewShape.updateShape(this.mousePosition, this.shiftPressed);
    this.svgService.addNewSVG(this.previewShape);
  }

}
