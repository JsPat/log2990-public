/* tslint:disable:no-unused-variable */
// tslint:disable: no-string-literal
// tslint:disable: no-any
// tslint:disable: no-magic-numbers

import { NO_ERRORS_SCHEMA, RendererFactory2 } from '@angular/core';
import { async, inject, TestBed } from '@angular/core/testing';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { Color } from 'src/app/color-palette/color/color';
import { Coordinate } from 'src/app/components/app/attributes/coordinate';
import { Pen } from 'src/app/components/app/attributes/pen/pen';
import { DrawSheetService } from '../../draw-sheet/draw-sheet.service';
import { ToolsService } from '../toolsService/tools-service';
import { PenService } from './pen.service';

// tslint:disable-next-line: max-classes-per-file
class DrawSheetServiceMock extends DrawSheetService {
  svgToString(): string {
    return '<svg></svg>';
  }
}

// tslint:disable-next-line: max-classes-per-file
class MockColor extends Color {
  constructor() {
      super(0, 0, 0);
  }
}

describe('PenService', () => {
  let service: PenService;

  let rendererSpy: any;
  let rendererFactorySpy: any;

  let color: Color;
  let mousePosition: Coordinate;

  beforeEach(async(() => {

    rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute', 'appendChild']);

    rendererFactorySpy = {
      createRenderer(_0: null, _1: null): any {
        return rendererSpy;
      }
    };

    TestBed.configureTestingModule({
      providers: [
        PenService,
        { provide: Color, useClass: MockColor},
        { provide: RendererFactory2, useValue: rendererFactorySpy}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  }));

  beforeEach(inject([PenService], (injected: PenService) => {

    service = injected;

    color = new MockColor();
    service['commandInvoker']['autoSave']['data'] = new DrawSheetServiceMock(new ToolsService(), new ColorService());

    mousePosition = {x: 1, y: 1};
    service['shape'] = new Pen(mousePosition, color, 5, rendererSpy);
  }));

  // mouseLeave
  it('mouseLeave should call mouseUp', () => {
    spyOn(service, 'mouseUp');
    service.mouseLeave();
    expect(service.mouseUp).toHaveBeenCalled();
  });

  // toolSwap
  it('toolSwap should call mouseUp', () => {
    spyOn(service, 'mouseUp');
    service.toolSwap();
    expect(service.mouseUp).toHaveBeenCalled();
  });

  // mouseUp
  it('mouseUp makes shapes undifined', () => {
    service.mouseUp();
    expect(service['shape']).not.toBeDefined();
  });

  // mouseDown
  it('mousedown should update the current position', () => {
    const event = new MouseEvent('click');
    service.mouseDown(event);
    let dots = '';
    if (service['shape'] !== undefined) {
      dots = service['shape']['dotsToWrite'];
    }
    expect(dots).toEqual(`${event.offsetX},${event.offsetY} ${event.offsetX},${event.offsetY} `);
  });

  it(' mouseDown should update the shapes attributes', () => {
    const event = new MouseEvent('click');
    const newColor = new MockColor();
    const newPosition = new Coordinate();
    newPosition.x = 100;
    newPosition.y = 100;
    service['shape'] = new Pen(newPosition, newColor, 25, rendererSpy);
    service.mouseDown(event);
    expect(service['shape']['strokeWidth']).toBe(service.strokeWidth);
  });

  it('mouseDown should do nothing if the click is not the left click', () => {
    const event = new MouseEvent('click');
    Object.defineProperty(event, 'button', {value: 'not_left_click'});
    const createNewLine = spyOn<any>(service, 'createNewLine');
    service.mouseDown(event);
    expect(createNewLine).not.toHaveBeenCalled();
  });

  // mouseDrag
  it('mousedrag should add new dot to the dot list', () => {
    const event = new MouseEvent('move');

    spyOnProperty(event, 'offsetX', 'get').and.returnValue(2);
    spyOnProperty(event, 'offsetY', 'get').and.returnValue(2);

    service['shape'] = new Pen({x: 1, y: 1}, color, 5, rendererSpy);
    spyOn(service['shape'], 'pushDot');
    service.mouseDrag(event);
    expect(service['shape'].pushDot).toHaveBeenCalledWith({x: 2, y: 2});
  });

  it('mousedrag should not add new dots if shape is undefined', () => {
    const event = new MouseEvent('move');
    service['shape'] = undefined;
    service.mouseDrag(event);
    expect(service['shape']).toBeUndefined();
  });

  it('creatNewLine should send a new command', () => {
    const newDots = new Coordinate();
    spyOn<any>(service['commandInvoker'], 'execute');

    service['createNewLine'](newDots);
    expect(service['commandInvoker'].execute).toHaveBeenCalled();
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });
});
