import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { CreateShapeCommand } from 'src/app/components/app/attributes/command/create-shape-command';
import { Coordinate } from 'src/app/components/app/attributes/coordinate';
import { Pen } from 'src/app/components/app/attributes/pen/pen';
import { BASIC_WIDTH, Tool } from 'src/app/components/app/attributes/tool';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { SVGChildService } from '../../svg-child/svg-child.service';

@Injectable({
  providedIn: 'root'
})
export class PenService extends Tool {

  strokeWidth: number = BASIC_WIDTH;

  protected shape: Pen | undefined;
  protected colorService: ColorService;
  protected renderer: Renderer2;
  protected svgService: SVGChildService;
  protected commandInvoker: CommandInvokerService;

  constructor(colorService: ColorService, rendererFactory: RendererFactory2,
              svgService: SVGChildService, commandInvoker: CommandInvokerService) {
    super();
    this.colorService = colorService;
    this.renderer = rendererFactory.createRenderer(null, null);
    this.svgService = svgService;
    this.commandInvoker = commandInvoker;
  }

  mouseDown(event: MouseEvent): void {
    if (event.button !== this.LEFT_CLICK) { return; }
    const mousePosition = {x: event.offsetX, y: event.offsetY};
    this.createNewLine(mousePosition);
  }

  mouseUp(): void {
    this.shape = undefined;
  }

  mouseLeave(): void {
    this.mouseUp();
  }

  toolSwap(): void {
    this.mouseUp();
  }

  mouseDrag(event: MouseEvent): void {
    if (this.shape) {
      const mousePosition = {x: event.offsetX, y: event.offsetY};
      this.shape.pushDot(mousePosition);
    }
  }

  protected createNewLine(mousePosition: Coordinate): void {
    this.shape = new Pen(
      mousePosition,
      this.colorService.mainColor,
      this.strokeWidth,
      this.renderer
    );
    this.shape.pushDot(mousePosition);
    this.commandInvoker.execute(new CreateShapeCommand(this.shape, this.svgService));
  }

}
