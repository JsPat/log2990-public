 /* tslint:disable: no-unused-variable
    tslint:disable: no-magic-numbers
    tslint:disable: no-any
    tslint:disable: no-string-literal
    tslint:disable: max-file-line-count */
 import { NO_ERRORS_SCHEMA, Renderer2, RendererFactory2 } from '@angular/core';
 import { async, inject, TestBed } from '@angular/core/testing';
 import { Color } from 'src/app/color-palette/color/color';
 import { Ellipse } from 'src/app/components/app/attributes/ellipse/ellipse';
 import { RectanglePreview } from 'src/app/components/app/attributes/rectangle/rectangle-preview';
 import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
 import { SVGChildService } from '../../svg-child/svg-child.service';
 import { EllipseService } from './ellipse.service';
 describe('EllipseService', () => {
   let service: EllipseService;
   let color: Color;
   let strokeColor: Color;
   let svgServiceSpy: SVGChildService;
   let commandInvokerSpy: CommandInvokerService;
   let rendererFactorySpy: any;
   let rendererSpy: any;
   let event: MockMouseEvent;
   let ellipse: any;
   class MockColor extends Color {
     constructor() {
         super(0, 0, 0);
     }
   }
   // tslint:disable-next-line: max-classes-per-file
   class MockMouseEvent extends MouseEvent {
     mockoffsetX: number;
     mockoffsetY: number;
     constructor() {
       super('mousedown');
     }
   }
   beforeEach(async(() => {

    rendererSpy = jasmine.createSpyObj('Renderer2', ['createElement', 'setAttribute', 'appendChild']);
    svgServiceSpy = jasmine.createSpyObj('SvgChildService', ['appendNewSvg', 'removeSVG', 'addNewSVG', 'appendSVGOnTop']);
    ellipse = jasmine.createSpyObj('Ellipse', ['updateShape', 'isNull']);
    commandInvokerSpy = jasmine.createSpyObj('CommandInvokerService', ['execute', 'removeLastExecuted']);

    rendererFactorySpy = {
       createRenderer(_0: null, _1: null): any {
         return rendererSpy;
       }
     };
    TestBed.configureTestingModule({
       providers: [
         EllipseService,
         Renderer2,
         { provide: Color, useClass: MockColor},
         { provide: MouseEvent, useClass: MockMouseEvent},
         { provide: RendererFactory2, useValue: rendererFactorySpy},
         { provide: SVGChildService, useValue: svgServiceSpy},
         { provide: CommandInvokerService, useValue: commandInvokerSpy}
       ],
       schemas: [NO_ERRORS_SCHEMA]
     });
   }));
   beforeEach(inject([EllipseService], (injected: EllipseService) => {
     service = injected;
     service['start'] = {x: 0, y: 0};
     service['mousePosition'] = {x: 0, y: 0};
     color = new MockColor();
     strokeColor = new MockColor();
     service['ellipse'] = ellipse;
     service['previewShape'] = new RectanglePreview(service['mousePosition'], rendererSpy);
     event = new MockMouseEvent();
   }));

   it('should create', () => {
    expect(service).toBeTruthy();
  });
  // createPreview()
   it('createPreview should add the preview to the svg', () => {
    service['createPreview']();
    expect(svgServiceSpy.addNewSVG).toHaveBeenCalled();
  });
   it('createPreview should reassign previewShape to a new Ellipse', () => {
    service['start'] = {x: 1, y: 2};
    service['svgService'] = svgServiceSpy;
    service['previewShape']['width'] = 3;
    service['previewShape']['height'] = 4;
    service['previewShape']['position'].x = 10;
    service['previewShape']['position'].y = 10;
    service['createPreview']();
    expect(service['previewShape']['position'].x).toEqual(service['start'].x);
    expect(service['previewShape']['position'].y).toEqual(service['start'].y);
    expect(service['previewShape']['width']).toEqual(service['previewShape']['width']);
    expect(service['previewShape']['height']).toEqual(service['previewShape']['height']);
  });
  // mouseDown()
   it('if event.button is not left click, updateShape is not called', () => {
    service['ellipse'] = new Ellipse({x: 0, y: 0}, 10, 10, color, 0, strokeColor, true, true, rendererSpy);
    spyOn(service['ellipse'], 'updateShape');
    spyOn<any>(service, 'createPreview').and.returnValue({});
    const evnt = new MouseEvent('click');
    // le 2 indique un rightClick
    evnt.initMouseEvent(
      'type',
      true,
      true,
      window,
      3,
      3,
      3,
      3,
      3,
      true,
      true,
      true,
      true,
      2,
      null
    );
    service.mouseDown(evnt);
    expect(service['ellipse'].updateShape).not.toHaveBeenCalled();
  });

   it('mouseDown should send a ellipse via commands', () => {
    spyOn<any>(service, 'createPreview').and.returnValue({});
    service.mouseDown(event);
    expect(commandInvokerSpy.execute).toHaveBeenCalled();
  });
   it('mouseDown calls updateShape and execute', () => {
    spyOn<any>(service, 'createPreview').and.returnValue({});
    service['ellipse'] =  new Ellipse({x: 0, y: 0}, 10, 10, color, 0, strokeColor, true, true, rendererSpy);
    spyOn(service['ellipse'], 'updateShape');
    service.mouseDown(event);
    expect(service['commandInvoker'].execute).toHaveBeenCalled();
  });
   it('mouseDown should reassign ellipse to a new Ellipse', () => {
    spyOn<any>(service, 'createPreview').and.returnValue({});
    service['ellipse'] = new Ellipse({x: 0, y: 0}, 10, 10, color, 0, strokeColor, true, true, rendererSpy);
    service.mouseDown(event);
    expect(service['previewShape']['position']).toEqual({x: event.offsetX, y: event.offsetY});
    expect(service['ellipse']['height']).toEqual(0);
    expect(service['ellipse']['width']).toEqual(0);
  });
  // mouseUp()
   it('mouseUp should (not remove the preview) if ellipse is undefined', () => {
    spyOn(service, 'mouseUp').and.callThrough();
    service['ellipse'] = undefined;
    service.mouseUp();
    expect(svgServiceSpy.removeSVG).not.toHaveBeenCalled();
  });
   it('mouseUp should assigned ellipse and remove the preview if ellipse is defined', () => {
    spyOn(service, 'mouseUp').and.callThrough();
    service.mouseUp();
    expect(svgServiceSpy.removeSVG).toHaveBeenCalled();
    expect(service['ellipse']).toBeUndefined();
  });
   it('mouseUp should remove the ellipse if the ellipse is null', () => {
    ellipse.isNull.and.returnValue(true);
    service['ellipse'] = ellipse;
    service.mouseUp();
    expect(commandInvokerSpy.removeLastExecuted).toHaveBeenCalled();
    expect(svgServiceSpy.removeSVG).toHaveBeenCalledWith(ellipse);
  });

   it('mouseUp should not remove the ellipse if the ellipse is not null', () => {
     ellipse.isNull.and.returnValue(false);
     service['ellipse'] = ellipse;
     service.mouseUp();
     expect(commandInvokerSpy.removeLastExecuted).not.toHaveBeenCalled();
     expect(svgServiceSpy.removeSVG).not.toHaveBeenCalledWith(ellipse);
   });
   // mouseLeave()
   it('mouseLeave should call mouseUp', () => {
     spyOn(service, 'mouseLeave').and.callThrough();
     spyOn(service, 'mouseUp');
     service.mouseLeave();
     expect(service.mouseUp).toHaveBeenCalled();
   });
   // toolSwap()
   it('toolSwap should call mouseUp if ellipse is defined', () => {
     spyOn(service, 'toolSwap').and.callThrough();
     spyOn(service, 'mouseUp');
     service.toolSwap();
     expect(service.mouseUp).toHaveBeenCalled();
   });
   // mouseDrag()
   it('mouseDrag should do nothing (not call updateShape or change attributes) if ellipse is undefined', () => {
     spyOn(service, 'mouseDrag').and.callThrough();
     service['ellipse'] = undefined;
     service.mouseDrag(event);
     expect(ellipse.updateShape).not.toHaveBeenCalled();
   });
   it('mouseDrag should always return void', () => {
     const result = service.mouseDrag(event);
     expect(result).toBeUndefined();
   });
   it('mouseDrag should call updateShape and change mousePosition if ellipse is defined', () => {
     spyOn(service, 'mouseDrag').and.callThrough();
     service['mousePosition'] = {x: 2, y: 2};
     service.mouseDrag(event);
     expect(ellipse.updateShape).toHaveBeenCalled();
     // the event.offset will set the currentposition to {0,0}
     // because event.offset is readonly and can't be set to another value for testing
     expect(service['mousePosition']).toEqual({x: event.offsetX, y: event.offsetY});
   });
   it('mouseDrag should call updateShape and change mousePosition if ellipse is defined and shiftPressed=true', () => {
     spyOn(service['previewShape'], 'updateShape').and.callThrough();
     // ['mousePosition'] is different than {0,0}
     service['mousePosition'] = {x: 2, y: 2};
     service['shiftPressed'] = true;
     service.mouseDrag(event);
     expect(service['previewShape'].updateShape).toHaveBeenCalled();
     // if only to make possible undefined error disappear (but will never be undefined)
     if (service['ellipse']) {
       expect(ellipse.updateShape).toHaveBeenCalled();
     }
     // the event.offset will set the currentposition to {0,0}
     // because event.offset is readonly and can't be set to another value for testing
     expect(service['mousePosition']).toEqual({x: event.offsetX, y: event.offsetY});
   });
   // shiftDown()
   it('shiftDown should only assign shiftPressed to true (not call updateShape) if ellipse is undefined', () => {
     spyOn(service, 'shiftDown').and.callThrough();
     service['ellipse'] = undefined;
     service.shiftDown();
     expect(service['shiftPressed']).toBeTruthy();
     expect(ellipse.updateShape).not.toHaveBeenCalled();
   });

   it('shiftDown should only assign shiftPressed to true (not call updateShape) if ellipse is undefined', () => {
    spyOn(service, 'shiftDown').and.callThrough();
    service['ellipse'] = undefined;
    service.shiftDown();
    expect(service['shiftPressed']).toBeTruthy();
    // if only to make possible undefined error disappear (but will never be undefined)
    expect(ellipse.updateShape).not.toHaveBeenCalled();
  });

   it('shiftDown should only assign shiftPressed to true (not call updateShape) if ellipse is undefined', () => {
    spyOn(service, 'shiftDown').and.callThrough();
    service['ellipse'] = undefined;
    service.shiftDown();
    expect(service['shiftPressed']).toBeTruthy();
    expect(ellipse.updateShape).not.toHaveBeenCalled();
  });

   it('shiftDown should call updateShape if ellipse is defined', () => {
    spyOn(service, 'shiftDown').and.callThrough();
    spyOn(service['previewShape'], 'makeInvisible');
    service.shiftDown();
    expect(service['shiftPressed']).toBeTruthy();
    // if only to make possible undefined error disappear (but will never be undefined)
    if (service['ellipse']) {
      expect(service['ellipse'].updateShape).toHaveBeenCalled();
    }
  });

   it('shiftUp should only assign shiftPressed to false (not call updateShape) if ellipse is undefined', () => {
    spyOn(service, 'shiftUp').and.callThrough();
    service['ellipse'] = undefined;
    service.shiftUp();
    expect(service['shiftPressed']).toBeFalsy();
    expect(ellipse.updateShape).not.toHaveBeenCalled();
  });

   it('shiftUp should only assign shiftPressed to false (not call updateShape) if ellipse is defined', () => {
    service['ellipse'] = undefined;
    service.shiftUp();
    expect(service['shiftPressed']).toBeFalsy();
    expect(ellipse.updateShape).not.toHaveBeenCalled();
  });
   it('shiftUp should call updateShape if ellipse is defined', () => {
    service['ellipse'] = new Ellipse({x: 0, y: 0}, 10, 10, color, 0, strokeColor, true, true, rendererSpy);
    spyOn(service['ellipse'], 'updateShape');
    service.shiftUp();
    expect(service['ellipse'].updateShape).toHaveBeenCalled();
  });
});
