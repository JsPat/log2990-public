import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { CreateShapeCommand } from 'src/app/components/app/attributes/command/create-shape-command';
import { Coordinate } from 'src/app/components/app/attributes/coordinate';
import { Ellipse } from 'src/app/components/app/attributes/ellipse/ellipse';
import { RectanglePreview } from 'src/app/components/app/attributes/rectangle/rectangle-preview';
import { BASIC_WIDTH, Tool } from 'src/app/components/app/attributes/tool';
import { CommandInvokerService } from '../../command-invoker/command-invoker.service';
import { SVGChildService } from '../../svg-child/svg-child.service';

@Injectable({
  providedIn: 'root'
})
export class EllipseService extends Tool {

  borderEnabled: boolean = true;
  fillEnabled: boolean = true;

  private strokeWidth: number = BASIC_WIDTH;
  private shiftPressed: boolean;
  private mousePosition: Coordinate;
  private start: Coordinate;
  private ellipse: Ellipse | undefined;
  private previewShape: RectanglePreview;
  private renderer: Renderer2;

  constructor(rendererFactory: RendererFactory2, private colorService: ColorService,
              private svgService: SVGChildService, private commandInvoker: CommandInvokerService) {
    super();
    this.renderer = rendererFactory.createRenderer(null, null);
    this.shiftPressed = false;
    this.start = {x: 0, y: 0};
  }

  mouseDown(event: MouseEvent): void {
    if (event.button !== this.LEFT_CLICK) { return; }
    this.mousePosition = {x: event.offsetX, y: event.offsetY};
    this.start = this.mousePosition;
    this.ellipse = new Ellipse(
      this.start,
      0,
      0,
      this.colorService.mainColor,
      this.strokeWidth,
      this.colorService.secondaryColor,
      this.borderEnabled,
      this.fillEnabled,
      this.renderer
    );
    this.ellipse.updateShape(this.mousePosition, this.shiftPressed);
    this.createPreview();
    this.previewShape.updateShape(this.mousePosition, false);
    this.commandInvoker.execute(new CreateShapeCommand(this.ellipse, this.svgService));
  }

  mouseUp(): void {
    if (this.ellipse) {
      if (this.ellipse.isNull()) {
        this.svgService.removeSVG(this.ellipse);
        this.commandInvoker.removeLastExecuted();
      }
      this.ellipse = undefined;
      this.svgService.removeSVG(this.previewShape);
    }
  }

  mouseLeave(): void {
    this.mouseUp();
  }

  toolSwap(): void {
    this.mouseUp();
  }

  mouseDrag(event: MouseEvent): void {
    if (this.ellipse) {
      this.mousePosition = {x: event.offsetX, y: event.offsetY};
      this.ellipse.updateShape(this.mousePosition, this.shiftPressed);
      this.previewShape.updateShape(this.mousePosition, false);
    }
  }

  shiftDown(): void {
    this.shiftPressed = true;
    if (this.ellipse) {
        this.ellipse.updateShape(this.mousePosition, this.shiftPressed);
        this.previewShape.updateShape(this.mousePosition, false);
    }
  }

  shiftUp(): void {
    this.shiftPressed = false;
    if (this.ellipse) {
        this.ellipse.updateShape(this.mousePosition, this.shiftPressed);
    }
  }

  private createPreview(): void {
    this.previewShape = new RectanglePreview(this.start, this.renderer);
    this.previewShape.updateShape(this.mousePosition, false);
    this.svgService.addNewSVG(this.previewShape);
  }
}
