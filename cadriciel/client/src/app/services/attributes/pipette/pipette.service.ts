import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { Color } from 'src/app/color-palette/color/color';
import { Tool } from 'src/app/components/app/attributes/tool';
import * as JSON from 'src/app/components/app/export/export.json';
import { DrawSheetService } from '../../draw-sheet/draw-sheet.service';

const config = JSON.default;

@Injectable({
  providedIn: 'root'
})
export class PipetteService extends Tool {

  private color: Color = new Color(0, 0, 0, 0);
  private context: CanvasRenderingContext2D | null;
  private image: HTMLImageElement;
  private canvas: HTMLCanvasElement;
  private renderer: Renderer2;
  private event: MouseEvent;

  private imageOnLoad = () => {
    if (this.context) {
      this.context.drawImage(this.image, 0, 0);
      const pixelColor = this.context.getImageData(this.event.offsetX, this.event.offsetY, 1, 1).data;
      this.color =  new Color (pixelColor[0], pixelColor[1], pixelColor[2]);
      this.setColor(this.event);
    }
  }

  constructor(rendererFactory: RendererFactory2, private colorService: ColorService, private data: DrawSheetService) {
    super();
    this.renderer = rendererFactory.createRenderer(null, null);
    this.canvas = this.renderer.createElement(config.canvas);
  }

  mouseDown(event: MouseEvent): void {
    this.createPixelAndSendColorOfIT(event);
  }

  private setColor(event: MouseEvent): void {
    switch (event.button) {
      case 0: // left click
        this.colorService.sendBaseColor(this.color);
        this.colorService.setMainFromCurrentColor();
        break;
      case 2: // rigth click
        this.colorService.sendBaseColor(this.color);
        this.colorService.setSecondaryFromCurrentColor();
        break;
      default:
        break;
    }
  }

  private createPixelAndSendColorOfIT(event: MouseEvent): void {
    const svgString = this.data.svgToString();

    this.context = this.canvas.getContext(config.twoDimensions);

    this.canvas.width = this.data.dataDrawSheet.width;
    this.canvas.height = this.data.dataDrawSheet.height;

    const blop = new Blob([svgString], {type: config.type});
    const url = window.URL.createObjectURL(blop);
    this.image = new Image();
    this.image.src = url;

    this.event = event;
    this.image.onload = this.imageOnLoad;
  }
}
