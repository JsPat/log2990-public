// tslint:disable: no-any
// tslint:disable: no-string-literal
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, inject, TestBed } from '@angular/core/testing';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { DrawSheetService } from '../../draw-sheet/draw-sheet.service';
import { ToolsService } from '../toolsService/tools-service';
import { PipetteService } from './pipette.service';

// tslint:disable-next-line: max-classes-per-file
class DrawSheetServiceMock extends DrawSheetService {

  svgToString(): string {
    return '<svg></svg>';
  }
}

describe('Service: Pipette', () => {

  let service: PipetteService;

  let drawSheetDataSpy: DrawSheetServiceMock;
  let toolsServiceSpy: ToolsService;
  let colorServiceSpy: ColorService;
  const mockImageData = 'Data';
  const mockData = {
    data: mockImageData
  } as unknown as ImageData;
  const mockContext = {
    drawImage: () => { // simulating drawImage function
    },
    getImageData: () => { // simulating drawImage function
    },
  } as unknown as CanvasRenderingContext2D;

  beforeEach(async(() => {
    toolsServiceSpy = new ToolsService();
    colorServiceSpy = jasmine.createSpyObj('ColorService', ['sendBaseColor', 'setMainFromCurrentColor', 'setSecondaryFromCurrentColor']);
    drawSheetDataSpy = new DrawSheetServiceMock(toolsServiceSpy, colorServiceSpy);

    TestBed.configureTestingModule({
      providers: [
        PipetteService,
          { provide: ToolsService, useValue: toolsServiceSpy },
          { provide: DrawSheetService, useValue: drawSheetDataSpy},
          { provide: ColorService, useValue: colorServiceSpy},
        ],
      schemas: [NO_ERRORS_SCHEMA]
  });
}));

  beforeEach(inject([PipetteService], (injected: PipetteService) => {
    service = injected;
  }));

  it('should create', () => {
     expect(service).toBeTruthy();
  });

  it('mousedown should call setColor', () => {
    const mouseEventInit: MouseEventInit = {
      screenX: 0,
      screenY: 0,
      clientX: 0,
      clientY: 0,
      ctrlKey: false,
      shiftKey: false,
      altKey: false,
      metaKey: false,
      button: 1
    };
    const event = new MouseEvent('mouseDown', mouseEventInit);
    spyOn<any>(service, 'createPixelAndSendColorOfIT');
    service.mouseDown(event);

    expect(service['createPixelAndSendColorOfIT']).toHaveBeenCalled();
  });

  it('setColor with left clic should call setMainFromCurrentColor', () => {
    const mouseEventInit: MouseEventInit = {
      screenX: 0,
      screenY: 0,
      clientX: 0,
      clientY: 0,
      ctrlKey: false,
      shiftKey: false,
      altKey: false,
      metaKey: false,
      button: 0
    };

    const event = new MouseEvent('left click', mouseEventInit);
    spyOn<any>(service, 'setColor').and.callThrough();
    spyOn<any>(service, 'createPixelAndSendColorOfIT');

    service['setColor'](event);

    expect(colorServiceSpy.sendBaseColor).toHaveBeenCalled();
    expect(colorServiceSpy.setMainFromCurrentColor).toHaveBeenCalled();
  });

  it('setColor with right clic should call setSecondaryFromCurrentColor', () => {
    const mouseEventInit: MouseEventInit = {
      screenX: 0,
      screenY: 0,
      clientX: 0,
      clientY: 0,
      ctrlKey: false,
      shiftKey: false,
      altKey: false,
      metaKey: false,
      button: 2
    };

    const event = new MouseEvent('right click', mouseEventInit);
    spyOn<any>(service, 'createPixelAndSendColorOfIT');

    service['setColor'](event);

    expect(colorServiceSpy.sendBaseColor).toHaveBeenCalled();
    expect(colorServiceSpy.setSecondaryFromCurrentColor).toHaveBeenCalled();
  });

  it('setColor with not rigth or left clic should not call setSecondaryFromCurrentColor or setMainFromCurrentColor', () => {
    const mouseEventInit: MouseEventInit = {
      screenX: 0,
      screenY: 0,
      clientX: 0,
      clientY: 0,
      ctrlKey: false,
      shiftKey: false,
      altKey: false,
      metaKey: false,
      button: 1
    };
    const event = new MouseEvent('not rigth or left click', mouseEventInit);
    spyOn<any>(service, 'createPixelAndSendColorOfIT');
    service['setColor'](event);

    expect(colorServiceSpy.setMainFromCurrentColor).not.toHaveBeenCalled();
    expect(colorServiceSpy.setSecondaryFromCurrentColor).not.toHaveBeenCalled();
  });

  it('createPixelAndSendColorOfIT should call getContext', () => {
    const mouseEventInit: MouseEventInit = {
      screenX: 0,
      screenY: 0,
      clientX: 0,
      clientY: 0,
      ctrlKey: false,
      shiftKey: false,
      altKey: false,
      metaKey: false,
      button: 1
    };
    const event = new MouseEvent('mouseDown', mouseEventInit);
    spyOn(service['canvas'], 'getContext').and.returnValue(mockContext);
    service['createPixelAndSendColorOfIT'](event);

    expect(service['canvas'].getContext).toHaveBeenCalled();
  });

  it('imageOnLoad should call drawImage, getImageData', () => {
    const mouseEventInit: MouseEventInit = {
      screenX: 0,
      screenY: 0,
      clientX: 0,
      clientY: 0,
      ctrlKey: false,
      shiftKey: false,
      altKey: false,
      metaKey: false,
      button: 1
    };
    const event = new MouseEvent('mouseDown', mouseEventInit);
    service['context'] = mockContext;
    spyOn<any>(service['context'], 'drawImage');
    spyOn<any>(service['context'], 'getImageData').and.returnValue(mockData);
    spyOn<any>(service, 'setColor');

    service['event'] = event;
    service['imageOnLoad']();

    expect(service['context'].drawImage).toHaveBeenCalled();
    expect(service['context'].getImageData).toHaveBeenCalled();
    expect(service['setColor']).toHaveBeenCalled();
  });

  it('imageOnLoad should not call drawImage, getImageData if context is null', () => {
    const mouseEventInit: MouseEventInit = {
      screenX: 0,
      screenY: 0,
      clientX: 0,
      clientY: 0,
      ctrlKey: false,
      shiftKey: false,
      altKey: false,
      metaKey: false,
      button: 1
    };
    const event = new MouseEvent('mouseDown', mouseEventInit);
    service['context'] = null;
    spyOn<any>(service, 'setColor');

    service['event'] = event;
    service['imageOnLoad']();

    expect(service['setColor']).toHaveBeenCalledTimes(0);
  });

});
