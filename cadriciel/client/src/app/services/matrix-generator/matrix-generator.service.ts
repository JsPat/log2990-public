import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { Coordinate } from 'src/app/components/app/attributes/coordinate';

const SVG_ELEMENT: string = 'svg';

@Injectable({
  providedIn: 'root'
})
export class MatrixGeneratorService {

  private renderer: Renderer2;
  private svgRef: SVGSVGElement;

  constructor(rendererFactory: RendererFactory2) {
    this.renderer = rendererFactory.createRenderer(null, null);
    this.svgRef = this.renderer.createElement(SVG_ELEMENT, SVG_ELEMENT) as SVGSVGElement;
  }

  // Creates a translation matrix that gets something at pointA to pointB
  createTranslationMatrixFromPoints(pointA: Coordinate, pointB: Coordinate): SVGTransform {
    const translationMatrix = this.svgRef.createSVGTransform();
    translationMatrix.setTranslate(pointB.x - pointA.x, pointB.y - pointA.y);
    return translationMatrix;
  }

  // Creates a translation matrix based on the passed x&y variations
  createTranslationMatrixFromVariation(xVar: number, yVar: number): SVGTransform {
    const translationMatrix = this.svgRef.createSVGTransform();
    translationMatrix.setTranslate(xVar, yVar);
    return translationMatrix;
  }

  createRotationMatrix(angle: number, centerOfRotation: Coordinate): SVGTransform {
    const rotationMatrix = this.svgRef.createSVGTransform();
    rotationMatrix.setRotate(angle, centerOfRotation.x, centerOfRotation.y);
    return rotationMatrix;
  }
}
