/* tslint:disable:no-unused-variable */
/* tslint:disable:no-magic-numbers*/

import { async, inject, TestBed } from '@angular/core/testing';
import { MatrixGeneratorService } from './matrix-generator.service';

let service: MatrixGeneratorService;
describe('Service: MatrixGenerator', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [MatrixGeneratorService]
    });
  }));
  beforeEach(inject([MatrixGeneratorService], (injected: MatrixGeneratorService) => {
    service = injected;
  }));

  it('should create', () => {
    expect(service).toBeTruthy();
  });
  it('should create a translation matrix of 0,0 if passed the same point twice', () => {
    const mockPoint = {x: 5, y: 5};
    const matrix = service.createTranslationMatrixFromPoints(mockPoint, mockPoint);
    expect(matrix.matrix.e).toEqual(0);
    expect(matrix.matrix.f).toEqual(0);
  });
  it('should create a translation matrix representing the difference in distance between the two points', () => {
    const mockPoint = {x: 5, y: 5};
    const mockPoint2 = {x: 7, y: 3};
    const matrix = service.createTranslationMatrixFromPoints(mockPoint, mockPoint2);
    expect(matrix.matrix.e).toEqual(mockPoint2.x - mockPoint.x);
    expect(matrix.matrix.f).toEqual(mockPoint2.y - mockPoint.y);
  });
  it('should create a translation matrix representing an x&y variation', () => {
    const xVar = 5;
    const yVar = 62;
    const matrix = service.createTranslationMatrixFromVariation(xVar, yVar);
    expect(matrix.matrix.e).toEqual(xVar);
    expect(matrix.matrix.f).toEqual(yVar);
  });
  it('should create a rotation matrix representing a rotation around a given point', () => {
    const angle = 90;
    const center = {x: 0, y: 0};
    const matrix = service.createRotationMatrix(angle, center);
    expect(matrix.matrix.b).toEqual(1);
    expect(matrix.matrix.c).toEqual(-1);
  });
});
