/* tslint:disable:no-unused-variable */
// tslint:disable: no-string-literal
import { inject, TestBed } from '@angular/core/testing';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { ToolsService } from '../attributes/toolsService/tools-service';
import { DrawSheetService } from '../draw-sheet/draw-sheet.service';
import { AutoSaveService } from './auto-save.service';

// tslint:disable-next-line: max-classes-per-file
class DrawSheetServiceMock extends DrawSheetService {
  svgToString(): string {
    return '<svg></svg>';
  }
}

describe('Service: AutoSave', () => {

  let service: AutoSaveService;

  beforeEach(() => {

    TestBed.configureTestingModule({
      providers: [AutoSaveService]
    });
  });

  beforeEach(inject([AutoSaveService], (injected: AutoSaveService) => {

    service = injected;
    service['data'] = new DrawSheetServiceMock(new ToolsService(), new ColorService());
  }));

  it('should ...', inject([AutoSaveService], (autoSaveService: AutoSaveService) => {
    expect(autoSaveService).toBeTruthy();
  }));

  it('storedItem should return localStorage content', () => {
    expect(service.storedItem).toBe(service['storedItem']);
  });

  it('save should call localStorage.set', () => {
    spyOn(service['localStorage'], 'set');
    service.save();
    expect(service['localStorage'].set).toHaveBeenCalled();
  });

});
