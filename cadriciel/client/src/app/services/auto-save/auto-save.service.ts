import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { DrawSheetService } from '../draw-sheet/draw-sheet.service';

const KEY: string = 'svg';

@Injectable({
  providedIn: 'root'
})
export class AutoSaveService {

  constructor(@Inject(LOCAL_STORAGE) private localStorage: StorageService, private data: DrawSheetService) { }

  get storedItem(): string {
    return this.localStorage.get(KEY);
  }

  save(): void {
    this.localStorage.set(KEY, this.data.svgToString());
  }
}
