/* tslint:disable:no-unused-variable */

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material';
import { Observable, of } from 'rxjs';
import { Drawing } from '../../../../../common/drawing';
import { HandleRequestErrorService } from '../handle-request-error/handle-request-error.service';
import { DatabaseDrawingsService } from './database-drawings.service';

// tslint:disable-next-line: max-classes-per-file
class HTMLInputElementMock {
  value: string;
}

// tslint:disable-next-line: max-classes-per-file
class HandleRequestErrorServiceMock extends HandleRequestErrorService {
  handleError<T>(errorMessage: string, result?: T): (error: Error) => Observable<T> {
    return (error: Error): Observable<T> => {
      return of(result as T);
    };
  }
}

describe('DatabaseDrawingsService', () => {
  const BASE_URL: string = 'http://localhost:3000/api/database/drawings';
  const JOIN_CHARACTER: string = '-';
  const ROUTING_DELETE: string = '/delete/';
  const ROUTING_GET: string = '/get/';

  let httpMock: HttpTestingController;
  let databaseDrawingsService: DatabaseDrawingsService;
  // tslint:disable-next-line: prefer-const because can't initialize HTMLInputElementMock if it's const
  let hTMLInputElementMock: HTMLInputElementMock;
  let matSnackBarMock: MatSnackBar;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        DatabaseDrawingsService,
        { provide: MatSnackBar, useValue: matSnackBarMock },
        { provide: HandleRequestErrorService, useClass: HandleRequestErrorServiceMock },
        { provide: HTMLInputElement, useValue: hTMLInputElementMock},
      ]
    });
    databaseDrawingsService = TestBed.get(DatabaseDrawingsService);
    httpMock = TestBed.get(HttpTestingController);
    matSnackBarMock = jasmine.createSpyObj('MatSnackBar', ['']);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should inject the service', inject([DatabaseDrawingsService], (service: DatabaseDrawingsService) => {
    expect(service).toBeTruthy();
  }));

  // getCurrentDrawing
  it('should return expected current drawing (HttpClient called once)', () => {
    const expectedDrawing: Drawing = new Drawing('0', '<svg>');
    expectedDrawing.name = 'expectedDrawing';
    expectedDrawing.tags = new Array<string>('a', 'b');
    // check the content of the mocked call
    databaseDrawingsService.getCurrentDrawing(expectedDrawing).subscribe((response: Drawing) => {
      expect(response.id).toEqual(expectedDrawing.id, 'id check');
      expect(response.name).toEqual(expectedDrawing.name, 'name check');
      expect(response.tags).toEqual(expectedDrawing.tags, 'tags check');
      expect(response.svgContent).toEqual(expectedDrawing.svgContent, 'svgContent check');
    }, fail);
    // tslint:disable-next-line: no-string-literal
    const testURL = BASE_URL + ROUTING_GET + expectedDrawing.id;
    const req = httpMock.expectOne(testURL);
    expect(req.request.method).toBe('GET');
    req.flush(expectedDrawing);
  });

  // getDrawings
  it('should return only drawings containing the tag', () => {
    const expectedDrawing1: Drawing = new Drawing('0', '<svg>');
    expectedDrawing1.tags = new Array<string>('a');
    const expectedDrawing2: Drawing = new Drawing('1', '<svg>');
    expectedDrawing2.tags = new Array<string>('a', 'b');
    const expectedDrawing3: Drawing = new Drawing('2', '<svg>');
    expectedDrawing3.tags = new Array<string>('b');
    const tags = new Array<string>('a');
    const expectedDrawings = new Array<Drawing>(expectedDrawing1, expectedDrawing2);
    // check the content of the mocked call
    databaseDrawingsService.getDrawings(tags).subscribe((response: Drawing[]) => {
      expect(response).toEqual(expectedDrawings);
    }, fail);
    // tslint:disable-next-line: no-string-literal
    const testURL = BASE_URL + '/' + tags.join(JOIN_CHARACTER);
    const req = httpMock.expectOne(testURL);
    expect(req.request.method).toBe('GET');
    req.flush(expectedDrawings);
  });

  it('should return no drawings if no one contain the tags', () => {
    const expectedDrawing1: Drawing = new Drawing('0', '<svg>');
    expectedDrawing1.tags = new Array<string>('a');
    const expectedDrawing2: Drawing = new Drawing('1', '<svg>');
    expectedDrawing2.tags = new Array<string>('a', 'b');
    const expectedDrawing3: Drawing = new Drawing('2', '<svg>');
    expectedDrawing3.tags = new Array<string>('b');
    const tags = new Array<string>('wrongTag', 'wrong');
    const expectedDrawings = new Array<Drawing>();
    // check the content of the mocked call
    databaseDrawingsService.getDrawings(tags).subscribe((response: Drawing[]) => {
      expect(response).toEqual(expectedDrawings);
    }, fail);
    // tslint:disable-next-line: no-string-literal
    const testURL = BASE_URL + '/' + tags.join(JOIN_CHARACTER);
    const req = httpMock.expectOne(testURL);
    expect(req.request.method).toBe('GET');
    req.flush(expectedDrawings);
  });

  it('should return drawings containg at least one of the tags', () => {
    const expectedDrawing1: Drawing = new Drawing('0', '<svg>');
    expectedDrawing1.tags = new Array<string>('a');
    const expectedDrawing2: Drawing = new Drawing('1', '<svg>');
    expectedDrawing2.tags = new Array<string>('a', 'b');
    const expectedDrawing3: Drawing = new Drawing('2', '<svg>');
    expectedDrawing3.tags = new Array<string>('b');
    const tags = new Array<string>('a', 'b');
    const expectedDrawings = new Array<Drawing>(expectedDrawing1, expectedDrawing2, expectedDrawing3);
    // check the content of the mocked call
    databaseDrawingsService.getDrawings(tags).subscribe((response: Drawing[]) => {
      expect(response).toEqual(expectedDrawings);
    }, fail);
    // tslint:disable-next-line: no-string-literal
    const testURL = BASE_URL + '/' + tags.join(JOIN_CHARACTER);
    const req = httpMock.expectOne(testURL);
    expect(req.request.method).toBe('GET');
    req.flush(expectedDrawings);
  });

  it('should return all drawings if no tags', () => {
    const expectedDrawing1: Drawing = new Drawing('0', '<svg>');
    expectedDrawing1.tags = new Array<string>('a');
    const expectedDrawing2: Drawing = new Drawing('1', '<svg>');
    expectedDrawing2.tags = new Array<string>('a', 'b');
    const expectedDrawing3: Drawing = new Drawing('2', '<svg>');
    expectedDrawing3.tags = new Array<string>('b');
    const tags = new Array<string>();
    const expectedDrawings = new Array<Drawing>(expectedDrawing1, expectedDrawing2, expectedDrawing3);
    // check the content of the mocked call
    databaseDrawingsService.getDrawings(tags).subscribe((response: Drawing[]) => {
      expect(response).toEqual(expectedDrawings);
    }, fail);
    // tslint:disable-next-line: no-string-literal
    const testURL = BASE_URL;
    const req = httpMock.expectOne(testURL);
    expect(req.request.method).toBe('GET');
    req.flush(expectedDrawings);
  });

  it('should not return drawings not containg at least one of the tags', () => {
    const expectedDrawing1: Drawing = new Drawing('0', '<svg>');
    expectedDrawing1.tags = new Array<string>('a');
    const expectedDrawing2: Drawing = new Drawing('1', '<svg>');
    expectedDrawing2.tags = new Array<string>('a', 'b');
    const expectedDrawing3: Drawing = new Drawing('2', '<svg>');
    expectedDrawing3.tags = new Array<string>('b');
    const tags = new Array<string>('a');
    const expectedDrawings = new Array<Drawing>(expectedDrawing1, expectedDrawing2);
    // check the content of the mocked call
    databaseDrawingsService.getDrawings(tags).subscribe((response: Drawing[]) => {
      expect(response).not.toContain(expectedDrawing3);
    }, fail);
    // tslint:disable-next-line: no-string-literal
    const testURL = BASE_URL + '/' + tags.join(JOIN_CHARACTER);
    const req = httpMock.expectOne(testURL);
    expect(req.request.method).toBe('GET');
    req.flush(expectedDrawings);
  });

  // addDrawing
  it('should return expected current drawing and POST (HttpClient called once)', () => {
    const expectedDrawing: Drawing = new Drawing('0', '<svg>');
    expectedDrawing.name = 'expectedDrawing';
    expectedDrawing.tags = new Array<string>('a', 'b');
    // check the content of the mocked call
    databaseDrawingsService.addDrawing(expectedDrawing).subscribe((response: Drawing) => {
      expect(response.id).toEqual(expectedDrawing.id, 'id check');
      expect(response.name).toEqual(expectedDrawing.name, 'name check');
      expect(response.tags).toEqual(expectedDrawing.tags, 'tags check');
      expect(response.svgContent).toEqual(expectedDrawing.svgContent, 'svgContent check');
    }, fail);
    // tslint:disable-next-line: no-string-literal
    const testURL = BASE_URL;
    const req = httpMock.expectOne(testURL);
    expect(req.request.method).toBe('POST');
    req.flush(expectedDrawing);
  });

  // deleteDrawing
  it('should return expected current drawing and DELETE (HttpClient called once)', () => {
    const expectedDrawing: Drawing = new Drawing('0', '<svg>');
    expectedDrawing.name = 'expectedDrawing';
    expectedDrawing.tags = new Array<string>('a', 'b');
    // check the content of the mocked call
    databaseDrawingsService.deleteDrawing(expectedDrawing).subscribe((response: Drawing) => {
      expect(response.id).toEqual(expectedDrawing.id, 'id check');
      expect(response.name).toEqual(expectedDrawing.name, 'name check');
      expect(response.tags).toEqual(expectedDrawing.tags, 'tags check');
      expect(response.svgContent).toEqual(expectedDrawing.svgContent, 'svgContent check');
    }, fail);
    // tslint:disable-next-line: no-string-literal
    const testURL = BASE_URL + ROUTING_DELETE + expectedDrawing.id;
    const req = httpMock.expectOne(testURL);
    expect(req.request.method).toBe('DELETE');
    req.flush(expectedDrawing);
  });

  // addTag
  it('addTag() should call tags.push if validateOneTag and event.input.value !== empty string and event.value is defined', () => {
    const eventInput = new HTMLInputElementMock();
    eventInput.value = 'eventInputvalue';
    const eventValue = 'eventValue';
    // tslint:disable-next-line: no-any
    const event: any = {input: eventInput, value: eventValue};
    const tags = new Array<string>('a', 'b');
    spyOn(databaseDrawingsService, 'addTag').and.callThrough();
    spyOn(tags, 'push');
    databaseDrawingsService.addTag(event, tags);
    expect(tags.push).toHaveBeenCalled();
  });

  it('addTag() should not call tags.push if not validateOneTag', () => {
    const eventInput = new HTMLInputElementMock();
    eventInput.value = 'wrongTag$$$$';
    const eventValue = 'wrongTag$$$$';
    // tslint:disable-next-line: no-any
    const event: any = {input: eventInput, value: eventValue};
    const tags = new Array<string>('a', 'b');
    spyOn(databaseDrawingsService, 'addTag').and.callThrough();
    spyOn(tags, 'push');
    databaseDrawingsService.addTag(event, tags);
    expect(tags.push).not.toHaveBeenCalled();
  });

  // removeTagFilter
  it('removeTagFilter() should call tags.splice if index == 0 && length > 0 && index < length ', () => {
    const tag = 'a';
    const tags = new Array<string>('a', 'b', 'c');
    spyOn(databaseDrawingsService, 'removeTag').and.callThrough();
    // tslint:disable-next-line: no-any
    spyOn<any>(tags, 'splice');
    databaseDrawingsService.removeTag(tags, tag);
    // tslint:disable-next-line: no-string-literal
    expect(tags.splice).toHaveBeenCalledWith(tags.indexOf(tag), 1);
  });

  it('removeTagFilter() should call tags.splice if index > 0 && length > 0 && index < length ', () => {
    const tag = 'b';
    const tags = new Array<string>('a', 'b', 'c');
    spyOn(databaseDrawingsService, 'removeTag').and.callThrough();
    // tslint:disable-next-line: no-any
    spyOn<any>(tags, 'splice');
    databaseDrawingsService.removeTag(tags, tag);
    // tslint:disable-next-line: no-string-literal
    expect(tags.splice).toHaveBeenCalledWith(tags.indexOf(tag), 1);
  });

  it('removeTagFilter() should not call tags.splice if index < 0', () => {
    const tag = 'notGoodTag';
    const tags = new Array<string>('a', 'b', 'c');
    spyOn(databaseDrawingsService, 'removeTag').and.callThrough();
    // tslint:disable-next-line: no-any
    spyOn<any>(tags, 'splice');
    databaseDrawingsService.removeTag(tags, tag);
    // tslint:disable-next-line: no-string-literal
    expect(tags.splice).not.toHaveBeenCalled();
  });

  it('removeTagFilter() should not call tags.splice if tags.length == 0', () => {
    const tag = 'notGoodTag';
    const tags = new Array<string>();
    spyOn(databaseDrawingsService, 'removeTag').and.callThrough();
    // tslint:disable-next-line: no-any
    spyOn<any>(tags, 'splice');
    databaseDrawingsService.removeTag(tags, tag);
    // tslint:disable-next-line: no-string-literal
    expect(tags.splice).not.toHaveBeenCalled();
  });

  // validateAllTags
  it('validateAllTags() should return true if all tags are valid', () => {
    const drawing = new Drawing('0', '<svg>');
    drawing.tags = new Array<string>('a', 'b', 'c');
    const result = databaseDrawingsService.validateAllTags(drawing);
    expect(result).toBeTruthy();
  });

  it('validateAllTags() should return false if one tag is invalid', () => {
    const drawing = new Drawing('0', '<svg>');
    drawing.tags = new Array<string>('a', 'invalidTag$$$$', 'c');
    const result = databaseDrawingsService.validateAllTags(drawing);
    expect(result).toBeFalsy();
  });

  it('validateAllTags() should return true if no tags', () => {
    const drawing = new Drawing('0', '<svg>');
    drawing.tags = new Array<string>();
    const result = databaseDrawingsService.validateAllTags(drawing);
    expect(result).toBeTruthy();
  });

  // validateOneTag
  it('validateOneTag() should return false if empty tag', () => {
    const tag = '';
    // tslint:disable-next-line: no-string-literal
    const result = databaseDrawingsService['validateOneTag'](tag);
    expect(result).toBeFalsy();
  });

  it('validateOneTag() should return false if invalid tag', () => {
    const tag = '$$$$';
    // tslint:disable-next-line: no-string-literal
    const result = databaseDrawingsService['validateOneTag'](tag);
    expect(result).toBeFalsy();
  });

  it('validateOneTag() should return false if too long tag ( > 25 caracters)', () => {
    const tag = 'aaaaaaaaaaaaaaaaaaaaaaaaaa';
    // tslint:disable-next-line: no-string-literal
    const result = databaseDrawingsService['validateOneTag'](tag);
    expect(result).toBeFalsy();
  });

  it('validateOneTag() should return true if valid tag and 25 caracters long', () => {
    const tag = 'aaaaaaaaaaaaaaaaaaaaaaaaa';
    // tslint:disable-next-line: no-string-literal
    const result = databaseDrawingsService['validateOneTag'](tag);
    expect(result).toBeTruthy();
  });

  it('validateOneTag() should return true if valid tag and < 25 caracters long', () => {
    const tag = 'aaaaaaaaa';
    // tslint:disable-next-line: no-string-literal
    const result = databaseDrawingsService['validateOneTag'](tag);
    expect(result).toBeTruthy();
  });

  it('validateOneTag() should return true if valid tag (number) and < 25 caracters long', () => {
    const tag = '1111111111';
    // tslint:disable-next-line: no-string-literal
    const result = databaseDrawingsService['validateOneTag'](tag);
    expect(result).toBeTruthy();
  });

  it('validateOneTag() should return true if valid tag (caps) and < 25 caracters long', () => {
    const tag = 'AAAAAAAA';
    // tslint:disable-next-line: no-string-literal
    const result = databaseDrawingsService['validateOneTag'](tag);
    expect(result).toBeTruthy();
  });

  it('validateOneTag() should return true if valid tag (lower and caps and number) and < 25 caracters long', () => {
    const tag = 'a1efn38fh438fh348fh87A';
    // tslint:disable-next-line: no-string-literal
    const result = databaseDrawingsService['validateOneTag'](tag);
    expect(result).toBeTruthy();
  });

// tslint:disable-next-line: max-file-line-count
});
