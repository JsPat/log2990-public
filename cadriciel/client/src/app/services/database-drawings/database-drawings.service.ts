import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatChipInputEvent } from '@angular/material';
import { Observable, Subject } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Drawing } from '../../../../../common/drawing';
import { HandleRequestErrorService } from '../handle-request-error/handle-request-error.service';

const BASE_URL: string = 'http://localhost:3000/api/database/drawings';
const CONNEXION_ERROR_ALERT: string = 'La connexion avec le serveur a échoué. Veuillez réessayer.';
const DELETE_ERROR_ALERT: string =
  'La suppresion ne s\'est pas bien effectuée, car la connexion avec le serveur a échoué. Veuillez réessayer.';
const GET_ERROR_ALERT: string = 'Le chargement ne s\'est pas bien effectué, car la connexion avec le serveur a échoué. Veuillez réessayer.';
const JOIN_CHARACTER: string = '-';
const ROUTING_CHARACTER: string = '/';
const ROUTING_DELETE: string = '/delete/';
const ROUTING_GET: string = '/get/';

@Injectable({
  providedIn: 'root'
})
export class DatabaseDrawingsService {

  readonly MAX_TAG_LENGHT: number = 25;

  allDrawings$: Observable<Drawing[]> = new Subject<Drawing[]>().asObservable();

  constructor(private http: HttpClient, private handleRequestErrorService: HandleRequestErrorService) { }

  getCurrentDrawing(drawing: Drawing): Observable<Drawing> {
    return this.http.get<Drawing>(BASE_URL + ROUTING_GET + drawing.id)
      .pipe(catchError(this.handleRequestErrorService.handleError<Drawing>(GET_ERROR_ALERT, drawing)));
  }

  getDrawings(tags: string[]): Observable<Drawing[]> {
    if (tags.length !== 0) {
      const tagsJoined = tags.join(JOIN_CHARACTER);
      this.allDrawings$ = this.http.get<Drawing[]>(BASE_URL + ROUTING_CHARACTER + tagsJoined)
        .pipe(catchError(this.handleRequestErrorService.handleError<Drawing[]>(CONNEXION_ERROR_ALERT)));
    } else {
      this.allDrawings$ = this.http.get<Drawing[]>(BASE_URL)
        .pipe(catchError(this.handleRequestErrorService.handleError<Drawing[]>(CONNEXION_ERROR_ALERT)));
    }
    return this.allDrawings$;
  }

  addDrawing(drawing: Drawing): Observable<Drawing> {
   return this.http.post<Drawing>(BASE_URL, drawing)
      .pipe(catchError(this.handleRequestErrorService.handleError<Drawing>(CONNEXION_ERROR_ALERT, drawing)));
  }

  deleteDrawing(drawing: Drawing): Observable<Drawing> {
    return this.http.delete<Drawing>(BASE_URL + ROUTING_DELETE + drawing.id)
      .pipe(catchError(this.handleRequestErrorService.handleError<Drawing>(DELETE_ERROR_ALERT, drawing)));
  }

  // inspired from https://material.angular.io/components/chips/examples
  addTag(event: MatChipInputEvent, tags: string[]): void {
    const inputTag = event.input;
    const valueTag = event.value;

    if (this.validateOneTag(valueTag) && inputTag.value !== '') {
      tags.push(valueTag.trim());
    }
    inputTag.value = '';
  }

  removeTag(tags: string[], tag: string): void {
    const index = tags.indexOf(tag);

    if (index >= 0 && tags.length > 0 && index < tags.length) {
      tags.splice(index, 1);
    }
  }

  validateAllTags(drawing: Drawing): boolean {
    let valid = true;
    drawing.tags.forEach((tag) => {
      valid = valid && this.validateOneTag(tag);
    });
    return valid;
  }

  private validateOneTag(tag: string): boolean {
      // tags can only contain alphanumerical characters
      return tag !== '' && /^[0-9a-zA-Z]*$/g.test(tag) && tag.length <= this.MAX_TAG_LENGHT;
  }

}
