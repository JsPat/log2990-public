/* tslint:disable:no-unused-variable */
// tslint:disable: no-string-literal

import { inject, TestBed } from '@angular/core/testing';
import { LoadingSpinnerService } from './loading-spinner.service';

describe('Service: LoadingSpinner', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoadingSpinnerService]
    });
  });

  it('should inject', inject([LoadingSpinnerService], (service: LoadingSpinnerService) => {
    expect(service).toBeTruthy();
  }));
  it('showLoadingSpinner should call next(true)', inject([LoadingSpinnerService], (service: LoadingSpinnerService) => {
    spyOn(service, 'showLoadingSpinner').and.callThrough();
    spyOn(service['isVisible'], 'next');
    service.showLoadingSpinner();
    expect(service['isVisible'].next).toHaveBeenCalledWith(true);
  }));
  it('hideLoadingSpinner should call next(false)', inject([LoadingSpinnerService], (service: LoadingSpinnerService) => {
    spyOn(service, 'hideLoadingSpinner').and.callThrough();
    spyOn(service['isVisible'], 'next');
    service.hideLoadingSpinner();
    expect(service['isVisible'].next).toHaveBeenCalledWith(false);
  }));
});
