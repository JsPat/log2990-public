import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

// inspired from https://onthecode.co.uk/angular-display-spinner-every-request/
export class LoadingSpinnerService {
  isVisible: BehaviorSubject<boolean>;

  constructor() {
    this.isVisible = new BehaviorSubject(false);
  }

  showLoadingSpinner(): void {
    this.isVisible.next(true);
  }

  hideLoadingSpinner(): void {
    this.isVisible.next(false);
  }

}
