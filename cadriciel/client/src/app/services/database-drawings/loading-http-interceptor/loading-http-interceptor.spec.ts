import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed } from '@angular/core/testing';
import { Drawing } from '../../../../../../common/drawing';
import { DatabaseDrawingsService } from '../database-drawings.service';
import { LoadingSpinnerService } from '../loading-spinner/loading-spinner.service';
import { LoadingHttpInterceptor } from './loading-http-interceptor';

// inspired from https://alligator.io/angular/testing-http-interceptors/
describe('LoadingHttpInterceptor', () => {
  let loadingSpinnerServiceMock: LoadingSpinnerService;
  let httpMock: HttpTestingController;
  // tslint:disable-next-line: prefer-const because can't initialize http if it's const
  let httpClient: HttpClient;
  let loadingHttpInterceptor: LoadingHttpInterceptor;

  beforeEach(async(() => {
    loadingSpinnerServiceMock = new LoadingSpinnerService();

    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [
        LoadingSpinnerService,
        DatabaseDrawingsService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: LoadingHttpInterceptor,
          multi: true,
        },
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();

    httpMock = TestBed.get(HttpTestingController);
    httpClient = TestBed.get(HttpClient);
    loadingSpinnerServiceMock = TestBed.get(LoadingSpinnerService);
    loadingHttpInterceptor = new LoadingHttpInterceptor(loadingSpinnerServiceMock);
  }));

  it ('should create', () => {
      expect(loadingHttpInterceptor).toBeTruthy();
  });

  it('should intercept for error', (done) => {
    // inspired from https://stackoverflow.com/questions/31634863/jasmine-test-case-error-spy-to-have-been-called
    spyOn(loadingHttpInterceptor, 'intercept').and.callThrough();
    spyOn(loadingSpinnerServiceMock, 'showLoadingSpinner').and.callFake(() => {
      return true;
    });
    spyOn(loadingSpinnerServiceMock, 'hideLoadingSpinner').and.callFake(() => {
      return true;
    });
    // tslint:disable-next-line: no-empty
    httpClient.get('/').subscribe(() => {}, () => {
      expect(loadingSpinnerServiceMock.showLoadingSpinner).toHaveBeenCalled();
      expect(loadingSpinnerServiceMock.hideLoadingSpinner).toHaveBeenCalled();
      done();
    });
    httpMock.expectOne('/').error(new ErrorEvent('Error'), {
      status: 404
    });
    httpMock.verify();
  });

  it('should intercept for response', () => {
    // inspired from https://github.com/angular/angular/issues/27241
    const expectedDrawings = new Array<Drawing>(new Drawing('0', '<svg>'));
    httpClient.get('http://localhost:3000/api/database/drawings/')
      .subscribe((data: Drawing[]) => {
        expect(data.length).toBeGreaterThan(0);
      });
    const req: TestRequest = httpMock.expectOne('http://localhost:3000/api/database/drawings/');
    expect(req.request).toBeDefined();
    req.flush(expectedDrawings);
  });

});
