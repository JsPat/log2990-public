import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LoadingSpinnerService } from '../loading-spinner/loading-spinner.service';

@Injectable({
    providedIn: 'root'
})
// inspired from https://onthecode.co.uk/angular-display-spinner-every-request/
export class LoadingHttpInterceptor implements HttpInterceptor {
    private readonly TIME_TO_WAIT_SPINNER: number = 500;

    constructor(private loadingSpinnerService: LoadingSpinnerService) { }

    intercept(req: HttpRequest<string>, next: HttpHandler): Observable<HttpEvent<string>> {
        this.loadingSpinnerService.showLoadingSpinner();
        return next
            .handle(req)
            .pipe(
                tap((event: HttpEvent<string>) => {
                    if (event instanceof HttpResponse) {
                        // we wait to see the spinner
                        setTimeout(() => this.loadingSpinnerService.hideLoadingSpinner(), this.TIME_TO_WAIT_SPINNER);
                    }
                }, (error) => {
                    this.loadingSpinnerService.hideLoadingSpinner();
                })
            );
    }
}
