// tslint:disable: no-string-literal
// tslint:disable: no-any

import { Renderer2 } from '@angular/core';
import { SVGChildService } from 'src/app/services/svg-child/svg-child.service';
import { Grid } from './grid';

describe('Grid class', () => {
    let grid: Grid;

    let svgServiceSpy: SVGChildService;
    let rendererSpy: Renderer2 | any;

    beforeEach(() => {
        svgServiceSpy = jasmine.createSpyObj('SVGChildService', ['removeSVG', 'appendSVGOnTop']);
        rendererSpy = jasmine.createSpyObj('Renderer2', ['setAttribute', 'createElement', 'appendChild']);
        grid = new Grid(svgServiceSpy, rendererSpy);
    });

    it('should create', () => {
        expect(grid).toBeTruthy();
    });

    it('constructor should create a new svg element', () => {
        expect(rendererSpy.createElement).toHaveBeenCalledWith('g', 'svg');
    });

    it('display should create the right amount of horizontal line (height / squareDimension) - 1', () => {
        const height = 1000;
        const squareDimension = 25;
        const nLines = height / squareDimension - 1;
        const createHLine = spyOn<any>(grid, 'createHLine');

        grid.display(height, 0, squareDimension, 0);

        expect(rendererSpy.appendChild).toHaveBeenCalledTimes(nLines);
        expect(createHLine).toHaveBeenCalledTimes(nLines);
    });

    it('display should create the right amount of vertical line (width / squareDimension) - 1', () => {
        const width = 2000;
        const squareDimension = 40;
        const nLines = width / squareDimension - 1;
        const createVLine = spyOn<any>(grid, 'createVLine');

        grid.display(0, width, squareDimension, 0);

        expect(rendererSpy.appendChild).toHaveBeenCalledTimes(nLines);
        expect(createVLine).toHaveBeenCalledTimes(nLines);
    });

    it('display should call appendSVGOnTop at the end', () => {
        grid.display(0, 0, 0, 0);

        expect(svgServiceSpy.appendSVGOnTop).toHaveBeenCalledWith(grid['grid']);
    });

    it('hide should call the remove svg of the svg service', () => {
        grid.hide();
        expect(svgServiceSpy.removeSVG).toHaveBeenCalledWith(grid['grid']);
    });

    it('hide should create a new svg', () => {
        grid.hide();
        expect(rendererSpy.createElement).toHaveBeenCalledWith('g', 'svg');
    });

    it('createHLine should create a line with the right parameter', () => {
        const position = 20;
        const length = 12;
        const opacity = 0.25;
        const line  = rendererSpy.createElement('g', 'svg');
        rendererSpy.createElement.and.returnValue(line);

        grid['createHLine'](position, length, opacity);

        expect(rendererSpy.createElement).toHaveBeenCalledWith('line', 'svg');
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(line, 'x1', `${0}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(line, 'y1', `${position}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(line, 'x2', `${length}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(line, 'y2', `${position}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(line, 'style', 'stroke:#000000');
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(line, 'stroke-width', '3');
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(line, 'stroke-opacity', `${opacity}`);
    });

    it('createVLine should create a line with the right parameter', () => {
        const position = 20;
        const length = 12;
        const opacity = 0.25;
        const line  = rendererSpy.createElement('g', 'svg');
        rendererSpy.createElement.and.returnValue(line);

        grid['createVLine'](position, length, opacity);

        expect(rendererSpy.createElement).toHaveBeenCalledWith('line', 'svg');
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(line, 'x1', `${position}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(line, 'y1', `${0}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(line, 'x2', `${position}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(line, 'y2', `${length}`);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(line, 'style', 'stroke:#000000');
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(line, 'stroke-width', '3');
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(line, 'stroke-opacity', `${opacity}`);
    });

});
