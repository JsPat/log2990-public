/* tslint:disable:no-unused-variable */
// tslint:disable: no-string-literal
// tslint:disable: no-any

import { NO_ERRORS_SCHEMA, Renderer2, RendererFactory2 } from '@angular/core';
import { async, inject, TestBed } from '@angular/core/testing';
import { DrawSheetData } from 'src/app/components/app/new-drawing/draw-sheet-model';
import { ToolsService } from '../attributes/toolsService/tools-service';
import { DrawSheetService } from '../draw-sheet/draw-sheet.service';
import { SVGChildService } from '../svg-child/svg-child.service';
import { Grid } from './grid';
import { GridService } from './grid.service';

describe('Gridservice', () => {
  let service: GridService;

  let svgServiceSpy: SVGChildService;
  let rendererSpy: Renderer2;
  let rendererFactorySpy: any;
  let drawSheetDataServiceSpy: DrawSheetService;
  let toolServiceSpy: ToolsService;
  let drawSheetDataSpy: DrawSheetData;
  let gridSpy: Grid;

  const height = 1000;
  const width = 500;

  beforeEach(async(() => {

    svgServiceSpy = jasmine.createSpyObj('SVGChildService', ['appendNewSVG', 'removeSVG', 'appendSVGOnTop']);
    rendererSpy = jasmine.createSpyObj('Renderer2', ['setAttribute', 'createElement', 'appendChild']);
    toolServiceSpy = jasmine.createSpyObj('ToolsService', ['']);
    rendererFactorySpy = jasmine.createSpyObj('RendererFactory2', ['createRenderer']);

    rendererFactorySpy.createRenderer.and.returnValue(rendererSpy);

    drawSheetDataServiceSpy = new DrawSheetService(toolServiceSpy, jasmine.createSpyObj('Color', ['']));
    drawSheetDataSpy = new DrawSheetData(height, width, jasmine.createSpyObj('Color', ['']));
    gridSpy = jasmine.createSpyObj('Grid', ['display', 'hide']);

    drawSheetDataServiceSpy['currentDrawData'] = drawSheetDataSpy;

    TestBed.configureTestingModule({
      providers: [ GridService,
        { provide: SVGChildService, useValue: svgServiceSpy },
        { provide: RendererFactory2, useValue: rendererFactorySpy },
        { provide: DrawSheetService, useValue: drawSheetDataServiceSpy },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  }));

  beforeEach(inject([GridService], (injected: GridService) => {
    service = injected;
    service['grid'] = gridSpy;
  }));

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('display grid should call the display method of the grid with the right arguments', () => {
    service.displayGrid();

    // tslint:disable-next-line: no-magic-numbers
    expect(gridSpy.display).toHaveBeenCalledWith(height, width, service.gridWidth, service.opacity / 100);
  });

  it('hide grid should call the hide method of the grid', () => {
    service.hideGrid();

    expect(gridSpy.hide).toHaveBeenCalled();
  });

  it('redrawGrid should call hideGrid and display grid in this order', () => {
    spyOn(service, 'hideGrid');
    const displayGrid = spyOn(service, 'displayGrid');
    service['redrawGrid']();
    expect(service.hideGrid).toHaveBeenCalledBefore(displayGrid);
  });

});
