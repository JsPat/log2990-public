import { Renderer2 } from '@angular/core';
import { SVGChildService } from 'src/app/services/svg-child/svg-child.service';
import * as JSON from './grid.json';

const config = JSON.default;

export class Grid {
  private grid: SVGGraphicsElement;

  constructor(private svgService: SVGChildService, private renderer: Renderer2) {
    this.grid = this.renderer.createElement(config.svg.group, config.svg.svg);
  }

  display(height: number, width: number, squareDimension: number, opacity: number): void {
    for (let i = squareDimension; i < height; i += squareDimension) {
      this.renderer.appendChild(this.grid, this.createHLine(i, width, opacity));
    }

    for (let i = squareDimension; i < width; i += squareDimension) {
      this.renderer.appendChild(this.grid, this.createVLine(i, height, opacity));
    }

    this.svgService.appendSVGOnTop(this.grid);
  }

  hide(): void {
    this.svgService.removeSVG(this.grid);
    this.grid = this.renderer.createElement(config.svg.group, config.svg.svg);
  }

  private createHLine(position: number, length: number, opacity: number): SVGGraphicsElement {
    const line = this.renderer.createElement(config.svg.line, config.svg.svg) as SVGGraphicsElement;
    this.renderer.setAttribute(line, config.attributes.position.x1, `${0}`);
    this.renderer.setAttribute(line, config.attributes.position.y1, `${position}`);
    this.renderer.setAttribute(line, config.attributes.position.x2, `${length}`);
    this.renderer.setAttribute(line, config.attributes.position.y2, `${position}`);
    this.setStyleAttributes(line, opacity);

    return line;
  }

  private createVLine(position: number, length: number, opacity: number): SVGGraphicsElement {
    const line = this.renderer.createElement(config.svg.line, config.svg.svg) as SVGGraphicsElement;
    this.renderer.setAttribute(line, config.attributes.position.x1, `${position}`);
    this.renderer.setAttribute(line, config.attributes.position.y1, `${0}`);
    this.renderer.setAttribute(line, config.attributes.position.x2, `${position}`);
    this.renderer.setAttribute(line, config.attributes.position.y2, `${length}`);
    this.setStyleAttributes(line, opacity);

    return line;
  }

  private setStyleAttributes(line: SVGElement, opacity: number): void {
    this.renderer.setAttribute(line, config.attributes.color, config.values.color);
    this.renderer.setAttribute(line, config.attributes.width, config.values.width);
    this.renderer.setAttribute(line, config.attributes.opacity, `${opacity}`);
    this.renderer.setAttribute(line, config.attributes.events, config.values.none);
  }

}
