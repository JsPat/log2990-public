import { Injectable, RendererFactory2 } from '@angular/core';
import { BASIC_WIDTH } from 'src/app/components/app/attributes/tool';
import { Grid } from 'src/app/services/grid/grid';
import { DrawSheetService } from '../draw-sheet/draw-sheet.service';
import { SVGChildService } from '../svg-child/svg-child.service';

const NUMBER_PERCENTILE: number = 100;
const BASIC_OPACITY: number = 30;

@Injectable({
  providedIn: 'root'
})
export class GridService {

  gridWidth: number = BASIC_WIDTH;
  opacity: number = BASIC_OPACITY;

  private grid: Grid;
  private drawSheetService: DrawSheetService;

  constructor(svgService: SVGChildService, rendererFactory: RendererFactory2, drawSheetService: DrawSheetService) {
    this.drawSheetService = drawSheetService;

    this.grid = new Grid(svgService, rendererFactory.createRenderer(null, null));
    this.displayGrid();
    this.drawSheetService = drawSheetService;
  }

  displayGrid(): void {
    const height = this.drawSheetService.dataDrawSheet.height;
    const width = this.drawSheetService.dataDrawSheet.width;

    this.grid.display(height, width, this.gridWidth, this.opacity / NUMBER_PERCENTILE);
  }

  hideGrid(): void {
    this.grid.hide();
  }

  redrawGrid(): void {
    this.hideGrid();
    this.displayGrid();
  }

}
