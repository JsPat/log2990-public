export enum PossibleTools {
    SELECT,
    PEN,
    PAINTBRUSH,
    CALLIGRAPHY,
    SPRAYPAINT,
    RECTANGLE,
    ELLIPSE,
    POLYGONE,
    LINE,
    TEXT,
    COLORIZE ,
    PAINTBUCKET,
    ERASER,
    STAMP,
    PIPETTE,
    N_TOOLS,
}

export enum Direction {
    UP,
    RIGHT,
    DOWN,
    LEFT,
    N_DIRECTIONS
}

export const PERCENTILE = 100;
