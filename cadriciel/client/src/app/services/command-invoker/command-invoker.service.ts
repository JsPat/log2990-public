import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { AutoSaveService } from '../auto-save/auto-save.service';
import { Command } from './command';

@Injectable({
  providedIn: 'root'
})
export class CommandInvokerService {

  private commandDone: Command[]; // use as a stack
  private commandUndone: Command[]; // use as a stack
  private emptyCommand: Subject<null>;

  constructor(private autoSave: AutoSaveService) {
    this.commandDone = [];
    this.commandUndone = [];
    this.emptyCommand = new Subject<null>();
  }

  get emptyCommandObs(): Observable<null> {
    return this.emptyCommand.asObservable();
  }

  execute(command: Command): void {
    command.do();
    this.commandDone.push(command);
    this.commandUndone = [];
    this.emptyCommand.next(null);
  }

  undo(): void {
    const command = this.commandDone.pop();
    if (command) {
      command.undo();
      this.commandUndone.push(command);
      this.emptyCommand.next(null);
      this.sendEmptyCommandIfNeeded();
    }
    this.autoSave.save();
  }

  redo(): void {
    const command = this.commandUndone.pop();
    if (command) {
      command.do();
      this.commandDone.push(command);
      this.emptyCommand.next(null);
      this.sendEmptyCommandIfNeeded();
    }
    this.autoSave.save();
  }

  removeLastExecuted(): void {
    this.commandDone.pop();
  }

  hasCommandDone(): boolean {
    return this.commandDone.length > 0;
  }

  hasCommandUndone(): boolean {
    return this.commandUndone.length > 0;
  }

  reset(): void {
    this.commandDone = [];
    this.commandUndone = [];
  }

  private sendEmptyCommandIfNeeded(): void {
    if (!this.hasCommandUndone()) {
      this.emptyCommand.next(null);
    }
  }
}
