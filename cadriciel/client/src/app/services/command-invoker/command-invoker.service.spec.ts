// tslint:disable: no-string-literal
import { inject } from '@angular/core/testing';
import { ColorService } from 'src/app/color-palette/color-service/color.service';
import { ToolsService } from '../attributes/toolsService/tools-service';
import { DrawSheetService } from '../draw-sheet/draw-sheet.service';
import { Command } from './command';
import { CommandInvokerService } from './command-invoker.service';

// tslint:disable-next-line: max-classes-per-file
class DrawSheetServiceMock extends DrawSheetService {
  svgToString(): string {
    return '<svg></svg>';
  }
}

describe('Service: CommandInvoker', () => {
  const nCommands = 5;
  let commandInvoker: CommandInvokerService;
  let commands: Command[];

  beforeEach(inject([CommandInvokerService], (service: CommandInvokerService) => {
      commands = new Array<Command>();
      commandInvoker = service;
      commandInvoker['autoSave']['data'] = new DrawSheetServiceMock(new ToolsService(), new ColorService());

      for (let i = 0; i < nCommands; ++i) {
        commands.push(jasmine.createSpyObj('Command', ['do', 'undo']));
      }
    }));

  it('can be created', () => {
    expect(commandInvoker).toBeTruthy();
  });

  it('each command should be done when we give it to execute', () => {
    for (const command of commands) {
      commandInvoker.execute(command);
      expect(command.do).toHaveBeenCalled();
    }
  });

  it('each time a command is done, it should be added to the command Done array', () => {
    for (const command of commands) {
      commandInvoker.execute(command);
    }

    for (const command of commands) {
      expect(commandInvoker['commandDone'].includes(command)).toBeTruthy();
    }

  });

  it('undo should be call in reverse order on all command when we are calling undo on the invoker', () => {
    const subject = spyOn(commandInvoker['emptyCommand'], 'next');

    for (const command of commands) {
      commandInvoker.execute(command);
    }

    for (const command of commands.reverse()) {
      commandInvoker.undo();
      expect(command.undo).toHaveBeenCalled();
    }

    expect(subject).toHaveBeenCalled();

  });

  it('each time a command is undone, it should be added to the command undone array', () => {
    const subject = spyOn(commandInvoker['emptyCommand'], 'next');

    for (const command of commands) {
      commandInvoker.execute(command);
    }

    for (const _ of commands) {
      commandInvoker.undo();
    }

    for (const command of commands) {

      expect(commandInvoker['commandUndone'].includes(command)).toBeTruthy();
    }

    expect(subject).toHaveBeenCalled();

  });

  it('each time a command is undone, it should be removed from the command done array', () => {
    for (const command of commands) {
      commandInvoker.execute(command);
    }

    for (const _ of commands) {
      commandInvoker.undo();
    }

    for (const command of commands) {

      expect(commandInvoker['commandDone'].includes(command)).toBeFalsy();
    }
  });

  it('each time a undone command is done again, we should call do one more time', () => {
    for (const command of commands) {
      commandInvoker.execute(command);
    }

    for (const _ of commands) {
      commandInvoker.undo();
    }

    for (const _ of commands) {
      commandInvoker.redo();
    }

    for (const command of commands) {
      expect(command.do).toHaveBeenCalledTimes(2);
    }
  });

  it('each time an undone command is redo, it should be added to the done array', () => {
    for (const command of commands) {
      commandInvoker.execute(command);
    }

    for (const _ of commands) {
      commandInvoker.undo();
    }

    for (const _ of commands) {
      commandInvoker.redo();
    }

    for (const command of commands) {

      expect(commandInvoker['commandDone'].includes(command)).toBeTruthy();
    }
  });

  it('when we call execute, we have to clear the undone array', () => {
    for (const command of commands) {
      commandInvoker.execute(command);
    }

    for (const _ of commands) {
      commandInvoker.undo();
    }

    commandInvoker.execute(jasmine.createSpyObj('Command', ['do', 'undo']));

    expect(commandInvoker['commandUndone'].length).toBe(0);

  });

  it('a command should be remove from the commandDone if it was pop', () => {
    for (const command of commands) {
      commandInvoker.execute(command);
      commandInvoker.removeLastExecuted();
    }

    expect(commandInvoker['commandDone'].length).toBe(0);
  });

  it('hasCommandDone should return false if their is no command in the commandDone', () => {
    expect(commandInvoker.hasCommandDone()).toBeFalsy();
  });

  it('hasCommandDone should return true if their is command in the commandDone', () => {
    commandInvoker.execute(commands[0]);

    expect(commandInvoker.hasCommandDone()).toBeTruthy();
  });

  it('hasCommandUnone should return false if their is no command in the commandUndone', () => {
    expect(commandInvoker.hasCommandUndone()).toBeFalsy();
  });

  it('hasCommandUndone should return true if their is command in the commandUndone', () => {
    commandInvoker.execute(commands[0]);
    commandInvoker.undo();

    expect(commandInvoker.hasCommandUndone()).toBeTruthy();
  });

  it('when we call undo and their is no command, we should not push an undefined to the undone', () => {
    spyOn(commandInvoker['commandUndone'], 'push');
    commandInvoker.undo();

    expect(commandInvoker['commandUndone'].push).not.toHaveBeenCalled();
  });

  it('when we call redo and their is no command, we should not push an undefined to the done', () => {
    spyOn(commandInvoker['commandDone'], 'push');
    commandInvoker.redo();

    expect(commandInvoker['commandDone'].push).not.toHaveBeenCalled();
  });

  it('reset should clear all the done and undone command from the invoker', () => {
    commandInvoker.reset();

    expect(commandInvoker['commandDone'].length).toBe(0);
    expect(commandInvoker['commandUndone'].length).toBe(0);
  });

  it('emptyCommand observer should return ', () => {
    expect(commandInvoker.emptyCommandObs).toEqual(commandInvoker['emptyCommand'].asObservable());
  });

  it('undo should call save of autoSave ', () => {
    spyOn(commandInvoker['autoSave'], 'save');
    commandInvoker.undo();
    expect(commandInvoker['autoSave'].save).toHaveBeenCalled();
  });

  it('redo should call save of autoSave ', () => {
    spyOn(commandInvoker['autoSave'], 'save');
    commandInvoker.redo();
    expect(commandInvoker['autoSave'].save).toHaveBeenCalled();
  });

});
