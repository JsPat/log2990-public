/* tslint:disable:no-unused-variable */

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material';
import { Observable, of } from 'rxjs';
import { DataToEmail } from '../../../../../common/data-to-email';
import { HandleRequestErrorService } from '../handle-request-error/handle-request-error.service';
import { EmailService } from './email.service';

// tslint:disable-next-line: max-classes-per-file
class HandleRequestErrorServiceMock extends HandleRequestErrorService {
  handleError<T>(errorMessage: string, result?: T): (error: Error) => Observable<T> {
    return (error: Error): Observable<T> => {
      return of(result as T);
    };
  }
}

describe('Service: Email', () => {
  const BASE_EMAIL_URL: string = 'http://localhost:3000/api/email';

  let httpMock: HttpTestingController;
  let emailService: EmailService;
  let matSnackBarMock: MatSnackBar;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        EmailService,
        { provide: MatSnackBar, useValue: matSnackBarMock },
        { provide: HandleRequestErrorService, useClass: HandleRequestErrorServiceMock },
      ]
    });
    emailService = TestBed.get(EmailService);
    httpMock = TestBed.get(HttpTestingController);
    matSnackBarMock = jasmine.createSpyObj('MatSnackBar', ['']);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should inject', inject([EmailService], (service: EmailService) => {
    expect(service).toBeTruthy();
  }));

  it('sendEmail should return expected dataToSend and POST (HttpClient called once)', () => {
    const expectedDataToSend: DataToEmail = {
      to: 'a@a.a',
      drawingName: 'drawing',
      format: 'png',
      payload: 'data'
    };
    // check the content of the mocked call
    emailService.sendEmail('a@a.a', 'drawing', 'png', 'data').subscribe((response: DataToEmail) => {
      expect(response.to).toEqual(expectedDataToSend.to, 'email address check');
      expect(response.drawingName).toEqual(expectedDataToSend.drawingName, 'name check');
      expect(response.format).toEqual(expectedDataToSend.format, 'format check');
      expect(response.payload).toEqual(expectedDataToSend.payload, 'data check');
    }, fail);
    // tslint:disable-next-line: no-string-literal
    const testURL = BASE_EMAIL_URL;
    const req = httpMock.expectOne(testURL);
    expect(req.request.method).toBe('POST');
    req.flush(expectedDataToSend);
  });
});
