import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { DataToEmail } from '../../../../../common/data-to-email';
import { HandleRequestErrorService } from '../handle-request-error/handle-request-error.service';

const BASE_EMAIL_URL: string = 'http://localhost:3000/api/email';
const EMAIL_ERROR_ALERT: string = 'L\'envoi du courriel ne s\'est pas bien effectué. Veuillez réessayer.';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  constructor(private http: HttpClient, private handleRequestErrorService: HandleRequestErrorService) { }

  sendEmail(emailAddress: string, drawingName: string, format: string, data: string): Observable<DataToEmail> {
    const dataToSend: DataToEmail = {
      to: emailAddress,
      drawingName,
      format,
      payload: data
    };

    return this.http.post<DataToEmail>(BASE_EMAIL_URL, dataToSend)
      .pipe(catchError(this.handleRequestErrorService.handleError<DataToEmail>(EMAIL_ERROR_ALERT)));
  }
}
