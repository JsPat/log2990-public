import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Coordinate } from 'src/app/components/app/attributes/coordinate';

export enum PossibleTabs {
  NONE,
  ATTRIBUT,
  NEW,
  GALLERY,
  SAVE,
  EXPORT,
  HELP,
}

@Injectable({
  providedIn: 'root'
})
export class DrawViewService {
  readonly SIDE_NAV: number = 60;
  readonly TOOLS_MENU_SIZE: number = 250;

  undoDisabled: boolean = true;
  redoDisabled: boolean = true;

  private navBarOpened: boolean;
  private navBar: Subject<PossibleTabs>;

  constructor() {
    this.navBarOpened = true;
    this.navBar = new Subject<PossibleTabs>();
  }

  get navBarState(): boolean {
    return this.navBarOpened;
  }

  get navBarObs(): Observable<PossibleTabs> {
    return this.navBar.asObservable();
  }

  toggle(tab: PossibleTabs): void {
    this.navBarOpened = tab.valueOf() ? true : false;
    this.navBar.next(tab);
  }

  alignCoordinateToDrawView(position: Coordinate): Coordinate {
    const xPos = position.x + window.scrollX - this.SIDE_NAV - ((this.navBarState) ? this.TOOLS_MENU_SIZE : 0);
    const yPos = position.y + window.scrollY;
    return {x : xPos, y : yPos};
  }

}
