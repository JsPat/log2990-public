/* tslint:disable:no-unused-variable */
// tslint:disable: no-any
// tslint:disable: no-magic-numbers
// tslint:disable: no-string-literal

import { DrawViewService, PossibleTabs } from './draw-view.service';

describe('Service: DrawView', () => {
  let drawViewService: DrawViewService;

  beforeEach(() => {
      drawViewService = new DrawViewService();
  });

  it('should create', () => {
    expect(drawViewService).toBeTruthy();
  });

  it('Should be able to subscribe to the toggle observable and be updated when a new Tab is selected', () => {
    drawViewService.navBarObs.subscribe();

    drawViewService.toggle(PossibleTabs.ATTRIBUT);
    expect(drawViewService.navBarState).toBeTruthy();
  });

  it('Should be able to subscribe to the toggle observable and be updated when a new Tab is selected', () => {
    drawViewService.navBarObs.subscribe();

    drawViewService.toggle(PossibleTabs.NONE);
    expect(drawViewService.navBarState).toBeFalsy();
  });

  it('the navBar should be opened by default', () => {
    expect(drawViewService.navBarState).toBeTruthy();
  });

  it('alignCoordinateToDrawView should return coordinates with scroll bars and sidenav removed', () => {
    drawViewService['navBarOpened'] = true;
    const result = drawViewService.alignCoordinateToDrawView({x : 500, y : 500});
    expect(result.x).toEqual(190);
    expect(result.y).toEqual(500);
  });

  it('alignCoordinateToDrawView should return coordinates with only scroll barsremoved', () => {
    drawViewService['navBarOpened'] = false;
    const result = drawViewService.alignCoordinateToDrawView({x : 500, y : 500});
    expect(result.x).toEqual(440);
    expect(result.y).toEqual(500);
  });

});
