import { Component } from '@angular/core';
import { ColorService } from '../color-service/color.service';
import { ColorSliderAbstract } from '../color-slider-abstract/color-slider-abstract';
import { Color } from '../color/color';
import { BLUE, GREEN, MAX_COLOR_VALUE, PINK, RED, TEAL, YELLOW } from '../constant';

const R = 65;
const THICKNESS = 25;
const GRADIENT_COLOR: Color[] = [ RED, YELLOW, GREEN, TEAL, BLUE, PINK ];
const PERCENTILE_DIVIDER = 100;

@Component({
  selector: 'app-colorChooser',
  templateUrl: './color-chooser.component.html',
  styleUrls: ['./color-chooser.component.scss']
})
export class ColorChooserComponent extends ColorSliderAbstract {

  constructor(colorService: ColorService) {
    super ( colorService );
  }

  protected emitColor(): void {
    this.colorService.sendBaseColor(this.readColor());
  }

  protected readColor(): Color {
    // read the color in the center of the line, the color is the same in the radial direction
    const theta = Math.atan2(this.getYCoordinateFromCenter(), this.getXCoordinateFromCenter());
    let distX = Math.cos(theta) * R;
    let distY = Math.sin(theta) * R;

    distX = this.getXCoordinateFromCorner(distX);
    distY = this.getYCoordinateFromCorner(distY);
    const image = this.canvasContext.getImageData(distX, distY, 1, 1).data;

    return new Color(image[0], image[1], image[2], MAX_COLOR_VALUE);
  }

  // the next 4 methods is to be able to work with coordinate as if they were from the center of the canvas
  private getXCoordinateFromCenter(): number {
    return this.selectedPositionX - (this.canvas.nativeElement.width / 2);
  }

  private getYCoordinateFromCenter(): number {
    return (this.canvas.nativeElement.height / 2) - this.selectedPositionY;
  }

  private getXCoordinateFromCorner(xCoordinateFromCenter: number): number {
    return (this.canvas.nativeElement.width / 2) + xCoordinateFromCenter;
  }

  private getYCoordinateFromCorner(yCoordinateFromCenter: number): number {
    return (this.canvas.nativeElement.height / 2) - yCoordinateFromCenter;
  }

  protected displayColors(): void {
    const width = this.canvas.nativeElement.width / 2;
    const height = this.canvas.nativeElement.height / 2;

    // reinitialise white background
    this.canvasContext.clearRect(0, 0, width * 2, height * 2);

    // the length of 1 color zone
    const partLength = (2 * Math.PI) / GRADIENT_COLOR.length;
    let start = 0;
    let gradient;
    let startColor;
    let endColor;

    for (let i = 0; i < GRADIENT_COLOR.length; i++) {
        startColor = GRADIENT_COLOR[i];

        // the % is to get the first color when we have completed a full rotation
        endColor = GRADIENT_COLOR[(i + 1) % GRADIENT_COLOR.length];

        // x start / end of the next arc to draw
        const xStart = (width + Math.cos(start) * R);
        const xEnd = (width + Math.cos(start + partLength) * R);

        // y start / end of the next arc to draw
        const yStart = (height + Math.sin(start) * R);
        const yEnd = (height + Math.sin(start + partLength) * R);

        gradient = this.canvasContext.createLinearGradient(xStart, yStart, xEnd, yEnd);
        gradient.addColorStop(0, startColor.rgba_string);
        gradient.addColorStop(1.0, endColor.rgba_string);

        // draw the arc that contain the previous colors
        this.canvasContext.beginPath();
        this.canvasContext.strokeStyle = gradient;

        // draw in arc not in single line
        // the partLength/100 is there to add some superposition between parth to not see a line of pixel
        this.canvasContext.arc(width, height, R, start, start + partLength + partLength / PERCENTILE_DIVIDER);

        // width of the circle (outer r - inner r)
        this.canvasContext.lineWidth = THICKNESS;
        this.canvasContext.stroke();
        this.canvasContext.closePath();

        start += partLength;
    }
  }

  protected displayIndicator(): void {
    const theta = Math.atan2(this.getYCoordinateFromCenter(), this.getXCoordinateFromCenter());
    let distX = Math.cos(theta) * R;
    let distY = Math.sin(theta) * R;

    distX = this.getXCoordinateFromCorner(distX);
    distY = this.getYCoordinateFromCorner(distY);

    this.canvasContext.strokeStyle = this.INDICATOR_COLOR; // style of the cursor
    this.canvasContext.beginPath();
    // give a zoom effect
    // cursor in circle
    this.canvasContext.arc(
      distX,
      distY,
      THICKNESS / 2,
      0,
      2 * Math.PI
    );
    this.canvasContext.lineWidth = this.INDICATOR_WIDTH;
    this.canvasContext.stroke();
    this.canvasContext.closePath();
  }

}
