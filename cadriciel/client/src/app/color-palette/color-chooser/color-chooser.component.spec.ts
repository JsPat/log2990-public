// tslint:disable: no-string-literal

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Observable, Subject } from 'rxjs';
import { ColorService } from '../color-service/color.service';
import { ColorShadderComponent } from '../color-shadder/color-shadder.component';
import { Color } from '../color/color';
import { MAX_COLOR_VALUE } from '../constant';
import { ColorChooserComponent } from './color-chooser.component';

describe('colorChooser Component', () => {
    let fixture: ComponentFixture<ColorChooserComponent>;
    let colorChooser: ColorChooserComponent;
    let baseColorSource: Subject<Color>;

    // tslint:disable-next-line: no-any
    let colorService: any; // any here to not depend on the actual class
    // tslint:disable-next-line: no-any
    let canvas: any;

    beforeEach( async(() => {
        baseColorSource = new Subject<Color>();

        colorService = {
            sendBaseColor(color: Color): void { baseColorSource.next(color); },
            get baseColorObs(): Subject<Color> { return baseColorSource; },
            get textInputColorObs(): Observable<Color> { return new Subject<Color>().asObservable(); } // for the shadder component
        };

        spyOn(colorService, 'sendBaseColor');

        TestBed.configureTestingModule( {
            declarations: [ColorChooserComponent, ColorShadderComponent],
            providers: [ { provide: ColorService, useValue: colorService }]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ColorChooserComponent);
        colorChooser = fixture.componentInstance;
        canvas = fixture.debugElement.query(By.css('#colorChooser'));
        fixture.detectChanges();

        colorChooser.ngAfterViewInit();
    });

    it('should create', () => {
        expect(colorChooser).toBeTruthy();
    });

    it('the x coordinate from the center should be a negative value if it is on the left of the center', () => {
        colorChooser['selectedPositionX'] = (canvas.nativeElement.width / 2 ) - 1;
        expect(colorChooser['getXCoordinateFromCenter']()).toBeLessThan(0);
    });

    it('the x coordinate from the center should be positive if it is on the right of the center', () => {
        colorChooser['selectedPositionX'] = (canvas.nativeElement.width / 2 ) + 1;
        expect(colorChooser['getXCoordinateFromCenter']()).toBeGreaterThan(0);
    });

    it('the y coordinate from the center should be negative if it is under the center of the canvas', () => {
        colorChooser['selectedPositionY'] = (canvas.nativeElement.height / 2) + 1;
        expect(colorChooser['getYCoordinateFromCenter']()).toBeLessThan(0);
    });

    it('the y coordinate from the center should be positive if it is above the center of the canvas', () => {
        colorChooser['selectedPositionY'] = (canvas.nativeElement.height / 2) - 1;
        expect(colorChooser['getYCoordinateFromCenter']()).toBeGreaterThan(0);
    });

    it('emit color should call the sendBaseColor of the service', () => {
        colorChooser['emitColor']();
        expect(colorService.sendBaseColor).toHaveBeenCalledTimes(1);
    });

    it('read color should return the color read by the image data of the canvasContext color whit an alpha of 1', async(() => {
        colorChooser['selectedPositionX'] = canvas.nativeElement.width;
        colorChooser['selectedPositionY'] = canvas.nativeElement.height / 2;

        const array = [MAX_COLOR_VALUE, 0, 0]; // red

        const color = colorChooser['readColor']();
        expect(color.r).toBe(array[0]);
        expect(color.g).toBe(array[1]);
        expect(color.b).toBe(array[2]);
        expect(color.a).toBe(1);
    }));

    it('the displayColor methods should add each color into the gradient and display them', () => {
        const context =  jasmine.createSpyObj('CanvasRenderingContext2D',
        ['createLinearGradient', 'clearRect', 'beginPath', 'arc', 'stroke', 'closePath']);
        const gradient = jasmine.createSpyObj('CanvasGradient', ['addColorStop']);
        context.createLinearGradient.and.returnValue(gradient);

        colorChooser['canvasContext'] = context;

        colorChooser['displayColors']();

        const nColor = 6;

        expect(gradient.addColorStop).toHaveBeenCalledTimes(nColor * 2);
        expect(context.createLinearGradient).toHaveBeenCalledTimes(nColor);
        expect(context.clearRect).toHaveBeenCalledTimes(1);
        expect(context.beginPath).toHaveBeenCalledTimes(nColor);
        expect(context.arc).toHaveBeenCalledTimes(nColor);
        expect(context.stroke).toHaveBeenCalledTimes(nColor);
        expect(context.closePath).toHaveBeenCalledTimes(nColor);
    });

    it('The displayIndicator should display a circle empty', () => {
        const context =  jasmine.createSpyObj('CanvasRenderingContext2D',
        ['beginPath', 'arc', 'stroke', 'closePath']);

        colorChooser['canvasContext'] = context;
        colorChooser['displayIndicator']();

        expect(context.beginPath).toHaveBeenCalledTimes(1);
        expect(context.arc).toHaveBeenCalledTimes(1);
        expect(context.stroke).toHaveBeenCalledTimes(1);
        expect(context.closePath).toHaveBeenCalledTimes(1);

    });

});
