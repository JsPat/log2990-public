import { Component } from '@angular/core';
import { ColorService } from '../color-service/color.service';
import { Color } from '../color/color';
import { PossibleColorSelected } from '../constant';

@Component({
  selector: 'app-colorPalette',
  templateUrl: './color-palette.component.html',
  styleUrls: ['./color-palette.component.scss']
})
export class ColorPaletteComponent {

  // tslint:disable-next-line: no-any --- disable here because there is not really a type that match the enum
  readonly POSSIBLE_COLOR_SLECTED: any = PossibleColorSelected;

  constructor(private colorService: ColorService ) {
    this.colorService.pushColor(this.secondaryColor);
    this.colorService.pushColor(this.mainColor);
  }

  get mainColor(): Color {
    return this.colorService.mainColor;
  }

  get secondaryColor(): Color {
    return this.colorService.secondaryColor;
  }

  get preset(): Color[] {
    return this.colorService.presetColor;
  }

  protected setMainFromPreset(index: number): void {
    this.colorService.setMainFromPreset(index);
  }

  protected setSecondaryFromPreset(index: number, event: MouseEvent): void {
    event.preventDefault(); // stop the real context menu to pop up
    this.colorService.setSecondaryFromPreset(index);
  }

  protected switchColor(): void {
    this.colorService.switchColor();
  }

  protected setMain(): void {
    this.colorService.setMainFromCurrentColor();
  }

  protected setSecondary(): void {
    this.colorService.setSecondaryFromCurrentColor();
  }
}
