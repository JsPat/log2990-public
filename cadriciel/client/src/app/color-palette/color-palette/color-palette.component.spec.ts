// tslint:disable: no-string-literal
// tslint:disable: no-magic-numbers

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ColorPaletteModule } from '../color-palette.module';
import { ColorService } from '../color-service/color.service';
import { Color } from '../color/color';
import { ColorPaletteComponent } from './color-palette.component';

describe('color Palette Component', () => {
    let fixture: ComponentFixture<ColorPaletteComponent>;
    let colorPalette: ColorPaletteComponent;
    let colorService: ColorService;

    beforeEach( async(() => {

        // we use the real color sevice object but evry time we need a value from him we use a spy to limit error coming from him
        colorService = new ColorService();

        TestBed.configureTestingModule( {
            imports: [ColorPaletteModule], // we need all declarations
            providers: [ { provide: ColorService, useValue: colorService }]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ColorPaletteComponent);
        colorPalette = fixture.componentInstance;
    });

    it('should create', () => {
        expect(colorPalette).toBeTruthy();
    });

    it('The main color should be the main color given by the service', () => {
        const mainColorService = new Color(0, 0, 0);
        spyOnProperty(colorService, 'mainColor', 'get').and.returnValue(mainColorService);
        const mainColorPalette = colorPalette.mainColor;

        expect(mainColorService).toBe(mainColorPalette);
    });

    it('The secondary color should be the secondary color given by the service', () => {
        const secondaryColorService = new Color(0, 0, 0);
        spyOnProperty(colorService, 'secondaryColor', 'get').and.returnValue(secondaryColorService);
        const secondaryColorPalette = colorPalette.secondaryColor;

        expect(secondaryColorService).toBe(secondaryColorPalette);
    });

    it('The preset should be the preset given by the service', () => {
        const presetServiceColor = Array<Color>();
        for (let i = 0; i < 10; ++i) {
            presetServiceColor.push(new Color(i , i, i));
        }
        spyOnProperty(colorService, 'presetColor', 'get').and.returnValue(presetServiceColor);
        const presetPaletteColor = colorPalette.preset;

        expect(presetPaletteColor).toBe(presetServiceColor);
    });

    it('the setMainFromPreset should call the same function of the service', () => {
        spyOn(colorService, 'setMainFromPreset');
        colorPalette['setMainFromPreset'](0);

        expect(colorService.setMainFromPreset).toHaveBeenCalledTimes(1);
    });

    it('the setSecondaryFromPreset should call the same function of the service', () => {
        const event = jasmine.createSpyObj('Event', ['preventDefault']);
        spyOn(colorService, 'setSecondaryFromPreset');
        colorPalette['setSecondaryFromPreset'](0, event);

        expect(colorService.setSecondaryFromPreset).toHaveBeenCalledTimes(1);
    });

    it('when etSecondaryFromPreset is call, the context menue should not appear', () => {
        const event = jasmine.createSpyObj('Event', ['preventDefault']);
        colorPalette['setSecondaryFromPreset'](0, event);

        expect(event.preventDefault).toHaveBeenCalledTimes(1);
    });

    it('the switchColor should call the same function of the service', () => {
        spyOn(colorService, 'switchColor');
        colorPalette['switchColor']();

        expect(colorService.switchColor).toHaveBeenCalledTimes(1);
    });

    it('the setMain should call the setMainFromCurrentColor of the service', () => {
        spyOn(colorService, 'setMainFromCurrentColor');
        colorPalette['setMain']();

        expect(colorService.setMainFromCurrentColor).toHaveBeenCalledTimes(1);
    });

    it('the setSecondary should call the setSecondaryFromCurrentColor of the service', () => {
        spyOn(colorService, 'setSecondaryFromCurrentColor');
        colorPalette['setSecondary']();

        expect(colorService.setSecondaryFromCurrentColor).toHaveBeenCalledTimes(1);
    });
});
