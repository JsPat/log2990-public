// tslint:disable: no-magic-numbers
import { Color } from './color';

describe('color class', () => {
    let color: Color;

    beforeEach(() => {
        color = new Color(100, 100, 100, 100);
    });

    it('testing the constructor with number', () => {
        let constructorOk = true;

        constructorOk = constructorOk && color.r === 100;
        constructorOk = constructorOk && color.g === 100;
        constructorOk = constructorOk && color.b === 100;
        constructorOk = constructorOk && color.a === 100 / 255;

        expect(constructorOk).toBeTruthy();
    });

    it ('r can be set from a string', () => {
        color.r_string = '47';
        expect(color.r).toBe(47);
    });

    it ('g can be set from a string', () => {
        color.g_string = '47';
        expect(color.g).toBe(47);
    });

    it ('b can be set from a string', () => {
        color.b_string = '47';
        expect(color.b).toBe(47);
    });

    it ('a can be set from a string', () => {
        color.a_string = '47';
        expect(color.a).toBe(47 / 255);
    });

    it ('r can be get as a string', () => {
        color.r = 47;
        expect(color.r_string).toBe('47');
    });

    it ('g can be get as a string', () => {
        color.g = 47;
        expect(color.g_string).toBe('47');
    });

    it ('b can be get as a string', () => {
        color.b = 47;
        expect(color.b_string).toBe('47');
    });

    it('rgba color can be set via string', () => {
        color.rgba_string = 'rgba(1,2,3,4)';
        const expectedAValue = 4 / 255;

        expect(color.rgba_string).toBe('rgba(1,2,3,' + expectedAValue.toString() + ')');
    });

    it('rgba from string can have any amount of whitespace in parameter', () => {
        color.rgba_string = 'rgba(   1    ,  2 , 3,    4)';
        const expectedAValue = 4 / 255;

        expect(color.rgba_string).toBe('rgba(1,2,3,' + expectedAValue.toString() + ')');
    });

    it('both color should be equal even if they dont have the same alpha', () => {
        const color1 = new Color(12, 23, 34, 0);
        const color2 = new Color(12, 23, 34, 234);

        expect(color1.isRGBequal(color2)).toBeTruthy();
    });

    it('both color should not be equal', () => {
        const color1 = new Color(34, 23, 12, 0);
        const color2 = new Color(12, 23, 34, 234);

        expect(color1.isRGBequal(color2)).toBeFalsy();
    });

    it('Color can be return as hexa', () => {
        expect(color.rgb_hexa).toBe('646464'); // 100 - 100 - 100
    });

});
