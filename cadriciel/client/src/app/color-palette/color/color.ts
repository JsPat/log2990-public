// these const a declare again here to supress the warning circular dependencies we have when we change
// these const in the const file
const HEXBASE = 16;
const MAX_COLOR_VALUE = 255;
const R_POS = 0;
const G_POS = 1;
const B_POS = 2;
const A_POS = 3;

export class Color {
    readonly SEPARATOR: string = ',';

    // the alpha parameter (a) is store as a value between 0 and 1;
    r: number;
    g: number;
    b: number;
    a: number;

    constructor(r: number, g: number, b: number, a: number = MAX_COLOR_VALUE) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a / MAX_COLOR_VALUE;
    }

    get r_string(): string {
        return this.r.toString();
    }

    set r_string(r: string) {
        this.r = Number(r);
    }

    get g_string(): string {
        return this.g.toString();
    }

    set g_string(g: string) {
        this.g = Number(g);
    }

    get b_string(): string {
        return this.b.toString();
    }

    set b_string(b: string) {
        this.b = Number(b);
    }

    get a_string(): string {
        return this.a.toString();
    }

    set a_string(a: string) {
        this.a = Number(a) / MAX_COLOR_VALUE;
    }

    get rgba_string(): string {
        return ('rgba(' + this.r_string +
            this.SEPARATOR + this.g_string +
            this.SEPARATOR + this.b_string +
            this.SEPARATOR + this.a_string + ')').normalize();
    }

    set rgba_string(color: string) {
        color = color.split('(')[1]; // remove the rgba
        color = color.split(')')[0]; // remove the colsing )
        const value = color.split(this.SEPARATOR);

        this.r_string = value[R_POS];
        this.g_string = value[G_POS];
        this.b_string = value[B_POS];
        this.a_string = value[A_POS];
    }

    get r_hexa(): string {
        return this.asTwoCharHex(this.r.toString(HEXBASE)).toUpperCase();
    }

    set r_hexa(color: string) {
        this.r = parseInt(color, HEXBASE);
    }

    get g_hexa(): string {
        return this.asTwoCharHex(this.g.toString(HEXBASE)).toUpperCase();
    }

    set g_hexa(color: string) {
        this.g = parseInt(color, HEXBASE);
    }

    get b_hexa(): string {
        return this.asTwoCharHex(this.b.toString(HEXBASE)).toUpperCase();
    }

    set b_hexa(color: string) {
        this.b = parseInt(color, HEXBASE);
    }

    get rgb_hexa(): string {
        return this.r_hexa + this.g_hexa + this.b_hexa;
    }

    isRGBequal(color: Color, tolerance: number = 0): boolean {
        return Math.abs(this.r - color.r) <= tolerance &&
               Math.abs(this.g - color.g) <= tolerance &&
               Math.abs(this.b - color.b) <= tolerance;
    }

    private asTwoCharHex(num: string): string {
        while (num.length < 2) {
            num = '0'.concat(num);
        }
        return num;
    }
}
