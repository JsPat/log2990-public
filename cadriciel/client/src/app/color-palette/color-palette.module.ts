import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ColorChooserComponent } from './color-chooser/color-chooser.component';
import { ColorIndicatorComponent } from './color-indicator/color-indicator.component';
import { ColorInputTextComponent } from './color-input-text/color-input-text.component';
import { ColorPaletteComponent } from './color-palette/color-palette.component';
import { ColorShadderComponent } from './color-shadder/color-shadder.component';
import { ColorTransparencyComponent } from './color-transparency/color-transparency.component';

@NgModule({
  declarations: [
    ColorChooserComponent,
    ColorPaletteComponent,
    ColorShadderComponent,
    ColorIndicatorComponent,
    ColorTransparencyComponent,
    ColorInputTextComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    ColorPaletteComponent,
    ColorChooserComponent,
    ColorInputTextComponent,
    ColorIndicatorComponent,
    ColorTransparencyComponent
  ],
})
export class ColorPaletteModule { }
