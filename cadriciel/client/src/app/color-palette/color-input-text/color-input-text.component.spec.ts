// tslint:disable: no-string-literal
// tslint:disable: no-magic-numbers

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Subject } from 'rxjs';
import { ColorService } from '../color-service/color.service';
import { Color } from '../color/color';
import { ColorInputTextComponent } from './color-input-text.component';

describe('Color Input Text', () => {
    let fixture: ComponentFixture<ColorInputTextComponent>;
    let shadeColorSource: Subject<Color>;
    let colorInputText: ColorInputTextComponent;

    // tslint:disable-next-line: no-any
    let colorService: any;

    beforeEach( async(() => {
        shadeColorSource = new Subject<Color>();

        colorService = {
            sendShadeColor(color: Color): void { shadeColorSource.next(color); },
            sendColorFromInputText(): null { return null; },
            get shadeColorObs(): Subject<Color> { return shadeColorSource; }
        };

        spyOn(colorService, 'sendShadeColor');
        spyOn(colorService, 'sendColorFromInputText');

        TestBed.configureTestingModule( {
            imports: [FormsModule],
            declarations: [ColorInputTextComponent],
            providers: [ { provide: ColorService, useValue: colorService }]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ColorInputTextComponent);
        colorInputText = fixture.componentInstance;
        fixture.detectChanges();

        colorInputText.ngOnInit();
    });

    it('should create', () => {
        expect(colorInputText).toBeTruthy();
    });

    it('hex value should be updated when giving an appropriate value', () => {
        const index = colorInputText.INPUT.R;
        const color = '1F';
        colorInputText['inputHexa'](color, index);
        expect(colorInputText.color[index]).toEqual(color);
    });

    it('hex value should not be updated when giving an bad value', () => {
        const index = colorInputText.INPUT.R;
        const color = 'kl';
        colorInputText['inputHexa'](color, index);
        expect(colorInputText.color[index]).not.toEqual(color);
    });

    it('hex value should alway be upperCase even if we give loweCase value', () => {
        const index = colorInputText.INPUT.R;
        const color = 'ab';
        colorInputText['inputHexa'](color, index);
        expect(colorInputText.color[index]).toEqual(color.toUpperCase());
    });

    it('hex value.length cannot be longer then 2 if we pass an input index r', () => {
        const index = colorInputText.INPUT.R;
        const color = '0123456789ABCDEF';
        colorInputText['inputHexa'](color, index);
        expect(colorInputText.color[index].length).toEqual(2);
    });

    it('hex value.length cannot be longer then 2 if we pass an input index g', () => {
        const index = colorInputText.INPUT.G;
        const color = '0123456789ABCDEF';
        colorInputText['inputHexa'](color, index);
        expect(colorInputText.color[index].length).toEqual(2);
    });

    it('hex value.length cannot be longer then 2 if we pass an input index b', () => {
        const index = colorInputText.INPUT.B;
        const color = '0123456789ABCDEF';
        colorInputText['inputHexa'](color, index);
        expect(colorInputText.color[index].length).toEqual(2);
    });

    it('hex value.length cannot be longer then 6 if we pass an input index rgb', () => {
        const index = colorInputText.INPUT.RGB;
        const color = '0123456789ABCDEF';
        colorInputText['inputHexa'](color, index);
        expect(colorInputText.color[index].length).toEqual(6);
    });

    it('hex value can accept all possible value of hex', () => {
        const possibleHexValue = '0123456789ABCDEF';
        const index = colorInputText.INPUT.R;

        for (const char of possibleHexValue) {
            colorInputText['inputHexa'](char, index);
            expect(colorInputText.color[index]).toEqual(char);
        }
    });

    it('service should receive a new color when a new color r has been entered', () => {
        const color = '00';
        const index = colorInputText.INPUT.R;
        colorInputText.color[colorInputText.INPUT.G] = 'FF';
        colorInputText.color[colorInputText.INPUT.B] = 'AA';

        colorInputText['inputHexa'](color, index);

        expect(colorService.sendColorFromInputText).toHaveBeenCalledWith(new Color(0, 255, 170));
    });

    it('service should receive a new color when a new color g has been entered', () => {
        const color = '00';
        const index = colorInputText.INPUT.G;
        colorInputText.color[colorInputText.INPUT.R] = 'FF';
        colorInputText.color[colorInputText.INPUT.B] = 'AA';

        colorInputText['inputHexa'](color, index);

        expect(colorService.sendColorFromInputText).toHaveBeenCalledWith(new Color(255, 0, 170));
    });

    it('service should receive a new color when a new color b has been entered', () => {
        const color = '00';
        const index = colorInputText.INPUT.B;
        colorInputText.color[colorInputText.INPUT.R] = 'FF';
        colorInputText.color[colorInputText.INPUT.G] = 'AA';

        colorInputText['inputHexa'](color, index);

        expect(colorService.sendColorFromInputText).toHaveBeenCalledWith(new Color(255, 170, 0));
    });

    it('service should receive a new color when a new color rgb has been entered', () => {
        const color = '00FFAA';
        const index = colorInputText.INPUT.RGB;

        colorInputText['inputHexa'](color, index);

        expect(colorService.sendColorFromInputText).toHaveBeenCalledWith(new Color(0, 255, 170));
    });

    it('ngOnInit should subscribe to shadeColor change and update all his value when a new shade color is sent', () => {
        const color = 'FF00AA';

        shadeColorSource.next(new Color(255, 0, 170));
        expect(colorInputText.color[colorInputText.INPUT.R]).toEqual(color.slice(0, 2));
        expect(colorInputText.color[colorInputText.INPUT.G]).toEqual(color.slice(2, 4));
        expect(colorInputText.color[colorInputText.INPUT.B]).toEqual(color.slice(4, 6));
        expect(colorInputText.color[colorInputText.INPUT.RGB]).toEqual(color);
    });

    it('a new shade color should only update the all rgb input if the new color came from 1 of the 3 others', () => {
        const color = 'AB';
        const index = colorInputText.INPUT.R;

        // tslint:disable-next-line: no-any
        spyOn<any>(colorInputText, 'setIndividualRGB');
        // tslint:disable-next-line: no-any
        spyOn<any>(colorInputText, 'setAllRGB');

        colorInputText['inputHexa'](color, index);

        expect(colorInputText['setIndividualRGB']).toHaveBeenCalledTimes(0);
        expect(colorInputText['setAllRGB']).toHaveBeenCalledTimes(1);
    });

    it('color should only update the indiviual rgb input if the new color came from the AllRGB', () => {
        const color = 'AB12DF';
        const index = colorInputText.INPUT.RGB;

        // tslint:disable-next-line: no-any
        spyOn<any>(colorInputText, 'setIndividualRGB');
        // tslint:disable-next-line: no-any
        spyOn<any>(colorInputText, 'setAllRGB');

        colorInputText['inputHexa'](color, index);

        expect(colorInputText['setAllRGB']).toHaveBeenCalledTimes(0);
        expect(colorInputText['setIndividualRGB']).toHaveBeenCalledTimes(1);
    });

    it('Color can be input on the rgb input even if there is not 6 char', () => {
        const color = '1290';
        const index = colorInputText.INPUT.RGB;

        colorInputText['inputHexa'](color, index);

        expect(colorInputText.color[index]).toEqual(color);
    });

    it('Color can be input on the r input even if there is not 2 char', () => {
        const color = '1';
        const index = colorInputText.INPUT.R;

        colorInputText['inputHexa'](color, index);

        expect(colorInputText.color[index]).toEqual(color);
    });

    it('Color can be input on the g input even if there is not 2 char', () => {
        const color = '3';
        const index = colorInputText.INPUT.G;

        colorInputText['inputHexa'](color, index);

        expect(colorInputText.color[index]).toEqual(color);
    });

    it('Color can be input on the b input even if there is not 2 char', () => {
        const color = 'A';
        const index = colorInputText.INPUT.B;

        colorInputText['inputHexa'](color, index);

        expect(colorInputText.color[index]).toEqual(color);
    });

    it('color can be emit event if the value of color(r, g, b) is \'\'. it should give 000000 on the all rgb', () => {
        colorInputText['inputHexa']('', colorInputText.INPUT.R);
        colorInputText['inputHexa']('', colorInputText.INPUT.G);
        colorInputText['inputHexa']('', colorInputText.INPUT.B);

        expect(colorInputText.color[colorInputText.INPUT.RGB]).toEqual('000000');
    });

    it('color can be emit event if the value of color[rgb] is \'\'. it should give 00 on individual composant', () => {
        colorInputText['inputHexa']('', colorInputText.INPUT.RGB);

        expect(colorInputText.color[colorInputText.INPUT.R]).toEqual('00');
        expect(colorInputText.color[colorInputText.INPUT.G]).toEqual('00');
        expect(colorInputText.color[colorInputText.INPUT.B]).toEqual('00');
    });

});
