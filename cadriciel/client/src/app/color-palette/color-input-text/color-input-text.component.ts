import { Component, OnInit } from '@angular/core';
import { ColorService } from '../color-service/color.service';
import { Color } from '../color/color';
import { HEXBASE, MAX_COLOR_VALUE, N_ATTRIBUTE_IN_RGB } from '../constant';

enum Input {R = 0, G = 1, B = 2, RGB = 3}
const NUMBER_CHAR = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
const HEXA_CHAR = ['A', 'B', 'C', 'D', 'E', 'F'].concat(NUMBER_CHAR);
const HEX_LENGTH = 2;
const NCHAR = 6;
const MAX_COLOR_VALUE_HEXA: string = MAX_COLOR_VALUE.toString(HEXBASE).toUpperCase();
const MAX_RGB_VALUE_HEXA: string = MAX_COLOR_VALUE_HEXA + MAX_COLOR_VALUE_HEXA + MAX_COLOR_VALUE_HEXA;
const NULL_HEXA_VALUE: string = '0';

@Component({
  selector: 'app-colorInputText',
  templateUrl: './color-input-text.component.html',
  styleUrls: ['./color-input-text.component.scss']
})
export class ColorInputTextComponent implements OnInit {

  // tslint:disable-next-line: no-any --- disable here because there is not really a type that match the enum
  readonly INPUT: any = Input;

  color: string[];

  constructor( private colorService: ColorService) {
    this.color = [MAX_COLOR_VALUE_HEXA, MAX_COLOR_VALUE_HEXA, MAX_COLOR_VALUE_HEXA, MAX_RGB_VALUE_HEXA];
  }

  ngOnInit(): void {
    this.colorService.shadeColorObs.subscribe(
      (color) => {
        this.setIndividualRGB(color);
        this.setAllRGB(color);
    });
  }

  protected inputHexa(value: string, index: Input): void {
    let finalText = '';
    const maxLength = index === Input.RGB ? HEX_LENGTH * N_ATTRIBUTE_IN_RGB : HEX_LENGTH;

    for (const char of (value).toUpperCase()) {
      if (HEXA_CHAR.includes(char)) {
        finalText += char;
      }
      if (finalText.length >= maxLength) { break; }
    }

    this.color[index] = finalText;
    index === Input.RGB ?  this.emitColorFromAllRGB() : this.emitColorFromOneRGB();
  }

  private setAllRGB(color: Color): void {
    this.color[Input.RGB] = color.rgb_hexa;
  }

  private setIndividualRGB(color: Color): void {
    this.color[Input.R] = color.r_hexa;
    this.color[Input.G] = color.g_hexa;
    this.color[Input.B] = color.b_hexa;
  }

  private emitColorFromOneRGB(): void {
    const color = new Color(0, 0, 0);
    color.r_hexa = this.color[Input.R].length ? this.color[Input.R] : NULL_HEXA_VALUE;
    color.g_hexa = this.color[Input.G].length ? this.color[Input.G] : NULL_HEXA_VALUE;
    color.b_hexa = this.color[Input.B].length ? this.color[Input.B] : NULL_HEXA_VALUE;

    this.colorService.sendColorFromInputText(color);
    this.setAllRGB(color);
  }

  private emitColorFromAllRGB(): void {
    let hexaStr = this.color[Input.RGB].length ? this.color[Input.RGB] : NULL_HEXA_VALUE;

    while (hexaStr.length < HEX_LENGTH * N_ATTRIBUTE_IN_RGB) {
      hexaStr = hexaStr.concat(NULL_HEXA_VALUE);
    }

    const r = parseInt(hexaStr.slice( 0, NCHAR / N_ATTRIBUTE_IN_RGB ), HEXBASE);
    const g = parseInt(hexaStr.slice(NCHAR / N_ATTRIBUTE_IN_RGB, 2 * NCHAR / N_ATTRIBUTE_IN_RGB), HEXBASE);
    const b = parseInt(hexaStr.slice(2 * NCHAR / N_ATTRIBUTE_IN_RGB, N_ATTRIBUTE_IN_RGB * NCHAR / N_ATTRIBUTE_IN_RGB),
                                     HEXBASE);

    const color = new Color(r, g, b);

    this.colorService.sendColorFromInputText(color);
    this.setIndividualRGB(color);
  }
}
