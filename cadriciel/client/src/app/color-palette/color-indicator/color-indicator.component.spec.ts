// tslint:disable: no-magic-numbers

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Observable, Subject } from 'rxjs';
import { ColorService } from '../color-service/color.service';
import { Color } from '../color/color';
import { ColorIndicatorComponent } from './color-indicator.component';

describe('Color Indicator component', () => {
    let fixture: ComponentFixture<ColorIndicatorComponent>;
    let colorIndicator: ColorIndicatorComponent;

    // tslint:disable-next-line: no-any
    let colorService: any; // any here to not depend on the actual class
    const transparencyColorSource = new Subject<Color>();
    const inputTextColorSource = new Subject<Color>();

    beforeEach( async(() => {
        colorService = {
            get transparencyColorObs(): Observable<Color> { return transparencyColorSource.asObservable(); },
            get textInputColorObs(): Observable<Color> { return inputTextColorSource.asObservable(); }
    };
        TestBed.configureTestingModule( {
            declarations: [ColorIndicatorComponent],
            providers: [{provide: ColorService, useValue: colorService}]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ColorIndicatorComponent);
        colorIndicator = fixture.componentInstance;
        colorIndicator.ngOnInit();
        fixture.detectChanges();

    });

    it('should create', () => {
        expect(colorIndicator).toBeTruthy();
    });

    it('The background color should be the same as the color in the component', () => {
        const de = fixture.debugElement.query(By.css('#indicator'));
        const color = de.nativeElement.style['background-color'];
        const expectedColor = colorIndicator.currentColor;

        expect(color).toEqual(`rgb(${expectedColor.r_string}, ${expectedColor.g_string}, ${expectedColor.b_string})`);
    });

    it('Color should be update when the transparency color is updated', () => {
        const color = new Color(1, 4, 56, 78);

        transparencyColorSource.next(color); // send the new color

        expect(colorIndicator.currentColor).toBe(color);
    });

    it('Color should be update when a color is entered via input text', () => {
        const color = new Color(34, 0, 243, 178);

        inputTextColorSource.next(color); // send the new color

        expect(colorIndicator.currentColor).toBe(color);
    });
});
