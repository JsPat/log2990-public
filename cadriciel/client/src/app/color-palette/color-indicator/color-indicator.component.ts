import { Component, OnInit } from '@angular/core';
import { ColorService } from '../color-service/color.service';
import { Color } from '../color/color';
import { WHITE } from '../constant';

@Component({
  selector: 'app-colorIndicator',
  templateUrl: './color-indicator.component.html',
  styleUrls: ['./color-indicator.component.scss']
})
export class ColorIndicatorComponent implements OnInit {

  currentColor: Color;

  constructor(private colorService: ColorService) {
    this.currentColor = WHITE;
  }

  ngOnInit(): void {
    this.colorService.transparencyColorObs.subscribe(
      (color) => {
        this.currentColor = color;
      });
    this.colorService.textInputColorObs.subscribe(
      (color) => {
        this.currentColor = color;
      }
    );
  }
}
