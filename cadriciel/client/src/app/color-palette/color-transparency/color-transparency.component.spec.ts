// tslint:disable: no-any
// tslint:disable: no-string-literal
// tslint:disable: no-magic-numbers

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Observable, Subject } from 'rxjs';
import { ColorService } from '../color-service/color.service';
import { Color } from '../color/color';
import { ColorTransparencyComponent } from './color-transparency.component';

describe('colorTransparency Component', () => {
    let fixture: ComponentFixture<ColorTransparencyComponent>;
    let colorTransparency: ColorTransparencyComponent;
    let shadeColorSource: Subject<Color>;
    let inputFromTextColorSource: Subject<Color>;

    let colorService: any;
    let canvas: any;

    beforeEach( async(() => {
        shadeColorSource = new Subject<Color>();
        inputFromTextColorSource = new Subject<Color>();

        colorService = {
            sendTransparencyColor(color: Color): void { shadeColorSource.next(color); },
            get shadeColorObs(): Observable<Color> { return shadeColorSource; },
            get textInputColorObs(): Observable<Color> { return inputFromTextColorSource.asObservable(); } // for the shadder component
        };

        spyOn(colorService, 'sendTransparencyColor');

        TestBed.configureTestingModule( {
            declarations: [ColorTransparencyComponent],
            providers: [ { provide: ColorService, useValue: colorService }]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ColorTransparencyComponent);
        colorTransparency = fixture.componentInstance;
        canvas = fixture.debugElement.query(By.css('#colorTransparency'));
        fixture.detectChanges();

        colorTransparency.ngAfterViewInit();
    });

    it('should create', () => {
        expect(colorTransparency).toBeTruthy();
    });

    it('emit color should call the sendTransparencyColor of the service', () => {
        spyOn<any>(colorTransparency, 'readColor').and.returnValue(jasmine.createSpyObj('Color', ['']));

        colorTransparency['emitColor']();
        expect(colorService.sendTransparencyColor).toHaveBeenCalledTimes(1);
    });

    it('read color should return the color read by the image data of the canvasContext color whit an the right alpha value', () => {

         // send a known color
         shadeColorSource.next(new Color(255, 0, 0));
         colorTransparency['selectedPositionX'] = canvas.nativeElement.width / 2;

         const array = [255, 0, 0]; // half red half white
         const color = colorTransparency['readColor']();

         expect(color.r).toBe(array[0]);
         expect(color.g).toBe(array[1]);
         expect(color.b).toBe(array[2]);
         expect(color.a).toBeCloseTo(0.5, 0.01);
    });

    it('The color should be updated when the shade color change in the service', () => {
        spyOn<any>(colorTransparency, 'display');
        spyOn<any>(colorTransparency, 'emitColor');

        const color = new Color(34, 56, 1);

        shadeColorSource.next(color); // send the new color

        expect(colorTransparency['shadeColor']).toBe(color);
        expect(colorTransparency['display']).toHaveBeenCalledTimes(1);
        expect(colorTransparency['emitColor']).toHaveBeenCalledTimes(1);
    });

    it('The color should be update when there is a new color enter via input text. emit should not be called', () => {

        spyOn<any>(colorTransparency, 'display');
        spyOn<any>(colorTransparency, 'emitColor');

        const color = new Color(90, 78, 156);

        inputFromTextColorSource.next(color); // send the new color

        expect(colorTransparency['shadeColor']).toBe(color);
        expect(colorTransparency['display']).toHaveBeenCalledTimes(1);
        expect(colorTransparency['emitColor']).toHaveBeenCalledTimes(0);
    });

    it('the displayColor methods should make a graident of transparency', () => {
        const context =  jasmine.createSpyObj('CanvasRenderingContext2D',
        ['createLinearGradient', 'clearRect', 'fillRect']);
        const gradient = jasmine.createSpyObj('CanvasGradient', ['addColorStop']);
        context.createLinearGradient.and.returnValue(gradient);

        colorTransparency['canvasContext'] = context;
        colorTransparency['displayColors']();

        expect(gradient.addColorStop).toHaveBeenCalledTimes(2);
        expect(context.createLinearGradient).toHaveBeenCalledTimes(1);
        expect(context.clearRect).toHaveBeenCalledTimes(1);
        expect(context.fillRect).toHaveBeenCalledTimes(1);
    });

    it('The displayIndicator should display 2 triangle', () => {
        const context =  jasmine.createSpyObj('CanvasRenderingContext2D',
        ['beginPath', 'moveTo', 'lineTo', 'fill', 'closePath']);

        colorTransparency['canvasContext'] = context;

        colorTransparency['displayIndicator']();

        expect(context.beginPath).toHaveBeenCalledTimes(2);
        expect(context.moveTo).toHaveBeenCalledTimes(2);
        expect(context.lineTo).toHaveBeenCalledTimes(4);
        expect(context.closePath).toHaveBeenCalledTimes(2);

    });

});
