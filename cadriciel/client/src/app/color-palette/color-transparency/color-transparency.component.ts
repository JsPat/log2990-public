import { Component, OnInit } from '@angular/core';
import { ColorService } from '../color-service/color.service';
import { ColorSliderAbstract } from '../color-slider-abstract/color-slider-abstract';
import { Color } from '../color/color';
import { A_POS, B_POS, G_POS, R_POS, WHITE } from '../constant';

const POSITION_OFFSET = 0.1;

@Component({
  selector: 'app-colorTransparency',
  templateUrl: './color-transparency.component.html',
  styleUrls: ['./color-transparency.component.scss']
})
export class ColorTransparencyComponent extends ColorSliderAbstract implements OnInit {
  protected readonly INDICATOR_COLOR: string = '#000000';

  shadeColor: Color;

  constructor(colorService: ColorService) {
    super(colorService);
    this.shadeColor = WHITE;
   }

   ngOnInit(): void {
    this.colorService.shadeColorObs.subscribe(
      (color) => {this.runWhenUpdated(color);
                  this.emitColor();
      }
    );

    this.colorService.textInputColorObs.subscribe(
      (color) => { this.runWhenUpdated(color); }
    );
  }

  protected displayColors(): void {

    const height = this.canvas.nativeElement.height;
    const width = this.canvas.nativeElement.width;

    // reinitialise white background
    this.canvasContext.clearRect(0, 0, width, height);

    // make a gradient of transparency
    const gradient = this.canvasContext.createLinearGradient(0, 0, width, 0);
    this.shadeColor.a = 0;
    gradient.addColorStop(0.0, this.shadeColor.rgba_string);
    this.shadeColor.a = 1;
    gradient.addColorStop(1.0, this.shadeColor.rgba_string);

    this.canvasContext.fillStyle = gradient;
    this.canvasContext.fillRect(0, 0, width, height);
  }

  protected displayIndicator(): void {
    // make the tringale on the left side
    this.canvasContext.beginPath();
    this.canvasContext.moveTo(this.selectedPositionX + this.INDICATOR_WIDTH, 0);
    this.canvasContext.lineTo(this.selectedPositionX, this.INDICATOR_WIDTH);
    this.canvasContext.lineTo(this.selectedPositionX - this.INDICATOR_WIDTH, 0);
    this.canvasContext.closePath();
    this.canvasContext.fillStyle = this.INDICATOR_COLOR;
    this.canvasContext.fill();

    // ... on the right side
    const height = this.canvas.nativeElement.height;
    this.canvasContext.beginPath();
    this.canvasContext.moveTo(this.selectedPositionX + this.INDICATOR_WIDTH, height);
    this.canvasContext.lineTo(this.selectedPositionX, height - this.INDICATOR_WIDTH);
    this.canvasContext.lineTo(this.selectedPositionX - this.INDICATOR_WIDTH, height);
    this.canvasContext.closePath();
    this.canvasContext.fillStyle = this.INDICATOR_COLOR;
    this.canvasContext.fill();
  }

  protected emitColor(): void {
    this.colorService.sendTransparencyColor(this.readColor());
  }

  protected readColor(): Color {
    // this._canvas.nativeElement.width/2 to alway read in the center of the rectangle
    const image = this.canvasContext.getImageData(this.selectedPositionX, this.canvas.nativeElement.height / 2, 1, 1).data;

    // send the color to the service
    return new Color(image[R_POS], image[G_POS], image[B_POS], image[A_POS]);
  }

  private runWhenUpdated(color: Color): void {
    this.shadeColor = color;
    this.selectedPositionX = this.canvas.nativeElement.width - POSITION_OFFSET;
    this.display();
  }
}
