// tslint:disable: no-string-literal
// tslint:disable: no-any

import { ColorSliderAbstract } from './color-slider-abstract';

class ColorSliderAbstractMock extends ColorSliderAbstract {
    // we disable the no empty because there is no need to implement these methods

    // tslint:disable-next-line: no-empty
    displayColors(): void {}

    // tslint:disable-next-line: no-empty
    displayIndicator(): void {}

    // tslint:disable-next-line: no-empty
    emitColor(): void {}

    // tslint:disable-next-line: no-empty
    readColor(): any {}

    get mouseDownMock(): boolean {
        return  this.mouseDown;
    }

    set mouseDownMock(state: boolean) {
        this.mouseDown = state;
    }

    set canvasMock(canvas: any) {
        this.canvas = canvas;
    }
}

// need an other class to represent the canvas and his native element
// tslint:disable-next-line: max-classes-per-file
class NativeElementMock {

    constructor(private context: any) {}
    getContext(_: string): any { // _ is never used. it is just to follow the real implementation
        return this.context;
    }
}

// we need 2 stub because elementRef and HTMLCanvasElement
// tslint:disable-next-line: max-classes-per-file
class CanvasMock {
      constructor(public nativeElement: NativeElementMock) {}
}

describe('Color Slider Abstract', () => {
    let colorSlider: ColorSliderAbstractMock;
    let colorService: any; // dont want specified the service to keeep it from importing it

    beforeEach(() => {
        colorService = jasmine.createSpyObj('ColorService', ['']);
        colorSlider = new ColorSliderAbstractMock(colorService);
    });

    it('the slider should be ready to display then display after view On Init', () => {
        spyOn<any>(colorSlider, 'display');
        spyOn<any>(colorSlider, 'getCanvaContext');

        colorSlider.ngAfterViewInit();

        expect(colorSlider['display']).toHaveBeenCalledTimes(1);
        expect(colorSlider['getCanvaContext']).toHaveBeenCalledTimes(1);
    });

    it('display should display the color and the indicator', () => {
        spyOn<any>(colorSlider, 'displayColors');
        spyOn<any>(colorSlider, 'displayIndicator');

        colorSlider['display']();

        expect(colorSlider['displayColors']).toHaveBeenCalledTimes(1);
        expect(colorSlider['displayIndicator']).toHaveBeenCalledTimes(1);
    });

    it('Mouse mooving should have no effect if left click have not been press', () => {
        const event = new MouseEvent('mouseDown');
        spyOn<any>(colorSlider, 'display');
        spyOn<any>(colorSlider, 'emitColor');

        colorSlider['mouseMoving'](event);

        expect(colorSlider['display']).not.toHaveBeenCalled();
        expect(colorSlider['emitColor']).not.toHaveBeenCalled();
    });

    it('Mouse mooving should emit the new color and display again when the left click is beeing press', () => {
        const event = new MouseEvent('mouseDown');

        colorSlider.mouseDownMock = true;

        spyOn<any>(colorSlider, 'display');
        spyOn<any>(colorSlider, 'emitColor');

        colorSlider['mouseMoving'](event);

        expect(colorSlider['display']).toHaveBeenCalledTimes(1);
        expect(colorSlider['emitColor']).toHaveBeenCalledTimes(1);
    });

    it('mouseDown attribute should stay false if the left click have not been pressed', () => {

        expect(colorSlider.mouseDownMock).toBeFalsy();
    });

    it('mousDown should become true when leftClick is beeing pressed . It will then read color as if mouse was mooving', () => {
        colorSlider.mouseDownMock = false;
        const event = new MouseEvent('mouseDown');

        spyOn<any>(colorSlider, 'mouseMoving');

        colorSlider['leftClickPressed'](event);

        expect(colorSlider.mouseDownMock).toBeTruthy();
        expect(colorSlider['mouseMoving']).toHaveBeenCalledTimes(1);

    });

    it('mousDown should become false when leftClick is releasedg', () => {
        colorSlider.mouseDownMock = false;

        spyOn<any>(colorSlider, 'mouseMoving');

        colorSlider.leftClickReleased();

        expect(colorSlider.mouseDownMock).toBeFalsy();
    });

    it('Get Canvas should throw error if the context is null', () => {
        const canvas = new CanvasMock(new NativeElementMock(null));
        colorSlider.canvasMock = canvas;

        expect(() => {colorSlider['getCanvaContext'](); }).toThrow(new Error('Failed to get the canvas context'));
    });

    it('Get Canvas context should return the context if there is no probleme', () => {
         const context = jasmine.createSpyObj('CanvasRenderingContext2D', ['']);
         const canvas = new CanvasMock(new NativeElementMock(context));
         colorSlider.canvasMock = canvas;

         colorSlider['getCanvaContext']();

         expect(colorSlider['canvasContext']).toBe(context);
    });
});
