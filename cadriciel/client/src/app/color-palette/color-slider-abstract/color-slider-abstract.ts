import { AfterViewInit, ElementRef, HostListener, ViewChild } from '@angular/core';
import { ColorService } from '../color-service/color.service';
import { Color } from '../color/color';

const CHILD_SELECTOR: string = 'canvas';

export abstract class ColorSliderAbstract implements AfterViewInit {
  protected readonly INDICATOR_WIDTH: number = 6;
  protected readonly INDICATOR_COLOR: string = '#E0E0E0';

  @ViewChild(CHILD_SELECTOR, {static: false})
  protected canvas: ElementRef<HTMLCanvasElement>;

  protected canvasContext: CanvasRenderingContext2D;
  protected mouseDown: boolean;
  protected selectedPositionX: number;
  protected selectedPositionY: number;

  constructor(protected colorService: ColorService) {
    this.mouseDown = false;
    this.selectedPositionX = 0;
    this.selectedPositionY = 0;
  }

  protected abstract displayColors(): void;
  protected abstract displayIndicator(): void;
  protected abstract emitColor(): void;
  protected abstract readColor(): Color | null;

  // need global listening because the mouse might be off the canvas when released
  @HostListener('window:mouseup', ['$event'])
  leftClickReleased(): void {
      this.mouseDown = false;
  }

   // display the canva only when the component is visible at the screen
  ngAfterViewInit(): void {
    this.getCanvaContext();
    this.display();
  }

  protected display(): void {
    this.displayColors();
    this.displayIndicator();
  }

  protected mouseMoving(event: MouseEvent): void {
      if (this.mouseDown) { // if the left click is being press
        this.selectedPositionX = event.offsetX;
        this.selectedPositionY = event.offsetY;
        this.display(); // update the selector
        this.emitColor();
      }
  }

  protected leftClickPressed(event: MouseEvent): void {
      // when the user click on the left click of his mouse
      this.mouseDown = true;
      this.mouseMoving(event);
  }

  private getCanvaContext(): void {
    const context = this.canvas.nativeElement.getContext('2d');

    // verified that it is not null
    if (context === null) {
      // what we need to do here when we cant get the context? need to ask team
      throw new Error('Failed to get the canvas context');
    }
    this.canvasContext = context;
  }
}
