import { Component, OnInit } from '@angular/core';
import { ColorService } from '../color-service/color.service';
import { ColorSliderAbstract } from '../color-slider-abstract/color-slider-abstract';
import { Color } from '../color/color';
import { BLACK, MAX_COLOR_VALUE, WHITE } from '../constant';

const TRANPARENT_BLACK = new Color(0, 0, 0, 0);
const TRANPARENT_WHITE = new Color(MAX_COLOR_VALUE, MAX_COLOR_VALUE, MAX_COLOR_VALUE, 0);
const POSITION_OFFSET = 0.1;
const INDICATOR_OFFSET = 7;
const MAX_COLOR_STOP_OFFSET = 0.95;
const MIN_COLOR_STOP_OFFSET = 0.05;

@Component({
  selector: 'app-colorShadder',
  templateUrl: './color-shadder.component.html',
  styleUrls: ['./color-shadder.component.scss']
})
export class ColorShadderComponent extends ColorSliderAbstract implements OnInit {

  private mainColor: Color;

  constructor( colorService: ColorService ) {
   super( colorService );
   this.mainColor = WHITE;
  }

  ngOnInit(): void {
    this.colorService.baseColorObs.subscribe(
      (color) => {
        this.mainColor = color;
        // bring back the indicator into the top right of the canvas
        this.selectedPositionX = this.canvas.nativeElement.width - POSITION_OFFSET;
        this.selectedPositionY = 0;
        this.display();
        this.emitColor(); // the indicator moved. there is new color
      });
    this.colorService.textInputColorObs.subscribe(
      (color) => {
        this.mainColor = color;
        // bring back the indicator into the top right of the canvas
        this.selectedPositionX = this.canvas.nativeElement.width - POSITION_OFFSET;
        this.selectedPositionY = 0;
        this.display();
      }
    );
  }

  protected emitColor(): void {
    this.colorService.sendShadeColor(this.readColor());
  }

  protected readColor(): Color {
    const image = this.canvasContext.getImageData(this.selectedPositionX, this.selectedPositionY, 1, 1).data;

    // send the color to the service
    return new Color(image[0], image[1], image[2], MAX_COLOR_VALUE);
  }

  protected displayColors(): void {

    const height = this.canvas.nativeElement.height;
    const width = this.canvas.nativeElement.width;

    // reinitialise white background
    this.canvasContext.clearRect(0, 0, width, height);
    this.canvasContext.fillStyle = this.mainColor.rgba_string;
    this.canvasContext.fillRect(0, 0, width, height);

    // make a gradient of white
    let gradient = this.canvasContext.createLinearGradient(0, 0, width, 0);
    gradient.addColorStop(MIN_COLOR_STOP_OFFSET, WHITE.rgba_string);
    gradient.addColorStop(MAX_COLOR_STOP_OFFSET, TRANPARENT_WHITE.rgba_string);
    this.canvasContext.fillStyle = gradient;
    this.canvasContext.fillRect(0, 0, width, height);

    // make a gradient of black
    gradient = this.canvasContext.createLinearGradient(0, 0, 0, height);
    // the 0.05 is to make sure we can still read the color with no deformation
    gradient.addColorStop(MAX_COLOR_STOP_OFFSET, BLACK.rgba_string);
    gradient.addColorStop(MIN_COLOR_STOP_OFFSET, TRANPARENT_BLACK.rgba_string);
    this.canvasContext.fillStyle = gradient;
    this.canvasContext.fillRect(0, 0, width, height);
  }

  protected displayIndicator(): void {
    // cursor in square
    this.canvasContext.strokeStyle = this.INDICATOR_COLOR;
    this.canvasContext.beginPath();
    this.canvasContext.rect(this.selectedPositionX - INDICATOR_OFFSET,
                            this.selectedPositionY - INDICATOR_OFFSET,
                            2 * INDICATOR_OFFSET,
                            2 * INDICATOR_OFFSET);
    this.canvasContext.lineWidth = this.INDICATOR_WIDTH;
    this.canvasContext.stroke();
    this.canvasContext.closePath();
  }
}
