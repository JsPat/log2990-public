// tslint:disable: no-string-literal
// tslint:disable: no-magic-numbers
// tslint:disable: no-any

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Observable, Subject } from 'rxjs';
import { ColorService } from '../color-service/color.service';
import { Color } from '../color/color';
import { ColorShadderComponent } from './color-shadder.component';

describe('colorShadder Component', () => {
    let fixture: ComponentFixture<ColorShadderComponent>;
    let colorShadder: ColorShadderComponent;
    let baseColorSource: Subject<Color>;
    let inputFromTextColorSource: Subject<Color>;

    // tslint:disable-next-line: no-any
    let colorService: any;
    // tslint:disable-next-line: no-any
    let canvas: any;

    beforeEach( async(() => {
        baseColorSource = new Subject<Color>();
        inputFromTextColorSource = new Subject<Color>();

        colorService = {
            sendShadeColor(color: Color): void { baseColorSource.next(color); },
            get baseColorObs(): Observable<Color> { return baseColorSource; },
            get textInputColorObs(): Observable<Color> { return inputFromTextColorSource.asObservable(); } // for the shadder component
        };

        spyOn(colorService, 'sendShadeColor');

        TestBed.configureTestingModule( {
            declarations: [ColorShadderComponent],
            providers: [ { provide: ColorService, useValue: colorService }]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ColorShadderComponent);
        colorShadder = fixture.componentInstance;
        canvas = fixture.debugElement.query(By.css('#colorShadder'));
        fixture.detectChanges();

        colorShadder.ngAfterViewInit();
    });

    it('should create', () => {
        expect(colorShadder).toBeTruthy();
    });

    it('emit color should call the sendShadeColor of the service', () => {
        spyOn<any>(colorShadder, 'readColor').and.returnValue(jasmine.createSpyObj('Color', ['']));

        colorShadder['emitColor']();
        expect(colorService.sendShadeColor).toHaveBeenCalledTimes(1);
    });

    it('read color should return the color read by the image data of the canvasContext color whit an alpha of 1', () => {
        // send a known color
        baseColorSource.next(new Color(255, 0, 0));

        colorShadder['selectedPositionX'] = canvas.nativeElement.width / 2;
        colorShadder['selectedPositionY'] = 0;

        const array = [255, 126, 126]; // half red half white

        const color = colorShadder['readColor']();
        expect(color.r).toBeCloseTo(array[0], -1);
        expect(color.g).toBeCloseTo(array[1], -1);
        expect(color.b).toBeCloseTo(array[2], -1);
        expect(color.a).toBe(1);
    });

    it('The color should be updated when the main color change in the service', () => {
        spyOn<any>(colorShadder, 'display');
        spyOn<any>(colorShadder, 'emitColor');

        const color = new Color(255, 170 , 11);

        baseColorSource.next(color); // send the new color

        expect(colorShadder['mainColor']).toBe(color);
        expect(colorShadder['display']).toHaveBeenCalledTimes(1);
        expect(colorShadder['emitColor']).toHaveBeenCalledTimes(1);
    });

    it('The color should be update when there is a new color enter via input text. emit should not be called', () => {
        spyOn<any>(colorShadder, 'display');
        spyOn<any>(colorShadder, 'emitColor');

        const color = new Color(121, 89, 62);

        inputFromTextColorSource.next(color); // send the new color

        expect(colorShadder['mainColor']).toBe(color);
        expect(colorShadder['display']).toHaveBeenCalledTimes(1);
        expect(colorShadder['emitColor']).toHaveBeenCalledTimes(0);
    });

    it('the displayColor methods should make 2 gradient, 1 black and 1 white.\
    it should display them on an empty canvas whit the base color as background', () => {
        const context =  jasmine.createSpyObj('CanvasRenderingContext2D',
        ['createLinearGradient', 'clearRect', 'fillRect']);

        const gradient = jasmine.createSpyObj('CanvasGradient', ['addColorStop']);
        context.createLinearGradient.and.returnValue(gradient);

        colorShadder['canvasContext'] = context;
        colorShadder['displayColors']();

        expect(gradient.addColorStop).toHaveBeenCalledTimes(2 * 2);
        expect(context.createLinearGradient).toHaveBeenCalledTimes(2);
        expect(context.clearRect).toHaveBeenCalledTimes(1);
        expect(context.fillRect).toHaveBeenCalledTimes(3);
    });

    it('The displayIndicator should display a square empty', () => {
        const context =  jasmine.createSpyObj('CanvasRenderingContext2D',
        ['beginPath', 'rect', 'stroke', 'closePath']);

        colorShadder['canvasContext'] = context;
        colorShadder['displayIndicator']();

        expect(context.beginPath).toHaveBeenCalledTimes(1);
        expect(context.rect).toHaveBeenCalledTimes(1);
        expect(context.stroke).toHaveBeenCalledTimes(1);
        expect(context.closePath).toHaveBeenCalledTimes(1);

    });
});
