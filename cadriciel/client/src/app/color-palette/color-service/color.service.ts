import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Color } from '../color/color';
import { BLACK, PossibleColorSelected, WHITE } from '../constant';

@Injectable({
  providedIn: 'root' // providedIn root because other component will use it to get the color
})
export class ColorService {

  readonly N_PRESET: number = 10;

  private preset: Color[]; // is going to be used like a queue with a max length of 10

  private mainColorPreset: Color;
  private secondaryColorPreset: Color;

  // attributes to store temporary color to simplified setting the color when needed
  private currentColor: Color;
  private currentColorTransparency: Color;

  // create subjects
  private baseColorSource: Subject<Color>;
  private shadeColorSource: Subject<Color>;
  private transparencyColorSource: Subject<Color>;
  private textInputColorSource: Subject<Color>;

  constructor() {
    this.preset = new Array<Color>(this.N_PRESET);
    this.preset = this.preset.fill(WHITE);

    this.mainColorPreset = BLACK;
    this.secondaryColorPreset = WHITE;

    // attributes to store temporary color to simplified setting the color when needed
    this.currentColor = WHITE;
    this.currentColorTransparency = WHITE;

    // create subjects
    this.baseColorSource = new Subject<Color>();
    this.shadeColorSource = new Subject<Color>();
    this.transparencyColorSource = new Subject<Color>();
    this.textInputColorSource = new Subject<Color>();
  }

  get presetColor(): Color[] {
    return this.preset;
  }

  get mainColor(): Color {
    return this.mainColorPreset;
  }

  get secondaryColor(): Color {
    return this.secondaryColorPreset;
  }

  get baseColorObs(): Observable<Color> {
    return this.baseColorSource.asObservable();
  }

  get shadeColorObs(): Observable<Color> {
    return this.shadeColorSource.asObservable();
  }

  get transparencyColorObs(): Observable<Color> {
    return this.transparencyColorSource.asObservable();
  }

  get textInputColorObs(): Observable<Color> {
    return this.textInputColorSource.asObservable();
  }

  get color(): Color {
    return this.currentColor;
  }

  get colorWithTransparency(): Color {
    return this.currentColorTransparency;
  }

  pushColor(color: Color): void {
    for (const preset of this.preset) {
      if (preset.isRGBequal(color)) {
        return;
      }
    }
    this.preset.pop(); // remove the last element
    this.preset.unshift(color);
  }

  switchColor(): void {
    const tmp = this.mainColor;
    this.mainColorPreset = this.secondaryColorPreset;
    this.secondaryColorPreset = tmp;
  }

  setMainFromPreset(index: number): void {
    const color = this.getPresetColorFromIndex(index);
    this.mainColorPreset = color;
    this.sendBaseColor(color); // put the color in the center to adjust it
  }

  setSecondaryFromPreset(index: number): void {
    const color = this.getPresetColorFromIndex(index);
    this.secondaryColorPreset = color;
    this.sendBaseColor(color); // put the color in the center to adjust it
  }

  setMainFromCurrentColor(): void {
    this.mainColorPreset = this.colorWithTransparency;
    this.pushColor(this.color);
  }

  setSecondaryFromCurrentColor(): void {
    this.secondaryColorPreset = this.colorWithTransparency;
    this.pushColor(this.color);
  }

  sendBaseColor(color: Color): void {
    this.baseColorSource.next(color); // add the color into the subject
  }

  sendShadeColor(color: Color): void {
    this.shadeColorSource.next(color);
    this.currentColor = color;
  }

  sendTransparencyColor(color: Color): void {
    this.currentColorTransparency = color;
    this.transparencyColorSource.next(color);
  }

  sendColorFromInputText(color: Color): void {
    this.textInputColorSource.next(color);
    this.currentColorTransparency = color;
    this.currentColor = color;
  }

  private getPresetColorFromIndex(index: number): Color {
    if (index === PossibleColorSelected.MAIN) {
      return this.mainColorPreset;
    } else if ( index === PossibleColorSelected.SECONDARY) {
      return this.secondaryColorPreset;
    } else {
      return this.preset[index];
    }
  }
}
