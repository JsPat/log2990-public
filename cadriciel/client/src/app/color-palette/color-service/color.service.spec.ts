// tslint:disable: no-magic-numbers
// tslint:disable: no-string-literal
import { Color } from '../color/color';
import { PossibleColorSelected } from '../constant';
import { ColorService } from './color.service';

describe('ColorService', () => {
  let service: ColorService;

  beforeEach(() => {
    service = new ColorService();

  });

  it('preset color getter should return all the preset color', () => {
    expect(service.presetColor).toBe(service['preset']);
  });

  it('main color getter should return the main color', () => {
    expect(service.mainColor).toBe(service['mainColorPreset']);
  });

  it('secondary color getter should return the main color', () => {
    expect(service.secondaryColor).toBe(service['secondaryColorPreset']);
  });

  it('the color getter should return the last shade color given', () => {
    const colorGiven = new Color(1, 2, 3, 4);
    service.sendShadeColor(colorGiven);

    expect(service.color).toBe(colorGiven);
  });

  it('the color getter should not return the last transparency color updtaed', () => {
    const colorGiven = new Color(1, 2, 3, 4);
    service.sendTransparencyColor(colorGiven);

    expect(service.color).not.toBe(colorGiven);
  });

  it('the colorTransparency getter should return the last transparency color updated', () => {
    const colorGiven = new Color(1, 2, 3, 4);
    service.sendTransparencyColor(colorGiven);

    expect(service.colorWithTransparency).toBe(colorGiven);
  });

  it('should be able to subscribe to base color changes', () => {
    const colorGiven = new Color(1, 2, 3, 4);
    let result = new Color(4, 3, 2, 1);

    service.baseColorObs.subscribe((color) => {
      result = color;
    });

    service.sendBaseColor(colorGiven);
    expect(result).toBe(colorGiven);
  });

  it('should be able to subscribe to final color changes', () => {
    const colorGiven = new Color(1, 2, 3, 4);
    let result = new Color(4, 3, 2, 1);

    service.transparencyColorObs.subscribe((color) => {
      result = color;
    });

    service.sendTransparencyColor(colorGiven);
    expect(result).toBe(colorGiven);
  });

  it('should be able to subscribe to shade color changes', () => {
    const colorGiven = new Color(1, 2, 3, 4);
    let result = new Color(4, 3, 2, 1);

    service.shadeColorObs.subscribe((color) => {
      result = color;
    });

    service.sendShadeColor(colorGiven);
    expect(result).toBe(colorGiven);
  });

  it('should be able to subscribe to inputText color change', () => {
    const colorGiven = new Color(1, 2, 3, 4);
    let result = new Color(4, 3, 2, 1);

    service.textInputColorObs.subscribe((color) => {
      result = color;
    });

    service.sendColorFromInputText(colorGiven);
    expect(result).toBe(colorGiven);
  });

  it('shade color should not be subscribe to other color changes', () => {
    let valueGotUpdated = false;

    service.shadeColorObs.subscribe((_) => {
      valueGotUpdated = true;
    });

    service.sendTransparencyColor(new Color(1, 2, 3, 4));
    service.sendBaseColor(new Color(1, 2, 3, 4));
    service.sendColorFromInputText(new Color(1, 2, 3, 4));
    expect(valueGotUpdated).toBeFalsy();
  });

  it('main color should not be subscribe to any other color changes', () => {
    let valueGotUpdated = false;

    service.baseColorObs.subscribe((_) => {
      valueGotUpdated = true;
    });

    service.sendTransparencyColor(new Color(1, 2, 3, 4));
    service.sendShadeColor(new Color(1, 2, 3, 4));
    service.sendColorFromInputText(new Color(1, 2, 3, 4));
    expect(valueGotUpdated).toBeFalsy();
  });

  it('final color should not be subscribe to any other color changes', () => {
    let valueGotUpdated = false;

    service.transparencyColorObs.subscribe((_) => {
      valueGotUpdated = true;
    });

    service.sendShadeColor(new Color(1, 2, 3, 4));
    service.sendBaseColor(new Color(1, 2, 3, 4));
    service.sendColorFromInputText(new Color(1, 2, 3, 4));
    expect(valueGotUpdated).toBeFalsy();
  });

  it('input color should not be subscribe to any other color changes', () => {
    let valueGotUpdated = false;

    service.textInputColorObs.subscribe((_) => {
      valueGotUpdated = true;
    });

    service.sendShadeColor(new Color(1, 2, 3, 4));
    service.sendBaseColor(new Color(1, 2, 3, 4));
    service.sendTransparencyColor(new Color(1, 2, 3, 4));
    expect(valueGotUpdated).toBeFalsy();
  });

  it('pushColor should add the color at the beggining of the array', () => {
    const color = new Color(12, 34, 56);
    service.pushColor(color);

    expect(service.presetColor[0]).toBe(color);
  });

  it('pushColor should alway have a length of 10 whenever how many call we do', () => {
    expect(service.presetColor.length).toEqual(10);

    for (let i = 0; i < 100; ++i) {
      const color = new Color(i, i + 1, i + 2);
      service.pushColor(color);
      expect(service.presetColor.length).toEqual(10);
    }
  });

  it('pushColor should not allow to push a color that is already in the preset', () => {
    const color = new Color(123, 213, 231);
    service.pushColor(color);
    service.pushColor(color);

    expect(service.presetColor[0]).toBe(color);
    expect(service.presetColor[1]).not.toBe(color);
  });

  it('switch color should switch the main and the secondary color togheter', () => {
    const oldMain = service.mainColor;
    const oldSecondary = service.secondaryColor;

    service.switchColor();

    expect(service.mainColor).toBe(oldSecondary);
    expect(service.secondaryColor).toBe(oldMain);
  });

  it('setMainFromPreset should set the main color from the appropriate preset', () => {
    const index = 6;
    service.setMainFromPreset(index);

    expect(service.mainColor).toBe(service.presetColor[index]);
  });

  it('setSecondaryFromPreset should set the secondary color from the appropriate preset', () => {
    const index = 8;
    service.setSecondaryFromPreset(index);

    expect(service.secondaryColor).toBe(service.presetColor[index]);
  });

  it('setMainFromPreset should send a new color to all other component', () => {
    const index = 2;
    spyOn(service, 'sendBaseColor');
    service.setMainFromPreset(index);

    expect(service.sendBaseColor).toHaveBeenCalledTimes(1);
  });

  it('setMainFromPreset should send a new color to all other component', () => {
    const index = 9;
    spyOn(service, 'sendBaseColor');
    service.setSecondaryFromPreset(index);

    expect(service.sendBaseColor).toHaveBeenCalledTimes(1);
  });

  it('setMainFromCurrent should set the main color from the current color', () => {
    service.setMainFromCurrentColor();

    expect(service.mainColor).toBe(service.colorWithTransparency);
  });

  it('setSecondaryFromCurrent should set the main color from the current color', () => {
    service.setSecondaryFromCurrentColor();

    expect(service.secondaryColor).toBe(service.colorWithTransparency);
  });

  it('getPresetColorFromIndex return the main color if the index is PossibleColorSelected.main', () => {
    // ts lint is disable because we want to test private methode to simplify the test
    // tslint:disable-next-line: no-string-literal
    expect(service['getPresetColorFromIndex'](PossibleColorSelected.MAIN)).toBe(service.mainColor);
  });

  it('getPresetColorFromIndex return the main color if the index is PossibleColorSelected.secondary', () => {
    // ts lint is disable because we want to test private methode to simplify the test
    // tslint:disable-next-line: no-string-literal
    expect(service['getPresetColorFromIndex'](PossibleColorSelected.SECONDARY)).toBe(service.secondaryColor);
  });
});
