import { Color } from './color/color';

export const HEXBASE: number = 16;
export const N_ATTRIBUTE_IN_RGB = 3;
export const MAX_COLOR_VALUE = 255;
export const R_POS = 0;
export const G_POS = 1;
export const B_POS = 2;
export const A_POS = 3;

export const WHITE = new Color(MAX_COLOR_VALUE, MAX_COLOR_VALUE, MAX_COLOR_VALUE, MAX_COLOR_VALUE);
export const BLACK = new Color(0, 0, 0, MAX_COLOR_VALUE);
export const RED = new Color(MAX_COLOR_VALUE, 0, 0, MAX_COLOR_VALUE);
export const YELLOW = new Color(MAX_COLOR_VALUE, MAX_COLOR_VALUE, 0, MAX_COLOR_VALUE);
export const GREEN = new Color(0, MAX_COLOR_VALUE, 0, MAX_COLOR_VALUE);
export const TEAL = new Color(0, MAX_COLOR_VALUE, MAX_COLOR_VALUE, MAX_COLOR_VALUE);
export const BLUE = new Color(0, 0, MAX_COLOR_VALUE, MAX_COLOR_VALUE);
export const PINK = new Color(MAX_COLOR_VALUE, 0, MAX_COLOR_VALUE, MAX_COLOR_VALUE);
export const TRANSPARENT = new Color(0, 0, 0, 0);

// tslint:disable-next-line: no-magic-numbers
export const LIGHT_BLUE = new Color(0, 191, MAX_COLOR_VALUE);

export enum PossibleColorSelected { MAIN = 11, SECONDARY = 12}
