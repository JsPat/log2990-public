import { expect } from 'chai';
import * as supertest from 'supertest';
import { Stubbed, testingContainer } from '../../test/test-utils';
import { Application } from '../app';
import { EmailService } from '../services/email.service';
import Types from '../types';

const HTTP_BAD_REQUEST = 400;

describe('EmailController', () => {
    const ROUTING_EMAILS = '/api/email';

    let emailService: Stubbed<EmailService>;
    let app: Express.Application;

    beforeEach(async () => {
        const [container, sandbox] = await testingContainer();
        container.rebind(Types.EmailService).toConstantValue({
            sendEmail: sandbox.stub(),
        });
        emailService = container.get(Types.EmailService);
        app = container.get<Application>(Types.Application).app;
    });

    // sendEmail
    it('should return an error as sendEmail on service fail', async () => {
        emailService.sendEmail.rejects(new Error('service error'));

        return supertest(app)
            .post(ROUTING_EMAILS)
            .expect(HTTP_BAD_REQUEST)
            .then((response: any) => {
                expect(response.status).to.equal(HTTP_BAD_REQUEST);
            });
    });

});
