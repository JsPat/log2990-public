import { NextFunction, Request, Response, Router } from 'express';
import * as Httpstatus from 'http-status-codes';
import { inject, injectable } from 'inversify';
import { EmailService } from '../services/email.service';
import Types from '../types';

@injectable()
export class EmailController {
    private readonly ROUTING_EMAIL = '';
    router: Router;

    constructor(@inject(Types.EmailService) private emailService: EmailService) {
      this.configureRouter();
    }

    private configureRouter(): void {
        this.router = Router();

        this.router.post(this.ROUTING_EMAIL, async (req: Request, res: Response, next: NextFunction) => {
            this.emailService.sendEmail(req.body)
            .then()
            .catch((err) => {
                res.status(Httpstatus.BAD_REQUEST).send(err.message);
            });
        });
    }
}
