import { expect } from 'chai';
import * as supertest from 'supertest';
import { Drawing } from '../../../common/drawing';
import { Stubbed, testingContainer } from '../../test/test-utils';
import { Application } from '../app';
import { DatabaseService } from '../services/database.service';
import Types from '../types';

const HTTP_STATUS_OK = 200;
const HTTP_STATUS_NOT_FOUND = 404;

describe('DatabaseController', () => {
    const ROUTING_DRAWINGS = '/api/database/drawings';
    const ROUTING_TAGS = '/api/database/drawings/:tags';
    const ROUTING_GET_ID = '/api/database/drawings/get/:id';
    const ROUTING_DELETE_ID = '/api/database/drawings/delete/:id';

    let databaseService: Stubbed<DatabaseService>;
    let app: Express.Application;

    beforeEach(async () => {
        const [container, sandbox] = await testingContainer();
        container.rebind(Types.DatabaseService).toConstantValue({
            getAllDrawings: sandbox.stub(),
            getDrawingsByTags: sandbox.stub(),
            getDrawing: sandbox.stub(),
            addDrawing: sandbox.stub(),
            deleteDrawing: sandbox.stub(),
        });
        databaseService = container.get(Types.DatabaseService);
        app = container.get<Application>(Types.Application).app;
    });

    // getAllDrawings
    it('should return all drawings from databaseService on get request with routing for all drawings', async () => {
        const id0 = '0';
        const drawing0 = new Drawing(id0, '<svg>');
        drawing0.name = 'drawing0';
        drawing0.tags = new Array<string>('a', 'b');
        const id1 = '0';
        const drawing1 = new Drawing(id1, '<svg>');
        drawing1.name = 'drawing1';
        drawing1.tags = new Array<string>('a');
        const expectedDrawings: Drawing[] = new Array<Drawing>(drawing0, drawing1);
        databaseService.getAllDrawings.resolves(expectedDrawings);

        return supertest(app)
            .get(ROUTING_DRAWINGS)
            .expect(HTTP_STATUS_OK)
            .then((response: any) => {
                expect(response.body).to.deep.equal(expectedDrawings);
            });
    });

    it('should return an error as getAllDrawings on service fail', async () => {
        databaseService.getAllDrawings.rejects(new Error('service error'));

        return supertest(app)
            .get(ROUTING_DRAWINGS)
            .expect(HTTP_STATUS_NOT_FOUND)
            .then((response: any) => {
                expect(response.status).to.equal(HTTP_STATUS_NOT_FOUND);
            });
    });

    // getDrawingsByTags
    it('should return all drawings found by tags from databaseService on get request with routing for tags', async () => {
        const id0 = '0';
        const drawing0 = new Drawing(id0, '<svg>');
        drawing0.name = 'drawing0';
        drawing0.tags = new Array<string>('a', 'b');
        const id1 = '0';
        const drawing1 = new Drawing(id1, '<svg>');
        drawing1.name = 'drawing1';
        drawing1.tags = new Array<string>('a');
        const expectedDrawings: Drawing[] = new Array<Drawing>(drawing0, drawing1);
        databaseService.getDrawingsByTags.resolves(expectedDrawings);

        return supertest(app)
            .get(ROUTING_TAGS)
            .expect(HTTP_STATUS_OK)
            .then((response: any) => {
                expect(response.body).to.deep.equal(expectedDrawings);
            });
    });

    it('should return an error as getDrawingsByTags on service fail', async () => {
        databaseService.getDrawingsByTags.rejects(new Error('service error'));

        return supertest(app)
            .get(ROUTING_TAGS)
            .expect(HTTP_STATUS_NOT_FOUND)
            .then((response: any) => {
                expect(response.status).to.equal(HTTP_STATUS_NOT_FOUND);
            });
    });

    // getDrawing
    it('should return the drawing found by id from databaseService on get request with routing for id', async () => {
        const id0 = '0';
        const expectedDrawing = new Drawing(id0, '<svg>');
        expectedDrawing.name = 'drawing0';
        expectedDrawing.tags = new Array<string>('a', 'b');
        databaseService.getDrawing.resolves(expectedDrawing);

        return supertest(app)
            .get(ROUTING_GET_ID)
            .expect(HTTP_STATUS_OK)
            .then((response: any) => {
                expect(response.body).to.deep.equal(expectedDrawing);
            });
    });

    it('should return an error as getDrawing on service fail', async () => {
        databaseService.getDrawing.rejects(new Error('service error'));

        return supertest(app)
            .get(ROUTING_GET_ID)
            .expect(HTTP_STATUS_NOT_FOUND)
            .then((response: any) => {
                expect(response.status).to.equal(HTTP_STATUS_NOT_FOUND);
            });
    });

    // addDrawing
    it('should return an error as addDrawing on service fail', async () => {
        databaseService.addDrawing.rejects(new Error('service error'));

        return supertest(app)
            .post(ROUTING_DRAWINGS)
            .expect(HTTP_STATUS_NOT_FOUND)
            .then((response: any) => {
                expect(response.status).to.equal(HTTP_STATUS_NOT_FOUND);
            });
    });

    // deleteDrawing
    it('should return an error as deleteDrawing on service fail', async () => {
        databaseService.deleteDrawing.rejects(new Error('service error'));

        return supertest(app)
            .delete(ROUTING_DELETE_ID)
            .expect(HTTP_STATUS_NOT_FOUND)
            .then((response: any) => {
                expect(response.status).to.equal(HTTP_STATUS_NOT_FOUND);
            });
    });

});
