import { NextFunction, Request, Response, Router } from 'express';
import * as Httpstatus from 'http-status-codes';
import { inject, injectable } from 'inversify';
import { Drawing } from '../../../common/drawing';
import { DatabaseService } from '../services/database.service';
import Types from '../types';

@injectable()
// inspired from the log2990 example on Moodle (mongoDB.zip)
export class DatabaseController {
    private readonly ROUTING_DRAWINGS = '/drawings';
    private readonly ROUTING_TAGS = '/drawings/:tags';
    private readonly ROUTING_DELETE_ID = '/drawings/delete/:id';
    private readonly ROUTING_GET_ID = '/drawings/get/:id';
    private readonly JOINING_CHARACTER = '-';
    router: Router;

    constructor(@inject(Types.DatabaseService) private databaseService: DatabaseService) {
      this.configureRouter();
    }

    private configureRouter(): void {
        this.router = Router();

        this.router.get(this.ROUTING_DRAWINGS, async (req: Request, res: Response, next: NextFunction) => {
            this.databaseService.getAllDrawings()
                .then((drawings: Drawing[]) => {
                    res.json(drawings);
                })
                .catch((error: Error) => {
                    res.status(Httpstatus.NOT_FOUND).send(error.message);
                });
        });

        this.router.get(this.ROUTING_TAGS, async (req: Request, res: Response, next: NextFunction) => {
            this.databaseService.getDrawingsByTags(req.params.tags.split(this.JOINING_CHARACTER))
            .then((drawings: Drawing[]) => {
                res.json(drawings);
            })
            .catch((error: Error) => {
                res.status(Httpstatus.NOT_FOUND).send(error.message);
            });
        });

        this.router.get(this.ROUTING_GET_ID, async (req: Request, res: Response, next: NextFunction) => {
            this.databaseService.getDrawing(req.params.id)
                .then((drawing: Drawing) => {
                    res.json(drawing);
                })
                .catch((error: Error) => {
                    res.status(Httpstatus.NOT_FOUND).send(error.message);
                });
        });

        this.router.post(this.ROUTING_DRAWINGS, async (req: Request, res: Response, next: NextFunction) => {
            this.databaseService.addDrawing(req.body)
                .then()
                .catch((error: Error) => {
                    res.status(Httpstatus.NOT_FOUND).send(error.message);
                });
        });

        this.router.delete(this.ROUTING_DELETE_ID, async (req: Request, res: Response, next: NextFunction) => {
            this.databaseService.deleteDrawing(req.params.id)
                .then()
                .catch((error: Error) => {
                    res.status(Httpstatus.NOT_FOUND).send(error.message);
                });
        });
    }
}
