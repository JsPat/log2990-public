// tslint:disable: no-string-literal

import { expect } from 'chai';
import { MongoClient } from 'mongodb';
import * as sinon from 'sinon';
import { Drawing } from '../../../common/drawing';
import { testingContainer } from '../../test/test-utils';
import Types from '../types';
import { DatabaseService } from './database.service';

describe('DatabaseService', () => {
  let databaseService: DatabaseService;
  let clock: sinon.SinonFakeTimers;
  const DATABASE_URL = 'mongodb+srv://js:dqsfnl302@cluster0-hm4z3.mongodb.net/test?retryWrites=true&w=majority';
  const DATABASE_NAME = 'drawingsDB';
  const DATABASE_COLLECTION = 'testCollection';

  beforeEach(async () => {
    const [container] = await testingContainer();
    databaseService = container.get<DatabaseService>(Types.DatabaseService);
    clock = sinon.useFakeTimers();
  });

  afterEach(() => {
      clock.restore();
      sinon.restore();
  });

  // getAllDrawings
  it('getAllDrawings() should call find', async () => {
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection(DATABASE_COLLECTION);
        const spy = sinon.spy(databaseService['collection'], 'find');
        await databaseService.getAllDrawings();
        sinon.assert.called(spy);
      })
      .catch(() => {
        console.error('CONNECTION ERROR. CONTINUING');
      });
  });

  it('getAllDrawings() should return all the drawings', async () => {
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const id0 = '0';
        const id1 = '1';
        const firstDrawing = new Drawing(id0, '<svg>');
        firstDrawing.name = 'firstDrawing';
        firstDrawing.tags = new Array<string>('a', 'b');
        const secondDrawing = new Drawing(id1, '<svg>');
        secondDrawing.name = 'secondDrawing';
        secondDrawing.tags = new Array<string>('a', 'b');

        databaseService['collection'] = client.db(DATABASE_NAME).collection(DATABASE_COLLECTION);

        await databaseService['collection'].insertOne(firstDrawing);
        await databaseService['collection'].insertOne(secondDrawing);
        const result = await databaseService.getAllDrawings();
        expect(result).to.contain(firstDrawing);
        expect(result).to.contain(secondDrawing);
        await databaseService['collection'].findOneAndDelete({id0});
        await databaseService['collection'].findOneAndDelete({id1});
      })
      .catch(() => {
        console.error('CONNECTION ERROR. CONTINUING');
      });
  });

  it('getAllDrawings() should throw error if error', async () => {
    const spy = sinon.spy(databaseService, 'getAllDrawings');
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        databaseService['collection'] = client.db(DATABASE_NAME).collection('invalidCollection');
        databaseService.getAllDrawings()
          .then(() => {
            throw new Error();
          })
          .catch((error) => {
            // tslint:disable-next-line: no-unused-expression
            expect(error).to.not.be.undefined;
            throw new Error();
          })
      })
      .catch((error) => {
        console.error('CONNECTION ERROR. CONTINUING');
        expect(spy).to.throw(error);
      });

  });

  // getDrawing
  it('getDrawing() should return a valid drawing', async () => {
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const id0 = '0';
        const firstDrawing = new Drawing(id0, '<svg>');
        firstDrawing.name = 'firstDrawing';
        firstDrawing.tags = new Array<string>('a', 'b');

        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection(DATABASE_COLLECTION);

        await databaseService['collection'].insertOne(firstDrawing);
        const result = await databaseService.getDrawing(id0);
        expect(result.id).to.equal(id0);
        await databaseService['collection'].findOneAndDelete({id0});
      })
      .catch(() => {
        console.error('CONNECTION ERROR. CONTINUING');
      });
  });

  it('getDrawing() should call findOne', async () => {
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const id0 = '0';
        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection(DATABASE_COLLECTION);
        const spy = sinon.spy(databaseService['collection'], 'findOne');
        await databaseService.getDrawing(id0);
        sinon.assert.called(spy);
      })
      .catch(() => {
        console.error('CONNECTION ERROR. CONTINUING');
      });
  });

  it('getDrawing() should throw error if error', async () => {
    const spy = sinon.spy(databaseService, 'getDrawing');
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const id0 = '0';
        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection('invalidCollection');
        databaseService.getDrawing(id0)
          .then(() => {
            throw new Error();
          })
          .catch((error) => {
            // tslint:disable-next-line: no-unused-expression
            expect(error).to.not.be.undefined;
            throw new Error();
          })
      })
      .catch((error) => {
        console.error('CONNECTION ERROR. CONTINUING');
        expect(spy).to.throw(error);
      });

  });

  // getDrawingsByTags
  it('getDrawingsByTags() should call once getDrawingsByOneTag if one tag', async () => {
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const tags = new Array<string>('a');
        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection(DATABASE_COLLECTION);
        const spy = sinon.spy(databaseService, 'getDrawingsByOneTag');
        await databaseService.getDrawingsByTags(tags);
        sinon.assert.calledOnce(spy);
      })
      .catch(() => {
        console.error('CONNECTION ERROR. CONTINUING');
      });
  });

  it('getDrawingsByTags() should call twice getDrawingsByOneTag if two tags', async () => {
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const tags = new Array<string>('a', 'b');
        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection(DATABASE_COLLECTION);
        const spy = sinon.spy(databaseService, 'getDrawingsByOneTag');
        await databaseService.getDrawingsByTags(tags);
        sinon.assert.calledTwice(spy);
      })
      .catch(() => {
        console.error('CONNECTION ERROR. CONTINUING');
      });
  });

  it('getDrawingsByTags() should not call getDrawingsByOneTag if no tags', async () => {
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const tags = new Array<string>();
        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection(DATABASE_COLLECTION);
        const spy = sinon.spy(databaseService, 'getDrawingsByOneTag');
        await databaseService.getDrawingsByTags(tags);
        sinon.assert.notCalled(spy);
      })
      .catch(() => {
        console.error('CONNECTION ERROR. CONTINUING');
      });
  });

  it('getDrawingsByTags() should return only the drawings found by tag', async () => {
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const tags = new Array<string>('a');
        const id0 = '0';
        const firstDrawing = new Drawing(id0, '<svg>');
        firstDrawing.name = 'firstDrawing';
        firstDrawing.tags = new Array<string>('a', 'b');
        const id1 = '1';
        const secondDrawing = new Drawing(id1, '<svg>');
        secondDrawing.name = 'secondDrawing';
        secondDrawing.tags = new Array<string>('a');
        const id2 = '2';
        const thirdDrawing = new Drawing(id2, '<svg>');
        thirdDrawing.name = 'thirdDrawing';
        thirdDrawing.tags = new Array<string>('b');
        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection(DATABASE_COLLECTION);
        await databaseService['collection'].insertOne(firstDrawing);
        await databaseService['collection'].insertOne(secondDrawing);
        await databaseService['collection'].insertOne(thirdDrawing);
        const result = await databaseService.getDrawingsByTags(tags);
        expect(result).to.contain(firstDrawing);
        expect(result).to.contain(secondDrawing);
        expect(result).not.to.contain(thirdDrawing);
        await databaseService['collection'].findOneAndDelete({id0});
        await databaseService['collection'].findOneAndDelete({id1});
        await databaseService['collection'].findOneAndDelete({id2});
      })
      .catch(() => {
        console.error('CONNECTION ERROR. CONTINUING');
      });
  });

  it('getDrawingsByTags() should return the drawings found by tags (logical OR)', async () => {
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const tags = new Array<string>('a', 'b');
        const id0 = '0';
        const firstDrawing = new Drawing(id0, '<svg>');
        firstDrawing.name = 'firstDrawing';
        firstDrawing.tags = new Array<string>('a', 'b');
        const id1 = '1';
        const secondDrawing = new Drawing(id1, '<svg>');
        secondDrawing.name = 'secondDrawing';
        secondDrawing.tags = new Array<string>('a');
        const id2 = '2';
        const thirdDrawing = new Drawing(id2, '<svg>');
        thirdDrawing.name = 'thirdDrawing';
        thirdDrawing.tags = new Array<string>('b');
        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection(DATABASE_COLLECTION);
        await databaseService['collection'].insertOne(firstDrawing);
        await databaseService['collection'].insertOne(secondDrawing);
        await databaseService['collection'].insertOne(thirdDrawing);
        const result = await databaseService.getDrawingsByTags(tags);
        expect(result).to.contain(firstDrawing);
        expect(result).to.contain(secondDrawing);
        expect(result).to.contain(thirdDrawing);
        await databaseService['collection'].findOneAndDelete({id0});
        await databaseService['collection'].findOneAndDelete({id1});
        await databaseService['collection'].findOneAndDelete({id2});
      })
      .catch(() => {
        console.error('CONNECTION ERROR. CONTINUING');
      });
  });

  it('getDrawingsByTags() should return no drawings if none contain the tag', async () => {
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const tags = new Array<string>('z');
        const id0 = '0';
        const firstDrawing = new Drawing(id0, '<svg>');
        firstDrawing.name = 'firstDrawing';
        firstDrawing.tags = new Array<string>('a', 'b');
        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection(DATABASE_COLLECTION);
        await databaseService['collection'].insertOne(firstDrawing);
        const result = await databaseService.getDrawingsByTags(tags);
        expect(result).not.to.contain(firstDrawing);
        await databaseService['collection'].findOneAndDelete({id0});
      })
      .catch(() => {
        console.error('CONNECTION ERROR. CONTINUING');
      });
  });

  it('getDrawingsByTags() should return the drawings found by tags even if a tag is not contained', async () => {
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const tags = new Array<string>('a', 'z');
        const id0 = '0';
        const firstDrawing = new Drawing(id0, '<svg>');
        firstDrawing.name = 'firstDrawing';
        firstDrawing.tags = new Array<string>('a');
        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection(DATABASE_COLLECTION);
        await databaseService['collection'].insertOne(firstDrawing);
        const result = await databaseService.getDrawingsByTags(tags);
        expect(result).to.contain(firstDrawing);
        await databaseService['collection'].findOneAndDelete({id0});
      })
      .catch(() => {
        console.error('CONNECTION ERROR. CONTINUING');
      });
  });

  it('getDrawingsByTags() should return no drawings if no tag', async () => {
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const tags = new Array<string>();
        const id0 = '0';
        const firstDrawing = new Drawing(id0, '<svg>');
        firstDrawing.name = 'firstDrawing';
        firstDrawing.tags = new Array<string>('a');
        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection(DATABASE_COLLECTION);
        await databaseService['collection'].insertOne(firstDrawing);
        const result = await databaseService.getDrawingsByTags(tags);
        expect(result).not.to.contain(firstDrawing);
        await databaseService['collection'].findOneAndDelete({id0});
      })
      .catch(() => {
        console.error('CONNECTION ERROR. CONTINUING');
      });
  });

  it('getDrawingsByTags() should throw error if error', async () => {
    const spy = sinon.spy(databaseService, 'getDrawingsByTags');
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const tags = new Array<string>('a');
        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection('invalidCollection');
        databaseService.getDrawingsByTags(tags)
          .then(() => {
            throw new Error();
          })
          .catch((error) => {
            // tslint:disable-next-line: no-unused-expression
            expect(error).to.not.be.undefined;
            throw new Error();
          })
      })
      .catch((error) => {
        console.error('CONNECTION ERROR. CONTINUING');
        expect(spy).to.throw(error);
      });

  });

  // getDrawingsByOneTag
  it('getDrawingsByOneTag() should call find', async () => {
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const tag = 'a';
        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection(DATABASE_COLLECTION);
        const spy = sinon.spy(databaseService['collection'], 'find');
        await databaseService.getDrawingsByOneTag(tag);
        sinon.assert.called(spy);
      })
      .catch(() => {
        console.error('CONNECTION ERROR. CONTINUING');
      });
  });

  it('getDrawingsByOneTag() should return the drawings containing the tag', async () => {
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const tag = 'a';
        const id0 = '0';
        const firstDrawing = new Drawing(id0, '<svg>');
        firstDrawing.name = 'firstDrawing';
        firstDrawing.tags = new Array<string>('a');
        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection(DATABASE_COLLECTION);
        await databaseService['collection'].insertOne(firstDrawing);
        const result = await databaseService.getDrawingsByOneTag(tag);
        expect(result).to.contain(firstDrawing);
        await databaseService['collection'].findOneAndDelete({id0});
      })
      .catch(() => {
        console.error('CONNECTION ERROR. CONTINUING');
      });
  });

  it('getDrawingsByOneTag() should not return the drawings not containing the tag', async () => {
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const tag = 'z';
        const id0 = '0';
        const firstDrawing = new Drawing(id0, '<svg>');
        firstDrawing.name = 'firstDrawing';
        firstDrawing.tags = new Array<string>('a');
        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection(DATABASE_COLLECTION);
        await databaseService['collection'].insertOne(firstDrawing);
        const result = await databaseService.getDrawingsByOneTag(tag);
        expect(result).not.to.contain(firstDrawing);
        await databaseService['collection'].findOneAndDelete({id0});
      })
      .catch(() => {
        console.error('CONNECTION ERROR. CONTINUING');
      });
  });

  it('getDrawingsByOneTag() should throw error if error', async () => {
    const spy = sinon.spy(databaseService, 'getDrawingsByOneTag');
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const tag = 'a';
        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection('invalidCollection');
        databaseService.getDrawingsByOneTag(tag)
          .then(() => {
            throw new Error();
          })
          .catch((error) => {
            // tslint:disable-next-line: no-unused-expression
            expect(error).to.not.be.undefined;
            throw new Error();
          })
      })
      .catch((error) => {
        console.error('CONNECTION ERROR. CONTINUING');
        expect(spy).to.throw(error);
      });

  });

  // addDrawing
  it('addDrawing() should call insertOne if valid drawing', async () => {
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const id0 = '0';
        const firstDrawing = new Drawing(id0, '<svg>');
        firstDrawing.name = 'firstDrawing';
        firstDrawing.tags = new Array<string>('a');
        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection(DATABASE_COLLECTION);
        const spy = sinon.spy(databaseService['collection'], 'insertOne');
        await databaseService.addDrawing(firstDrawing);
        sinon.assert.called(spy);
        await databaseService['collection'].findOneAndDelete({id0});
      })
      .catch(() => {
        console.error('CONNECTION ERROR. CONTINUING');
      });
  });

  it('addDrawing() should not call insertOne if invalid drawing', async () => {
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const id0 = '0';
        const firstDrawing = new Drawing(id0, '<svg>');
        firstDrawing.name = '';
        firstDrawing.tags = new Array<string>('$$$$');
        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection(DATABASE_COLLECTION);
        const spy = sinon.spy(databaseService['collection'], 'insertOne');
        try {
          await databaseService.addDrawing(firstDrawing);
        } catch (error) {
          sinon.assert.notCalled(spy);
          // tslint:disable-next-line: no-unused-expression
          expect(error).not.to.be.undefined;
          await databaseService['collection'].findOneAndDelete({id0});
        }
      })
      .catch(() => {
        console.error('CONNECTION ERROR. CONTINUING');
      });
  });

  it('addDrawing() should throw error if error', async () => {
    const spy = sinon.spy(databaseService, 'addDrawing');
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const id0 = '0';
        const firstDrawing = new Drawing(id0, '<svg>');
        firstDrawing.name = 'firstDrawing';
        firstDrawing.tags = new Array<string>('a');
        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection('invalidCollection');
        databaseService.addDrawing(firstDrawing)
          .then(() => {
            throw new Error('Invalid drawing');
          })
          .catch((error) => {
            // tslint:disable-next-line: no-unused-expression
            expect(error).to.not.be.undefined;
            throw new Error();
          })
      })
      .catch((error) => {
        console.error('CONNECTION ERROR. CONTINUING');
        expect(spy).to.throw(error);
      });

  });

  // deleteDrawing
  it('deleteDrawing() should call findOneAndDelete', async () => {
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const id0 = '0';
        const firstDrawing = new Drawing(id0, '<svg>');
        firstDrawing.name = 'firstDrawing';
        firstDrawing.tags = new Array<string>('a');
        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection(DATABASE_COLLECTION);
        await databaseService['collection'].insertOne(firstDrawing);
        const spy = sinon.spy(databaseService['collection'], 'findOneAndDelete');
        await databaseService.deleteDrawing(id0);
        sinon.assert.called(spy);
        // tslint:disable-next-line: no-unused-expression
        expect(databaseService['collection']).not.to.be.undefined;
      })
      .catch(() => {
        console.error('CONNECTION ERROR. CONTINUING');
      });
  });

  it('deleteDrawing() should call deleteDrawing a second time if their is 2 drawings with the same id to delete', async () => {
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const id0 = '0';
        const firstDrawing = new Drawing(id0, '<svg>');
        firstDrawing.name = 'firstDrawing';
        firstDrawing.tags = new Array<string>('a');
        const id1 = '0';
        const secondDrawing = new Drawing(id1, '<svg>');
        secondDrawing.name = 'secondDrawing';
        secondDrawing.tags = new Array<string>('a');
        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection(DATABASE_COLLECTION);
        await databaseService['collection'].insertOne(firstDrawing);
        await databaseService['collection'].insertOne(secondDrawing);
        const spy = sinon.spy(databaseService['collection'], 'findOneAndDelete');
        const spyDeleteDrawing = sinon.spy(databaseService, 'deleteDrawing');
        await databaseService.deleteDrawing(id0);
        sinon.assert.called(spy);
        sinon.assert.called(spyDeleteDrawing);
        // tslint:disable-next-line: no-unused-expression
        expect(databaseService['collection']).not.to.be.undefined;
      })
      .catch(() => {
        console.error('CONNECTION ERROR. CONTINUING');
      });
  });

  it('deleteDrawing() should throw error if error', async () => {
    const spy = sinon.spy(databaseService, 'deleteDrawing');
    MongoClient.connect(DATABASE_URL, {})
      .then(async (client: MongoClient) => {
        const id0 = '#';
        // tslint:disable-next-line: no-string-literal
        databaseService['collection'] = client.db(DATABASE_NAME).collection('invalidCollection');
        databaseService.deleteDrawing(id0)
          .then(() => {
            throw new Error('Failed to delete drawing');
          })
          .catch((error) => {
            // tslint:disable-next-line: no-unused-expression
            expect(error).to.not.be.undefined;
            throw new Error();
          })
      })
      .catch((error) => {
        console.error('CONNECTION ERROR. CONTINUING');
        expect(spy).to.throw(error);
      });

  });

  // validateDrawing
  it('validateDrawing() should return true if valid name and valid tags', () => {
    const drawing = new Drawing('0', '<svg>');
    drawing.name = 'name';
    drawing.tags = new Array<string>('a', 'b');
    const result = databaseService['validateDrawing'](drawing);
    expect(result).to.eq(true);
  });

  it('validateDrawing() should return false if invalid name', () => {
    const drawing = new Drawing('0', '<svg>');
    drawing.name = '';
    drawing.tags = new Array<string>('a', 'b');
    const result = databaseService['validateDrawing'](drawing);
    expect(result).to.eq(false);
  });

  it('validateDrawing() should return false if invalid tags', () => {
    const drawing = new Drawing('0', '<svg>');
    drawing.name = 'name';
    drawing.tags = new Array<string>('$', 'b');
    const result = databaseService['validateDrawing'](drawing);
    expect(result).to.eq(false);
  });

  // validateName
  it('validateName() should return true if name defined and not empty', () => {
    const name = 'name';
    const result = databaseService['validateName'](name);
    expect(result).to.eq(true);
  });

  it('validateName() should return false if name empty', () => {
    const name = '';
    const result = databaseService['validateName'](name);
    expect(result).to.eq(false);
  });

  // validateAllTags
  it('validateAllTags() should return true if all tags are valid', () => {
    const tags = new Array<string>('a', 'b', 'c');
    const result = databaseService['validateAllTags'](tags);
    expect(result).to.eq(true);
  });

  it('validateAllTags() should return false if one tag is invalid', () => {
    const tags = new Array<string>('a', 'invalidTag$$$$', 'c');
    const result = databaseService['validateAllTags'](tags);
    expect(result).to.eq(false);
  });

  it('validateAllTags() should return true if no tags', () => {
    const tags = new Array<string>();
    const result = databaseService['validateAllTags'](tags);
    expect(result).to.eq(true);
  });

  // validateOneTag
  it('validateOneTag() should return false if empty tag', () => {
    const tag = '';
    const result = databaseService['validateOneTag'](tag);
    expect(result).to.eq(false);
  });

  it('validateOneTag() should return false if invalid tag', () => {
    const tag = '$$$$';
    const result = databaseService['validateOneTag'](tag);
    expect(result).to.eq(false);
  });

  it('validateOneTag() should return false if too long tag ( > 25 characters)', () => {
    const tag = 'aaaaaaaaaaaaaaaaaaaaaaaaaa';
    const result = databaseService['validateOneTag'](tag);
    expect(result).to.eq(false);
  });

  it('validateOneTag() should return true if valid tag and 25 characters long', () => {
    const tag = 'aaaaaaaaaaaaaaaaaaaaaaaaa';
    const result = databaseService['validateOneTag'](tag);
    expect(result).to.eq(true);
  });

  it('validateOneTag() should return true if valid tag and < 25 characters long', () => {
    const tag = 'aaaaaaaaa';
    const result = databaseService['validateOneTag'](tag);
    expect(result).to.eq(true);
  });

  it('validateOneTag() should return true if valid tag (number) and < 25 characters long', () => {
    const tag = '1111111111';
    const result = databaseService['validateOneTag'](tag);
    expect(result).to.eq(true);
  });

  it('validateOneTag() should return true if valid tag (caps) and < 25 characters long', () => {
    const tag = 'AAAAAAAA';
    const result = databaseService['validateOneTag'](tag);
    expect(result).to.eq(true);
  });

  it('validateOneTag() should return true if valid tag (lower and caps and number) and < 25 characters long', () => {
    const tag = 'a1efn38fh438fh348fh87A';
    const result = databaseService['validateOneTag'](tag);
    expect(result).to.eq(true);
  });

});
