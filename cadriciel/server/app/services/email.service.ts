import axios from 'axios';
import * as dotenv from 'dotenv';
import * as FormData from 'form-data';
import * as fs from 'fs';
import { injectable } from 'inversify';
import 'reflect-metadata';
import { DataToEmail } from '../../../common/data-to-email';
dotenv.config();

@injectable()
export class EmailService {
  private readonly FILE_TYPE_SEPARATOR: string = '.';
  private readonly IMAGE_BASE_SEPARATOR: string = ',';
  private readonly CHAR_VALID_EMAIL_ADDRESS: string = '@';
  private readonly SVG_TYPE: string = 'svg';
  private readonly FORM_DATA_TO: string = 'to';
  private readonly FORM_DATA_PAYLOAD: string = 'payload';
  private readonly BASE_EMAIL_URL: string | undefined = process.env.MAIL_API_URL;
  private readonly DIRECTORY_ROUTING: string = '/';
  private readonly INVALID_EMAIL_ADDRESS_ERROR: string = 'Invalid email Address';

  async sendEmail(dataToEmail: DataToEmail): Promise<void> {
    const emailAddress = dataToEmail.to;
    if (!emailAddress.includes(this.CHAR_VALID_EMAIL_ADDRESS)) {
        throw new Error(this.INVALID_EMAIL_ADDRESS_ERROR);
    }
    const drawingName = dataToEmail.drawingName;
    const format = dataToEmail.format;
    const data = dataToEmail.payload;
    const imagePath = __dirname + this.DIRECTORY_ROUTING + drawingName + this.FILE_TYPE_SEPARATOR + format;

    if (format !== this.SVG_TYPE) {
        // we have to let this magic 'base64' string here because Buffer.from() needs it exactly like that
        const buffer = Buffer.from(data.split(this.IMAGE_BASE_SEPARATOR)[1], 'base64');
        fs.writeFileSync(imagePath, buffer);
    } else {
        fs.writeFileSync(imagePath, data);
    }

    const form = new FormData();
    form.append(this.FORM_DATA_TO, emailAddress);
    form.append(this.FORM_DATA_PAYLOAD, fs.createReadStream(imagePath));

    const formHeaders = form.getHeaders();
    axios({
      // we let this "magic string" because it needs to stay like that to be accepted by axios
      method: 'post',
      url: this.BASE_EMAIL_URL,
      headers: {
          ...formHeaders,
          // we let these "magic strings" because they are in fact attributes and need to stay like that
          'accept': 'application/json',
          'x-team-key': process.env.X_TEAM_KEY,
          'Content-Type': 'multipart/form-data'
      },
      data: form
    })
    .then(() => {
      // delete the image created because it's not useful anymore
      fs.unlinkSync(imagePath);
    })
    .catch((err) => {
      throw err;
    });

  }

}
