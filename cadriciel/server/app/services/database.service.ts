import * as dotenv from 'dotenv';
import { injectable } from 'inversify';
import { Collection, MongoClient, MongoClientOptions } from 'mongodb';
import 'reflect-metadata';
import { Drawing } from '../../../common/drawing';
dotenv.config();

@injectable()
export class DatabaseService {
    // tslint:disable-next-line: no-non-null-assertion because this env variable is surely not undefined because we manually created it
    private readonly DATABASE_URL = process.env.MONGODB_URL!;
    private readonly DATABASE_NAME = process.env.MONGODB_DATABASE_NAME;
    // tslint:disable-next-line: no-non-null-assertion because this env variable is surely not undefined because we manually created it
    private readonly DATABASE_COLLECTION = process.env.MONGODB_COLLECTION_NAME!;
    private readonly CONNECTION_ERROR: string = 'CONNECTION ERROR. EXITING PROCESS';
    private readonly INVALID_DRAWING_ERROR: string = 'Invalid drawing';
    private readonly DELETE_ERROR: string = 'Failed to delete drawing';

    private readonly MAX_TAG_LENGHT = 25;
    private collection: Collection<Drawing>;
    private drawingsFound: Drawing[];

    private options: MongoClientOptions = {
      useNewUrlParser : true,
      useUnifiedTopology : true
    };

    constructor() {
      MongoClient.connect(this.DATABASE_URL, this.options)
        .then((client: MongoClient) => {
          this.drawingsFound = new Array<Drawing>();
          this.collection = client.db(this.DATABASE_NAME).collection(this.DATABASE_COLLECTION);
        })
        .catch(() => {
          console.error(this.CONNECTION_ERROR);
          process.exit(1);
        });
    }

    async getAllDrawings(): Promise<Drawing[]> {
      return  this.collection.find({}).toArray()
        .then((drawings: Drawing[]) => {
          return drawings;
        })
        .catch((error: Error) => {
          throw error;
        });
    }

    async getDrawing(id: string): Promise<Drawing> {
      return  this.collection.findOne({id})
        .then((drawing: Drawing) => {
          return drawing;
        })
        .catch((error: Error) => {
          throw error;
        });
    }

    async getDrawingsByTags(tags: string[]): Promise<Drawing[]> {
      const promises = new Array<Promise<Drawing[]>>();
      tags.forEach((tag) => {
        promises.push(this.getDrawingsByOneTag(tag))
      })

      return Promise.all(promises)
        .then(() => {
          const resultingDrawings = this.drawingsFound;
          this.drawingsFound = new Array<Drawing>();
          return resultingDrawings;
        })
        .catch((error: Error) => {
          throw error;
        });
    }

    async getDrawingsByOneTag(tag: string): Promise<Drawing[]> {
      return this.collection.find(
        {
          tags: {
            $all: [tag]
          }
        }
      ).toArray()
        .then((drawings: Drawing[]) => {
          this.drawingsFound = this.drawingsFound.concat(drawings);
          return drawings;
        })
        .catch((error: Error) => {
          throw error;
        });
    }

    async addDrawing(drawing: Drawing): Promise<void> {
      if (this.validateDrawing(drawing)) {
        this.collection.insertOne(drawing).catch((error: Error) => {
          throw error;
        });
      } else {
        throw new Error(this.INVALID_DRAWING_ERROR);
      }
    }

    async deleteDrawing(id: string): Promise<void> {
      this.collection.findOneAndDelete({ id })
      .then()
      .catch((error: Error) => {
        throw new Error(this.DELETE_ERROR);
      });
      // because, sometimes mongoDB add the same drawing multiple times
      this.collection.findOne({id})
      .then(() => {
        this.deleteDrawing(id);
      });
    }

    private validateDrawing(drawing: Drawing): boolean {
      return this.validateName(drawing.name) && this.validateAllTags(drawing.tags);
    }
    private validateName(name: string): boolean {
      return name !== undefined && name !== '';
    }
    private validateAllTags(tags: string[]): boolean {
      let valid = true;
      tags.forEach((tag) => {
        valid = valid && this.validateOneTag(tag);
      });
      return valid;
    }
    private validateOneTag(tag: string): boolean {
      // tags can only contain valid URL characters
      return tag !== '' && /^[0-9a-zA-Z]*$/g.test(tag) && tag.length <= this.MAX_TAG_LENGHT;
    }
}
