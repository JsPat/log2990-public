export class Drawing {
    id: string;
    name: string;
    tags: string[];
    svgContent: string;
    pngContent: string;

    constructor(id: string, svgContent: string) {
        this.id = id;
        this.name = '';
        this.tags = new Array<string>();
        this.svgContent = svgContent;
    }
}
