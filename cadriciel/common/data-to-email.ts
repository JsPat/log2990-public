export interface DataToEmail {
    to: string;
    drawingName: string;
    format: string;
    payload: string;
}
